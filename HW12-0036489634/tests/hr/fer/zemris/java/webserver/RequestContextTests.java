package hr.fer.zemris.java.webserver;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import hr.fer.zemris.java.webserver.RequestContext.RCCookie;

@SuppressWarnings("javadoc")
public class RequestContextTests {

	private static final String HEADER = "HTTP/1.1 200 OK\r\n" + "Content-Type: text/html; charset=UTF-8\r\n"
			+ "Set-Cookie: ime=Marko; Domain=fer.hr; Path=/; Max-Age=21\r\n"
			+ "Set-Cookie: ime=Ivan; Domain=ffzg.hr; Path=/; Max-Age=20\r\n\r\n";

	@SuppressWarnings("unused")
	@Test(expected = IllegalArgumentException.class)
	public void testNewInstance1() {
		RequestContext rc = new RequestContext(null, null, null, null);
	}

	@Test
	public void TestNewInctance2() {
		RequestContext rc = fillInSomeData(System.out);

		assertEquals("Invalid value", "Matej", rc.getParameters().get("1"));
		assertEquals("Invalid value", "Mislav", rc.getPersistentParameter("4"));
		assertEquals("Invalid value", "fer.hr", rc.getOutputCookies().get(0).getDomain());
	}

	@Test
	public void testCharset1() {
		RequestContext rc = fillInSomeData(System.out);
		assertEquals("Invalid value", null, rc.getCharset());
	}

	@Test
	public void testCharset2() throws IOException {
		OutputStream os = new ByteArrayOutputStream();
		RequestContext rc = fillInSomeData(os);
		rc.write("");
		assertEquals("Invalid value", StandardCharsets.UTF_8, rc.getCharset());
	}

	@Test
	public void testCharset3() throws IOException {
		OutputStream os = new ByteArrayOutputStream();
		RequestContext rc = fillInSomeData(os);
		rc.write("");
		rc.setCharset(StandardCharsets.US_ASCII);
		assertEquals("Invalid value", StandardCharsets.US_ASCII, rc.getCharset());
	}

	@Test
	public void testWrite1() throws IOException {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		RequestContext rc = fillInSomeData(os);
		rc.write("esi mi dobar");
		assertEquals("Invalid output", HEADER + "esi mi dobar", new String(os.toByteArray(), StandardCharsets.UTF_8));
	}

	@Test
	public void testWrite2() throws IOException {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		RequestContext rc = fillInSomeData(os);
		rc.write("");
		assertEquals("Invalid output", HEADER, new String(os.toByteArray(), StandardCharsets.UTF_8));
	}

	@Test
	public void testWrite3() throws IOException {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		RequestContext rc = fillInSomeData(os);
		rc.setCharset(StandardCharsets.ISO_8859_1);
		rc.write("nego šta nego da sam dobar");
		assertEquals("Invalid output",
				HEADER + new String("nego šta nego da sam dobar".getBytes(), StandardCharsets.ISO_8859_1),
				new String(os.toByteArray(), StandardCharsets.ISO_8859_1));
	}

	@Test(expected = RuntimeException.class)
	public void changeAfterHeader1() throws IOException {
		RequestContext rc = fillInSomeData(System.out);
		rc.write("");
		rc.setEncoding("UTF-8");

	}

	@Test(expected = RuntimeException.class)
	public void changeAfterHeader2() throws IOException {
		RequestContext rc = new RequestContext(System.out, null, null, null);
		rc.write("");
		rc.setMimeType("novi");
	}

	@Test(expected = RuntimeException.class)
	public void changeAfterHeader3() throws IOException {
		RequestContext rc = new RequestContext(System.out, null, null, null);
		rc.write("");
		rc.setEncoding("haha");
	}

	@Test(expected = RuntimeException.class)
	public void changeAfterHeader4() throws IOException {
		RequestContext rc = new RequestContext(System.out, null, null, null);
		rc.write("");
		rc.setStatusText("haha");
	}

	@SuppressWarnings("unused")
	@Test(expected = IllegalArgumentException.class)
	public void testCookie1() {
		RCCookie cookie = new RCCookie(null, "hej", null, null, null);
	}

	@SuppressWarnings("unused")
	@Test(expected = IllegalArgumentException.class)
	public void testCookie2() {
		RCCookie cookie = new RCCookie("hej", null, null, null, null);
	}

	@Test
	public void testCookie3() {
		RCCookie cookie = new RCCookie("Svi", "smo", 1, "Zdravko", "Mamić");
		assertEquals("Invalid output", "Svismo1ZdravkoMamić", cookie.getName() + cookie.getValue()
				+ Integer.toString(cookie.getMaxAge()) + cookie.getDomain() + cookie.getPath());
	}

	private RequestContext fillInSomeData(OutputStream os) {
		Map<String, String> params = new HashMap<>();
		params.put("1", "Matej");
		params.put("2", "Dunja");

		Map<String, String> perParams = new HashMap<>();
		perParams.put("3", "Ivo");
		perParams.put("4", "Mislav");

		List<RCCookie> cookies = new ArrayList<>();
		cookies.add(new RCCookie("ime", "Marko", 21, "fer.hr", "/"));
		cookies.add(new RCCookie("ime", "Ivan", 20, "ffzg.hr", "/"));

		return new RequestContext(os, params, perParams, cookies);
	}
}
