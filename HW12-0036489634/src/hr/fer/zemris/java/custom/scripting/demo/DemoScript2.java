package hr.fer.zemris.java.custom.scripting.demo;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hr.fer.zemris.java.custom.scripting.exec.SmartScriptEngine;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParserException;
import hr.fer.zemris.java.webserver.RequestContext;
import hr.fer.zemris.java.webserver.RequestContext.RCCookie;

/**
 * This is the demonstration program for the SmartScript script for testing the
 * addition functionality of the SmartScript language.
 * 
 * @author Matej Balun
 *
 */
public class DemoScript2 {

	/**
	 * The main method that executes the program.
	 * 
	 * @param args
	 *            not used in this method
	 */
	public static void main(String[] args) {

		String docBody = null;

		try {
			docBody = new String(Files.readAllBytes(Paths.get("scripts/zbrajanje.smscr")));
		} catch (IOException e) {
			System.err.println("Could not read file with provided path");
			System.exit(-1);
		}

		SmartScriptParser parser = null;
		try {
			parser = new SmartScriptParser(docBody);
		} catch (SmartScriptParserException e) {
			System.err.println("unable to parse document");
			System.exit(-1);
		}

		Map<String, String> parameters = new HashMap<String, String>();
		Map<String, String> persistentParameters = new HashMap<String, String>();
		List<RCCookie> cookies = new ArrayList<RequestContext.RCCookie>();
		parameters.put("a", "4");
		parameters.put("b", "2");

		new SmartScriptEngine(parser.getDocumentNode(),
				new RequestContext(System.out, parameters, persistentParameters, cookies)).execute();
	}

}
