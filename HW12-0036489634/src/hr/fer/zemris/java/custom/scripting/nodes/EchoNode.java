package hr.fer.zemris.java.custom.scripting.nodes;

import hr.fer.zemris.java.custom.scripting.elems.Element;
import hr.fer.zemris.java.custom.scripting.elems.ElementConstantDouble;
import hr.fer.zemris.java.custom.scripting.elems.ElementConstantInteger;
import hr.fer.zemris.java.custom.scripting.elems.ElementFunction;
import hr.fer.zemris.java.custom.scripting.elems.ElementOperator;
import hr.fer.zemris.java.custom.scripting.elems.ElementString;
import hr.fer.zemris.java.custom.scripting.elems.ElementVariable;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;

/**
 * This node represents the content of the echo inside the tags. It contains an
 * array of {@link SmartScriptParser} {@link Element}s created in the specified
 * way. The elements can be {@link ElementConstantDouble},
 * {@link ElementFunction}, {@link ElementOperator}, {@link ElementVariable},
 * {@link ElementConstantInteger} and {@link ElementString}.
 * 
 * @author Matej Balun
 *
 */
public class EchoNode extends Node {

	/**
	 * Array of the node's {@link Element}s.
	 */
	private Element[] elements;

	/**
	 * This constructor initializes the node with its array of {@link Element}s.
	 * 
	 * @param elements
	 *            - an array of node's {@link Element}s.
	 */
	public EchoNode(Element[] elements) {
		super();
		this.elements = elements;
	}

	/**
	 * Returns the {@link Element}s of this echo node.
	 * 
	 * @return elements of the node
	 */
	public Element[] getElements() {
		return this.elements;
	}

	/**
	 * Return a textual representation of the {@link EchoNode}, appending the
	 * textual representations of its {@link Element}s.
	 */
	@Override
	public String toString() {
		StringBuilder string = new StringBuilder();

		string.append("{$= ");

		for (int i = 0; i < elements.length; i++) {
			string.append(elements[i].asText());
			string.append(" ");
		}

		string.append("$}");
		return string.toString();
	}

	@Override
	public void accept(INodeVisitor visitor) {
		visitor.visitEchoNode(this);
	}

}
