package hr.fer.zemris.java.custom.scripting.elems;

/**
 * This class represents the function element of the parser tree node. it
 * contains no actual functionality. This class extends the {@link Element}
 * class.
 * 
 * @author Matej Balun
 *
 */
public class ElementFunction extends Element {

	/**
	 * The name of the function.
	 */
	private String name;

	/**
	 * This constructor initializes the function name.
	 * 
	 * @param name
	 *            - the function name.
	 */
	public ElementFunction(String name) {
		this.name = name;
	}

	/**
	 * Returns the name of this function.
	 */
	@Override
	public String asText() {
		return this.name;
	}

}
