package hr.fer.zemris.java.custom.scripting.parser;

import javax.lang.model.element.VariableElement;

import hr.fer.zemris.java.custom.scripting.elems.Element;
import hr.fer.zemris.java.custom.scripting.elems.ElementConstantDouble;
import hr.fer.zemris.java.custom.scripting.elems.ElementConstantInteger;
import hr.fer.zemris.java.custom.scripting.elems.ElementFunction;
import hr.fer.zemris.java.custom.scripting.elems.ElementOperator;
import hr.fer.zemris.java.custom.scripting.elems.ElementString;
import hr.fer.zemris.java.custom.scripting.elems.ElementVariable;
import hr.fer.zemris.java.custom.scripting.lexer.SmartScriptLexer;
import hr.fer.zemris.java.custom.scripting.lexer.SmartScriptToken;
import hr.fer.zemris.java.custom.scripting.lexer.SmartScriptTokenType;
import hr.fer.zemris.java.custom.scripting.nodes.EchoNode;
import hr.fer.zemris.java.custom.scripting.nodes.ForLoopNode;

/**
 * This factory static class creates the elements for the {@link ForLoopNode}
 * and {@link EchoNode} to help the {@link SmartScriptParser} to create a parse
 * tree.
 * 
 * @author Matej Balun
 *
 */
public class ParserFactory {

	/**
	 * index of for the {@link Element} array of the {@link EchoNode}.
	 */
	private static int index;

	/**
	 * size of the {@link Element} array for the {@link EchoNode}.
	 */
	private static int size;

	/**
	 * capacity of the {@link Element} array for the {@link EchoNode}.
	 */
	private static int capacity;

	/**
	 * This method resolves a given token and creates a possible parameter for
	 * the {@link ForLoopNode}. The token's content is resolved via specified
	 * regular expressions describing the legal expressions for the
	 * {@link ForLoopNode}.
	 * 
	 * @param token
	 *            - the specified token of {@link SmartScriptLexer}
	 * @return the {@link Element} value for the {@link ForLoopNode}
	 * @throws SmartScriptParserException
	 *             if the token does not match any of the legal expressions for
	 *             the {@link ForLoopNode}
	 */
	public static Element forFactory(SmartScriptToken token) {

		if (token.getType() == SmartScriptTokenType.STRING) {
			return new ElementString(token.getValue().toString());

		} else if (token.getValue().toString().matches("(\\+|-){0,1}\\s*[0-9]+\\.[0-9]*")) {
			return new ElementConstantDouble(Double.parseDouble(token.getValue().toString()));

		} else if (token.getValue().toString().matches("(\\+|-){0,1}\\s*[0-9]+")) {
			return new ElementConstantInteger(Integer.parseInt(token.getValue().toString()));

		} else if (token.getValue().toString().matches("^[a-zA-Z][a-zA-Z0-9_]*")) {
			return new ElementVariable(token.getValue().toString());

		} else {
			throw new SmartScriptParserException("Error in parsing elements");
		}
	}

	/**
	 * This method resolves a given token into an array of {@link Element}s for
	 * the {@link EchoNode}. The method converts the token into an character
	 * array and consumes the character grouping them into legal {@link Element}
	 * s. if the given token is a string, the method return that element as the
	 * string is already a legal element of the {@link EchoNode}. The method
	 * uses the {@link EchoAutomata} for consuming the characters and determing
	 * the legal states and expressions for the {@link EchoNode}.
	 * 
	 * @param element
	 *            - the specified token
	 * @return {@link Element} array for the {@link EchoNode}
	 */
	public static Element[] echoFactory(SmartScriptToken element) {
		size = 0;
		capacity = 16;
		Element[] elements = new Element[capacity];

		if (element.getType() == SmartScriptTokenType.STRING) {
			elements[size] = new ElementString(element.getValue().toString());
			return elements;
		} else {

			char[] word = element.getValue().toString().toCharArray();
			index = 0;
			EchoAutomata state = null;
			StringBuilder string = new StringBuilder();

			while (index != word.length) {
				char current = word[index];

				if (Character.isLetter(current) && index < word.length) {
					EchoAutomata.setState(EchoAutomata.VARIABLE);
					returnVariable(elements, word, string, state);
					continue;
				}

				if (current == '@' && index < word.length) {
					EchoAutomata.setState(EchoAutomata.FUNCTION);
					returnFunction(elements, word, string, state);
					continue;
				}

				if (Character.isDigit(current) && index < word.length) {
					EchoAutomata.setState(EchoAutomata.DOUBLE);
					returnDouble(elements, word, string, state);
					continue;
				}

				if ((current == '/' || current == '*' || current == '+' || current == '-' || current == '^')
						&& index < word.length) {
					EchoAutomata.setState(EchoAutomata.OPERATOR);
					returnOperator(elements, word, string, state);
					continue;
				}

				if (Character.isWhitespace(current) && index < word.length) {
					index++;
					continue;
				}

			}

			return elements;
		}
	}

	/**
	 * Consumes the characters grouping them into a {@link VariableElement} as
	 * long as the {@link EchoAutomata} is in that state.
	 * 
	 * @param elements
	 *            - {@link Element} array for the {@link EchoNode}
	 * @param word
	 *            - array of characters from the token
	 * @param string
	 *            - a {@link StringBuilder} for building the {@link Element}
	 * @param state
	 *            - the current {@link EchoAutomata} state.
	 */
	private static void returnVariable(Element[] elements, char[] word, StringBuilder string, EchoAutomata state) {

		do {
			char current = word[index];
			state = EchoAutomata.consume(current);
			if (state == EchoAutomata.VARIABLE) {
				string.append(current);
				index++;
			} else {
				break;
			}
		} while (state == EchoAutomata.VARIABLE && index < word.length);

		checkAndExtend(elements);
		elements[size++] = new ElementVariable(string.toString());
		string.delete(0, string.length());
	}

	/**
	 * Consumes the characters grouping them into a {@link ElementFunction} as
	 * long as the {@link EchoAutomata} is in that state.
	 * 
	 * @param elements
	 *            - {@link Element} array for the {@link EchoNode}
	 * @param word
	 *            - array of characters from the token
	 * @param string
	 *            - a {@link StringBuilder} for building the {@link Element}
	 * @param state
	 *            - the current {@link EchoAutomata} state.
	 */
	private static void returnFunction(Element[] elements, char[] word, StringBuilder string, EchoAutomata state) {

		do {
			char current = word[index];
			state = EchoAutomata.consume(current);
			if (state == EchoAutomata.FUNCTION) {
				string.append(current);
				index++;
			} else {
				break;
			}
		} while (state == EchoAutomata.FUNCTION && index < word.length);

		checkAndExtend(elements);
		elements[size++] = new ElementFunction(string.toString());
		string.delete(0, string.length());
	}

	/**
	 * Consumes the characters grouping them into a {@link ElementOperator} as
	 * long as the {@link EchoAutomata} is in that state.
	 * 
	 * @param elements
	 *            - {@link Element} array for the {@link EchoNode}
	 * @param word
	 *            - array of characters from the token
	 * @param string
	 *            - a {@link StringBuilder} for building the {@link Element}
	 * @param state
	 *            - the current {@link EchoAutomata} state.
	 */
	private static void returnOperator(Element[] elements, char[] word, StringBuilder string, EchoAutomata state) {

		do {
			char current = word[index];
			state = EchoAutomata.consume(current);
			if (state == EchoAutomata.OPERATOR) {
				string.append(current);
				index++;
			} else {
				break;
			}
		} while (state == EchoAutomata.OPERATOR && index < word.length);

		checkAndExtend(elements);
		elements[size++] = new ElementOperator(string.toString());
		string.delete(0, string.length());
	}

	/**
	 * Consumes the characters grouping them into a
	 * {@link ElementConstantDouble} as long as the {@link EchoAutomata} is in
	 * that state.
	 * 
	 * @param elements
	 *            - {@link Element} array for the {@link EchoNode}
	 * @param word
	 *            - array of characters from the token
	 * @param string
	 *            - a {@link StringBuilder} for building the {@link Element}
	 * @param state
	 *            - the current {@link EchoAutomata} state.
	 */
	private static void returnDouble(Element[] elements, char[] word, StringBuilder string, EchoAutomata state) {
		do {
			char current = word[index];
			state = EchoAutomata.consume(current);
			if (state == EchoAutomata.DOUBLE) {
				string.append(current);
				index++;
			} else {
				break;
			}
		} while (state == EchoAutomata.DOUBLE && index < word.length);

		checkAndExtend(elements);
		elements[size++] = new ElementConstantDouble(Double.parseDouble(string.toString()));
		string.delete(0, string.length());
	}

	/**
	 * checks if the specified {@link Element} array is full and extends it if
	 * needed.
	 * 
	 * @param elements
	 *            - {@link Element} array
	 * @return new {@link Element}
	 */
	public static Element[] checkAndExtend(Element[] elements) {
		if (size == capacity) {
			Element[] tmp = elements;
			capacity *= 2;
			elements = new Element[capacity];
			for (int i = 0; i < size; i++) {
				elements[i] = tmp[i];
			}
			return elements;
		} else {
			return elements;
		}
	}

}
