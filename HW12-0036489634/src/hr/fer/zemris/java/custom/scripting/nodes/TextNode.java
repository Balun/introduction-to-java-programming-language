package hr.fer.zemris.java.custom.scripting.nodes;

import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;

/**
 * This class represents a textual node of the {@link SmartScriptParser} tree.
 * It inherits the class {@link Node} and specifies a text outside of the tag
 * symbols.
 * 
 * @author Matej Balun
 *
 */
public class TextNode extends Node {

	/**
	 * The textual content of the node.
	 */
	private String text;

	/**
	 * This constructor initializes the node with it's textual content.
	 * 
	 * @param text
	 *            - textual conten of the node.
	 */
	public TextNode(String text) {
		super();
		this.text = text;
	}

	/**
	 * Return the content of the node.
	 * 
	 * @return - content of the node.
	 */
	public String getText() {
		return this.text;
	}

	@Override
	public String toString() {
		return this.text;
	}

	@Override
	public void accept(INodeVisitor visitor) {
		visitor.visitTextNode(this);
	}

}
