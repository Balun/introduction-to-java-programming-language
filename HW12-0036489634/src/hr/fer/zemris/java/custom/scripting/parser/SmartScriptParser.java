package hr.fer.zemris.java.custom.scripting.parser;

import static hr.fer.zemris.java.custom.scripting.parser.ParserFactory.*;

import hr.fer.zemris.java.custom.collections.ArrayIndexedCollection;
import hr.fer.zemris.java.custom.collections.EmptyStackException;
import hr.fer.zemris.java.custom.collections.ObjectStack;
import hr.fer.zemris.java.custom.scripting.elems.Element;
import hr.fer.zemris.java.custom.scripting.elems.ElementVariable;
import hr.fer.zemris.java.custom.scripting.lexer.SmartScriptLexer;
import hr.fer.zemris.java.custom.scripting.lexer.SmartScriptLexerState;
import hr.fer.zemris.java.custom.scripting.lexer.SmartScriptToken;
import hr.fer.zemris.java.custom.scripting.lexer.SmartScriptTokenType;
import hr.fer.zemris.java.custom.scripting.nodes.DocumentNode;
import hr.fer.zemris.java.custom.scripting.nodes.EchoNode;
import hr.fer.zemris.java.custom.scripting.nodes.ForLoopNode;
import hr.fer.zemris.java.custom.scripting.nodes.Node;
import hr.fer.zemris.java.custom.scripting.nodes.TextNode;

/**
 * This class represents a parser for the Smart Script language. The parser
 * takes the given document and initializes the lexer. The parser is taking the
 * raw tokens from the lexer until the lexer reaches the end of file. The parser
 * consumes the tokens, gives them logical values, resolves them and creates the
 * nodes for the parse tree. The parser inserts nodes into the stack for
 * appropriate for-end syntax and pops them if needed. After the parsing is done
 * the main node {@link DocumentNode} contains a parse tree consisting of all
 * other nodes in the text. It offers a method to return the nodes into original
 * textual representations (with some possible small errors in white spaces
 * etc.). As the lexer offers only four types of tokens, the parser must
 * determine their logical values and build the parse tree with reasonable
 * amount of private methods.
 * 
 * @author Matej Balun
 *
 */
public class SmartScriptParser {

	/**
	 * The parser's lexer for production of tokens based on the input text.
	 */
	private SmartScriptLexer lexer;

	/**
	 * A stack for achieving the correct node hierarchy and for-end syntax
	 */
	private ObjectStack stack;

	/**
	 * The main node witch contains all other nodes of the parsing process
	 */
	private DocumentNode documentNode;

	/**
	 * This constructor initiates the parser with specified input text and calls
	 * the method for starting the parsing process.
	 * 
	 * @param document
	 *            - the specified input document for parsing.
	 * @throws IllegalArgumentException
	 *             if the document is a null-reference
	 */
	public SmartScriptParser(String document) {
		if (document == null) {
			throw new IllegalArgumentException("The given document is a null-reference");
		}

		this.lexer = new SmartScriptLexer(document);
		this.stack = new ObjectStack();
		this.documentNode = null;
		stack.push(new DocumentNode());
		this.parse();
	}

	/**
	 * Return the document node of the parse tree.
	 * 
	 * @return the document node
	 * @throws SmartScriptParserException
	 *             if the document node is not initialized.
	 */
	public DocumentNode getDocumentNode() {
		if (this.documentNode == null) {
			throw new SmartScriptParserException("The document node is not initialised.");
		} else {
			return this.documentNode;
		}
	}

	/**
	 * This method consumes the next token from the lexer and determines which
	 * type of {@link Element} should be created based on the type and content
	 * of the token. After the node is created, the method delegates the
	 * insertion of the node to insertion method. When the end of file token is
	 * consumed, the method checks the stack and determines if there is an error
	 * in syntax.
	 * 
	 * @throws SmartScriptParserException
	 *             if there is an error in for-end syntax
	 */
	private void parse() {
		SmartScriptToken token;
		Node newNode;

		do {
			token = lexer.nextToken();

			switch (token.getType()) {

			case SYMBOL:
				newNode = consumeTag(token);
				break;

			case TEXT:
				newNode = consumeText(token);
				break;

			case EOF:
				newNode = consumeEOF(token);
				break;

			default:
				throw new SmartScriptParserException("Eror in consuming tokens");
			}

			insertNode(newNode);

		} while (!lexer.getToken().getType().equals(SmartScriptTokenType.EOF));

		if (this.stack.size() != 1) {
			throw new SmartScriptParserException("Error in FOR - END syntax");
		} else {
			this.documentNode = (DocumentNode) this.stack.pop();
		}

	}

	/**
	 * This method inserts the given node into the {@link DocumentNode}, or
	 * possible {@link ForLoopNode}, depending on the type of the node. The
	 * {@link ForLoopNode} is pushed on the stack to achieve the correct for-end
	 * syntax. All other nodes are added to the collection of the node on the
	 * top of the stack.
	 * 
	 * @param node
	 *            the specified node to insert int parse tree
	 */
	private void insertNode(Node node) {
		if (node != null) {
			if (node instanceof ForLoopNode) {
				this.stack.peek().addChildNode((ForLoopNode) node);
				this.stack.push((ForLoopNode) node);
			} else {
				this.stack.peek().addChildNode(node);
			}
		}

	}

	/**
	 * Consumes the end of file token, returning a null-reference and delivering
	 * a message.
	 * 
	 * @param token
	 *            the specified token, not used
	 * @return the specifed {@link Node}, not used
	 */
	private Node consumeEOF(SmartScriptToken token) {
		return null;
	}

	/**
	 * This method consumes the text token, creating a {@link TextNode}.
	 * 
	 * @param token
	 *            - the specified text token.
	 * @return the new text node for parse tree.
	 * @throws SmartScriptParserException
	 *             if the token's content is a null-reference
	 */
	private Node consumeText(SmartScriptToken token) {
		String text = token.getValue().toString();
		if (text == null) {
			throw new SmartScriptParserException("The text cannot be a null reference.");
		} else {
			return new TextNode(text);
		}
	}

	/**
	 * Consumes the word token, determines the state of the lexer and delegates
	 * the creation of the new nod depending on symbol's content (it can be a
	 * {@link ForLoopNode}, {@link EchoNode} or an "End" tag for the
	 * {@link ForLoopNode}).
	 * 
	 * @param token
	 *            - the specified token
	 * @return new {@link Node}
	 * @throws SmartScriptParserException
	 *             if is an error in parsing the token
	 */
	private Node consumeTag(SmartScriptToken token) {
		lexer.setState(SmartScriptLexerState.EXTENDED);
		SmartScriptToken newToken = this.lexer.nextToken();

		if (newToken.getType() == SmartScriptTokenType.WORD
				&& newToken.getValue().toString().toUpperCase().contains("FOR")) {
			return createForLoop();

		} else if (newToken.getType() == SmartScriptTokenType.SYMBOL
				&& newToken.getValue().toString().toUpperCase().contains("=")) {
			return createEcho();

		} else if (newToken.getType() == SmartScriptTokenType.WORD
				&& newToken.getValue().toString().toUpperCase().contains("END")) {
			return createEndFor();

		} else {
			throw new SmartScriptParserException("Error in parsing.");
		}

	}

	/**
	 * Pops the {@link Node} from top of the stack and determines if there was
	 * an error in for-end syntax. Returns a null-reference as representation of
	 * the "END" tag.
	 * 
	 * @return a null-reference for indication of the correct for-end syntax
	 * @throws SmartScriptParserException
	 *             if there is an error in for-end syntax
	 */
	private Node createEndFor() {

		while (lexer.nextToken().getType() != SmartScriptTokenType.SYMBOL)
			;

		try {
			this.stack.pop();
		} catch (EmptyStackException e) {
			throw new SmartScriptParserException("Error in FOR - END syntax");
		}
		return null;
	}

	/**
	 * Creates the {@link EchoNode} consuming the correct tokens for the echo
	 * expression. It resolves the tokens, grouping them into {@link Element}
	 * array for the node by a static class {@link ParserFactory} and its
	 * methods. The method reds tokens as long as there is no tag symbol to
	 * indicate the end of the echo expression.
	 * 
	 * @return the new {@link EchoNode} created based on the specified elements
	 */
	private Node createEcho() {
		ArrayIndexedCollection elements = new ArrayIndexedCollection();
		ArrayIndexedCollection tokens = new ArrayIndexedCollection();
		SmartScriptToken token;

		do {
			token = lexer.nextToken();
			if (token.getType() == SmartScriptTokenType.STRING || token.getType() == SmartScriptTokenType.WORD) {
				tokens.add(token);
			}
		} while (lexer.getToken().getType() != SmartScriptTokenType.SYMBOL);

		if (tokens.size() == 0) {
			return null;
		}

		for (Object element : tokens.toArray()) {
			Element[] nodeElement = echoFactory((SmartScriptToken) element);

			int i = 0;
			while (nodeElement[i] != null) {
				elements.add(nodeElement[i]);
				i++;
			}
		}

		int i = 0;
		Element[] finalElements = new Element[elements.size()];
		for (Object element : elements.toArray()) {
			finalElements[i] = (Element) element;
			i++;
		}

		return new EchoNode(finalElements);
	}

	/**
	 * Creates the new {@link ForLoopNode} consuming the tokens and grouping
	 * them into parameters for the correct for-loop syntax. It uses the static
	 * class {@link ParserFactory} and its methods to resolve the tokens and
	 * insert them as parameters for the {@link ForLoopNode}.
	 * 
	 * @return the new {@link ForLoopNode}
	 * @throws SmartScriptParserException
	 *             if there is an error in for-loop expression.
	 */
	private Node createForLoop() {
		ArrayIndexedCollection tokens = new ArrayIndexedCollection();
		SmartScriptToken token;

		do {
			token = lexer.nextToken();
			if (token.getType() == SmartScriptTokenType.STRING || token.getType() == SmartScriptTokenType.WORD) {
				tokens.add(token);
			}
		} while (lexer.getToken().getType() != SmartScriptTokenType.SYMBOL);

		switch (tokens.size()) {

		case 3:
			if (((SmartScriptToken) tokens.get(0)).getType() == SmartScriptTokenType.WORD) {

				return new ForLoopNode((ElementVariable) forFactory((SmartScriptToken) tokens.get(0)),
						forFactory((SmartScriptToken) tokens.get(1)), forFactory((SmartScriptToken) tokens.get(2)));

			} else {
				throw new SmartScriptParserException("The variable must be a word.");
			}

		case 4:
			if (((SmartScriptToken) tokens.get(0)).getType() == SmartScriptTokenType.WORD) {

				return new ForLoopNode((ElementVariable) forFactory((SmartScriptToken) tokens.get(0)),
						forFactory((SmartScriptToken) tokens.get(1)), forFactory((SmartScriptToken) tokens.get(2)),
						forFactory((SmartScriptToken) tokens.get(3)));

			} else {
				throw new SmartScriptParserException("The variable must be a word.");
			}

		default:
			throw new SmartScriptParserException("The for-construct must have a variable and two or three parameters!");
		}

	}

}
