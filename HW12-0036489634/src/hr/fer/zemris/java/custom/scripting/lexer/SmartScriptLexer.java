package hr.fer.zemris.java.custom.scripting.lexer;

import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;

/**
 * This is the main class that represents a lexer for the Smart Script
 * programming language. The lexer works on the standard way - it consumes the
 * given textual representation of the program and reads the text character by
 * character grouping them into specified legal tokens. As this is a simple
 * lexer, it consists of three token types; symbol (for special symbols that
 * define the parsing state of the {@link SmartScriptParser}), string (special
 * groups of characters inside the "" symbols), text (for specified textual
 * parts of the program) and word (groups of all other legal characters). The
 * lexer also defines legal escape sequences (\", \{ etc.). The actual grouping
 * of tokens into logical elements is done in {@link SmartScriptParser} as this
 * lexer produces only raw tokens. To ease the token production, the lexer
 * defines three legal states; Basic(for text), extended (for characters in
 * tags) and string (for specified strings) state.
 * 
 * @author Matej Balun
 *
 */
public class SmartScriptLexer {

	/**
	 * Character array of the program file.
	 */
	private char[] data;

	/**
	 * A currently produced token of the lexer.
	 */
	private SmartScriptToken token;

	/**
	 * current index of the character array.
	 */
	private int currentIndex;

	/**
	 * Current state of the lexer.
	 */
	private SmartScriptLexerState state;

	/**
	 * This constructor initializes the lexer with specified textual document
	 * for parsing and sets the current state to basic state.
	 * 
	 * @param text
	 *            - A textual document for parsing.
	 */
	public SmartScriptLexer(String text) {
		if (text == null) {
			throw new IllegalArgumentException("The input string must not be a null-reference.");
		}

		this.data = text.toCharArray();
		this.currentIndex = 0;
		this.state = SmartScriptLexerState.BASIC;
	}

	/**
	 * Sets the state of the lexer.
	 * 
	 * @param state
	 *            - the specified state for the lexer.
	 */
	public void setState(SmartScriptLexerState state) {
		this.state = state;
	}

	/**
	 * Returns the current state of the lexer.
	 * 
	 * @return state of the lexer.
	 */
	public SmartScriptLexerState getState() {
		return this.state;
	}

	/**
	 * This method produces the next token from the text. The way the token is
	 * produced is determined via the lexer's current state. If the lexer has
	 * reached the end of file the methods throws an appropriate exception. The
	 * token production by type is delegated to specified methods.
	 * 
	 * @return the next token of the lexer.
	 * @throws LexerException
	 *             if the lexer has reached the end of file.
	 */
	public SmartScriptToken nextToken() {
		if (token != null && token.getType().equals(SmartScriptTokenType.EOF)) {
			throw new LexerException("The lexer has reached the end of file.");
		}

		if (currentIndex == data.length) {
			token = new SmartScriptToken(SmartScriptTokenType.EOF, null);
			return token;
		}

		if (this.state.equals(SmartScriptLexerState.EXTENDED)) {
			return getExtendedToken();
		} else if (this.state.equals(SmartScriptLexerState.STRING)) {
			return getStringToken();
		} else {
			return getTextToken();
		}

	}

	/**
	 * This method returns the text-type token. The lexer reads from the file as
	 * long as there are no special symbols that terminate the textual part of
	 * the script. When the specified symbols occur, the lexer changes the state
	 * specified by the symbol and returns the text token. This method also
	 * takes care of escape sequences for the specified symbols.
	 * 
	 * @return the textual Smart Script token
	 */
	private SmartScriptToken getTextToken() {
		char current = data[currentIndex];
		StringBuilder text = new StringBuilder();

		if (current == '{' && data[currentIndex + 1] == '$' && currentIndex < data.length - 1
				&& this.state == SmartScriptLexerState.BASIC) {
			text.append("{$");
			this.token = new SmartScriptToken(SmartScriptTokenType.SYMBOL, text.toString());
			currentIndex += 2;
			this.state = SmartScriptLexerState.EXTENDED;
			return this.token;
		}

		try {
			if (current == '\\' && data[currentIndex + 1] == '{') {
				current = data[++currentIndex];
				text.append(current);
				currentIndex++;
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			// There is no escape sequence, continue...
		}

		try {
			while (current != '{' && data[currentIndex + 1] != '$' && currentIndex < (data.length)) {
				current = data[currentIndex];

				if (current == '\\' && data[currentIndex + 1] == '{') {
					currentIndex++;
				} else {
					text.append(current);
					currentIndex++;
				}
			}
		} catch (IndexOutOfBoundsException e) {
			if (currentIndex == data.length - 1) {
				current = data[currentIndex];
				text.append(current);
				currentIndex++;
			}
		}

		this.token = new SmartScriptToken(SmartScriptTokenType.TEXT, text.toString());
		return this.token;
	}

	/**
	 * This method works in the string state of the lexer. It consumes the
	 * characters as long as there is no legal string-closing symbol("). After
	 * reading the string, the method reverts the state of the lexer back to the
	 * extended state as that is the only state where a legal string element can
	 * be in the Smart Script. The method also takes care of legal escape
	 * sequences and returns the string token of the lexer.
	 * 
	 * @return A string token of the lexer.
	 */
	private SmartScriptToken getStringToken() {
		char current = data[currentIndex];
		StringBuilder string = new StringBuilder();

		if (current == '"') {
			this.token = new SmartScriptToken(SmartScriptTokenType.STRING, string.toString());
			currentIndex++;
			this.state = SmartScriptLexerState.EXTENDED;
			return this.token;
		}
		try {
			if (current == '\\' && data[currentIndex + 1] == '"') {
				current = data[++currentIndex];
				string.append(current);
				currentIndex++;
			}
		} catch (IndexOutOfBoundsException e) {
			// There is no escape sequence, continue
		}

		while (current != '"' && currentIndex < data.length) {
			current = data[currentIndex];

			if (current == '\\' && data[currentIndex + 1] == '"') {
				currentIndex++;
			} else if (current == '"' && data[currentIndex - 1] == '\\') {
				string.append(current);
				current = data[++currentIndex];
			} else if (current == '"' && data[currentIndex - 1] != '\\') {
				currentIndex++;
				this.token = new SmartScriptToken(SmartScriptTokenType.STRING, string.toString());
				this.state = SmartScriptLexerState.EXTENDED;
				return this.token;
			} else {
				string.append(current);
				currentIndex++;
			}
		}

		this.token = new SmartScriptToken(SmartScriptTokenType.STRING, string.toString());
		return this.token;
	}

	/**
	 * This method works in the extended (tag) state of the lexer. In the
	 * extended state, the only legal tokens are word or strings. The method
	 * consumes the character as long as there is no legal closing symbol
	 * ("$}"). If the tag is closed, the method reverts the state of the lexer
	 * in basic state.
	 * 
	 * @return a tag token of the extended lexer state.
	 */
	private SmartScriptToken getExtendedToken() {
		char current = data[currentIndex];
		StringBuilder tag = new StringBuilder();

		while (Character.isWhitespace(current)) {
			current = data[++currentIndex];
		}

		if (current == '$' && data[currentIndex + 1] == '}' && currentIndex < data.length - 1) {
			tag.append("$}");
			this.token = new SmartScriptToken(SmartScriptTokenType.SYMBOL, tag.toString());
			currentIndex += 2;
			this.state = SmartScriptLexerState.BASIC;
			return this.token;
		}

		if (current == '=') {
			tag.append(current);
			this.token = new SmartScriptToken(SmartScriptTokenType.SYMBOL, tag.toString());
			currentIndex++;
			return this.token;
		}

		if (current == '"') {
			currentIndex++;
			this.state = SmartScriptLexerState.STRING;
			getStringToken();
			return this.token;
		}

		try {
			while (current != '$' && data[currentIndex + 1] != '}' && currentIndex < (data.length)) {
				current = data[currentIndex];
				if (Character.isWhitespace(current)) {
					break;
				} else if (current == '=') {
					currentIndex++;
					this.token = new SmartScriptToken(SmartScriptTokenType.SYMBOL, current);
					return this.token;
				} else if (current == '"') {
					currentIndex++;
					this.state = SmartScriptLexerState.STRING;
					this.token = new SmartScriptToken(SmartScriptTokenType.WORD, tag.toString());
					return this.token;
				}
				tag.append(current);
				currentIndex++;
			}
		} catch (IndexOutOfBoundsException e) {
			if (currentIndex == data.length - 1) {
				current = data[currentIndex];
				if (current == '=') {
					currentIndex++;
					this.token = new SmartScriptToken(SmartScriptTokenType.SYMBOL, current);
					return this.token;
				}
				tag.append(current);
				currentIndex++;
			}
		}

		this.token = new SmartScriptToken(SmartScriptTokenType.WORD, tag.toString());
		return this.token;
	}

	/**
	 * Return the current token of the lexer.
	 * 
	 * @return current token of the lexer.
	 */
	public SmartScriptToken getToken() {
		return this.token;
	}

}
