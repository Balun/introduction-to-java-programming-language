package hr.fer.zemris.java.custom.scripting.exec;

import java.text.DecimalFormat;
import java.util.Stack;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;

import hr.fer.zemris.java.webserver.RequestContext;

/**
 * This utility class offers methods and fields for {@link SmartScriptEngine}
 * compiler for executing the SmartScript scripts with generated parser tree.
 * The class covers most of the built-in functionalities of the
 * {@link SmartScriptEngine}.
 * 
 * @author Matej Balun
 *
 */
public class FunctionUtility {

	/**
	 * The sine {@link String} constant.
	 */
	private static final String SINE = "sin";

	/**
	 * The decimal format {@link String} constant
	 */
	private static final String DEC_FMT = "decfmt";

	/**
	 * The duplicate {@link String} constant.
	 */
	private static final String DUP = "dup";

	/**
	 * The swap {@link String} constant.
	 */
	private static final String SWAP = "swap";

	/**
	 * The set mime type {@link String} constant.
	 */
	private static final String SET_MIME_TYPE = "setMimeType";

	/**
	 * The get parameter {@link String} constant.
	 */
	private static final String PARAM_GET = "paramGet";

	/**
	 * The persistent parameter get {@link String} constant.
	 */
	private static final String P_PARAM_GET = "pparamGet";

	/**
	 * The persistent parameter set {@link String} constant.
	 */
	private static final String P_PARAM_SET = "pparamSet";

	/**
	 * The persistent parameter delete {@link String} constant.
	 */
	private static final String P_PARAM_DEL = "pparamDel";

	/**
	 * The temporary parameter get {@link String} constant.
	 */
	private static final String T_PARAM_GET = "tparamGet";

	/**
	 * THe temporary parameter set {@link String} constant
	 */
	private static final String T_PARAM_SET = "tparamSet";

	/**
	 * The temporary parameter delete {@link String} constant.
	 */
	private static final String T_PARAM_DEL = "tparamDel";

	/**
	 * The private constructor to forbid the instantiation of the class.
	 */
	private FunctionUtility() {
	}

	/**
	 * This method executes the function specified by the function name and
	 * parameters found on the given object stack for the program. The method
	 * puts the results back to stack, or activates the certain features of the
	 * {@link RequestContext}.
	 * 
	 * @param stack
	 *            the specified stack for script's compilation
	 * @param functionName
	 *            specified name of the function
	 * @param context
	 *            THe {@link RequestContext} object for changing specified
	 *            features.
	 */
	public static void execute(Stack<Object> stack, String functionName, RequestContext context) {

		String newName = functionName.replace("@", "").trim();

		switch (newName) {

		case SINE:
			apply(x -> {
				return Math.sin(Double.parseDouble(x) / 360.0 * Math.PI * 2);
			}, stack);
			break;

		case DEC_FMT:
			apply((f, x) -> {
				return new DecimalFormat(f).format(Double.parseDouble(x));
			}, stack);
			break;

		case DUP:
			stack.push(stack.peek());
			break;

		case SWAP:
			apply((a, b) -> {
				stack.push(a);
				stack.push(b);
			}, stack);
			break;

		case SET_MIME_TYPE:
			apply((x) -> context.setMimeType(x), stack);
			break;

		case PARAM_GET:
			apply((dv, name) -> {
				return context.getParameter(name) == null ? dv : context.getParameter(name);
			}, stack);
			break;

		case P_PARAM_GET:
			apply((dv, name) -> {
				return context.getPersistentParameter(name) == null ? dv : context.getPersistentParameter(name);
			}, stack);
			break;

		case P_PARAM_SET:
			apply((name, value) -> context.setPersistentParameter(name.toString(), value.toString()), stack);
			break;

		case P_PARAM_DEL:
			apply((name) -> context.removePersistentParameter(name), stack);
			break;

		case T_PARAM_GET:
			apply((dv, name) -> {
				return context.getTemporaryParameter(name) == null ? dv : context.getTemporaryParameter(name);
			}, stack);
			break;

		case T_PARAM_SET:
			apply((name, value) -> context.setTemporaryParameter(name.toString(), value.toString()), stack);
			break;

		case T_PARAM_DEL:
			apply((name) -> context.removeTemporaryParameter(name), stack);
			break;

		default:
			throw new IllegalArgumentException("Invalid function name \"" + newName + "\".");
		}
	}

	/**
	 * Applies the operation specified by {@link Function} interface and saves
	 * the result on the stack
	 * 
	 * @param function
	 *            the specified {@link Function} implementation
	 * @param stack
	 *            the specified stack
	 */
	private static void apply(Function<String, Object> function, Stack<Object> stack) {
		stack.push(function.apply(stack.pop().toString()));
	}

	/**
	 * Applies the operation specified by {@link BiFunction} interface and saves
	 * the result on the stack.
	 * 
	 * @param function
	 *            the specified {@link BiFunction} implementation
	 * @param stack
	 *            the specified stack
	 */
	private static void apply(BiFunction<String, String, Object> function, Stack<Object> stack) {
		stack.push(function.apply(stack.pop().toString(), stack.pop().toString()));
	}

	/**
	 * Applies the operation specified by {@link BiConsumer} interface using the
	 * parameters from stack.
	 * 
	 * @param function
	 *            the specified {@link BiConsumer} implementation
	 * @param stack
	 *            the specified stack
	 */
	private static void apply(BiConsumer<Object, Object> function, Stack<Object> stack) {
		function.accept(stack.pop(), stack.pop());
	}

	/**
	 * Applies the operation specified by {@link Consumer} interface using the
	 * parameters from stack
	 * 
	 * @param function
	 *            the specified {@link BiConsumer} implementation
	 * @param stack
	 *            the specified stack
	 */
	private static void apply(Consumer<String> function, Stack<Object> stack) {
		function.accept(stack.pop().toString());
	}
}
