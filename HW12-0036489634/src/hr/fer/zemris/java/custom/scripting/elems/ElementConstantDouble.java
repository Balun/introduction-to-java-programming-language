package hr.fer.zemris.java.custom.scripting.elems;

/**
 * This class represents a numeric value element of the parser tree node that
 * can be interpreted in decimal format. This class extends the {@link Element}
 * class.
 * 
 * @author Matej Balun
 *
 */
public class ElementConstantDouble extends Element {

	/**
	 * The actual value of this element.
	 */
	private double value;

	/**
	 * This constructor Initializes the double element.
	 * 
	 * @param value
	 *            - double value of the element.
	 */
	public ElementConstantDouble(double value) {
		this.value = value;
	}

	/**
	 * Return the textual representation of this element.
	 */
	@Override
	public String asText() {
		return Double.toString(value);
	}

}
