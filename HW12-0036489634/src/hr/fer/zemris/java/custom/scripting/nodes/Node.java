package hr.fer.zemris.java.custom.scripting.nodes;

import hr.fer.zemris.java.custom.collections.ArrayIndexedCollection;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;

/**
 * This is the base class that represents a node of the
 * {@link SmartScriptParser} tree. Other node classes inherit this class and
 * specify the nodes correctly. The node contains an array of its possible
 * children.
 * 
 * @author Matej Balun
 *
 */
public class Node {

	/**
	 * An array of the node's children.
	 */
	private ArrayIndexedCollection collection;

	/**
	 * This constructor initializes the node.
	 */
	public Node() {
		super();
		this.collection = null;
	}

	/**
	 * Adds a {@link Node} child into the array.
	 * 
	 * @param child
	 *            specified child of the node.
	 */
	public void addChildNode(Node child) {
		if (this.collection == null) {
			this.collection = new ArrayIndexedCollection();
			this.collection.add(child);
		} else {
			this.collection.add(child);
		}
	}

	/**
	 * Returns the number of node's children.
	 * 
	 * @return number of node's children.
	 */
	public int numberOfChildren() {
		if (this.collection == null) {
			return 0;
		} else {
			return this.collection.size();
		}
	}

	/**
	 * Returns the child on the specified index.
	 * 
	 * @param index
	 *            - the specified index
	 * @return the specified child
	 */
	public Node getChild(int index) {
		if (this.collection == null) {
			throw new IndexOutOfBoundsException("The node has no children.");
		} else {
			return (Node) this.collection.get(index);
		}
	}

	/**
	 * This method defines the action executed in for the current object in the
	 * {@link INodeVisitor} data traversal.
	 * 
	 * @param visitor
	 *            the specified {@link INodeVisitor} for traversal.
	 */
	public void accept(INodeVisitor visitor) {
		// implementation not provided
	}

}
