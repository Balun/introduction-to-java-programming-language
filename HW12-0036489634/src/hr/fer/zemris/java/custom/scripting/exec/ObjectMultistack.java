package hr.fer.zemris.java.custom.scripting.exec;

import java.util.EmptyStackException;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * This class represents the map-like collection with Strings as keys, and stack
 * collections as values. The class offers regular methods for operating with
 * stacks, specified by keys. The average time complexity of retrieval (pop,
 * peek) is O(1), and time complexity of insertion is also O(1). the stacks are
 * built by the {@link MultistackEntry} objects, that contain
 * {@link ValueWrapper}s as values and a reference to the next
 * {@link MultistackEntry} in the stack (to the nottom of the stack).
 * 
 * @author Matej Balun
 *
 */
public class ObjectMultistack {

	/**
	 * The internal map for the collection of stacks.
	 */
	private Map<String, MultistackEntry> map;

	/**
	 * Number of all values in the collection.
	 */
	private int size;

	/**
	 * This constructor initialises the collection, with new instance of
	 * {@link Map} and size seto to zero.
	 */
	public ObjectMultistack() {
		this.map = new LinkedHashMap<>();
		this.size = 0;
	}

	/**
	 * Returns the number of elements in the collection.
	 * 
	 * @return number of elements in the collection
	 */
	public int size() {
		return this.size;
	}

	/**
	 * Checks if the whole collection is empty.
	 * 
	 * @return true if there are no elements in the collection, otherwise false
	 */
	public boolean isEmpty() {
		return size == 0;
	}

	/**
	 * Inserts the element in the calculated slot of the map, pushing it to the
	 * top of specified stack.
	 * 
	 * @param name
	 *            key of the vale
	 * @param valueWrapper
	 *            value to push
	 * @throws IllegalArgumentException
	 *             if the name or the value is a null-reference
	 */
	public void push(String name, ValueWrapper valueWrapper) {
		if (name == null || valueWrapper == null) {
			throw new IllegalArgumentException("The name and the value must not be a null-references");
		}

		MultistackEntry tmp = map.get(name);
		map.put(name, new MultistackEntry(valueWrapper, tmp));
		size++;
	}

	/**
	 * Returns the element from the top of the stack with specified key in the
	 * map, erasing it from the stack.
	 * 
	 * @param name
	 *            key of the stack
	 * @return element from the top of the specified stack
	 * @throws EmptyStackException
	 *             if the specified stack is empty
	 */
	public ValueWrapper pop(String name) {
		if (isEmpty(name)) {
			throw new EmptyStackException();
		}

		MultistackEntry popped = map.get(name);
		map.put(name, popped.next);
		return popped.getValue();

	}

	/**
	 * Returns the element from the top of the stack with the specified key,
	 * without removal of the element.
	 * 
	 * @param name
	 *            key of the stack
	 * @return element from the top of the specified stack
	 * @throws EmptyStackException
	 *             if the specified stack is empty
	 */
	public ValueWrapper peek(String name) {
		if (isEmpty(name)) {
			throw new EmptyStackException();
		}

		return map.get(name).value;
	}

	/**
	 * Checks if the stack with the specified key is empty.
	 * 
	 * @param name
	 *            key of the specified stack
	 * @return true if the specified stack is empty, otherwise false
	 * @throws IllegalArgumentException
	 *             if the specified key is a null-reference
	 */
	public boolean isEmpty(String name) {
		if (name == null) {
			throw new IllegalArgumentException("The key must not be a null-reference");

		} else if (map.containsKey(name) && map.get(name).value != null) {
			return false;
		}

		return true;
	}

	/**
	 * This nested static class represents a single entry in one of the stacks
	 * in the {@link ObjectMultistack} collection. The {@link MultistackEntry}
	 * consists of a {@link ValueWrapper} value and a reference to the next
	 * element in the specified stack.
	 * 
	 * @author Matej Balun
	 *
	 */
	private static class MultistackEntry {

		/**
		 * Value of the entry.
		 */
		private ValueWrapper value;

		/**
		 * Reference to the next entry in the current stack.
		 */
		private MultistackEntry next;

		/**
		 * Initialises the entry with the value and a reference to the next
		 * entry (can be a null-reference).
		 * 
		 * @param value
		 *            the specified value
		 * @param next
		 *            the reference to the next entry, which can be a
		 *            null-reference
		 * @throws IllegalArgumentException
		 *             if the value is a null-reference (although the internal
		 *             value of the {@link ValueWrapper} can be a
		 *             null-reference)
		 */
		public MultistackEntry(ValueWrapper value, MultistackEntry next) {
			if (value == null) {
				throw new IllegalArgumentException("The value in stack must not be a null-reference");
			}

			this.value = value;
			this.next = next;
		}

		/**
		 * Returns the {@link ValueWrapper} value of the entry.
		 * 
		 * @return the value of the entry.
		 */
		public ValueWrapper getValue() {
			return value;
		}
	}

}
