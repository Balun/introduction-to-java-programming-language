package hr.fer.zemris.java.custom.scripting.exec;

import java.io.IOException;
import java.util.Objects;
import java.util.Stack;

import hr.fer.zemris.java.custom.scripting.elems.Element;
import hr.fer.zemris.java.custom.scripting.elems.ElementConstantDouble;
import hr.fer.zemris.java.custom.scripting.elems.ElementConstantInteger;
import hr.fer.zemris.java.custom.scripting.elems.ElementFunction;
import hr.fer.zemris.java.custom.scripting.elems.ElementOperator;
import hr.fer.zemris.java.custom.scripting.elems.ElementString;
import hr.fer.zemris.java.custom.scripting.elems.ElementVariable;
import hr.fer.zemris.java.custom.scripting.nodes.DocumentNode;
import hr.fer.zemris.java.custom.scripting.nodes.EchoNode;
import hr.fer.zemris.java.custom.scripting.nodes.ForLoopNode;
import hr.fer.zemris.java.custom.scripting.nodes.INodeVisitor;
import hr.fer.zemris.java.custom.scripting.nodes.TextNode;
import hr.fer.zemris.java.webserver.RequestContext;

/**
 * This class represents the simple executor for SmartScript scripts. The engine
 * takes the parser's generated {@link DocumentNode} which contains the
 * generated parser tree for script compiling. The engine traverses the tree in
 * specified order, executing the functions with variables and parameters from
 * {@link ObjectMultistack}.
 * 
 * @author Matej Balun
 *
 */
public class SmartScriptEngine {

	/**
	 * The {@link DocumentNode} with the generated parser tree.
	 */
	private DocumentNode documentNode;

	/**
	 * The {@link RequestContext} for writing and request operations.
	 */
	private RequestContext requestContext;

	/**
	 * The stack for storing parameters and variables.
	 */
	private ObjectMultistack multistack;

	/**
	 * The specified visitor pattern implementation for parser tree traversal.
	 */
	private INodeVisitor visitor;

	/**
	 * This constructor initialises the {@link SmartScriptEngine} with specified
	 * document node and request context.
	 * 
	 * @param node
	 *            the specified {@link DocumentNode} containing the parser tree
	 * @param requestContext
	 *            the specified {@link RequestContext}
	 */
	public SmartScriptEngine(DocumentNode node, RequestContext requestContext) {
		Objects.requireNonNull(node, "The document node must not be a null-reference");
		Objects.requireNonNull(requestContext, "The request context must not be a null-reference");

		this.documentNode = node;
		this.requestContext = requestContext;
		this.multistack = new ObjectMultistack();
		this.visitor = new SmartScriptVisitor();
	}

	/**
	 * Starts the iterative/recursive execution of the parser tree with the
	 * specified {@link INodeVisitor}.
	 */
	public void execute() {
		this.documentNode.accept(visitor);
	}

	/**
	 * This class implements the {@link INodeVisitor} interface which represents
	 * the standard visitor design pattern implementation. It traverses the
	 * parser tree, executing specified operations and functions.
	 * 
	 * @author Matej Balun
	 *
	 */
	private class SmartScriptVisitor implements INodeVisitor {

		@Override
		public void visitTextNode(TextNode node) {
			try {
				requestContext.write(node.getText());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		@Override
		public void visitForLoopNode(ForLoopNode node) {
			String variable = node.getVariable().asText();
			multistack.push(variable, new ValueWrapper(node.getStartExpression().asText()));

			while (multistack.peek(variable).numCompare(node.getEndExpression().asText()) <= 0) {
				for (int i = 0; i < node.numberOfChildren(); i++) {
					node.getChild(i).accept(this);
				}

				ValueWrapper tmp = multistack.peek(variable);
				tmp.increment(node.getStepExpression().asText());
				multistack.push(variable, tmp);

				if (tmp.numCompare(node.getEndExpression().asText()) > 0) {
					multistack.pop(variable);
					break;
				}
			}
		}

		@Override
		public void visitEchoNode(EchoNode node) {
			Stack<Object> tmpStack = new Stack<>();

			for (Element element : node.getElements()) {
				if (element instanceof ElementConstantInteger || element instanceof ElementConstantDouble
						|| element instanceof ElementString) {
					tmpStack.push(element.asText());

				} else if (element instanceof ElementVariable) {
					tmpStack.push(multistack.peek(element.asText()).getValue());

				} else if (element instanceof ElementOperator) {
					Object arg1 = tmpStack.pop();
					Object arg2 = tmpStack.pop();
					tmpStack.push(getOperationResult(arg1, arg2, element.asText()));

				} else if (element instanceof ElementFunction) {
					FunctionUtility.execute(tmpStack, element.asText(), requestContext);
				}

			}

			for (Object elem : tmpStack) {
				try {
					requestContext.write(elem.toString());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		@Override
		public void visitDocumentNode(DocumentNode node) {
			for (int i = 0; i < node.numberOfChildren(); i++) {
				node.getChild(i).accept(this);
			}
		}

		/**
		 * THis method computes the result of the standard arithmetic operations
		 * specified by operator and arguments.
		 * 
		 * @param arg1
		 *            the first argument
		 * @param arg2
		 *            the second argument
		 * @param operator
		 *            the specified operator
		 * @return result of the specified arithmetic operation
		 */
		private Object getOperationResult(Object arg1, Object arg2, String operator) {
			ValueWrapper argument = new ValueWrapper(arg1);

			switch (operator) {

			case "+":
				argument.increment(arg2);
				break;

			case "-":
				argument.decrement(arg2);
				break;

			case "*":
				argument.multiply(arg2);
				break;

			case "/":
				argument.divide(arg2);
				break;

			default:
				throw new ArithmeticException("Invalid arithmetic operator specified");
			}

			return argument.getValue();
		}

	}
}
