package hr.fer.zemris.java.custom.scripting.demo;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import hr.fer.zemris.java.custom.scripting.nodes.DocumentNode;
import hr.fer.zemris.java.custom.scripting.nodes.EchoNode;
import hr.fer.zemris.java.custom.scripting.nodes.ForLoopNode;
import hr.fer.zemris.java.custom.scripting.nodes.INodeVisitor;
import hr.fer.zemris.java.custom.scripting.nodes.Node;
import hr.fer.zemris.java.custom.scripting.nodes.TextNode;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParserException;

/**
 * This program demonstrates the work of the {@link INodeVisitor} interface and
 * its concrete implementation {@link WriterVisitor}. The interface represents
 * the visitor design pattern interface, offering the organised traversal of the
 * parser tree and generation of text representing the script
 * 
 * @author Matej Balun
 *
 */
public class TreeWriter {

	/**
	 * The main method that executes the program.
	 * 
	 * @param args
	 *            not used in this method
	 */
	public static void main(String[] args) {
		if (args.length != 1) {
			System.err.println("Invalid number of arguments provided.");
			System.exit(-1);
		}

		String docBody = null;

		try {
			docBody = new String(Files.readAllBytes(Paths.get(args[0].trim()).toAbsolutePath()));
		} catch (IOException e) {
			System.err.println("Could not read file with provided path");
			System.exit(-1);
		}

		SmartScriptParser parser = null;
		try {
			parser = new SmartScriptParser(docBody);
		} catch (SmartScriptParserException e) {
			System.err.println("unable to parse document");
			System.exit(-1);
		}

		INodeVisitor visitor = new WriterVisitor();
		parser.getDocumentNode().accept(visitor);
	}

	/**
	 * This class represents the implementation of {@link INodeVisitor} visitor
	 * for specification of visiting actions in parser tree traversal. This
	 * class mainly serves for simple purposes for writing the script body in
	 * correct order to the standard output.
	 * 
	 * @author Matej Balun
	 *
	 */
	private static class WriterVisitor implements INodeVisitor {

		@Override
		public void visitTextNode(TextNode node) {
			System.out.print(node.toString());
		}

		@Override
		public void visitForLoopNode(ForLoopNode node) {
			System.out.print(node.toString());
			for (int i = 0; i < node.numberOfChildren(); i++) {
				Node currentNode = node.getChild(i);
				currentNode.accept(this);
			}

			System.out.print("{$END$}");
		}

		@Override
		public void visitEchoNode(EchoNode node) {
			System.out.print(node.toString());
		}

		@Override
		public void visitDocumentNode(DocumentNode node) {
			for (int i = 0; i < node.numberOfChildren(); i++) {
				Node currentNode = node.getChild(i);
				currentNode.accept(this);
			}
		}

	}

}
