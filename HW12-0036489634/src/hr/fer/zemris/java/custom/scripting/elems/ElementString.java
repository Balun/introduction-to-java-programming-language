package hr.fer.zemris.java.custom.scripting.elems;

/**
 * This class represents the string element of the parser tree node. It extends
 * {@link Element} class;
 * 
 * @author Matej Balun
 *
 */
public class ElementString extends Element {

	/**
	 * Value of this string element.
	 */
	private String value;

	/**
	 * This constructor initializes this string element.
	 * 
	 * @param value
	 *            - a string value of the element.
	 */
	public ElementString(String value) {
		if (value.contains("\\r\\n") || value.contains("\\t") || value.contains("\"") || value.contains("\\n")) {
			char[] data = value.toCharArray();
			StringBuilder builder = new StringBuilder();
			for (int i = 0; i < data.length; i++) {
				if (data[i] == '\\' && data[i + 1] == 'r') {
					builder.append('\r');
					i++;
				} else if (data[i] == '\\' && data[i + 1] == 'n') {
					builder.append('\n');
					i++;
				} else if (data[i] == '\\' && data[i + 1] == '"') {
					builder.append('\"');
					i++;
				} else if (data[i] == '\\' && data[i + 1] == 't') {
					builder.append('\t');
					i++;
				} else {
					builder.append(data[i]);
				}
			}
			value = builder.toString();
		}
		this.value = value;

	}

	/**
	 * Return the textual representation of this element.
	 */
	@Override
	public String asText() {
		return value;
	}

}
