package hr.fer.zemris.java.custom.scripting.lexer;

/**
 * This class represents the exception for the lexer if there was an error in
 * production of tokens. This class extends the {@link RuntimeException} class.
 * 
 * @author Matej Balun
 *
 */
public class LexerException extends RuntimeException {

	/**
	 * The serial version UID for this class.
	 */
	private static final long serialVersionUID = -3418417678755472299L;

	/**
	 * The default constructor.
	 */
	public LexerException() {
		super();
	}

	/**
	 * This constructor passes the string message to the super class
	 * constructor.
	 * 
	 * @param text
	 *            - the specified message.
	 */
	public LexerException(String text) {
		super(text);
	}

}
