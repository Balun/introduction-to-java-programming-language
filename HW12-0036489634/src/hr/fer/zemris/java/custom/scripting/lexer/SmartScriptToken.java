package hr.fer.zemris.java.custom.scripting.lexer;

import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;

/**
 * This class represents a basic token in Smart Script lexer. The token can have
 * four types; word, string, symbol or a text. The type of the token is determined by
 * lexer. The token is later consumed via {@link SmartScriptParser} and
 * specified as the legal value for the parser.
 * 
 * @author Matej Balun
 *
 */
public class SmartScriptToken {

	/**
	 * The type of this token.
	 */
	private SmartScriptTokenType type;

	/**
	 * The value of this token.
	 */
	private Object value;

	/**
	 * This constructor initializes the token and specifies its type and value.
	 * 
	 * @param type
	 *            - type of the token.
	 * @param value
	 *            - value of the token.
	 */
	public SmartScriptToken(SmartScriptTokenType type, Object value) {
		this.type = type;
		this.value = value;
	}

	/**
	 * Returns the type of this token.
	 * 
	 * @return type of the token
	 */
	public SmartScriptTokenType getType() {
		return this.type;
	}

	/**
	 * Returns the value of this token.
	 * 
	 * @return value of the token
	 */
	public Object getValue() {
		return this.value;
	}
}
