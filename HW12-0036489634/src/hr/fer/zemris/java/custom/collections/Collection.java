package hr.fer.zemris.java.custom.collections;

/**
 * This class represents a custom collection. The class offers all the methods
 * needed for using a collection. In this particular class, the methods are not
 * implemented and the class is suitable for extending.
 * 
 * @author Matej Balun
 *
 */
public class Collection {

	/**
	 * A default constructor, it calls the super class Object's constructor.
	 */
	protected Collection() {
		super();
	}

	/**
	 * Checks if the specified collection is empty.
	 * 
	 * @return true if the collection is empty, otherwise false.
	 */
	public boolean isEmpty() {
		if (size() == 0) {
			return true;
		}
		return false;
	}

	/**
	 * Returns the size of the specified collection.
	 * 
	 * @return size of the collection.
	 */
	public int size() {
		return 0;
	}

	/**
	 * Adds the new object into the collection.
	 * 
	 * @param value
	 *            - the object for adding.
	 */
	public void add(Object value) {

	}

	/**
	 * Checks if there is the specified object in the collection.
	 * 
	 * @param value
	 *            - The specified object
	 * @return True if there is the specified object, otherwise false
	 */
	public boolean contains(Object value) {
		return false;
	}

	/**
	 * Removes the specified object from the collection.
	 * 
	 * @param value
	 *            - The specified object for removal.
	 * @return true if the removal was successful, false otherwise
	 */
	public boolean remove(Object value) {
		return false;
	}

	/**
	 * Returns the specified collection as an fixed size array. The size of the
	 * array is specified by the number of elements in the collection (size).
	 * 
	 * @return the array representation of the collection.
	 */
	public Object[] toArray() {
		throw new UnsupportedOperationException();
	}

	/**
	 * Executes the specified operation for each element in the collection. The
	 * execution is accomplished by the class Processor with it's method
	 * process.
	 * 
	 * @param processor
	 *            - The processor class for the specified operation.
	 */
	public void forEach(Processor processor) {

	}

	/**
	 * Adds all the elements from specified custom collection to this
	 * collection. The addition is accomplished by local extended Processor
	 * class with it's custom process method.
	 * 
	 * @param other
	 *            - The custom collection specified for addition.
	 */
	public void addAll(Collection other) {
		
		if(other==null){
			return;
		}

		class LocalProcessor extends Processor {

			@Override
			public void process(Object value) {
					add(value);
			}
		}

		LocalProcessor proc = new LocalProcessor();
		other.forEach(proc);
	}

	/**
	 * Removes all elements from this custom collection.
	 */
	public void clear() {

	}

}
