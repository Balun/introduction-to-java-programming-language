package hr.fer.zemris.java.custom.collections;

import hr.fer.zemris.java.custom.scripting.nodes.Node;

/**
 * This class implements the custom collection stack. The class offers the
 * expected user interface for a stack collection. The class internally works
 * with the custom collection ArrayIndexedCollection and its methods.
 * 
 * @author Matej Balun
 *
 */
public class ObjectStack {

	/**
	 * Internal list for the stack.
	 */
	private ArrayIndexedCollection list;

	/**
	 * The default constructor for the stack. Initiates the internal collection
	 * an sets the stack pointer to -1 (empty stack).
	 */
	public ObjectStack() {
		super();
		this.list = new ArrayIndexedCollection();
	}

	/**
	 * Checks if the stack is empty.
	 * 
	 * @return true if the stack is empty, otherwise false.
	 */
	public boolean isEmpty() {
		if (this.list.isEmpty()) {
			return true;
		}

		return false;
	}

	/**
	 * Returns the size of the stack (number of elements).
	 * 
	 * @return size of the stack.
	 */
	public int size() {
		return this.list.size();
	}

	/**
	 * Sets the specified object on top of the object stack. The object must not
	 * be a null-reference.
	 * 
	 * @param value
	 *            - the specified object
	 * @throws IllegalArgumentException
	 *             if the value is a null-reference.
	 */
	public void push(Node value) {
		if (value == null) {
			throw new IllegalArgumentException("The value must not be null-reference.");
		}

		this.list.add(value);
	}

	/**
	 * Returns and removes the element from top of the stack.
	 * 
	 * @return element on top of the stack.
	 * @throws EmptyStackException
	 *             if the stack is empty.
	 */
	public Node pop() {
		if (this.list.isEmpty()) {
			throw new EmptyStackException("The stack is empty");
		}

		Node element = (Node) this.list.get(this.size() - 1);
		this.list.remove(this.size() - 1);
		return element;
	}

	/**
	 * Returns but doesn't remove the object from top of the stack.
	 * 
	 * @return - the object from top of the stack.
	 * @throws EmptyStackException
	 *             if the stack is empty.
	 */
	public Node peek() {
		if (this.list.isEmpty()) {
			throw new EmptyStackException("The stack is empty");
		}

		return (Node) this.list.get(this.size() - 1);
	}

	/**
	 * Removes all the elements from the stack and sets the stack pointer to -1.
	 */
	public void clear() {
		this.list.clear();
	}

}
