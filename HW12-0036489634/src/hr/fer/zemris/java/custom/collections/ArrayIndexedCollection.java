package hr.fer.zemris.java.custom.collections;

/**
 * This class implements the array-like indexed collection. The collection
 * implements the unimplemented methods from the custom class Collection. The
 * operations of addition are done in O(1) time complexity, while the operations
 * of retrieval are also done in O(1) average complexity. The collection
 * consists of internal array that reallocates in need.
 * 
 * @author Matej Balun
 *
 */
public class ArrayIndexedCollection extends Collection {

	/**
	 * size of the collection.
	 */
	private int size;

	/**
	 * capacity of the collection.
	 */
	private int capacity;

	/**
	 * internal array of the collection.
	 */
	private Object[] elements;

	/**
	 * The default constructor, initializes the internal array, and sets the
	 * initial capacity to 16.
	 */
	public ArrayIndexedCollection() {
		this(null, 16);
	}

	/**
	 * This constructor passes the initialization to the default constructor and
	 * reallocates the internal array with the specified capacity.
	 * 
	 * @param initialCapacity
	 *            - The user specified-capacity of the collection
	 */
	public ArrayIndexedCollection(int initialCapacity) {
		this(null, initialCapacity);
	}

	/**
	 * This constructor initializes the collection and adds all the elements
	 * from another collection into this collection. The collection will
	 * reallocate if needed.
	 * 
	 * @param other
	 *            - the user-specified collection for insertion
	 */
	public ArrayIndexedCollection(Collection other) {
		this(other, 16);
	}

	/**
	 * This constructor passes the initialization to the simpler constructor
	 * along with the specified initial capacity, and adds all of the elements
	 * from the specified collection to this collection.
	 * 
	 * @param other
	 *            - the user-specified collection for insertion
	 * @param initialCapacity
	 *            - the user-specified inital capacity
	 */
	public ArrayIndexedCollection(Collection other, int initialCapacity) {
		this.capacity = initialCapacity;
		this.elements = new Object[initialCapacity];
		this.addAll(other);
	}

	/**
	 * Adds the elements into the collection. The Addition is accomplished in
	 * O(1) average time complexity.
	 * 
	 * @throws IllegalArgumentException
	 *             if the null-reference is sent.
	 */
	@Override
	public void add(Object element) {
		if (element.equals(null)) {
			throw new IllegalArgumentException("Null reference cannot be added as an element.");
		}

		if (size == capacity) {
			Object[] tmp = this.elements;
			this.elements = new Object[2 * capacity];
			for (int i = 0; i < size; i++) {
				this.elements[i] = tmp[i];
			}
			capacity *= 2;
		}

		this.elements[size] = element;
		size++;
	}

	/**
	 * Returns the value of the element on the specified position. The average
	 * complexity is O(1).
	 * 
	 * @param index
	 *            - index of the specified object.
	 * @return the object on the specified index.
	 * @throws IndexOutOfBoundsException
	 *             if the index is out of range.
	 */
	public Object get(int index) {
		if (index < 0 || index > (size - 1)) {
			throw new IndexOutOfBoundsException(
					"The index must be a value greater or equal to 0 and less or equal to size - 1.");
		}

		return this.elements[index];
	}

	@Override
	public void clear() {
		for (int i = 0; i < size; i++) {
			elements[i] = null;
		}
		this.size = 0;
	}

	/**
	 * Inserts the value into the specified position in the collection. The
	 * insertion along is done in O(1) time complexity, but shifting the
	 * elements to right and possible reallocation of the array makes it O(n).
	 * 
	 * @param value
	 *            - the object for insertion.
	 * @param position
	 *            - the specified position for insertion.
	 * @throws IllegalArgumentException
	 *             if the position is out of bounds.
	 */
	public void insert(Object value, int position) {
		if (position < 0 || position > (size)) {
			throw new IndexOutOfBoundsException();
		}

		add(value);
		Object[] temp = new Object[capacity];
		System.arraycopy(elements, 0, temp, 0, position);
		temp[position] = value;

		for (int i = position; i < size - 1; i++) {
			temp[i + 1] = elements[i];
		}
		elements = temp;
	}

	/**
	 * Returns the index of the specified object in the collection.
	 * 
	 * @param value
	 *            - the specified object
	 * @return - index of the element, -1 if there is no element specified in
	 *         the collection
	 */
	public int indexOf(Object value) {
		for (int i = 0; i < size; i++) {
			if (elements[i].equals(value)) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * Removes the element on the specified position from the collection. The
	 * removal is accomplished in O(n) average time complexity.
	 * 
	 * @param index
	 *            - specified index of the element for removal.
	 * @throws IndexOutOfBoundsException
	 *             if the specified index is out of range.
	 */
	public void remove(int index) {
		if (index < 0 || index > (size - 1)) {
			throw new IndexOutOfBoundsException("The index must greater or equal to 0 and less or equal to size - 1.");
		}

		for (int i = index; i < size - 1; i++) {
			elements[i] = elements[i + 1];
		}
		elements[size - 1] = null;
		size--;
	}

	@Override
	public boolean remove(Object value) {
		int index = this.indexOf(value);
		if (index >= 0) {
			this.remove(index);
			return true;
		}

		return false;
	}

	@Override
	public boolean contains(Object element) {
		for (int i = 0; i < size; i++) {
			if (elements[i].equals(element)) {
				return true;
			}
		}

		return false;
	}

	@Override
	public int size() {
		return this.size;
	}

	@Override
	public Object[] toArray() {
		Object[] array = new Object[size];
		for (int i = 0; i < size; i++) {
			array[i] = this.elements[i];
		}
		return array;
	}

	@Override
	public void forEach(Processor processor) {
		for (Object element : this.toArray()) {
			processor.process(element);
		}
	}

}
