package hr.fer.zemris.java.webserver.workers;

import java.io.IOException;
import java.util.Date;
import java.text.SimpleDateFormat;

import hr.fer.zemris.java.webserver.IWebWorker;
import hr.fer.zemris.java.webserver.RequestContext;
import hr.fer.zemris.java.webserver.SmartHttpServer;

/**
 * This class represents a simple action for the {@link SmartHttpServer}
 * program. It writes a simple hello message on the {@link RequestContext}'s
 * specified output stream on the currently specified web address in a html text
 * format.
 * 
 * @author Matej Balun
 *
 */
public class HelloWorker implements IWebWorker {

	@Override
	public void processRequest(RequestContext context) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date now = new Date();

		context.setMimeType("text/html");

		String name = context.getParameter("name");
		try {
			context.write("<html><body>");
			context.write("<h1>Hello!!!</h1>");
			context.write("<p>Now is: " + sdf.format(now) + "</p>");

			if (name == null || name.trim().isEmpty()) {
				context.write("<p>You did not send me your name!</p>");

			} else {
				context.write("<p>Your name has " + name.trim().length() + " letters.</p>");
			}

			context.write("</body></html>");
		} catch (IOException ex) {
			// Log exception to servers log...
			ex.printStackTrace();
		}
	}

}
