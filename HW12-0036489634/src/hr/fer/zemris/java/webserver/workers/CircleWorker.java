package hr.fer.zemris.java.webserver.workers;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;

import hr.fer.zemris.java.webserver.IWebWorker;
import hr.fer.zemris.java.webserver.RequestContext;
import hr.fer.zemris.java.webserver.SmartHttpServer;

/**
 * This class represents the simple action for {@link SmartHttpServer} program.
 * It draws the circle of random colour on the output stream of the
 * {@link RequestContext}, which is displayed on the actual web address
 * specified by the {@link RequestContext}
 * 
 * @author Matej Balun
 *
 */
public class CircleWorker implements IWebWorker {

	@Override
	public void processRequest(RequestContext context) {
		context.setMimeType("image/png");
		BufferedImage bim = new BufferedImage(200, 200, BufferedImage.TYPE_3BYTE_BGR);

		Graphics2D graphics2d = bim.createGraphics();

		Random rnd = new Random();
		graphics2d.setColor(new Color(rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256)));
		graphics2d.fillOval(0, 0, bim.getWidth(), bim.getHeight());

		graphics2d.dispose();

		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			ImageIO.write(bim, "png", bos);
			context.write(bos.toByteArray());

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
