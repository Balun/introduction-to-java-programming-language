package hr.fer.zemris.java.webserver.workers;

import java.io.IOException;
import java.util.Map.Entry;

import hr.fer.zemris.java.webserver.IWebWorker;
import hr.fer.zemris.java.webserver.RequestContext;
import hr.fer.zemris.java.webserver.SmartHttpServer;

/**
 * This class represents a simple action for the {@link SmartHttpServer}
 * program. It writes the parameters of the {@link RequestContext} in a
 * html-based table on the currently specified web address in the
 * {@link RequestContext}.
 * 
 * @author Matej Balun
 *
 */
public class EchoParams implements IWebWorker {

	@Override
	public void processRequest(RequestContext context) {
		StringBuilder builder = new StringBuilder(
				"<html>\r\n" + "   <body>\r\n" + "    <h1>Parameters</h1>\r\n\r\n" + "     <table border='1'>\r\n"
						+ "       <tr>\r\n" + "         <th>  Name  </th>\r\n" + "         <th>  Value  </th>\r\n");

		for (Entry<String, String> entry : context.getParameters().entrySet()) {
			builder.append("       <tr>\r\n" + "         <td>  " + entry.getKey() + "  </td>\r\n" + "         <td>"
					+ entry.getValue() + "</td>\r\n" + "       </tr>");
		}

		builder.append("     </table>\r\n" + "   </body>\r\n" + "</html>\r\n");

		try {
			context.write(builder.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
