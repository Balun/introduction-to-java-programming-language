package hr.fer.zemris.java.webserver;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * This class represents the request and content of the request from the client
 * to the simple server implementation {@link SmartHttpServer}. The request
 * consist of a generated header (generated only once) ad a various parameters
 * and possible cookies for the specification of the data retrieval. As
 * mentioned, header is generated only once, when the first writing to output
 * stream is initiated. After the header is generated, the user is not allowed
 * to change encoding, mimeType, status code and status text (the attempt throws
 * a {@link RuntimeException}). The writing on the output stream is achieved via
 * UTF-8 encoding by default, and the default mime type is set to "text/html".
 * 
 * @author Matej Balun
 *
 */
public class RequestContext {

	/**
	 * The {@link OutputStream} for data transaction.
	 */
	private OutputStream outputStream;

	/**
	 * The default {@link Character} for data encoding.
	 */
	private Charset charset;

	/**
	 * The {@link String} representation of the encoding charset.
	 */
	private String encoding;

	/**
	 * Status code of the request.
	 */
	private int statusCode;

	/**
	 * The statues text of the request.
	 */
	private String statusText;

	/**
	 * The mime type of the request.
	 */
	private String mimeType;

	/**
	 * The parameters of the request.
	 */
	private Map<String, String> parameters;

	/**
	 * The temporary parameters of the request.
	 */
	private Map<String, String> temporaryParameters;

	/**
	 * The persistent parameters of the request.
	 */
	private Map<String, String> persistentParameters;

	/**
	 * The list of request's cookies.
	 */
	private List<RCCookie> outputCookies;

	/**
	 * Indicator of the header state.
	 */
	private boolean headerGenerated;

	/**
	 * This constructor initialises the {@link RequestContext} with the
	 * dedicated {@link OutputStream} for writing content (non-null), and
	 * various parameters for the request (can be null).
	 * 
	 * @param outputStream
	 *            {@link OutputStream} for data transferring
	 * @param parameters
	 *            {@link Map} of the dedicated parameters
	 * @param persistentParameters
	 *            {@link Map} of the persistent parameters
	 * @param outputCookies
	 *            list of the request's cookies
	 */
	public RequestContext(OutputStream outputStream, Map<String, String> parameters,
			Map<String, String> persistentParameters, List<RCCookie> outputCookies) {

		if (outputStream == null) {
			throw new IllegalArgumentException("");
		}
		this.outputStream = outputStream;

		if (parameters == null) {
			this.parameters = Collections.unmodifiableMap(new HashMap<String, String>());
		} else {
			this.parameters = Collections.unmodifiableMap(parameters);
		}

		if (persistentParameters == null) {
			this.persistentParameters = new HashMap<>();
		} else {
			this.persistentParameters = persistentParameters;
		}

		if (outputCookies == null) {
			this.outputCookies = new LinkedList<>();
		} else {
			this.outputCookies = outputCookies;
		}

		this.encoding = "UTF-8";
		this.statusCode = 200;
		this.statusText = "OK";
		this.mimeType = "text/html";
		this.temporaryParameters = new HashMap<>();
		this.headerGenerated = false;
	}

	/**
	 * Returns the list of the request's cookies
	 * 
	 * @return list of {@link RCCookie}s.
	 */
	public List<RCCookie> getOutputCookies() {
		return outputCookies;
	}

	/**
	 * Returns the specified {@link OutputStream}.
	 * 
	 * @return the specified {@link OutputStream}.
	 */
	public OutputStream getOutputStream() {
		return outputStream;
	}

	/**
	 * Sets the specified {@link OutputStream} of the request
	 * 
	 * @param outputStream
	 *            the specified {@link OutputStream}
	 */
	public void setOutputStream(OutputStream outputStream) {
		this.outputStream = outputStream;
	}

	/**
	 * Returns the currently defined charset for the data encoding during
	 * transfer.
	 * 
	 * @return currently defined {@link Character}
	 */
	public Charset getCharset() {
		return charset;
	}

	/**
	 * Sets the charset tot the specified value.
	 * 
	 * @param charset
	 *            the specified {@link Charset}
	 */
	public void setCharset(Charset charset) {
		this.charset = charset;
	}

	/**
	 * Sets the current encoding ot the specified value
	 * 
	 * @param encoding
	 *            the new specified encoding
	 */
	public void setEncoding(String encoding) {
		if (this.headerGenerated) {
			throw new RuntimeException("The header has already been generated.");
		}

		this.encoding = encoding;
	}

	/**
	 * Sets the status code of the {@link RequestContext}.
	 * 
	 * @param statusCode
	 *            the new specified status code.
	 */
	public void setStatusCode(int statusCode) {
		if (this.headerGenerated) {
			throw new RuntimeException("The header has already been generated.");
		}

		this.statusCode = statusCode;
	}

	/**
	 * Sets he status text for the {@link RequestContext}.
	 * 
	 * @param statusText
	 *            the new specified status text
	 */
	public void setStatusText(String statusText) {
		if (this.headerGenerated) {
			throw new RuntimeException("The header has already been generated.");
		}

		this.statusText = statusText;
	}

	/**
	 * Sets the current mime type of the request
	 * 
	 * @param mimeType
	 *            the new specified mime type
	 */
	public void setMimeType(String mimeType) {
		if (this.headerGenerated) {
			throw new RuntimeException("The header has already been generated.");
		}

		this.mimeType = mimeType;
	}

	/**
	 * Sets the map of temporary parameters
	 * 
	 * @param temporaryParameters
	 *            the temporary parameters
	 */
	public void setTemporaryParameters(Map<String, String> temporaryParameters) {
		this.temporaryParameters = temporaryParameters;
	}

	/**
	 * Sets the {@link Map} of persistent parameters.
	 * 
	 * @param persistentParameters
	 *            map of persistent parameters
	 */
	public void setPersistentParameters(Map<String, String> persistentParameters) {
		this.persistentParameters = persistentParameters;
	}

	/**
	 * Sets the list of {@link RCCookie} for the request
	 * 
	 * @param outputCookies
	 *            list of {@link RCCookie}
	 */
	public void setOutputCookies(List<RCCookie> outputCookies) {
		if (this.headerGenerated) {
			throw new RuntimeException("The header has already been generated.");
		}

		this.outputCookies = outputCookies;
	}

	/**
	 * Sets the value of the header generated value.
	 * 
	 * @param headerGenerated
	 *            the value of header generated indicator
	 */
	public void setHeaderGenerated(boolean headerGenerated) {
		this.headerGenerated = headerGenerated;
	}

	/**
	 * Returns the request's parameters {@link Map}.
	 * 
	 * @return the parameters {@link Map}
	 */
	public Map<String, String> getParameters() {
		return parameters;
	}

	/**
	 * Returns the map of temporary parameters of the request
	 * 
	 * @return the temporary parameters {@link Map}
	 */
	public Map<String, String> getTemporaryParameters() {
		return temporaryParameters;
	}

	/**
	 * Returns the persistent parameters {@link Map}
	 * 
	 * @return the persistent parameters map
	 */
	public Map<String, String> getPersistentParameters() {
		return persistentParameters;
	}

	/**
	 * Returns the specified parameter of from the parameters map
	 * 
	 * @param name
	 *            the parameter name
	 * @return the specified parameter
	 */
	public String getParameter(String name) {
		return this.parameters.get(name);
	}

	/**
	 * Returns the {@link Set} of the parameter keys.
	 * 
	 * @return the {@link Set} of parameter names
	 */
	public Set<String> getParameterNames() {
		return Collections.unmodifiableSet(this.parameters.keySet());
	}

	/**
	 * Returns the specified persistent parameter from the map
	 * 
	 * @param name
	 *            name of the parameter
	 * @return the specified persistent parameter
	 */
	public String getPersistentParameter(String name) {
		return this.persistentParameters.get(name);
	}

	/**
	 * Returns the {@link Set} of persistent parameter names
	 * 
	 * @return the persistent parameter names
	 */
	public Set<String> getPersistentParameterNames() {
		return Collections.unmodifiableSet(this.persistentParameters.keySet());
	}

	/**
	 * Adds or sets the new persistent parameter in the {@link Map}
	 * 
	 * @param name
	 *            specified name of the parameter
	 * @param value
	 *            specified value of the paramter
	 */
	public void setPersistentParameter(String name, String value) {
		this.persistentParameters.put(name, value);
	}

	/**
	 * Removes the specified persistent parameter from the {@link Map}.
	 * 
	 * @param name
	 *            name of the parameter for removal
	 */
	public void removePersistentParameter(String name) {
		this.persistentParameters.remove(name);
	}

	/**
	 * Return the specified temporary parameter.
	 * 
	 * @param name
	 *            name of the specified parameter
	 * @return the specified parameter
	 */
	public String getTemporaryParameter(String name) {
		return this.temporaryParameters.get(name);
	}

	/**
	 * Returns the {@link Set} of the temporary parameter names.
	 * 
	 * @return the temporary parameter names
	 */
	public Set<String> getTemporaryParameterNames() {
		return Collections.unmodifiableSet(this.temporaryParameters.keySet());
	}

	/**
	 * Sets the specified temporary parameter to the specified value.
	 * 
	 * @param name
	 *            name of the parameter
	 * @param value
	 *            value of the parameter
	 */
	public void setTemporaryParameter(String name, String value) {
		this.temporaryParameters.put(name, value);
	}

	/**
	 * Removes the specified temporary parameter from the collection
	 * 
	 * @param name
	 *            name of the specified parameter
	 */
	public void removeTemporaryParameter(String name) {
		this.temporaryParameters.remove(name);
	}

	/**
	 * Writes the content given as argument to the currently specified
	 * {@link OutputStream} using the currently specified {@link Charset} for
	 * encoding. When this method is called from the first time, then and only
	 * then the header is being generated for the protocol. After the header is
	 * generated any further modifications to the mime type, status code, status
	 * text and encoding are disabled (the methods throw
	 * {@link RuntimeException}).
	 * 
	 * @param data
	 *            the data to write on the {@link OutputStream} for the server.
	 * @return this {@link RequestContext}
	 * @throws IOException
	 *             if there was an error in writing
	 */
	public RequestContext write(byte[] data) throws IOException {
		if (!this.headerGenerated) {
			charset = Charset.forName(encoding);

			byte[] header = generateHeader();
			this.outputStream.write(header);
			this.headerGenerated = true;
		}

		this.outputStream.write(data);
		this.outputStream.flush();
		return this;
	}

	/**
	 * Generates the header based on the current header attributes
	 * 
	 * @return the generated header
	 */
	private byte[] generateHeader() {
		StringBuilder builder = new StringBuilder();

		builder.append("HTTP/1.1 " + this.statusCode + " " + statusText + "\r\n");

		if (this.mimeType.startsWith("text/")) {
			builder.append("Content-Type: " + mimeType + "; charset=" + encoding + "\r\n");
		} else {
			builder.append("Content-Type: " + mimeType + "\r\n");
		}

		if (!this.outputCookies.isEmpty()) {
			for (RCCookie cookie : this.outputCookies) {
				appendCookie(cookie, builder);
			}
		}

		builder.append("\r\n");
		return builder.toString().getBytes(StandardCharsets.ISO_8859_1);

	}

	/**
	 * Helper method for appending {@link RCCookie} value to the header output
	 * 
	 * @param cookie
	 *            the specified {@link RCCookie} to append
	 * @param builder
	 *            the specified {@link StringBuilder}
	 */
	private void appendCookie(RCCookie cookie, StringBuilder builder) {
		builder.append("Set-Cookie: " + cookie.name + "=" + cookie.value);

		if (cookie.domain != null) {
			builder.append("; Domain=" + cookie.domain);
		}

		if (cookie.path != null) {
			builder.append("; Path=" + cookie.path);
		}

		if (cookie.maxAge != null) {
			builder.append("; Max-Age=" + cookie.maxAge);
		}

		builder.append("\r\n");
	}

	/**
	 * This method takes a {@link String} as argument and passes its byte value
	 * to the default write method with the byte array as argument.
	 * 
	 * @param text
	 *            the specified string to write on the {@link OutputStream}
	 * @return this {@link RequestContext}
	 * @throws IOException
	 *             if there was an error in writing
	 */
	public RequestContext write(String text) throws IOException {
		return write(text.getBytes(encoding));
	}

	/**
	 * Adds he specified {@link RCCookie} to the list of cookies.
	 * 
	 * @param cookie
	 *            he specified {@link RCCookie}
	 */
	public void addRCCookie(RCCookie cookie) {
		if (cookie == null) {
			throw new IllegalArgumentException("The cookie must not be a null-reference");
		}

		this.outputCookies.add(cookie);
	}

	/**
	 * This class represents the cookie data for the {@link RequestContext} and
	 * client-server data manipulation. It has the necessary parameters to
	 * determine cookie evaluation.
	 * 
	 * @author Matej Balun
	 *
	 */
	public static class RCCookie {

		/**
		 * Name f the cookie.
		 */
		private String name;

		/**
		 * Value of the cookie.
		 */
		private String value;

		/**
		 * Domain of the service.
		 */
		private String domain;

		/**
		 * Path of the web root directory.
		 */
		private String path;

		/**
		 * The max age.
		 */
		private Integer maxAge;

		/**
		 * Initialises The {@link RCCookie} with its specified parameters.
		 * 
		 * @param name
		 *            Name of the cookie
		 * @param value
		 *            value of the cookie
		 * @param maxAge
		 *            max age of the cookie
		 * @param domain
		 *            domain of the we service
		 * @param path
		 *            path of the web root directory
		 */
		public RCCookie(String name, String value, Integer maxAge, String domain, String path) {
			if (name == null) {
				throw new IllegalArgumentException("The name cannot be a null-reference.");

			} else if (value == null) {
				throw new IllegalArgumentException("The value acnnot be a null-reference.");
			}

			this.name = name;
			this.value = value;
			this.domain = domain;
			this.path = path;
			this.maxAge = maxAge;
		}

		/**
		 * Returns the name of the cookie.
		 * 
		 * @return name of the cookie
		 */
		public String getName() {
			return name;
		}

		/**
		 * Returns the value of the cookie.
		 * 
		 * @return value of the cookie
		 */
		public String getValue() {
			return value;
		}

		/**
		 * Returns the domain of the we service.
		 * 
		 * @return domain of the web service
		 */
		public String getDomain() {
			return domain;
		}

		/**
		 * Return the path of the web root directory.
		 * 
		 * @return path of the web root directory
		 */
		public String getPath() {
			return path;
		}

		/**
		 * Returns the max age of the cookie
		 * 
		 * @return max age of the cookie
		 */
		public int getMaxAge() {
			return maxAge;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((domain == null) ? 0 : domain.hashCode());
			result = prime * result + ((maxAge == null) ? 0 : maxAge.hashCode());
			result = prime * result + ((name == null) ? 0 : name.hashCode());
			result = prime * result + ((path == null) ? 0 : path.hashCode());
			result = prime * result + ((value == null) ? 0 : value.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			RCCookie other = (RCCookie) obj;
			if (domain == null) {
				if (other.domain != null)
					return false;
			} else if (!domain.equals(other.domain))
				return false;
			if (maxAge == null) {
				if (other.maxAge != null)
					return false;
			} else if (!maxAge.equals(other.maxAge))
				return false;
			if (name == null) {
				if (other.name != null)
					return false;
			} else if (!name.equals(other.name))
				return false;
			if (path == null) {
				if (other.path != null)
					return false;
			} else if (!path.equals(other.path))
				return false;
			if (value == null) {
				if (other.value != null)
					return false;
			} else if (!value.equals(other.value))
				return false;
			return true;
		}

	}

}
