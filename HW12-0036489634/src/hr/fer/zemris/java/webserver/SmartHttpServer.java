package hr.fer.zemris.java.webserver;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PushbackInputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import hr.fer.zemris.java.custom.scripting.exec.SmartScriptEngine;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;
import hr.fer.zemris.java.webserver.RequestContext.RCCookie;

/**
 * This program represents a simple HTTP-protocol-based server demonstration.
 * When the {@link SmartHttpServer}'s method start is called, the new server
 * thread is initiated, along with the specified number of worker threads
 * created by the {@link ExecutorService}. The server waits for requests, reads
 * the nature and the specifications of the requests along with their cookies
 * and all the relevant data to determine the type and content of the response.
 * The communication between the clients and the server is managed by
 * {@link RequestContext} class, which offers methods for writing and
 * manipulation of the request's parameters and cookies.
 * 
 * @author Matej Balun
 *
 */
public class SmartHttpServer {

	/**
	 * The web address.
	 */
	private String address;

	/**
	 * The specified server port for connection.
	 */
	private int port;

	/**
	 * Specified number of worker thread.
	 */
	private int workerThreads;

	/**
	 * maximum time of the session.
	 */
	private int sessionTimeout;

	/**
	 * Map of mime types.
	 */
	private Map<String, String> mimeTypes;

	/**
	 * The main {@link SmartHttpServer} thread.
	 */
	private ServerThread serverThread;

	/**
	 * Executor service for worker threads.
	 */
	private ExecutorService threadPool;

	/**
	 * Root directory of the pecified files.
	 */
	private Path documentRoot;

	/**
	 * Map of the specified {@link IWebWorker}s
	 */
	private Map<String, IWebWorker> workersMap;

	/**
	 * Map of the {@link SessionMapEntry} values in the currently active
	 * session.
	 */
	private Map<String, SessionMapEntry> sessions;

	/**
	 * The random generator.
	 */
	private Random sessionRandom;

	/**
	 * The default package for workers class.
	 */
	private static final String WORKER_PACKAGE = "hr.fer.zemris.java.webserver.workers.";

	/**
	 * This constructor initialises the {@link SmartHttpServer} with the
	 * specified path to the configuration file, where all the basic settings of
	 * the server are written. It also load the needed parameter using
	 * configuration files.
	 * 
	 * @param configFileName
	 *            path to the configuration file.
	 */
	public SmartHttpServer(String configFileName) {
		Objects.requireNonNull(configFileName, "The config file name must not be a null-reference.");

		Properties serverProp = new Properties();
		Properties mimeProp = new Properties();
		Properties workersProp = new Properties();

		try {
			serverProp.load(Files.newInputStream(Paths.get(configFileName).toAbsolutePath()));
			mimeProp.load(Files.newInputStream(Paths.get(serverProp.getProperty("server.mimeConfig"))));
			workersProp.load(Files.newInputStream(Paths.get(serverProp.getProperty("server.workers"))));
		} catch (IOException e) {
			e.printStackTrace();
		}

		initServer(serverProp, mimeProp, workersProp);
	}

	/**
	 * Helper method for the constructor. It initiates a thread which awakens
	 * every 5 minutes and checks whether the cookies and other contet are upa
	 * to date.
	 * 
	 * @param serverProp
	 *            path for the server properties file
	 * @param mimeProp
	 *            path for the mime properties
	 * @param workersProp
	 *            map for the workers properties
	 */
	private void initServer(Properties serverProp, Properties mimeProp, Properties workersProp) {

		this.address = serverProp.getProperty("server.address");
		this.port = Integer.parseInt(serverProp.getProperty("server.port"));
		this.workerThreads = Integer.parseInt(serverProp.getProperty("server.workerThreads"));
		this.sessionTimeout = Integer.parseInt(serverProp.getProperty("session.timeout"));
		this.documentRoot = Paths.get(serverProp.getProperty("server.documentRoot")).toAbsolutePath();
		this.sessions = new HashMap<>();
		this.sessionRandom = new Random();

		this.mimeTypes = new HashMap<>();
		for (Object key : mimeProp.keySet()) {
			mimeTypes.put(key.toString(), mimeProp.getProperty(key.toString()));
		}

		this.workersMap = new HashMap<>();
		for (Object key : workersProp.keySet()) {
			String fqcn = workersProp.getProperty(key.toString());
			try {
				Class<?> referenceToClass = this.getClass().getClassLoader().loadClass(fqcn);
				Object newObject = referenceToClass.newInstance();
				IWebWorker worker = (IWebWorker) newObject;
				workersMap.put(key.toString(), worker);

			} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}

		Thread mapCheckThread = new Thread(() -> {
			while (true) {
				Iterator<String> mapIterator = sessions.keySet().iterator();
				while (mapIterator.hasNext()) {
					String key = mapIterator.next();
					if ((System.currentTimeMillis() / 1000 - sessions.get(key).validUntil) > 0) {
						mapIterator.remove();
					}
				}
				try {
					Thread.sleep(300000);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		mapCheckThread.start();
	}

	/**
	 * This methods starts the {@link SmartHttpServer} with initialising a new
	 * {@link Thread} for server manipulation, as well as the
	 * {@link ExecutorService} thread pool for faster response manipulation.
	 * 
	 */
	protected synchronized void start() {
		if (this.serverThread == null) {
			serverThread = new ServerThread();
			serverThread.start();
		} else if (!serverThread.isAlive()) {
			serverThread.start();
		}

		this.threadPool = Executors.newFixedThreadPool(workerThreads);
	}

	/**
	 * Stops the work of the {@link SmartHttpServer}.
	 */
	protected synchronized void stop() {
		serverThread.stopThread();
	}

	/**
	 * This class represents a thread for manipulating the basic
	 * {@link SmartHttpServer} functionalities. I extends the {@link Thread}
	 * class, with implementing the run method in infinite while loop. The
	 * thread must be stopped from outside
	 * 
	 * @author Matej Balun
	 *
	 */
	protected class ServerThread extends Thread {

		/**
		 * The indicator for the end of the thread.
		 */
		private volatile boolean flag;

		@Override
		public void run() {
			this.flag = true;

			try (ServerSocket socket = new ServerSocket()) {
				socket.bind(new InetSocketAddress(address, port));

				while (this.flag) {
					Socket client = socket.accept();
					ClientWorker worker = new ClientWorker(client);
					threadPool.submit(worker);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		/**
		 * Sets the indicator to false.
		 */
		public void stopThread() {
			this.flag = false;
		}

	}

	/**
	 * Generates the random serial ID consisting of 20 random capital letter.
	 * 
	 * @return The random generated serial ID.
	 */
	private String generateSID() {
		int down = 65;
		int up = 91;
		char[] sid = new char[20];

		for (int i = 0; i < 20; i++) {
			sid[i] = (char) (sessionRandom.nextInt(up - down) + down);
		}

		return new String(sid);
	}

	/**
	 * This class represents a single entry in the session {@link Map}. It holds
	 * the information about the serial ID, life time, and a map of parameters
	 * 
	 * @author Matej Balun
	 *
	 */
	private static class SessionMapEntry {

		/**
		 * The serial ID.
		 */
		private String sid;

		/**
		 * THe timeout value
		 */
		private long validUntil;

		/**
		 * THe parameter map
		 */
		private Map<String, String> map;

		/**
		 * InitIalises the {@link SessionMapEntry} with its specified parameters
		 * 
		 * @param sid
		 *            the serial ID
		 * @param timeout
		 *            the life time
		 */
		public SessionMapEntry(String sid, long timeout) {
			this.sid = sid;
			this.validUntil = System.currentTimeMillis() / 1000 + timeout;
			this.map = new ConcurrentHashMap<>();
		}

		/**
		 * Returns the serial ID.
		 * 
		 * @return the serial ID
		 */
		@SuppressWarnings("unused")
		public String getSid() {
			return sid;
		}

		/**
		 * Return the timeout value.
		 * 
		 * @return the timeout value
		 */
		public long getValidUntil() {
			return validUntil;
		}

		/**
		 * Return the parameter {@link Map}
		 * 
		 * @return the parameter map
		 */
		public Map<String, String> getMap() {
			return map;
		}

		/**
		 * Sets the new timeout value
		 * 
		 * @param time
		 *            the new timeout value
		 */
		public void setValidUntil(long time) {
			this.validUntil = time;
		}
	}

	/**
	 * This class represents a worker for the single client in the
	 * {@link SmartHttpServer} program. It implements the {@link Runnable}
	 * interface, offering the basic thread functionalities
	 * 
	 * @author Matej Balun
	 *
	 */
	private class ClientWorker implements Runnable {

		/**
		 * The specified socket.
		 */
		private Socket csocket;

		/**
		 * THe input stream
		 */
		private PushbackInputStream istream;

		/**
		 * The specified output stream.
		 */
		private OutputStream ostream;

		/**
		 * The version.
		 */
		@SuppressWarnings("unused")
		private String version;

		/**
		 * THe method.
		 */
		@SuppressWarnings("unused")
		private String method;

		/**
		 * parameters for the {@link RequestContext}
		 */
		private Map<String, String> params;

		/**
		 * {@link Map} of the persistent parameters
		 */
		private Map<String, String> perParams;

		/**
		 * generated headers
		 *
		 */
		private List<String> headers;

		/**
		 * {@link List} of the {@link RCCookie}s.
		 */
		private List<RCCookie> outputCookies = new ArrayList<>();

		/**
		 * The serial ID of the request.
		 */
		@SuppressWarnings("unused")
		private String SID;

		/**
		 * Initialises the {@link ClientWorker} with its specified socket.
		 * 
		 * @param csocket
		 *            the specified socket
		 */
		public ClientWorker(Socket csocket) {
			super();
			this.csocket = csocket;
			this.params = new HashMap<>();
		}

		@Override
		public void run() {
			try {
				this.istream = new PushbackInputStream(csocket.getInputStream());
				this.ostream = new BufferedOutputStream(csocket.getOutputStream());
				headers = readRequest();

				String[] firstLine = headers.isEmpty() ? null : headers.get(0).split(" ");
				if (firstLine == null || firstLine.length != 3) {
					sendError(ostream, 400, "Bad request");
					return;
				}

				String method = firstLine[0].toUpperCase();
				if (!method.equals("GET")) {
					sendError(ostream, 400, "Method Not Allowed");
					return;
				}

				String version = firstLine[2].toUpperCase();
				if (!version.equals("HTTP/1.1")) {
					sendError(ostream, 400, "HTTP Version Not Supported");
					return;
				}

				String requestPath = firstLine[1];
				String path = requestPath;
				String paramString = null;

				if (requestPath.contains("?")) {
					path = requestPath.split("\\?")[0];
					paramString = requestPath.split("\\?")[1];
				}

				checkSession(headers);

				parseParameters(paramString);

				if (workersMap.containsKey(path)) {
					workersMap.get(path).processRequest(new RequestContext(ostream, params, perParams, outputCookies));
					ostream.flush();
					csocket.close();
					return;
				}

				path = path.substring(1);

				if (path.startsWith("ext")) {
					serveExt(path);
					ostream.flush();
					csocket.close();
					return;
				}

				requestPath = documentRoot.resolve(path).toString();
				if (!requestPath.startsWith(documentRoot.toAbsolutePath().toString())) {
					sendError(ostream, 403, "forbidden");
				}

				String extension = null;
				if (Files.exists(Paths.get(requestPath)) && Files.isReadable(Paths.get(requestPath))) {
					extension = requestPath.substring(requestPath.lastIndexOf(".") + 1, requestPath.length());
				} else {
					sendError(ostream, 404, "The requested file does not exsist or its not readable.");
					return;
				}

				String mimeType = mimeTypes.get(extension);
				if (extension.equals("smscr")) {
					executeSmartScript(requestPath);
				} else {
					executeRegularFiles(requestPath, mimeType);
				}

				ostream.flush();
				csocket.close();

			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		/**
		 * Executes the external, non-configuration type of requests.
		 * 
		 * @param path
		 *            path of the directory with content
		 */
		private void serveExt(String path) {
			String fqcn = WORKER_PACKAGE + path.substring(path.lastIndexOf("/") + 1, path.length());
			try {
				Class<?> referenceToClass = this.getClass().getClassLoader().loadClass(fqcn);
				Object newObject = referenceToClass.newInstance();
				IWebWorker worker = (IWebWorker) newObject;
				worker.processRequest(new RequestContext(ostream, params, perParams, outputCookies));
			} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}

		/**
		 * Validates the current session with its cookies and serial ID-s.
		 * 
		 * @param headers
		 *            the header of the request.
		 */
		private synchronized void checkSession(List<String> headers) {
			String sidCandidate = null;

			for (String line : headers) {

				if (line.startsWith("Cookie")) {
					line = line.substring(line.indexOf(":") + 1).trim();
					if (line.startsWith("sid")) {
						sidCandidate = line.split("=")[1].replaceAll("\"", "");
					}
				}
			}

			if (sidCandidate == null || !sessions.containsKey(sidCandidate)) {
				perParams = addSidCookie().getMap();

			} else {
				SessionMapEntry entry = sessions.get(sidCandidate);
				if ((System.currentTimeMillis() / 1000 - entry.getValidUntil()) > 0) {
					entry = addSidCookie();

				} else {
					entry.setValidUntil(System.currentTimeMillis() / 1000 + sessionTimeout);
				}

				perParams = entry.getMap();
			}
		}

		/**
		 * Adds a new {@link RCCookie} to the cookie map, along with the new,
		 * random generated {@link SessionMapEntry} in the session map.
		 * 
		 * @return the new instance of {@link SessionMapEntry}
		 */
		private synchronized SessionMapEntry addSidCookie() {
			String SID = generateSID();
			SessionMapEntry entry = new SessionMapEntry(SID, sessionTimeout);
			sessions.put(SID, entry);
			String hostAddres = null;

			for (String line : headers) {
				if (line.startsWith("Host")) {
					hostAddres = line.split(" ")[1].split(":")[0].trim();
				}
			}

			if (hostAddres == null) {
				hostAddres = address;
			}

			outputCookies.add(new RCCookie("sid", SID, null, hostAddres, "/"));
			return entry;
		}

		/**
		 * Executes the Smart-script types of requests with the default
		 * {@link SmartScriptEngine} and {@link SmartScriptParser} based on the
		 * scripts on the server.
		 * 
		 * @param requestPath
		 *            path to the script
		 * @throws IOException
		 *             if there was an error in writing
		 */
		private void executeSmartScript(String requestPath) throws IOException {
			RequestContext request = new RequestContext(ostream, params, perParams, outputCookies);
			new SmartScriptEngine(
					new SmartScriptParser(new String(Files.readAllBytes(Paths.get(requestPath).toAbsolutePath())))
							.getDocumentNode(),
					request).execute();
		}

		/**
		 * Executes the regular data requests.
		 * 
		 * @param requestPath
		 *            path of the data
		 * @param mimeType
		 *            the specified mime type
		 * @throws IOException
		 *             if there was an error in writing
		 */
		private void executeRegularFiles(String requestPath, String mimeType) throws IOException {
			if (mimeType == null) {
				mimeType = "application/octet-stream";
			}

			RequestContext request = new RequestContext(ostream, params, perParams, outputCookies);
			request.setMimeType(mimeType);
			request.setStatusCode(200);

			request.write(Files.readAllBytes(Paths.get(requestPath).toAbsolutePath()));
		}

		/**
		 * Parses the parameters given by a single String
		 * 
		 * @param paramString
		 *            the parameter String
		 */
		private void parseParameters(String paramString) {
			if (paramString == null) {
				return;
			}

			for (String pair : paramString.split("&")) {
				params.put(pair.split("=")[0], pair.split("=")[1]);
			}
		}

		/**
		 * Reads the upcoming requests from the specified {@link InputStream}.
		 * 
		 * @return The generated request
		 * @throws IOException
		 *             if there was an error in reading/writing
		 */
		private List<String> readRequest() throws IOException {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();

			int state = 0;
			l: while (true) {
				int b = istream.read();

				if (b == -1)
					return null;
				if (b != 13) {
					bos.write(b);
				}

				switch (state) {

				case 0:
					if (b == 13) {
						state = 1;
					} else if (b == 10)
						state = 4;
					break;
				case 1:
					if (b == 10) {
						state = 2;
					} else
						state = 0;
					break;
				case 2:
					if (b == 13) {
						state = 3;
					} else
						state = 0;
					break;
				case 3:
					if (b == 10) {
						break l;
					} else
						state = 0;
					break;
				case 4:
					if (b == 10) {
						break l;
					} else
						state = 0;
					break;
				}
			}

			byte[] request = bos.toByteArray();
			if (request == null) {
				sendError(ostream, 400, "Bad request");
				return null;
			}
			String requestStr = new String(request, StandardCharsets.US_ASCII);

			return extractHeaders(requestStr);
		}

		/**
		 * Sends the error message with the specified code an the error content.
		 * 
		 * @param cos
		 *            the specified {@link OutputStream}
		 * @param statusCode
		 *            the error status code
		 * @param statusText
		 *            the error text
		 * @throws IOException
		 *             if there was an error in writing
		 */
		private void sendError(OutputStream cos, int statusCode, String statusText) throws IOException {

			cos.write(("Error " + statusCode + " " + statusText + "\r\n" + "Server: simple java server\r\n"
					+ "Content-Type: text/plain;charset=UTF-8\r\n" + "Content-Length: 0\r\n" + "Connection: close\r\n"
					+ "\r\n").getBytes(StandardCharsets.US_ASCII));

			cos.flush();
			csocket.close();
		}

		/**
		 * Extracts the headers from the pending request
		 * 
		 * @param requestHeader
		 *            the request
		 * @return the header in {@link ListIterator}
		 */
		private List<String> extractHeaders(String requestHeader) {
			List<String> headers = new ArrayList<>();

			String currentLine = null;
			for (String s : requestHeader.split("\n")) {
				if (s.isEmpty())
					break;

				char c = s.charAt(0);
				if (c == 9 || c == 32) { // tab ili space
					currentLine += s;
				} else {
					if (currentLine != null) {
						headers.add(currentLine);
					}
					currentLine = s;
				}
			}

			if (!currentLine.isEmpty()) {
				headers.add(currentLine);
			}

			return headers;
		}
	}

	/**
	 * The main method that executes the program.
	 * 
	 * @param args
	 *            the path to the "server.properties" file
	 */
	public static void main(String[] args) {
		if (args.length != 1) {
			System.err.println("Invalid number of arguments provided");
			System.exit(-1);
		}

		if (!Files.exists(Paths.get(args[0].trim()))) {
			System.err.println("The specified document does not exist.");
			System.exit(-1);
		}

		SmartHttpServer server = new SmartHttpServer(args[0].trim());
		server.start();

		Scanner sc = new Scanner(System.in);
		if (sc.nextLine().equals("x")) {
			System.out.println("Server terminated");
			server.stop();
			sc.close();
			System.exit(0);
		}
	}
}
