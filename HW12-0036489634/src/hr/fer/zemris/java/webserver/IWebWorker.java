package hr.fer.zemris.java.webserver;

/**
 * This interface represents a simple action for the {@link SmartHttpServer}
 * program. The action writes its product on the specified
 * {@link RequestContext}'s output stream. CLasses that implement this interface
 * must Specify concrete actions for server-client cimmunication.
 * 
 * @author Matej Balun
 *
 */
public interface IWebWorker {

	/**
	 * This method executes the specified action for the {@link SmartHttpServer}
	 * and writes its result on the {@link RequestContext}'s specified output
	 * stream.
	 * 
	 * @param context
	 *            the specified {@link RequestContext}
	 */
	public void processRequest(RequestContext context);

}
