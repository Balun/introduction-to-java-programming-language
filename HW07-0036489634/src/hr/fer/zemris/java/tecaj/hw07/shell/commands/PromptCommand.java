package hr.fer.zemris.java.tecaj.hw07.shell.commands;

import hr.fer.zemris.java.tecaj.hw07.shell.MyShell;
import hr.fer.zemris.java.tecaj.hw07.shell.ShellCommand;
import hr.fer.zemris.java.tecaj.hw07.shell.ShellStatus;
import hr.fer.zemris.java.tecaj.hw07.shell.SymbolUtility;
import hr.fer.zemris.jmbag0036489634.cmdapps.trazilica.shell.Environment;

/**
 * This class represents the "PROMPT" {@link ShellCommand} for {@link MyShell}.
 * It displays the current {@link Environment}'s symbol for prompt if started
 * with no arguments. If started with single symbol argument, it changes the
 * current prompt symbol of the {@link Environment} .
 * 
 * @author Matej Balun
 *
 */
public class PromptCommand extends AbstractCommand {

	/**
	 * Name of the command.
	 */
	private static final String NAME = "prompt";

	/**
	 * First line of the description.
	 */
	private static final String DESCRIPTION_1 = "This command changes the symbol for prompt.";

	/**
	 * Second line of the description.
	 */
	private static final String DESCRIPTION_2 = "If the command is started without arguments,"
			+ " it displayes the current prompt symbol";

	/**
	 * Initialises the {@link PromptCommand}, passing its name and description
	 * to the super class constructor {@link AbstractCommand}.
	 */
	public PromptCommand() {
		super(NAME, DESCRIPTION_1, DESCRIPTION_2);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ShellStatus execute(Environment environment, String args) {
		return SymbolUtility.changeOrDisplaySymbol(environment, environment.getPromptSymbol(), args, NAME);
	}

}
