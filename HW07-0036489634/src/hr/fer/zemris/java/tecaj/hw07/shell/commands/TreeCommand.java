package hr.fer.zemris.java.tecaj.hw07.shell.commands;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;

import hr.fer.zemris.java.tecaj.hw07.shell.MyShell;
import hr.fer.zemris.java.tecaj.hw07.shell.ShellCommand;
import hr.fer.zemris.java.tecaj.hw07.shell.ShellStatus;
import hr.fer.zemris.jmbag0036489634.cmdapps.trazilica.shell.Environment;

/**
 * This class represents the "TREE" {@link ShellCommand} of the {@link MyShell}.
 * It displays the complete directory tree of the specified directory given as
 * argument. The class inherits the {@link AbstractCommand} class, implementing
 * the {@link ShellCommand} method execute().
 * 
 * @author Matej Balun
 *
 */
public class TreeCommand extends AbstractCommand {

	/**
	 * Name of the command.
	 */
	private static final String NAME = "tree";

	/**
	 * Description of the command.
	 */
	private static final String DESCRIPTION = "This command prints a file tree from the specified directory.";

	/**
	 * Initialises the {@link TreeCommand}, passing its name and description to
	 * the super class constructor {@link AbstractCommand}.
	 */
	public TreeCommand() {
		super(NAME, DESCRIPTION);
	}

	/**
	 * The custom implementation of the {@link FileVisitor} interface. This
	 * visitor visits all files in the specified directory tree, writing them on
	 * the {@link MyShell} with appropriate file/directory hierarchy.
	 * 
	 * @author Matej Balun
	 *
	 */
	private static class MyFileVisitor implements FileVisitor<Path> {

		/**
		 * Current level in the directory tree.
		 */
		private int level;

		/**
		 * The specified {@link MyShell} {@link Environment}.
		 */
		private Environment environment;

		/**
		 * initialises the {@link MyFileVisitor}, with the specified
		 * {@link MyFileVisitor} {@link Environment}.
		 * 
		 * @param environment
		 */
		public MyFileVisitor(Environment environment) {
			this.environment = environment;
		}

		/**
		 * Prints the path of the current file or directory in a formatted way.
		 * 
		 * @param file
		 *            the current file.
		 */
		private void print(Path file) {
			if (level == 0) {
				environment.writeln(file.normalize().toAbsolutePath().toString());
			} else {
				environment.write(String.format("%" + (2 * level) + "s%s%n", "", file.getFileName()));
			}
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
			level--;
			return FileVisitResult.CONTINUE;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
			print(dir);
			level++;
			return FileVisitResult.CONTINUE;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
			print(file);
			return FileVisitResult.CONTINUE;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
			return FileVisitResult.CONTINUE;
		}

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ShellStatus execute(Environment environment, String args) {
		Path directory = Paths.get(args.trim().replace("\"", "")).toAbsolutePath();

		if (!Files.exists(directory) || !Files.isDirectory(directory)) {
			environment.writeln("The specified path doesn't exsist or it's not a directory");
			return ShellStatus.CONTINUE;
		}

		try {
			Files.walkFileTree(Paths.get("."), new MyFileVisitor(environment));
		} catch (IOException e) {
			environment.writeln(e.getMessage());
		}

		return ShellStatus.CONTINUE;
	}

}
