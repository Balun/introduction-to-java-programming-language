package hr.fer.zemris.java.tecaj.hw07.crypto;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Scanner;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * This class represents a simple encryption/decryption cipher-like operator.
 * Based on the specified mode of the {@link CipherOperator}, the class encrypts
 * the specified file to the specified destination, or decrypts it. Internally
 * The class uses the {@link Cipher} class.
 * 
 * @author Matej Balun
 *
 */
public class CipherOperator extends CryptoOperator {

	/**
	 * The output buffer for writing files.
	 */
	private byte[] outputBuffer;

	/**
	 * The current mode of the {@link Cipher}.
	 */
	private boolean encryptMode;

	/**
	 * The source file path.
	 */
	private String sourcePath;

	/**
	 * The destination file path.
	 */
	private String outputPath;

	/**
	 * The internally used {@link Cipher} object.
	 */
	private Cipher cipher;

	/**
	 * Initialises the {@link CipherOperator}, with the specified encryption
	 * mode, source file path and destination file path.
	 * 
	 * @param encryptMode
	 *            true for the encrypt mode, otherwise false
	 * @param sourcePath
	 *            the source file path
	 * @param outputPath
	 *            the destination file path
	 */
	public CipherOperator(boolean encryptMode, String sourcePath, String outputPath) {
		super();

		if (sourcePath == null || outputPath == null) {
			throw new IllegalArgumentException("The source path or output path must not be a null-reference");
		}

		this.encryptMode = encryptMode;
		this.sourcePath = sourcePath;
		this.outputPath = outputPath;
	}

	/**
	 * {@inheritDoc} This method starts the encryption/decryption of the
	 * specified source file.
	 */
	@Override
	public void execute() {

		try {
			encryptOrDecrypt();

		} catch (NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException
				| BadPaddingException e) {
			throw new CryptoException("Error in encrypting/decrypting file");
		}
	}

	/**
	 * This method initialises the parameters for encryption/decryption, the
	 * initialisation vector and the password and delegates the encryption
	 * further.
	 */
	private void encryptOrDecrypt()
			throws NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {

		if (!Files.exists(Paths.get(sourcePath).toAbsolutePath())) {
			System.err.println("The specified source file does not exsist");
			System.exit(-1);
		}

		Scanner scanner = new Scanner(System.in);
		String keyText = provideKey(scanner);
		String ivText = provideIVText(scanner);
		scanner.close();

		SecretKeySpec keySpec = new SecretKeySpec(hexToByte(keyText), "AES");
		AlgorithmParameterSpec paramSpec = new IvParameterSpec(hexToByte(ivText));
		cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");

		try {
			cipher.init(encryptMode ? Cipher.ENCRYPT_MODE : Cipher.DECRYPT_MODE, keySpec, paramSpec);

		} catch (InvalidKeyException | InvalidAlgorithmParameterException e) {
			System.err.println("Invalid key specified");
		}

		readEncryptAndWrite();

		if (encryptMode) {
			System.out
					.println("Encryption completed. Generated file " + outputPath + " based on the file " + sourcePath);

		} else {
			System.out
					.println("Decryption completed. Generated file " + outputPath + " based on the file " + sourcePath);
		}

	}

	/**
	 * Reads from the specified source path, constructing a strategy to ensure
	 * that the content is encrypted/decrypted every time the specified amount
	 * of bytes (buffer size) is read. The method crypts the content in steps,
	 * updating the cipher, writing to the destination file in these steps and
	 * producing the final output after all bytes are read.
	 */
	private void readEncryptAndWrite() {

		if (Files.exists(Paths.get(outputPath))) {
			Paths.get(outputPath).toFile().delete();
		}

		try (OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(outputPath))) {

			readAndApply(Paths.get(sourcePath), (offset, length) -> {

				try {

					if (length < BUFFER_SIZE) {
						byte[] trimBuffer = new byte[length];
						System.arraycopy(inputBuffer, 0, trimBuffer, 0, length);
						outputBuffer = crypt(trimBuffer, false);
					} else {
						outputBuffer = crypt(inputBuffer, true);
					}

					outputStream.write(outputBuffer);
				} catch (Exception e) {
					System.err.println("Error in crypting/decrypting and writing file.");
				}
			});

		} catch (IOException e) {
			System.err.println("Error in crypting/decrypting and writing file.");
		}
	}

	/**
	 * Crypts the specified byte array, and updates the cipher, depending on the
	 * current position in the source file.
	 * 
	 * @param currentBuffer
	 *            the specified bytes to crypt
	 * @param hasNext
	 *            true if the cipher has not reached the end of file, otherwise
	 *            false
	 * @return the crypted byte array
	 */
	private byte[] crypt(byte[] currentBuffer, boolean hasNext) throws IllegalBlockSizeException, BadPaddingException {

		if (hasNext) {
			return cipher.update(currentBuffer);
		} else {
			return cipher.doFinal(currentBuffer);
		}
	}

	/**
	 * Provides the initialisation vector for the {@link Cipher} from the user.
	 * 
	 * @param scanner
	 *            the scanner for user's input
	 * @return the initialisation vector
	 */
	private String provideIVText(Scanner scanner) {

		System.out.print("Please provide initialization vector as hex-encoded text (32 hex-digits):\n> ");
		return scanner.nextLine();
	}

	/**
	 * Provides the key for the {@link Cipher} from the user.
	 * 
	 * @param scanner
	 *            the {@link Scanner} for user's input
	 * @return the key for the {@link Cipher}
	 */
	private String provideKey(Scanner scanner) {

		System.out.print("Please provide password as hex-encoded text (16 bytes, i.e. 32 hex-digits):\n> ");
		return scanner.nextLine();
	}

}
