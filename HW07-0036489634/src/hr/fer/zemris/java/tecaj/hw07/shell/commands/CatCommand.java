package hr.fer.zemris.java.tecaj.hw07.shell.commands;

import java.io.IOException;

import java.nio.charset.Charset;
import java.nio.charset.IllegalCharsetNameException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import hr.fer.zemris.java.tecaj.hw07.shell.MyShell;
import hr.fer.zemris.java.tecaj.hw07.shell.ShellStatus;
import hr.fer.zemris.java.tecaj.hw07.shell.Environment;

/**
 * This class describes the "CAT" command of the {@link MyShell}. The command
 * takes one or two arguments. First argument is the path to some existing file.
 * Second argument is optional and it is a charset for reading the file. The
 * command displays the content of the file on the {@link MyShell} based on the
 * optionally specified charset (UTF-8 is default).
 * 
 * @author Matej Balun
 *
 */
public class CatCommand extends AbstractCommand {

	/**
	 * Name of the command.
	 */
	private static final String NAME = "cat";

	/**
	 * First line of the description.
	 */
	private static final String DESCRIPTION_1 = "This command takes one or two arguments."
			+ " The first argument is path to some file and is mandatory.";

	/**
	 * Second line of the description.
	 */
	private static final String DESCRIPTION_2 = "The second argument is charset name that should be used to"
			+ " interpret chars from bytes (if not provided, charset is default).";

	/**
	 * Third line of the description.
	 */
	private static final String DESCRIPTION_3 = "This command opens the given file and writes its content to the console.";

	/**
	 * Initialises the {@link CatCommand}, passing its name and description to
	 * the super class {@link AbstractCommand} constructor.
	 */
	public CatCommand() {
		super(NAME, DESCRIPTION_1, DESCRIPTION_2, DESCRIPTION_3);

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ShellStatus execute(Environment environment, String args) {
		String[] arguments = args.split(" ");

		switch (arguments.length) {

		case 1:
			try {
				List<String> lines = Files.readAllLines(Paths.get(arguments[0].trim().replace("\"", "")));
				lines.forEach(l -> environment.writeln(l));

			} catch (IOException e) {
				environment.writeln("Error in reading specified file");
			}
			break;

		case 2:
			try {
				List<String> lines = Files.readAllLines(Paths.get(arguments[0].trim().replace("\"", "")),
						Charset.forName(arguments[1].trim()));
				lines.forEach(l -> environment.writeln(l));

			} catch (IOException | IllegalCharsetNameException e) {
				environment.writeln("Error in reading specified file, or unknown charset specified");
			}
			break;

		default:
			environment.writeln("Invalid number of arguments for the \"cat\" command");
			break;
		}

		return ShellStatus.CONTINUE;
	}

}
