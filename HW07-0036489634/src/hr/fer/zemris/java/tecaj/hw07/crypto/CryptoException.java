package hr.fer.zemris.java.tecaj.hw07.crypto;

public class CryptoException extends RuntimeException {

	/**
	 * The serial version UID for this {@link CryptoException}.
	 */
	private static final long serialVersionUID = 3953268443430823195L;

	/**
	 * The default constructor.
	 */
	public CryptoException() {
		super();
	}

	/**
	 * This constructor initialise the {@link CryptoException}, passing its
	 * message to the super constructor {@link RuntimeException}.
	 * 
	 * @param message
	 *            the specified message
	 */
	public CryptoException(String message) {
		super(message);
	}

}
