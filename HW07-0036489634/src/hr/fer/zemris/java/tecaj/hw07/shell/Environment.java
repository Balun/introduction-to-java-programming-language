package hr.fer.zemris.java.tecaj.hw07.shell;

/**
 * This interface describes the environment of the {@link MyShell} shell
 * application. It specifies methods for communication between the user and the
 * shell commands. All operations regarding the content of the shell are
 * executed via this methods.
 * 
 * @author Matej Balun
 *
 */
public interface Environment {

	/**
	 * Reads the user's input from the {@link MyShell}.
	 * 
	 * @return The users input from the {@link MyShell}.
	 */
	public String readLine();

	/**
	 * Writes the specified text on the {@link MyShell}, without the line
	 * separator.
	 * 
	 * @param text
	 *            to be written on the {@link MyShell}
	 */
	public void write(String text);

	/**
	 * Writes the specified text on the {@link MyShell}, with the line
	 * separator.
	 * 
	 * @param text
	 *            to be written on the {@link MyShell}
	 */
	public void writeln(String text);

	/**
	 * Returns the {@link Iterable} collection of all {@link ShellCommand}s.
	 * 
	 * @return the collection of {@link ShellCommand}s.
	 */
	public Iterable<ShellCommand> commands();

	/**
	 * Returns the current multi-line symbol.
	 * 
	 * @return the current multi-line symbol.
	 */
	public Character getMultilineSymbol();

	/**
	 * Sets the current value for the multi-line symbol.
	 * 
	 * @param symbol
	 *            specified value for the multi-line symbol
	 */
	public void setMultilineSymbol(Character symbol);

	/**
	 * Returns the current prompt symbol.
	 * 
	 * @return the current prompt symbol
	 */
	public Character getPromptSymbol();

	/**
	 * Sets the current value for the prompt symbol.
	 * 
	 * @param symbol
	 *            specified value for the prompt symbol
	 */
	public void setPromptSymbol(Character symbol);

	/**
	 * Returns the current symbol for more lines.
	 * 
	 * @return the current symbol for more lines
	 */
	public Character getMoreLinesSymbol();

	/**
	 * Sets the current value for the more lines symbol.
	 * 
	 * @param symbol
	 *            specified value for the more lines symbol
	 */
	public void setMoreLinesSymbol(Character symbol);

}
