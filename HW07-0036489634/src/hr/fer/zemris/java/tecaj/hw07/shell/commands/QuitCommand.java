package hr.fer.zemris.java.tecaj.hw07.shell.commands;

import hr.fer.zemris.java.tecaj.hw07.shell.MyShell;
import hr.fer.zemris.java.tecaj.hw07.shell.ShellCommand;
import hr.fer.zemris.java.tecaj.hw07.shell.ShellStatus;
import hr.fer.zemris.jmbag0036489634.cmdapps.trazilica.shell.Environment;

/**
 * This class represents the "EXIT" {@link ShellCommand} of the {@link MyShell}.
 * When called, the command does nothing but returns the {@link ShellStatus}
 * with the TERMINATE constant, after which the {@link MyShell} is terminated.
 * It inherits the {@link AbstractCommand} class, implementing the
 * {@link ShellCommand} method execute().
 * 
 * @author Matej Balun
 *
 */
public class QuitCommand extends AbstractCommand {

	/**
	 * Name of the command.
	 */
	private static final String NAME = "exit";

	/**
	 * Description of the command.
	 */
	private static final String DESCRIPTION = "This command exits the shell and terminates the program.";

	/**
	 * Initialises the {@link QuitCommand}, passing its name and description to
	 * the super class constructor {@link AbstractCommand}.
	 */
	public QuitCommand() {
		super(NAME, DESCRIPTION);

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ShellStatus execute(Environment environment, String args) {

		return ShellStatus.TERMINATE;
	}

}
