package hr.fer.zemris.java.tecaj.hw07.crypto;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 * This program demonstrates the work of a simple digester/encryption/decryption
 * application. The program takes two or the arguments, depending on the wanted
 * functionality. The first argument is the name of the mode for the program.
 * The modes are: "checksha" - Checks the SHA-256 digest of the specified file;
 * "encrypt" - encrypts the specified file to the specified destination path;
 * "decrypt" - decrypts the specified file to the specified destination path.
 * The application is memory stable, it reads the appropriate amount of bytes in
 * the specified time and encrypts/decrypts them.
 * 
 * @author Matej Balun
 *
 */
public class Crypto {

	/**
	 * The "encrypt" keyword.
	 */
	private static final String ENCRYPT = "encrypt";

	/**
	 * The "decrypt" keyword
	 */
	private static final String DECRYPT = "decrypt";

	/**
	 * The "checksha" keyword
	 */
	private static final String CHECKSHA = "checksha";

	/**
	 * The main method that executes the program.
	 */
	public static void main(String[] args)
			throws NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {

		switch (args.length) {

		case 2:
			if (!args[0].trim().equalsIgnoreCase(CHECKSHA)) {
				System.err.println("Invalid command.");
				System.exit(-1);
			}

			String source = args[1].trim();

			if (Files.exists(Paths.get(source)) && !Files.isDirectory(Paths.get(source))) {
				CryptoOperator digestCheck = new DigestCheckOperator(source);
				digestCheck.execute();

			} else {
				System.err.println("The specified file does not exsist");
				System.exit(-1);
			}
			break;

		case 3:
			if (args[0].trim().equalsIgnoreCase(ENCRYPT)) {
				CryptoOperator encryptOperator = new CipherOperator(true, args[1], args[2]);
				encryptOperator.execute();

			} else if (args[0].trim().equalsIgnoreCase(DECRYPT)) {
				CryptoOperator decryptOperator = new CipherOperator(false, args[1], args[2]);
				decryptOperator.execute();

			} else {
				System.err.println("Invalid command.");
				System.exit(-1);
			}
			break;

		default:
			System.err.println("Invalid number of arguments provided.");
			System.exit(-1);
			break;
		}
	}
}
