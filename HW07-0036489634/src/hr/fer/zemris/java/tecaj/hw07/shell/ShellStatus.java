package hr.fer.zemris.java.tecaj.hw07.shell;

import hr.fer.zemris.java.tecaj.hw07.shell.commands.QuitCommand;

/**
 * This enumeration describes the state of the {@link MyShell} after the
 * execution of the last command. The shell can continue its work, or end if the
 * {@link QuitCommand} has executed.
 * 
 * @author Matej Balun
 *
 */
public enum ShellStatus {

	/**
	 * The state that continues the work of the {@link MyShell}.
	 */
	CONTINUE,

	/**
	 * The state that terminates the work of the {@link MyShell}.
	 */
	TERMINATE;

}
