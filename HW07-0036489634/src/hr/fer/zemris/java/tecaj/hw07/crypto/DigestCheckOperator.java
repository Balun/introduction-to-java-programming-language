package hr.fer.zemris.java.tecaj.hw07.crypto;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

/**
 * This class represents the simple digest checker object. The digest algorithm
 * used is SHA-256. The class internally uses the {@link MessageDigest} object
 * for digesting. The class inherits the {@link CryptoOperator} class,
 * implementing the execute() method.
 * 
 * @author Matej Balun
 *
 */
public class DigestCheckOperator extends CryptoOperator {

	/**
	 * The source file path for digesting.
	 */
	private Path source;

	/**
	 * The internally used {@link MessageDigest} object with SHA-256 algorithm.
	 */
	private MessageDigest algorithm;

	/**
	 * Initialises the {@link DigestCheckOperator}, with the source file path
	 * and the new instance of {@link MessageDigest} object, set to the SHA-256
	 * digest algorithm.
	 * 
	 * @param source
	 *            the source file path to digest
	 */
	public DigestCheckOperator(String source) {
		if (source == null) {
			throw new IllegalArgumentException("The source path must not be a null-reference");
		}

		this.source = Paths.get(source);

		try {
			this.algorithm = MessageDigest.getInstance("SHA-256");

		} catch (NoSuchAlgorithmException e) {
			throw new CryptoException(e.getMessage());
		}
	}

	/**
	 * {@inheritDoc} This method starts the digestion process.
	 */
	@Override
	public void execute() {
		checkDigest();
	}

	/**
	 * Reads from the specified source file, updating the digester with the
	 * fixed amount of bytes every iteration, until it reaches the end of file.
	 */
	public void checkDigest() {

		readAndApply(source, (offset, length) -> algorithm.update(inputBuffer, offset, length));
		scanAndCompare();
	}

	/**
	 * Prompts the user to enter the expected SHA-256 digest, comparing it with
	 * the calculated digest of the specified file. It informs the user if the
	 * expected and calculated digests match or not.
	 */
	private void scanAndCompare() {
		Scanner scanner = new Scanner(System.in);

		System.out.println("Please provide expected sha-256 digest for " + source.getFileName());
		System.out.print("> ");
		String expected = scanner.nextLine().trim();
		scanner.close();

		byte[] digest = algorithm.digest();

		if (MessageDigest.isEqual(digest, hexToByte(expected))) {
			System.out.println("Digesting completed. Digest of " + source.getFileName() + " matches expected digest.");

		} else {
			System.out.println("Digesting completed. Digest of " + source.getFileName()
					+ " does not match the expected digest. Digest\nwas: " + byteToHex(digest));
		}

	}

}
