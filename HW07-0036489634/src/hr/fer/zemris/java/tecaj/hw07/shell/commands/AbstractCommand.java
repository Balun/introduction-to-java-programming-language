package hr.fer.zemris.java.tecaj.hw07.shell.commands;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import hr.fer.zemris.java.tecaj.hw07.shell.MyShell;
import hr.fer.zemris.java.tecaj.hw07.shell.ShellCommand;

/**
 * This class describes the abstract {@link MyShell} command. It implements the
 * {@link ShellCommand} interface with the getter methods for commands. Classes
 * that inherit this class must implement the {@link ShellCommand}'s method
 * execute().
 * 
 * @author Matej Balun
 *
 */
public abstract class AbstractCommand implements ShellCommand {

	/**
	 * Name of the command.
	 */
	private final String commandName;

	/**
	 * Lines with description of the command.
	 */
	private final List<String> commandDescription;

	/**
	 * Initialises the {@link AbstractCommand}, with values for name and short
	 * description of the command.
	 * 
	 * @param commandName
	 * @param commandDescription
	 */
	public AbstractCommand(String commandName, String... commandDescription) {

		this.commandName = commandName;
		this.commandDescription = new ArrayList<>(Arrays.asList(commandDescription));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getCommandName() {
		return commandName;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getCommandDescription() {
		return commandDescription;
	}

}
