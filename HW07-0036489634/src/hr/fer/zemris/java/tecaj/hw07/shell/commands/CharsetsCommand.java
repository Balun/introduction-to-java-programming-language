package hr.fer.zemris.java.tecaj.hw07.shell.commands;

import java.nio.charset.Charset;

import hr.fer.zemris.java.tecaj.hw07.shell.MyShell;
import hr.fer.zemris.java.tecaj.hw07.shell.ShellCommand;
import hr.fer.zemris.java.tecaj.hw07.shell.ShellStatus;
import hr.fer.zemris.java.tecaj.hw07.shell.Environment;

/**
 * This class describes the "CHARSETS" command of the {@link MyShell}. The
 * command lists all available charsets for the current present Java platform on
 * this system. The class inherits the {@link AbstractCommand}, implmenting the
 * execute command of the {@link ShellCommand} interface.
 * 
 * @author Matej Balun
 *
 */
public class CharsetsCommand extends AbstractCommand {

	/**
	 * Description of the command.
	 */
	private static final String DESCRIPTION = "This command lists the names of supported chartses for this Java platform";

	/**
	 * Name of the command.
	 */
	private static final String NAME = "charsets";

	/**
	 * Initialises the {@link CharsetsCommand}, passing its name and description
	 * to the super class constructor {@link AbstractCommand}.
	 */
	public CharsetsCommand() {
		super(NAME, DESCRIPTION);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ShellStatus execute(Environment environment, String args) {
		Charset.availableCharsets().keySet().forEach(c -> environment.writeln(c));
		return ShellStatus.CONTINUE;
	}

}
