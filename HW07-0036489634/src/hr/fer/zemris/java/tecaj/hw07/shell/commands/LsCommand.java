package hr.fer.zemris.java.tecaj.hw07.shell.commands;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import hr.fer.zemris.java.tecaj.hw07.shell.MyShell;
import hr.fer.zemris.java.tecaj.hw07.shell.ShellCommand;
import hr.fer.zemris.java.tecaj.hw07.shell.ShellStatus;
import hr.fer.zemris.jmbag0036489634.cmdapps.trazilica.shell.Environment;

/**
 * This class represents the "LS" {@link ShellCommand} of the {@link MyShell}.
 * It takes a valid directory as argument and lists the content of the
 * directory, displaying the basic file attributes of the content files and
 * directories. This class inherits the {@link AbstractCommand} class,
 * implementing the {@link ShellCommand}'s execute() method.
 * 
 * @author Matej Balun
 *
 */
public class LsCommand extends AbstractCommand {

	/**
	 * Name of the command.
	 */
	private static final String NAME = "ls";

	/**
	 * Description of the command.
	 */
	private static final String DESCRIPTION = "Writes a directory listing of the specified directory,"
			+ " with displayed file attributes.";

	/**
	 * The directory symbol.
	 */
	private static final char DIRECTORY = 'd';

	/**
	 * The readable symbol.
	 */
	private static final char READABLE = 'r';

	/**
	 * The writable symbol.
	 */
	private static final char WRITEABLE = 'w';

	/**
	 * The executable symbol.
	 */
	private static final char EXECUTABLE = 'x';

	/**
	 * The none of the above symbol.
	 */
	private static final char NONE = '-';

	/**
	 * Initialises the {@link HelpCommand}, passing the name and description to
	 * the super class constructor {@link AbstractCommand}.
	 */
	public LsCommand() {
		super(NAME, DESCRIPTION);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ShellStatus execute(Environment environment, String args) {
		Path directory = Paths.get(args.trim().replace("\"", "")).toAbsolutePath();

		if (!Files.exists(directory) || !Files.isDirectory(directory)) {
			environment.writeln("The specified path does not exsist or it's not a directory.");
			return ShellStatus.CONTINUE;
		}

		try {
			List<Path> elements = Files.list(directory).collect(Collectors.toList());
			elements.forEach(f -> environment.writeln(displayFileAttributes(f)));

		} catch (IOException e) {
			environment.writeln(e.getMessage());
		}

		return ShellStatus.CONTINUE;
	}

	/**
	 * Displays the basic file attributes of the file with specified path on the
	 * {@link MyShell}.
	 * 
	 * @param file
	 *            the path of the specified file or directory
	 * @return {@link String} containing the basic file attributes
	 */
	private String displayFileAttributes(Path file) {
		StringBuilder builder = new StringBuilder();

		try {
			appendAttributes(builder, file);
			builder.append(String.format("%10s ", Long.toString(Files.size(file))));
			appendDateTime(builder, file);
			builder.append(file.getFileName());
			return builder.toString();

		} catch (IOException e) {
			return e.getMessage();
		}
	}

	/**
	 * Appends the time and date of the file creation to the basic file
	 * attributes.
	 * 
	 * @param builder
	 *            {@link StringBuilder} for the attribute {@link String}
	 * @param file
	 *            the specified file path
	 * @throws IOException
	 *             if there was an error in reading time and date of creation
	 */
	private void appendDateTime(StringBuilder builder, Path file) throws IOException {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		BasicFileAttributeView faView = Files.getFileAttributeView(file.toAbsolutePath(), BasicFileAttributeView.class,
				LinkOption.NOFOLLOW_LINKS);

		BasicFileAttributes attributes = faView.readAttributes();
		FileTime fileTime = attributes.creationTime();
		String formattedDateTime = sdf.format(new Date(fileTime.toMillis()));
		builder.append(formattedDateTime);
		builder.append(" ");

	}

	/**
	 * Appends the readable, writable, executable and is directory attributes to
	 * the basic file attributes {@link String}.
	 * 
	 * @param builder
	 *            {@link StringBuilder} fo the attribute {@link String}
	 * @param file
	 *            the specified file path
	 */
	private void appendAttributes(StringBuilder builder, Path file) {
		if (Files.isDirectory(file)) {
			builder.append(DIRECTORY);
		} else {
			builder.append(NONE);
		}

		if (Files.isReadable(file)) {
			builder.append(READABLE);
		} else {
			builder.append(NONE);
		}

		if (Files.isWritable(file)) {
			builder.append(WRITEABLE);
		} else {
			builder.append(NONE);
		}

		if (Files.isExecutable(file)) {
			builder.append(EXECUTABLE);
		} else {
			builder.append(NONE);
		}

		builder.append(" ");
	}

}
