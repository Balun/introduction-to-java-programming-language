package hr.fer.zemris.java.tecaj.hw07.shell.commands;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import hr.fer.zemris.java.tecaj.hw07.shell.MyShell;
import hr.fer.zemris.java.tecaj.hw07.shell.ShellCommand;
import hr.fer.zemris.java.tecaj.hw07.shell.ShellStatus;
import hr.fer.zemris.jmbag0036489634.cmdapps.trazilica.shell.Environment;

/**
 * This class describes the "HEXDUMP" {@link ShellCommand} of the
 * {@link MyShell}. The command takes one argument, the path to an existing
 * file. It prints the hexdump format content of the specified file. It inherits
 * the {@link AbstractCommand} class, implementing the {@link ShellCommand}'s
 * method execute().
 * 
 * @author Matej Balun
 *
 */
public class HexDumpCommand extends AbstractCommand {

	/**
	 * Name of the command.
	 */
	private static final String NAME = "hexdump";

	/**
	 * Size of the buffer for reading bytes.
	 */
	private static final int BUFFER_SIZE = 16;

	/**
	 * Description of the command.
	 */
	private static final String DESCRIPTION = "This command displays the hex output of the specified file.";

	/**
	 * Initialises the {@link HexDumpCommand}, passing its name and description
	 * to the super class's constructor {@link AbstractCommand}.
	 */
	public HexDumpCommand() {
		super(NAME, DESCRIPTION);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ShellStatus execute(Environment env, String args) {

		Path file = Paths.get(args.replace("\"", "").trim()).toAbsolutePath();
		if (Files.isRegularFile(file)) {
			processHexdump(env, file);
		} else {
			env.writeln("Given path is directory.");
		}

		return ShellStatus.CONTINUE;
	}

	/**
	 * Processes the file, reads the blocks of bytes and creates the hexdump
	 * format content.
	 * 
	 * @param environment
	 *            the specified {@link MyShell} environment
	 * @param file
	 *            the specified file to be processed
	 */
	private void processHexdump(Environment environment, Path file) {
		try (BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file.toFile()))) {
			int offset = 0;
			byte[] buffer = new byte[BUFFER_SIZE];

			while (true) {
				int bytesRead = bis.read(buffer);
				if (bytesRead < 1)
					break;
				environment.write(String.format("%08X: ", offset & 0xFFFFFFFF));
				offset += bytesRead;

				printHex(environment, buffer, bytesRead);
				printText(environment, buffer, bytesRead);
			}
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}

	/**
	 * Prints the textual content of the fie to the hexdump format, Replacing
	 * all the non-standard characters with '.' symbol.
	 * 
	 * @param environment
	 *            the specified {@link MyShell} environment
	 * @param buffer
	 *            the byte array to be printed
	 * @param length
	 *            length of the line
	 */
	private void printText(Environment environment, byte[] buffer, int length) {

		for (int i = 0; i < length; i++) {
			int byteValue = (int) buffer[i];
			byte[] bytes = new byte[] { buffer[i] };
			boolean standard = byteValue >= 32 && byteValue <= 127;
			environment.write(standard ? new String(bytes) : ".");
		}
		environment.writeln("");
	}

	/**
	 * Prints the hexadecimal index value of the first byte in the upcoming line
	 * in the file to the hexdump format.
	 * 
	 * @param environment
	 *            the specified {@link MyShell} environment
	 * @param buffer
	 *            the byte array to be printed
	 * @param length
	 *            the length of the line
	 */
	private void printHex(Environment environment, byte[] buffer, int length) {
		printHexLine(environment, buffer, length);
		if (length < BUFFER_SIZE) {
			fillLine(environment, length);
		}
		environment.write("| ");
	}

	/**
	 * Prints the hexadecimal content of the current line in the file to the
	 * hexdump format.
	 * 
	 * @param environment
	 *            the specified {@link MyShell} environment
	 * @param buffer
	 *            the byte array to be printed
	 * @param length
	 *            the length of the line
	 */
	private void printHexLine(Environment environment, byte[] buffer, int length) {
		for (int i = 1; i <= length; i++) {
			environment.write(String.format("%02X ", buffer[i - 1]));
			if (i == BUFFER_SIZE / 2) {
				environment.write("| ");
			}
		}
	}

	/**
	 * Fills the current line in the hexdump format with the specified symbol
	 * and blank spaces.
	 * 
	 * @param environment
	 *            the specified {@link MyShell} environment
	 * @param length
	 *            length of the current line
	 */
	private void fillLine(Environment environment, int length) {
		int half = BUFFER_SIZE / 2;
		int spaces = 0;

		if (length <= half) {
			spaces = (half - length) * 3;
			int emptyHalfSpaces = half * 3 + 1;
			if (spaces > 0) {
				environment.write(String.format("%" + spaces + "s|%" + emptyHalfSpaces + "s", "", ""));
			} else {
				environment.write(String.format("|%" + emptyHalfSpaces + "s", ""));
			}
		} else {
			spaces = (BUFFER_SIZE - length) * 3;
			environment.write(String.format("%" + spaces + "s", ""));
		}
	}

}
