package hr.fer.zemris.java.tecaj.hw07.shell;

/**
 * This utility class offers methods for displaying the current {@link MyShell}
 * symbols or changing their values.
 * 
 * @author Matej Balun
 *
 */
public class SymbolUtility {

	/**
	 * This method displays or changes the specified current {@link MyShell}
	 * symbol based on the arguments. If there are no arguments, the method
	 * displays the symbol, if the argument is a symbol it changes the
	 * {@link MyShell} symbol. Otherwise the method prints an error on the
	 * shell.
	 * 
	 * @param environment
	 *            the specified shell environment.
	 * @param oldSymbol
	 *            the old symbol of the shell
	 * @param newSymbol
	 *            the optional new symbol
	 * @param commandName
	 *            name of the {@link ShellCommand} for symbol manipulation
	 * @return {@link ShellStatus} to continue the {@link MyShell} program
	 */
	public static ShellStatus changeOrDisplaySymbol(Environment environment, Character oldSymbol, String newSymbol,
			String commandName) {
		if (newSymbol.trim().isEmpty()) {
			displaySymbol(commandName, environment);

		} else if (newSymbol.trim().length() == 1) {
			changeSymbol(environment, newSymbol, commandName, oldSymbol);

		} else {
			environment.writeln("Invalid symbol for " + commandName.toUpperCase() + " command");
		}

		return ShellStatus.CONTINUE;
	}

	/**
	 * This method displays the specified {@link MyShell} on the shell.
	 * 
	 * @param commandName
	 *            the {@link ShellCommand} for the symbol manipulation
	 * @param environment
	 *            the specified {@link MyShell} environment.
	 */
	private static void displaySymbol(String commandName, Environment environment) {
		switch (commandName) {

		case "multiline":
			environment.writeln(
					"Symbol for " + commandName.toUpperCase() + " is " + "'" + environment.getMultilineSymbol() + "'");
			break;

		case "morelines":
			environment.writeln(
					"Symbol for " + commandName.toUpperCase() + " is " + "'" + environment.getMoreLinesSymbol() + "'");
			break;

		case "prompt":
			environment.writeln(
					"Symbol for " + commandName.toUpperCase() + " is " + "'" + environment.getPromptSymbol() + "'");
			break;

		default:
			break;
		}
	}

	/**
	 * This method changes the specified {@link MyShell} symbol if the symbol is
	 * a valid character.
	 * 
	 * @param environment
	 *            the specified {@link MyShell} environment
	 * @param newSymbol
	 *            the specified new symbol
	 * @param commandName
	 *            the {@link ShellCommand} for the symbol manipulation
	 * @param oldSymbol
	 *            the old symbol of the {@link MyShell}
	 */
	private static void changeSymbol(Environment environment, String newSymbol, String commandName,
			Character oldSymbol) {
		switch (commandName) {

		case "multiline":
			environment.setMultilineSymbol(newSymbol.charAt(0));
			break;

		case "morelines":
			environment.setMoreLinesSymbol(newSymbol.charAt(0));
			break;

		case "prompt":
			environment.setPromptSymbol(newSymbol.charAt(0));
			break;

		default:
			break;

		}

		environment.writeln(
				"Symbol for " + commandName.toUpperCase() + " changed from '" + oldSymbol + "' to '" + newSymbol + "'");
	}

}
