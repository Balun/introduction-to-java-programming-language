package hr.fer.zemris.java.tecaj.hw07.shell.commands;

import hr.fer.zemris.java.tecaj.hw07.shell.Environment;
import hr.fer.zemris.java.tecaj.hw07.shell.MyShell;
import hr.fer.zemris.java.tecaj.hw07.shell.ShellCommand;
import hr.fer.zemris.java.tecaj.hw07.shell.ShellStatus;

/**
 * This class describes the "HELP" {@link ShellCommand} of the {@link MyShell}.
 * If started with no arguments, it displays the list of all
 * {@link ShellCommand}. If called with concrete command name as argument, it
 * displays the description of the specified command. It inherits the
 * {@link AbstractCommand} class, implementing the {@link ShellCommand}'s method
 * execute().
 * 
 * @author Matej Balun
 *
 */
public class HelpCommand extends AbstractCommand {

	/**
	 * Name of the command.
	 */
	private static final String NAME = "help";

	/**
	 * First line of the description.
	 */
	private static final String DESCRIPTION_1 = "This command if started with no arguments list the names of all commands.";

	/**
	 * Second line of the description.
	 */
	private static final String DESCRIPTION_2 = "If started with a single argument, prints the name and description of the command";

	/**
	 * Initialises the {@link HelpCommand}, passing its name and description to
	 * the super class constructor {@link AbstractCommand}.
	 */
	public HelpCommand() {
		super(NAME, DESCRIPTION_1, DESCRIPTION_2);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ShellStatus execute(Environment environment, String args) {
		if (args.trim().isEmpty()) {
			environment.commands().forEach(c -> environment.writeln(c.getCommandName().toUpperCase()));
			return ShellStatus.CONTINUE;

		} else {
			for (ShellCommand command : environment.commands()) {
				if (args.trim().equalsIgnoreCase(command.getCommandName())) {
					environment.writeln(command.getCommandName().toUpperCase());
					command.getCommandDescription().forEach(d -> environment.writeln(d));
					return ShellStatus.CONTINUE;
				}
			}

			environment.writeln("Invalid command name");
			return ShellStatus.CONTINUE;
		}
	}

}
