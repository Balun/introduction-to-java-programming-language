package hr.fer.zemris.java.tecaj.hw07.shell.commands;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import hr.fer.zemris.java.tecaj.hw07.shell.MyShell;
import hr.fer.zemris.java.tecaj.hw07.shell.ShellCommand;
import hr.fer.zemris.java.tecaj.hw07.shell.ShellStatus;
import hr.fer.zemris.jmbag0036489634.cmdapps.trazilica.shell.Environment;

/**
 * This class represents the "MKDIR" {@link ShellCommand} of the {@link MyShell}
 * . It creates the directory tree based on the given argument. It inherits the
 * {@link AbstractCommand} class and implements the {@link ShellCommand}
 * interface, implementing its execute() method.
 * 
 * @author Matej Balun
 *
 */
public class MkdirCommand extends AbstractCommand {

	/**
	 * The name of the command.
	 */
	private static final String NAME = "mkdir";

	/**
	 * First line of the description.
	 */
	private static final String DESCRIPTION_1 = "This command takes a single argument: directory name.";

	/**
	 * Second line of the description.
	 */
	private static final String DESCRIPTION_2 = "It creates the appropriate directory structure";

	/**
	 * The "NO" symbol for the user's response.
	 */
	private static final String NO = "n";

	/**
	 * Initialises the {@link MkdirCommand}, passing its name and description to
	 * the super class constructor {@link AbstractCommand}.
	 */
	public MkdirCommand() {
		super(NAME, DESCRIPTION_1, DESCRIPTION_2);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ShellStatus execute(Environment environment, String args) {

		Path path = Paths.get(args.trim().replace("\"", "")).toAbsolutePath();

		if (Files.exists(path) && Files.isDirectory(path)) {

			environment.writeln("The specified directory structure already exsists. Overwrite? [Y/N]:");

			if (environment.readLine().equalsIgnoreCase(NO)) {
				return ShellStatus.CONTINUE;
			}
		}

		try {
			Files.createDirectories(path);
		} catch (IOException e) {
			environment.writeln(e.getMessage());
		}

		return ShellStatus.CONTINUE;
	}

}
