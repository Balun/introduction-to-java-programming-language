package hr.fer.zemris.java.tecaj.hw07.crypto;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.function.BiConsumer;

/**
 * This abstract class represents the general digest/encrypt/decrypt operator
 * for the {@link Crypto} program. Classes that inherit this class must
 * implement the abstract method execute(). The class offers some helper method
 * for cryptography.
 * 
 * @author Matej Balun
 *
 */
public abstract class CryptoOperator {

	/**
	 * Buffer for reading the files.
	 */
	protected byte[] inputBuffer;

	/**
	 * Default size of the buffer.
	 */
	protected static final int BUFFER_SIZE = 4096;

	/**
	 * Executes the specified cryptography action.
	 */
	public abstract void execute();

	/**
	 * Converts the hexadecimal {@link String} to byte array.
	 * 
	 * @param hex
	 *            the hexadecimal {@link String}
	 * @return the calculated byte array
	 */
	protected static byte[] hexToByte(String hex) {

		if (hex.length() % 2 == 1) {
			throw new IllegalArgumentException(
					"The hex string must be a valid hexadecimal format, with even number of characters.");
		}

		int length = hex.length();
		byte[] data = new byte[length / 2];
		for (int i = 0; i < length; i += 2) {
			data[i / 2] = (byte) ((Character.digit(hex.charAt(i), 16) << 4) + Character.digit(hex.charAt(i + 1), 16));
		}
		return data;
	}

	/**
	 * Converts the specified byte array to the hexadecimal {@link String}.
	 * 
	 * @param the
	 *            specified byte array
	 * @return the calculated hexadecimal {@link String}
	 */
	protected static String byteToHex(byte[] a) {
		StringBuilder sb = new StringBuilder(a.length * 2);
		for (byte b : a)
			sb.append(String.format("%02x", b & 0xff));
		return sb.toString();
	}

	/**
	 * This method reads bytes from the specified file iteratively, applying the
	 * specified each time the fixed amount of bytes was read.
	 * 
	 * @param source
	 *            the source file path
	 * @param the
	 *            specified {@link BiConsumer} action
	 */
	protected void readAndApply(Path source, BiConsumer<Integer, Integer> consumer) {

		try (InputStream input = new BufferedInputStream(new FileInputStream(source.toFile()))) {

			inputBuffer = new byte[BUFFER_SIZE];
			int read;

			do {
				read = input.read(inputBuffer);
				if (read > 0) {
					consumer.accept(0, read);
				}
			} while (read != -1);

		} catch (IOException e) {
			System.err.println("Error in reading file");
		}

	}
}
