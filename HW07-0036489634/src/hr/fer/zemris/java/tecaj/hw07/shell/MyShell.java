package hr.fer.zemris.java.tecaj.hw07.shell;

import java.util.HashMap;
import java.util.Map;

import hr.fer.zemris.java.tecaj.hw07.shell.commands.CatCommand;
import hr.fer.zemris.java.tecaj.hw07.shell.commands.CharsetsCommand;
import hr.fer.zemris.java.tecaj.hw07.shell.commands.CopyCommand;
import hr.fer.zemris.java.tecaj.hw07.shell.commands.HelpCommand;
import hr.fer.zemris.java.tecaj.hw07.shell.commands.HexDumpCommand;
import hr.fer.zemris.java.tecaj.hw07.shell.commands.LsCommand;
import hr.fer.zemris.java.tecaj.hw07.shell.commands.MkdirCommand;
import hr.fer.zemris.java.tecaj.hw07.shell.commands.MoreLinesCommand;
import hr.fer.zemris.java.tecaj.hw07.shell.commands.MultiLineCommand;
import hr.fer.zemris.java.tecaj.hw07.shell.commands.PromptCommand;
import hr.fer.zemris.java.tecaj.hw07.shell.commands.QuitCommand;
import hr.fer.zemris.java.tecaj.hw07.shell.commands.TreeCommand;

/**
 * This program demonstrates the simple shell implementation {@link MyShell}.
 * The {@link MyShell} offers the user set various {@link ShellCommand}s for
 * manipulating the data on the file system. Valid commands {@link MyShell} are:
 * 
 * PROMPT - {@link PromptCommand}; MORELINES - {@link MoreLinesCommand};
 * MULTILINE - {@link MultiLineCommand}; CHARSETS - {@link CharsetsCommand}; LS
 * - {@link LsCommand}; CAT - {@link CatCommand}; TREE - {@link TreeCommand};
 * COPY - {@link CopyCommand}; MKDIR - {@link MkdirCommand}; HEXDUMPT -
 * {@link HexDumpCommand}; HELP - {@link HelpCommand}; EXIT -
 * {@link QuitCommand}.
 * 
 * The program ends when the users enters "EXIT" command-
 * 
 * @author Matej Balun
 *
 */
public class MyShell {

	/**
	 * The static map with command names as keys and command implementations as
	 * values.
	 */
	private static Map<String, ShellCommand> commands;

	/**
	 * The static initialisation block for the shell commands.
	 */
	static {
		commands = new HashMap<>();
		ShellCommand[] shellCommands = { new CatCommand(), new CharsetsCommand(), new CopyCommand(), new HelpCommand(),
				new LsCommand(), new MkdirCommand(), new MoreLinesCommand(), new MultiLineCommand(),
				new PromptCommand(), new QuitCommand(), new TreeCommand(), new HexDumpCommand() };

		for (ShellCommand command : shellCommands) {
			commands.put(command.getCommandName(), command);
		}
	}

	/**
	 * The start message.
	 */
	private static final String MESSAGE = "Welcome to MyShell v 1.0";

	/**
	 * The symbol string constant.
	 */
	private static final String SYMBOL = "symbol";

	/**
	 * The end message.
	 */
	private static final String GOODBYE = "Thank you for using this shell. Goodbye!";

	/**
	 * The environment of the {@link MyShell}.
	 */
	private static Environment environment = new EnvironmentImpl('|', '>', '\\', commands.values());

	/**
	 * The main method that executes the program.
	 */
	public static void main(String[] args) {
		environment.writeln(MESSAGE);

		while (true) {
			environment.write(environment.getPromptSymbol().toString() + " ");
			String line = environment.readLine();

			if (line.trim().endsWith(environment.getMoreLinesSymbol().toString())) {
				line = readMoreLines(line);
			}

			if (line.startsWith(SYMBOL)) {
				line = line.replace(SYMBOL, "").trim();
			}

			String[] input = line.split(" ", 2);

			String cmd = input[0].trim();
			String arg;

			try {
				arg = input[1].trim();
			} catch (ArrayIndexOutOfBoundsException e) {
				arg = "";
			}

			ShellCommand command = commands.get(cmd.toLowerCase());
			if (command == null) {
				environment.writeln("Unknown command!");
				continue;
			}

			if (command.execute(environment, arg) == ShellStatus.TERMINATE) {
				break;
			}
		}

		environment.writeln(GOODBYE);
	}

	/**
	 * The method for reading multiple lines if the user indicates more lines
	 * with the more line symbol. The method will read the lines as long as they
	 * are terminated with the current more line symbol.
	 * 
	 * @param line
	 *            the first line in the multi - line construct.
	 * @return The single line composed of all lines, separated by a blank
	 *         space.
	 */
	private static String readMoreLines(String line) {
		StringBuilder builder = new StringBuilder();
		builder.append(line.substring(0, line.length() - 1).trim() + " ");

		while (true) {
			environment.write(environment.getMultilineSymbol() + " ");
			String currentLine = environment.readLine().trim();

			if (currentLine.endsWith(environment.getMoreLinesSymbol().toString())) {
				builder.append(currentLine.substring(0, currentLine.length() - 1).trim() + " ");
			} else {
				builder.append(currentLine.trim() + " ");
				break;
			}
		}

		return builder.toString();

	}

}
