package hr.fer.zemris.java.tecaj.hw07.shell.commands;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;

import hr.fer.zemris.java.tecaj.hw07.shell.MyShell;
import hr.fer.zemris.java.tecaj.hw07.shell.ShellCommand;
import hr.fer.zemris.java.tecaj.hw07.shell.ShellStatus;
import hr.fer.zemris.java.tecaj.hw07.shell.Environment;

/**
 * This class describes the "COPY" command of the {@link MyShell}. The class
 * copies the content of the specified file (first argument) to the specified
 * path (second argument). The first argument must be an existing readable file.
 * The class inherits the {@link AbstractCommand} class, implementing the
 * execute() method of the {@link ShellCommand} interface.
 * 
 * @author Matej Balun
 *
 */
public class CopyCommand extends AbstractCommand {

	/**
	 * The name of the command.
	 */
	private static final String NAME = "copy";

	/**
	 * The first description line.
	 */
	private static final String DESCRIPTION_1 = "This command takes two paths and copies file from the first path to the second.";

	/**
	 * The second description line.
	 */
	private static final String DESCRIPTION_2 = "If the destination file exsists, the user is prompted to overwrite it.";

	/**
	 * The third description line.
	 */
	private static final String DESCRIPTION_3 = "The source path must be an exsisting file";

	/**
	 * THe fourth description line.
	 */
	private static final String DESCRIPTION_4 = "If the target path is directory,"
			+ " the new file will be created with the original file name";

	/**
	 * The "yes" option for the user.
	 */
	private static final String YES = "y";

	/**
	 * Size of the buffer for byte transfer.
	 */
	private static final int BUFFER_SIZE = 4096;

	/**
	 * Initialises the {@link CopyCommand}, passing its name and description to
	 * the super class constructor {@link AbstractCommand}.
	 */
	public CopyCommand() {
		super(NAME, DESCRIPTION_1, DESCRIPTION_2, DESCRIPTION_3, DESCRIPTION_4);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ShellStatus execute(Environment environment, String args) {
		String[] arguments = args.split(" ", 2);
		Path source = Paths.get(arguments[0].trim().replace("\"", "")).toAbsolutePath();
		Path target = Paths.get(arguments[1].trim().replace("\"", "")).toAbsolutePath();

		if (!Files.exists(source) || Files.isDirectory(source)) {
			environment.writeln("The specified file does not essist or its a directory");
			return ShellStatus.CONTINUE;
		}

		if (Files.exists(target)) {

			if (!Files.isDirectory(target)) {

				environment.writeln("The specified target file already exsists. Overwrite? [Y/N]:");
				if (environment.readLine().equalsIgnoreCase(YES)) {
					copyFile(source, target, environment);
				} else {
					return ShellStatus.CONTINUE;
				}

			} else {
				target = target.resolve(source.getFileName());
				copyFile(source, target, environment);
			}

		} else {
			try {
				if (!Files.isDirectory(target) && target.toString().contains(".")) {
					Files.createDirectories(target.getParent());
				} else {
					Files.createDirectories(target);
					target = target.resolve(source.getFileName().toString());
				}
				
				copyFile(source, target, environment);
			} catch (IOException e) {
				environment.writeln(e.getMessage());
			}

		}

		return ShellStatus.CONTINUE;

	}

	/**
	 * Copies the content of the file with source path to the file with
	 * destination path.
	 * 
	 * @param source
	 *            path of the source file
	 * @param dest
	 *            path of the destination path
	 * @param environment
	 *            the specified {@link MyShell} environment.
	 */
	private void copyFile(Path source, Path dest, Environment environment) {

		try (InputStream input = new BufferedInputStream(new FileInputStream(source.toString()))) {

			try {
				if (Files.isSameFile(source, dest)) {
					environment.writeln("You have specified the same file to copy.");
					return;
				}
			} catch (NoSuchFileException e) {

			}

			try (OutputStream output = new BufferedOutputStream(new FileOutputStream(dest.toString()))) {

				byte[] buf = new byte[BUFFER_SIZE];
				int bytesRead;
				while ((bytesRead = input.read(buf)) > 0) {
					output.write(buf, 0, bytesRead);
				}
			}

		} catch (IOException e) {
			environment.writeln(e.getMessage());
		}
	}

}
