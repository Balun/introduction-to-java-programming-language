package hr.fer.zemris.java.tecaj.hw07.shell;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

/**
 * This class implements the {@link Environment} of the {@link MyShell} shell
 * application. It offers the concrete implementations of all operations
 * regarding the {@link MyShell} input/output.
 * 
 * @author Matej Balun
 *
 */
public class EnvironmentImpl implements Environment {

	/**
	 * The current symbol for multi lines.
	 */
	private Character multiLineSymbol;

	/**
	 * The current symbol for prompt.
	 */
	private Character promptSymbol;

	/**
	 * The current symbol for more lines.
	 */
	private Character moreLinesSymbol;

	/**
	 * The specified reader to read from prompt.
	 */
	private BufferedReader reader;

	/**
	 * The specified writer for writing on the {@link MyShell}.
	 */
	private BufferedWriter writer;

	/**
	 * {@link Iterable} collection of all {@link MyShell} {@link ShellCommand}s.
	 */
	private Iterable<ShellCommand> commands;

	/**
	 * Initialises the {@link EnvironmentImpl} with the specified initial
	 * symbols and collection of {@link ShellCommand}s.
	 * 
	 * @param multiLineSymbol
	 *            the specified symbol for multi lines
	 * @param promptSymbol
	 *            the specified symbol for prompt
	 * @param moreLinesSymbol
	 *            the specified symbol for more lines
	 * @param commands
	 *            {@link Iterable} collection of {@link ShellCommand}s
	 */
	public EnvironmentImpl(Character multiLineSymbol, Character promptSymbol, Character moreLinesSymbol,
			Iterable<ShellCommand> commands) {
		super();

		this.commands = commands;
		this.multiLineSymbol = multiLineSymbol;
		this.promptSymbol = promptSymbol;
		this.moreLinesSymbol = moreLinesSymbol;
		this.reader = new BufferedReader(new InputStreamReader(System.in));
		this.writer = new BufferedWriter(new OutputStreamWriter(System.out));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String readLine() {
		try {
			return reader.readLine();
		} catch (IOException e) {
			System.err.println("Error in reading");
			return null;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void write(String text) {
		try {
			writer.write(text);
			writer.flush();
		} catch (IOException e) {
			System.err.println("Error in writing");
		}

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void writeln(String text) {
		try {
			writer.write(text);
			writer.newLine();
			writer.flush();
		} catch (IOException e) {
			System.err.println("Error in writing");
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Iterable<ShellCommand> commands() {
		return commands;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Character getMultilineSymbol() {
		return this.multiLineSymbol;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setMultilineSymbol(Character symbol) {
		this.multiLineSymbol = symbol;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Character getPromptSymbol() {
		return this.promptSymbol;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setPromptSymbol(Character symbol) {
		this.promptSymbol = symbol;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Character getMoreLinesSymbol() {
		return this.moreLinesSymbol;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setMoreLinesSymbol(Character symbol) {
		this.moreLinesSymbol = symbol;
	}

}
