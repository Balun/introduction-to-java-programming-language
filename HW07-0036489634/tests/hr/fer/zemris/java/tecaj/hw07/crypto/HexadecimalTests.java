package hr.fer.zemris.java.tecaj.hw07.crypto;

import static hr.fer.zemris.java.tecaj.hw07.crypto.CryptoOperator.*;
import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;

public class HexadecimalTests {

	private static byte[] createByteArray(String hex) {
		byte[] array = new byte[hex.length() / 2];

		for (int i = 0, j = 0; i < array.length; i++, j += 2) {
			array[i] = Byte.parseByte(Character.toString(hex.charAt(j)) + Character.toString(hex.charAt(j + 1)));
		}

		return array;
	}

	@Test
	public void testHexToByte1() {
		String hex = "04";
		byte[] bytes = createByteArray(hex);
		assertEquals("Expected 00100011", Arrays.toString(bytes), Arrays.toString(hexToByte(hex)));
	}

	@Test
	public void testHexToByte2() {
		String hex = "0404";
		byte[] bytes = createByteArray(hex);
		assertEquals("Expected 0000010000000100", Arrays.toString(bytes), Arrays.toString(hexToByte(hex)));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testHexToByte3() {
		String hex = "040";
		byte[] bytes = createByteArray(hex);
		assertEquals("Expected 000001000000", Arrays.toString(bytes), Arrays.toString(hexToByte(hex)));
	}
	
	@Test
	public void testHexToBytes4(){
		String hex = "ab12";
		assertEquals("Expected AB12", hex, byteToHex(hexToByte(hex)));
	}
	
	@Test
	public void textHexToByte5(){
		String hex = "4d86";
		assertEquals("Expected 4d86", hex, byteToHex(hexToByte(hex)));
	}

}
