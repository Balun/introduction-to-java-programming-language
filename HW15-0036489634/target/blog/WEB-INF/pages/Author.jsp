<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="hr.fer.zemris.java.tecaj_13.model.BlogUser"%>
<%@page import="hr.fer.zemris.java.tecaj_13.model.BlogEntry"%>
<%@page import="java.util.List"%>

<%
	String user = "Not logged in";
	String nick = null;

	if (session.getAttribute("current.user.id") != null) {
		user = session.getAttribute("current.user.name").toString()
				+ session.getAttribute("current.user.surname").toString();
		nick = session.getAttribute("current.user.nick").toString();
	}

	List<BlogEntry> entries = (List<BlogEntry>) request.getAttribute("entries");
	String author = request.getAttribute("author").toString();
%>

<html>
	<jsp:include page="Head.jsp"></jsp:include>
	
	<h2>Entries of <%= author%>:</h2>
	<ul>
		<% 
			for(BlogEntry entry : entries){
				%><li><a href="<%= request.getContextPath()%>/servleti/author/<%= author%>/<%= entry.getId()%>"><%= entry.getTitle()%></a></li><% 
			}
		%>
	</ul>
	
	<%
		if(session.getAttribute("current.user.id") != null && nick.equals(author)){
			%><a href="<%= request.getContextPath()%>/servleti/author/<%= nick%>/new">Add a new entry</a><%
		}
	%>
	</body>
</html>