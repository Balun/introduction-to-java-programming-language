<%@page import="sun.util.logging.resources.logging"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="hr.fer.zemris.java.tecaj_13.model.BlogUser"%>
<%@page import="hr.fer.zemris.java.tecaj_13.forms.FormLogin"%>
<%@page import="java.util.Map.Entry"%>
<%@page import="java.util.List"%>

<html>
<jsp:include page="Head.jsp" />

<%
	FormLogin login = (FormLogin) request.getAttribute("model");
	String nick = request.getAttribute("nick").toString();
	String password = request.getAttribute("password").toString();
	List<BlogUser> authors = (List<BlogUser>) request.getAttribute("authors");
%>
<%
	if (session.getAttribute("current.user.id") == null) {
%>

<h3>login</h3>
<form action="<%=request.getContextPath()%>/servleti/main" method="post">
	nickname: <input type="text" id="nick" name="nick" value="<%=nick%>" /><br />
	password: <input type="password" id="password" name="password"
		<%=password%> /><br /> <input type="submit" value="login" />
</form>

<%
	if (request.getAttribute("wrong.info") == null) {
			out.println("");
		} else {
			out.println(request.getAttribute("wrong.info"));
		}
%>
<ul>
	<%
		for (String error : login.getErrors().values()) {
	%><li><%=error%></li>
	<%
		}
	%>

</ul>

<%
	
%><p>
	Click <a href="<%=request.getContextPath()%>/servleti/register">here</a>
	to register.
</p>
<%
	}
%>

<ul>
	<%
		for (BlogUser user : authors) {
	%>
	<li><a
		href="<%=request.getContextPath()%>/servleti/author/<%=user.getNick()%>"><%=user.getFirstName()%>
			<%=user.getLastName()%></a></li>
	<%
		}
	%>
</ul>

</body>
</html>