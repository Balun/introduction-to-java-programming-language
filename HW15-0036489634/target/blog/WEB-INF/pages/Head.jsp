<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%
	String loggedIn = "Not logged in";
	if (session.getAttribute("current.user.id") != null) {
		loggedIn = (String) session.getAttribute("current.user.name") + " "
				+ (String) session.getAttribute("current.user.surname");
	}
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title><%=request.getAttribute("pageTitle")%></title>
</head>
<body>

	<p>
		<a href="<%=request.getContextPath()%>/servleti/main">Home</a>
	</p>
	<h3><%=request.getAttribute("pageTitle")%>
		- User:
		<%=loggedIn%>,
		<%
		if (session.getAttribute("current.user.id") != null) {
	%>
		<a href="<%=request.getContextPath()%>/servleti/logout">logout</a>
		<%
			} else {
		%>
		<a href="<%=request.getContextPath()%>/servleti/main">login</a>
		<%
			}
		%>
	</h3>