<%@page import="hr.fer.zemris.java.tecaj_13.forms.FormRegister"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="hr.fer.zemris.java.tecaj_13.model.BlogUser"%>
<%@page import="java.util.Map.Entry"%>
<%@page import="java.util.List"%>

<%
	FormRegister register = (FormRegister) request.getAttribute("model");

	String name = register.getName()!=null ? register.getName() : "";
	String surname = register.getSurname()!=null ? register.getSurname() : "";
	String nick = register.getNick() != null ? register.getNick() : "";
	String email = register.getEmail() != null ? register.getEmail() : "";
	String pass = register.getPassword() != null ? register.getPassword() : "";
%>

<html>
	<head>
		<title><%= request.getAttribute("pageTitle")%></title>
	</head>
	
	<body>
		<h2><%= request.getAttribute("pageTitle") %></h2>
		<form action="<%= request.getContextPath()%>/servleti/register" method="post">
			First name: <input type="text" id="name" name="name" value="<%= name %>" /><br />
			Last name: <input type="text" id="surname" name="surname" value="<%= surname %>" /><br />
			Email: <input type="text" id="email" name="email" value="<%= email %>" /><br />
			Nick: <input type="text" id="nick" name="nick" value ="<%= nick %>" /><br />
			Password: <input type="password" id="password" name="password" <%= pass %> /><br />
		<input type="submit" value="Register" />
		</form>	
		
		<ul>
		<% for(String error : register.getErrors().values()){
			%><li><%= error%></li><%	
		}
		%>
		</ul>
	</body>
</html>