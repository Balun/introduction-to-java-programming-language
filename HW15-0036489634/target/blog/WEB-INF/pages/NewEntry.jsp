<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="hr.fer.zemris.java.tecaj_13.model.BlogUser"%>
<%@page import="hr.fer.zemris.java.tecaj_13.forms.FormEntry"%>

<% 
	FormEntry form = (FormEntry) request.getAttribute("model");

	String id = form.getId()!=null ? form.getId() : null;
	String title = form.getTitle()!=null ? form.getTitle() : "";
	String text = form.getText()!=null ? form.getText() : "";
	
	String loggedIn = "Not logged in";
	String nick = null;
	if(session.getAttribute("current.user.id")!=null){
		loggedIn = session.getAttribute("current.user.name").toString() + session.getAttribute("current.user.surname").toString();
		nick = session.getAttribute("current.user.nick").toString();
	}
%>

<html>
	<jsp:include page="Head.jsp"></jsp:include>
	
	<form action="<%= request.getContextPath()%>/servleti/author/<%= nick%>" method="post">
		Entry title: <input type="text" id="title" name="title" value="<%= title%>" /><br />
		Entry text: <textarea id="text" name="text" rows="20" cols="40"><%= text%></textarea><br />
		<input type="hidden" id="id" name="id" value="<%= id==null ? "" : id%>"/>
		<input type="submit" value = "<%= id==null ? "Create" : "Save"%>" />
	</form>
	
	<ul>
	<% 
		for(String error : form.getErrors().values()){
			%><li><%= error%></li><% 
		}
	%>
	</ul>
	
	</body>
</html>