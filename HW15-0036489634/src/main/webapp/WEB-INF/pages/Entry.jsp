<%@page import="java.text.SimpleDateFormat"%>
<%@page import="hr.fer.zemris.java.tecaj_13.model.BlogComment"%>
<%@page import="hr.fer.zemris.java.tecaj_13.model.BlogEntry"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<% 
	BlogEntry entry = (BlogEntry) request.getAttribute("model");

	String loggedIn = "Not logged in";
	String nick = null;
	if(session.getAttribute("current.user.id") != null){
		loggedIn = session.getAttribute("current.user.name").toString() + session.getAttribute("current.user.surname").toString();
		nick = session.getAttribute("current.user.nick").toString();
	}
%>

<html>
	<jsp:include page="Head.jsp"></jsp:include>
	<p><%= entry.getText()%></p>
	
	<% 
		if(session.getAttribute("current.user.id") != null && nick.equals(entry.getCreator().getNick())){
			%><a href="<%= request.getContextPath()%>/servleti/author/<%= nick%>/edit/<%= entry.getId()%>">Edit the entry</a><%
		}
	%>
	
	<ul>
		<% 
			for(BlogComment comment : entry.getComments()){
				%><li><%= comment.getUsersEMail()%> - <%=  new SimpleDateFormat("dd/MM/YY HH:MM").format(comment.getPostedOn())%>: <%= comment.getMessage()%></li><%
			}
		%>
	</ul>
	
	<% 
		if(session.getAttribute("current.user.id") != null){
			%><form action="<%= request.getContextPath()%>/servleti/author/<%= entry.getCreator().getNick()%>/<%= entry.getId()%>" method="post">
				Comment: <input type="text" id="comment" name ="comment"/><br />
				<input type="submit" value="send" />
			</form> <% 
		}
	%>
	</body>
</html>