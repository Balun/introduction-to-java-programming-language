package hr.fer.zemris.java.tecaj_13.dao.jpa;

import javax.persistence.EntityManagerFactory;

/**
 * Ovaj razred predstavlja singleton implementaciju koja drži referencu na
 * primjerak razreda {@link EntityManagerFactory} za mogućnost dohvata podataka
 * iz baze u JPA protokolu.
 * 
 * @author Matej Balun
 *
 */
public class JPAEMFProvider {

	/**
	 * Singleton {@link EntityManagerFactory} objekt.
	 */
	public static EntityManagerFactory emf;

	/**
	 * Vraća referencu na primjerak razreda {@link EntityManagerFactory}
	 * 
	 * @return {@link EntityManagerFactory} instanca.
	 */
	public static EntityManagerFactory getEmf() {
		return emf;
	}

	/**
	 * Postavlja specificirani primjerak {@link EntityManagerFactory} kao
	 * singleton objekt.
	 * 
	 * @param emf
	 *            specificirani primjerak {@link EntityManagerFactory} razreda.
	 */
	public static void setEmf(EntityManagerFactory emf) {
		JPAEMFProvider.emf = emf;
	}
}