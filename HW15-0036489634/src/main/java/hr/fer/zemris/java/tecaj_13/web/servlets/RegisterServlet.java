package hr.fer.zemris.java.tecaj_13.web.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.tecaj_13.dao.DAOProvider;
import hr.fer.zemris.java.tecaj_13.forms.FormRegister;
import hr.fer.zemris.java.tecaj_13.model.BlogUser;

/**
 * Ovaj sevlet služi za registraciju novog korisnika u bazu podataka za blog.
 * 
 * @author Matej Balun
 *
 */
@WebServlet(name = "register", urlPatterns = "/servleti/register")
public class RegisterServlet extends HttpServlet {

	/**
	 * UID za ovaj razred.
	 */
	private static final long serialVersionUID = -4603177849580274634L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		FormRegister register = new FormRegister();
		BlogUser user = new BlogUser();
		register.createfromModel(user);

		req.setAttribute("model", register);
		req.setAttribute("pageTitle", "Registration");
		req.getRequestDispatcher("/WEB-INF/pages/Registration.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		FormRegister register = new FormRegister();
		register.fillFromRequest(req);
		register.checkErrors();

		if (!register.hasErrors()) {
			BlogUser user = new BlogUser();
			register.toModel(user);

			if (DAOProvider.getDAO().getUser(user.getNick()) != null) {
				register.getErrors().put("name", "Nickname already exists.");

			} else {
				DAOProvider.getDAO().createUser(user);

				req.setAttribute("pageTitle", "Registration succeeded");
				req.getRequestDispatcher("/WEB-INF/pages/RegistrationSucceeded.jsp").forward(req, resp);
			}

		} else {
			req.setAttribute("model", register);
			req.setAttribute("pageTitle", "Registration");
			req.getRequestDispatcher("/WEB-INF/pages/Registration.jsp").forward(req, resp);
		}

	}
}
