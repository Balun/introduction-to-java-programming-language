package hr.fer.zemris.java.tecaj_13.forms;

import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;

import hr.fer.zemris.java.tecaj_13.model.BlogEntry;

/**
 * Ovaj razred predstavlja model podataka za formular pri unosu podaka za
 * određeni zapis u bloga na web aplikaciji. naslijeđuje {@link AbstractForm}
 * razred.
 * 
 * @author Matej Balun
 *
 */
public class FormEntry extends AbstractForm {

	/**
	 * id modela
	 */
	private Long modelID;

	/**
	 * naslov modela
	 */
	private String modelTitle;

	/**
	 * tekst modela
	 */
	private String modelText;

	/**
	 * id zapisa
	 */
	private String id;

	/**
	 * naslov zapisa
	 */
	private String title;

	/**
	 * tekst zapisa
	 */
	private String text;

	/**
	 * Default konstruktor, poziva konstruktor nad-razreda {@link AbstractForm}.
	 */
	public FormEntry() {
		super();
	}

	/**
	 * Vraća id zapisa.
	 * 
	 * @return id zapisa.
	 */
	public String getId() {
		return id;
	}

	/**
	 * Postavlja id zapisa.
	 * 
	 * @param id
	 *            id zapisa
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Vraća naslov zapisa.
	 * 
	 * @return naslov zapisa.
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Postavlja naslov zapisa.
	 * 
	 * @param title
	 *            naslov zapisa.
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Vraća tekst zapisa.
	 * 
	 * @return tekst zapisa.
	 */
	public String getText() {
		return text;
	}

	/**
	 * Postavlja tekst zapisa.
	 * 
	 * @param text
	 *            tekst zapisa.
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * Puni formular pomoću podataka iz {@link HttpServletRequest} razreda.
	 * 
	 * @param request
	 *            primjerak {@link HttpServletRequest} razreda.
	 */
	public void fillFromMrequest(HttpServletRequest request) {
		try {
			id = new String(request.getParameter("id").getBytes(), ENCODING);
			title = new String(request.getParameter("title").getBytes(), ENCODING);
			text = new String(request.getParameter("text").getBytes(), ENCODING);
		} catch (UnsupportedEncodingException e) {
		}
	}

	/**
	 * Provjerava postoje li pogreške u elementima formulara.
	 * 
	 * @return <code>true</code> ao postoje pogreške, inače <code>false</code>.
	 */
	public boolean checkErrors() {
		if (check("id", id)) {
			modelID = Long.parseLong(id);
		}

		if (check("title", title)) {
			modelTitle = title;
		}

		if (check("text", text)) {
			modelText = text;
		}

		return hasErrors();
	}

	/**
	 * Puni formular pomoću podataka iz modela koji predstavlja primjerak
	 * razreda {@link BlogEntry}.
	 * 
	 * @param entry
	 *            primjerak razreda {@link BlogEntry}
	 */
	public void createFromModel(BlogEntry entry) {
		modelID = entry.getId();
		modelText = entry.getText();
		modelTitle = entry.getTitle();

		if (modelID != null) {
			id = modelID.toString();
		}

		if (modelText != null) {
			text = modelText;
		}

		if (modelTitle != null) {
			title = modelTitle;
		}
	}

	/**
	 * Uzima podatke iz formulara u primjerak razreda {@link BlogEntry}.
	 * 
	 * @param entry
	 *            primjerak razreda {@link BlogEntry}.
	 */
	public void toModel(BlogEntry entry) {
		entry.setId(modelID);
		entry.setTitle(modelTitle);
		entry.setText(modelText);
	}
}
