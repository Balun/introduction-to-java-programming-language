package hr.fer.zemris.java.tecaj_13.web.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.tecaj_13.dao.DAOProvider;
import hr.fer.zemris.java.tecaj_13.forms.FormLogin;
import hr.fer.zemris.java.tecaj_13.model.BlogUser;

/**
 * Ovaj servlet predstavlja glavnu stranicu blog aplikacije koja nudi login na
 * blog, kao i pregled svih registriranih korisnika te link na registraciju
 * novog korisnika.
 * 
 * @author Matej Balun
 *
 */
@WebServlet(name = "main", urlPatterns = "/servleti/main")
public class MainServlet extends HttpServlet {

	/**
	 * UID za ovaj razred.
	 */
	private static final long serialVersionUID = 8067748696702751032L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		FormLogin login = new FormLogin();
		BlogUser user = new BlogUser();
		login.createFromModel(user);

		passLogin(req, resp, login);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		FormLogin login = new FormLogin();
		login.fillfromRequest(req);
		login.checkErrors();

		if (!login.hasErrors()) {
			BlogUser user = new BlogUser();
			login.toModel(user);
			login.checkErrors();

			BlogUser loggedIn = DAOProvider.getDAO().getUser(user.getNick(), user.getPasswordHash());

			if (loggedIn != null) {
				req.getSession().setAttribute("current.user.id", loggedIn.getId());
				req.getSession().setAttribute("current.user.name", loggedIn.getFirstName());
				req.getSession().setAttribute("current.user.surname", loggedIn.getLastName());
				req.getSession().setAttribute("current.user.nick", loggedIn.getNick());
			} else {
				req.setAttribute("wrong.info", "You provided wrong nickname or password. Please try again.");
			}
		}

		passLogin(req, resp, login);
	}

	/**
	 * proslijeđuje login korisnika dotičnom .jsp dokumentu.
	 * 
	 * @param req
	 *            {@link HttpServletRequest} objekt
	 * @param resp
	 *            {@link HttpServletResponse} objekt
	 * @param login
	 *            formula za login korisnika
	 * @throws ServletException
	 *             ako je došlo do pogreške
	 * @throws IOException
	 *             ako je došlo do pogreške
	 */
	private void passLogin(HttpServletRequest req, HttpServletResponse resp, FormLogin login)
			throws ServletException, IOException {
		List<BlogUser> users = DAOProvider.getDAO().getUsers();
		req.setAttribute("authors", users);
		req.setAttribute("model", login);

		if (login.getNick() == null) {
			req.setAttribute("nick", "");
		} else {
			req.setAttribute("nick", login.getNick());
		}

		if (login.getPassword() == null) {
			req.setAttribute("password", "");
		} else {
			req.setAttribute("password", login.getPassword());
		}

		req.setAttribute("pageTitle", "Main page");
		req.getRequestDispatcher("/WEB-INF/pages/Main.jsp").forward(req, resp);
	}
}
