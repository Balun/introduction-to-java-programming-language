package hr.fer.zemris.java.tecaj_13.dao.jpa;

import hr.fer.zemris.java.tecaj_13.dao.DAOException;

import javax.persistence.EntityManager;

/**
 * Ovaj razred pruža metode za dohvat primjerka razreda {@link EntityManager}
 * pomoću kojeg se dohvaćaju podaci iz baze podataka.
 * 
 * @author Matej Balun
 *
 */
public class JPAEMProvider {

	/**
	 * Referenca na primjerak razreda {@link ThreadLocal}.
	 */
	private static ThreadLocal<LocalData> locals = new ThreadLocal<>();

	/**
	 * Vraća referencu na primjerak {@link EntityManager} razreda pomoću
	 * {@link ThreadLocal} objekta.
	 * 
	 * @return refernca na {@link EntityManager} objekt
	 */
	public static EntityManager getEntityManager() {
		LocalData ldata = locals.get();
		if (ldata == null) {
			ldata = new LocalData();
			ldata.em = JPAEMFProvider.getEmf().createEntityManager();
			ldata.em.getTransaction().begin();
			locals.set(ldata);
		}
		return ldata.em;
	}

	/**
	 * Izvršava transakciju i zatvara resurse baze podataka.
	 * 
	 * @throws DAOException
	 *             ako je došlo do pogreške u operacijama s bazom podataka.
	 */
	public static void close() throws DAOException {
		LocalData ldata = locals.get();
		if (ldata == null) {
			return;
		}
		DAOException dex = null;
		try {
			if (ldata.em.getTransaction().isActive() && !ldata.em.getTransaction().getRollbackOnly()) {
				ldata.em.getTransaction().commit();
			} else if (ldata.em.getTransaction().isActive()) {
				ldata.em.getTransaction().rollback();
			}
		} catch (Exception ex) {
			dex = new DAOException("Unable to commit transaction.", ex);
		}
		try {
			ldata.em.close();
		} catch (Exception ex) {
			if (dex != null) {
				dex = new DAOException("Unable to close entity manager.", ex);
			}
		}
		locals.remove();
		if (dex != null)
			throw dex;
	}

	/**
	 * Ovaj statiči razred enkapsulira primjerak razreda {@link EntityManager}
	 * 
	 * @author Matej Balun
	 *
	 */
	private static class LocalData {

		/**
		 * Primjerak razreda {@link EntityManager}.
		 */
		EntityManager em;
	}

}