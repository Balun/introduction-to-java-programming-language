package hr.fer.zemris.java.tecaj_13.web.servlets;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.tecaj_13.dao.DAOProvider;
import hr.fer.zemris.java.tecaj_13.forms.FormEntry;
import hr.fer.zemris.java.tecaj_13.model.BlogComment;
import hr.fer.zemris.java.tecaj_13.model.BlogEntry;
import hr.fer.zemris.java.tecaj_13.model.BlogUser;

/**
 * Ovaj razred predstavlja servlet za prikaz zapisa jedno korisnika na blog
 * aplikaciji. Proslijeđuje zahtjeve stranicama za prikaz zapisa, mijenjanje
 * zapisa, kreiranje novog zapisa kao i komentiranje zapisa na blogu.
 * 
 * @author Matej Balun
 *
 */
@WebServlet(name = "author", urlPatterns = "/servleti/author/*")
public class AuthorServlet extends HttpServlet {

	/**
	 * UID za ovaj razred.
	 */
	private static final long serialVersionUID = -3758423398654073284L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String[] params = req.getPathInfo().substring(1).split("/");

		if (params.length == 1) {

			BlogUser author = DAOProvider.getDAO().getUser(params[0]);

			if (author == null) {
				resp.sendRedirect(req.getContextPath() + "/servleti/main");
				return;
			}

			req.setAttribute("entries", DAOProvider.getDAO().getBlogEntries(author));
			req.setAttribute("author", author.getNick());
			req.setAttribute("pageTitle", author.getFirstName() + " " + author.getLastName());
			req.getRequestDispatcher("/WEB-INF/pages/Author.jsp").forward(req, resp);

		} else if (params.length == 2 && params[1].equals("new")) {

			newEntry(req, resp, params[0]);

		} else if (params.length == 3 && params[1].equals("edit")) {

			editEntry(req, resp, params[2]);

		} else if (params.length == 2) {
			displayEntry(req, resp, params[0], Long.parseLong(params[1]));

		} else {
			resp.sendRedirect(req.getContextPath() + "/servleti/main");
		}
	}

	/**
	 * Prikazuje zapis dotičnog autora na blogu
	 * 
	 * @param req
	 *            {@link HttpServletRequest} objekt
	 * @param resp
	 *            {@link HttpServletResponse} objekt
	 * @param author
	 *            ime autora
	 * @param entryID
	 *            id zapisa
	 * @throws ServletException
	 *             ako je došlo do pogreške
	 * @throws IOException
	 *             ako je došlo do pogreške
	 */
	private void displayEntry(HttpServletRequest req, HttpServletResponse resp, String author, Long entryID)
			throws ServletException, IOException {
		BlogEntry entry = DAOProvider.getDAO().getBlogEntry(entryID);

		if (entry == null) {
			displayError(req, resp, "The entry with specified id does not exists!");
			return;
		}

		req.setAttribute("pageTitle", entry.getTitle());
		req.setAttribute("model", entry);
		req.getRequestDispatcher("/WEB-INF/pages/Entry.jsp").forward(req, resp);
	}

	/**
	 * Prikazuje određenu pogrešku na blog web aplikaciji
	 * 
	 * @param req
	 *            {@link HttpServletRequest} objekt
	 * @param resp
	 *            {@link HttpServletResponse} objekt
	 * @param string
	 *            Sadržaj pogreške
	 * @throws ServletException
	 *             ako je došlo do pogreške
	 * @throws IOException
	 *             ako je došlo do pogreške
	 */
	private void displayError(HttpServletRequest req, HttpServletResponse resp, String string)
			throws ServletException, IOException {
		req.setAttribute("error", string);
		req.getRequestDispatcher("/WEB-INF/pages/ErrorPage.jsp").forward(req, resp);
	}

	/**
	 * Mijenja sadržaj specificiranog blog zapisa na web aplikaciji.
	 * 
	 * @param req
	 *            {@link HttpServletRequest} objekt
	 * @param resp
	 *            {@link HttpServletResponse} objekt
	 * @param string
	 *            Sadržaj pogreške
	 * @throws ServletException
	 *             ako je došlo do pogreške
	 * @throws IOException
	 *             ako je došlo do pogreške
	 */
	private void editEntry(HttpServletRequest req, HttpServletResponse resp, String string)
			throws ServletException, IOException {
		String id = string;
		if (id == null) {
			displayError(req, resp, "No entry id provided!");
			return;
		}

		BlogEntry entry = DAOProvider.getDAO().getBlogEntry(Long.parseLong(id));
		FormEntry form = new FormEntry();
		form.createFromModel(entry);

		req.setAttribute("model", form);
		req.setAttribute("pageTitle", "Edit entry");
		req.getRequestDispatcher("/WEB-INF/pages/NewEntry.jsp").forward(req, resp);
	}

	/**
	 * Mijenja sadržaj specificiranog blog zapisa na web aplikaciji.
	 * 
	 * @param req
	 *            {@link HttpServletRequest} objekt
	 * @param resp
	 *            {@link HttpServletResponse} objekt
	 * @param params
	 *            parametri zahtjeva
	 * @throws ServletException
	 *             ako je došlo do pogreške
	 * @throws IOException
	 *             ako je došlo do pogreške
	 */
	private void newEntry(HttpServletRequest req, HttpServletResponse resp, String params)
			throws ServletException, IOException {
		FormEntry form = new FormEntry();
		BlogEntry entry = new BlogEntry();
		form.createFromModel(entry);

		req.setAttribute("model", form);
		req.setAttribute("pageTitle", "Create new entry");
		req.getRequestDispatcher("/WEB-INF/pages/NewEntry.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		if (req.getParameter("comment") != null) {
			comment(req, resp);
			return;
		}

		FormEntry form = new FormEntry();
		form.fillFromMrequest(req);

		if ((form.getErrors().size() == 1 && form.hasError("id")) || form.getId().isEmpty()) {
			create(req, resp, form);
			return;
		} else if (!form.hasErrors()) {
			update(req, resp, form);
			return;
		}

		req.setAttribute("model", form);
		req.setAttribute("pageTitle", "New Entry");
		req.getRequestDispatcher("/WEB-INF/pages/NewEntry.jsp").forward(req, resp);
	}

	/**
	 * Stvara novi zapis na blogu.
	 * 
	 * @param req
	 *            {@link HttpServletRequest} objekt
	 * @param resp
	 *            {@link HttpServletResponse} objekt
	 * @param form
	 *            formular za stvaranje novog zapisa
	 * @throws IOException
	 *             ako je došlo do pogreške
	 */
	private void create(HttpServletRequest req, HttpServletResponse resp, FormEntry form) throws IOException {
		BlogUser user = DAOProvider.getDAO().getUser(req.getSession().getAttribute("current.user.nick").toString());

		BlogEntry entry = new BlogEntry();

		entry.setCreatedAt(new Date());
		entry.setCreator(user);

		form.checkErrors();

		form.toModel(entry);

		entry.setLastModifiedAt(new Date());
		DAOProvider.getDAO().createEntry(entry);

		resp.sendRedirect(req.getContextPath() + "/servleti/author/" + user.getNick() + "/" + entry.getId());
	}

	/**
	 * Mijenja sadržaj specificiranog blog zapisa na web aplikaciji.
	 * 
	 * @param req
	 *            {@link HttpServletRequest} objekt
	 * @param resp
	 *            {@link HttpServletResponse} objekt
	 * @param form
	 *            formular za izmjenu zapisa
	 * @throws IOException
	 *             ako je došlo do pogreške
	 */
	private void update(HttpServletRequest req, HttpServletResponse resp, FormEntry form) throws IOException {
		BlogUser user = DAOProvider.getDAO().getUser(req.getSession().getAttribute("current.user.nick").toString());

		BlogEntry entry = DAOProvider.getDAO().getBlogEntry(Long.parseLong(form.getId()));

		form.checkErrors();
		form.toModel(entry);

		entry.setLastModifiedAt(new Date());
		DAOProvider.getDAO().createEntry(entry);

		resp.sendRedirect(req.getContextPath() + "/servleti/author/" + user.getNick() + "/" + entry.getId());
	}

	/**
	 * Kreira komentar na određeni zapis.
	 * 
	 * @param req
	 *            {@link HttpServletRequest} objekt
	 * @param resp
	 *            {@link HttpServletResponse} objekt
	 * @throws ServletException
	 *             ako je došlo do pogreške
	 * @throws IOException
	 *             ako je došlo do pogreške
	 */
	private void comment(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String[] params = req.getPathInfo().substring(1).split("/");

		if (req.getParameter("comment").isEmpty()) {
			req.getRequestDispatcher("/WEB-INF/pages/Entry.jsp").forward(req, resp);
			return;
		}

		BlogUser user = DAOProvider.getDAO().getUser(req.getSession().getAttribute("current.user.nick").toString());

		BlogEntry entry = DAOProvider.getDAO().getBlogEntry(Long.parseLong(params[1]));

		BlogComment comment = new BlogComment();
		comment.setMessage(req.getParameter("comment"));
		comment.setBlogEntry(entry);
		comment.setUsersEMail(user.getNick());
		comment.setPostedOn(new Date());

		DAOProvider.getDAO().createComment(comment);

		req.setAttribute("pageTitle", entry.getTitle());
		req.setAttribute("model", entry);
		req.getRequestDispatcher("/WEB-INF/pages/Entry.jsp").forward(req, resp);
	}
}
