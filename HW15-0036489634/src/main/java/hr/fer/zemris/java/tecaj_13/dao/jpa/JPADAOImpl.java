package hr.fer.zemris.java.tecaj_13.dao.jpa;

import java.util.List;

import javax.persistence.EntityManager;

import hr.fer.zemris.java.tecaj_13.DAO;
import hr.fer.zemris.java.tecaj_13.dao.DAOException;
import hr.fer.zemris.java.tecaj_13.model.BlogComment;
import hr.fer.zemris.java.tecaj_13.model.BlogEntry;
import hr.fer.zemris.java.tecaj_13.model.BlogUser;

/**
 * Implementacija podaktovnog sloja blog web aplikacije. Implementira sučelje
 * {@link DAO} koje definira metode za dohvat podataka iz baze.
 * 
 * @author Matej Balun
 *
 */
public class JPADAOImpl implements DAO {

	@Override
	public BlogEntry getBlogEntry(Long id) throws DAOException {
		BlogEntry blogEntry = JPAEMProvider.getEntityManager().find(BlogEntry.class, id);
		return blogEntry;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BlogEntry> getBlogEntries(BlogUser user) throws DAOException {
		return JPAEMProvider.getEntityManager()
				.createQuery("select entry from BlogEntry as entry where entry.creator=:creator")
				.setParameter("creator", user).getResultList();
	}

	@Override
	public void createUser(BlogUser user) throws DAOException {
		EntityManager em = JPAEMProvider.getEntityManager();

		if (user.getId() != null) {
			em.merge(user);
		} else {
			em.persist(user);
		}

		em.getTransaction().commit();
	}

	@Override
	public void createEntry(BlogEntry entry) throws DAOException {
		EntityManager em = JPAEMProvider.getEntityManager();

		if (entry.getId() != null) {
			em.merge(entry);
		} else {
			em.persist(entry);
		}

		em.getTransaction().commit();
	}

	@Override
	public void createComment(BlogComment comment) throws DAOException {
		EntityManager em = JPAEMProvider.getEntityManager();

		if (comment.getId() != null) {
			em.merge(comment);
		} else {
			em.persist(comment);
		}

		em.getTransaction().commit();

	}

	@SuppressWarnings("unchecked")
	@Override
	public BlogUser getUser(String nick) throws DAOException {
		List<BlogUser> users = JPAEMProvider.getEntityManager()
				.createQuery("select user from BlogUser as user where user.nick=:nick").setParameter("nick", nick)
				.getResultList();

		if (users.size() == 0) {
			return null;
		}

		return users.get(0);
	}

	@SuppressWarnings("unchecked")
	@Override
	public BlogUser getUser(String nick, String password) throws DAOException {

		System.out.println("Loggin in user " + nick + " with password hash " + password + ".");

		List<BlogUser> users = JPAEMProvider.getEntityManager()
				.createQuery(
						"select user from BlogUser as user where user.nick=:nick and user.passwordHash=:passwordHash")
				.setParameter("nick", nick).setParameter("passwordHash", password).getResultList();

		if (users.size() == 0) {
			return null;
		}

		return users.get(0);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BlogUser> getUsers() throws DAOException {
		List<BlogUser> users = JPAEMProvider.getEntityManager().createQuery("from BlogUser").getResultList();

		return users;
	}

}