package hr.fer.zemris.java.tecaj_13.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import hr.fer.zemris.java.tecaj_13.digester.PasswordDigester;

/**
 * Ovaj razred predstavlja model podataka za korisnika na blog aplikaciji. Model
 * koristi anotacije JPA protokola za upravljanje bazom podataka, naznačavajući
 * njegove atribute kao stupce jedne sql relacije.
 * 
 * @author Matej Balun
 *
 */
@Entity
@Table(name = "blog_users")
public class BlogUser {

	/**
	 * id korisnika, primarni kluč relacije.
	 */
	private Long id;

	/**
	 * ime korisnika
	 */
	private String firstName;

	/**
	 * prezime korisnika
	 */
	private String lastName;

	/**
	 * nadimak korisnika
	 */
	private String nick;

	/**
	 * email korisnika
	 */
	private String email;

	/**
	 * hash enkodirana lozinka korisnika
	 */
	private String passwordHash;

	/**
	 * lozinka korisnika
	 */
	private String password;

	/**
	 * zapisi korisnika.
	 */
	private List<BlogEntry> entries;

	/**
	 * Default konstruktor.
	 */
	public BlogUser() {
	}

	/**
	 * Vraća id korisnika
	 * 
	 * @return id korisnika
	 */
	@Id
	@GeneratedValue
	public Long getId() {
		return id;
	}

	/**
	 * Postavlja id korisnika
	 * 
	 * @param id
	 *            orisnika
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Vraća ime korisnika
	 * 
	 * @return ime korisnika
	 */
	@Column(length = 50, nullable = false)
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Postavlja ime korisnika
	 * 
	 * @param firstName
	 *            ime korisnika
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Vraća prezime korisnika
	 * 
	 * @return prezime korisnika
	 */
	@Column(length = 50, nullable = false)
	public String getLastName() {
		return lastName;
	}

	/**
	 * Postavlja prezime korisnika
	 * 
	 * @param lastName
	 *            prezime korisnika
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Vraća nadimak korisnika
	 * 
	 * @return nadimak korisnika
	 */
	@Column(length = 50, nullable = false, unique = true)
	public String getNick() {
		return nick;
	}

	/**
	 * vraća zapise korisnika
	 * 
	 * @return zapisi korisnika
	 */
	@OneToMany(mappedBy = "creator", fetch = FetchType.LAZY, cascade = CascadeType.PERSIST, orphanRemoval = true)
	public List<BlogEntry> getEntries() {
		return entries;
	}

	/**
	 * Postavlja zapise korisnika
	 * 
	 * @param entries
	 *            zapisi korisnika
	 */
	public void setEntries(List<BlogEntry> entries) {
		this.entries = entries;
	}

	/**
	 * Postavlja nadimak korisnika
	 * 
	 * @param nick
	 *            nadimak korisnika
	 */
	public void setNick(String nick) {
		this.nick = nick;
	}

	/**
	 * Vraća email korisnika
	 * 
	 * @return email korisnika
	 */
	@Column(length = 50, nullable = false)
	public String getEmail() {
		return email;
	}

	/**
	 * Postavlja emial korisnjika.
	 * 
	 * @param email
	 *            email korisnika
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Vraća kodiranu lozinku korisnika
	 * 
	 * @return kodirana lozinka korisnika
	 */
	@Column(length = 50, nullable = false)
	public String getPasswordHash() {
		return passwordHash;
	}

	/**
	 * Postavlja kodiranu lozinku korisnika
	 * 
	 * @param passwordHash
	 *            kodirana lozinka korisnika
	 */
	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}

	/**
	 * Vraća lozinku korisnika
	 * 
	 * @return lozinka korisnika
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Postavlja lozinku korisnika
	 * 
	 * @param password
	 *            lozinka korisnika.
	 */
	public void setPassword(String password) {
		this.password = password;
		this.passwordHash = PasswordDigester.digest(password);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((nick == null) ? 0 : nick.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BlogUser other = (BlogUser) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (nick == null) {
			if (other.nick != null)
				return false;
		} else if (!nick.equals(other.nick))
			return false;
		return true;
	}
}
