package hr.fer.zemris.java.tecaj_13.forms;

import java.util.HashMap;
import java.util.Map;

/**
 * Ovaj abstraktni razred predstavlja generalni html formular. Nudi metode
 * provjere grešaka u formularu. Razredi koji naslijeđuju ovaj razred trebali bi
 * ponuditi konkretne implementacije fromulara za web aplikaciju.
 * 
 * @author Matej Balun
 *
 */
public abstract class AbstractForm {

	/**
	 * {@link Map} instanca koja drži eventualne greške u formularu.
	 */
	private Map<String, String> errors;

	/**
	 * Kodna stranica za čitanje podataka.
	 */
	protected static final String ENCODING = "UTF-8";

	/**
	 * Default konstruktor.
	 */
	public AbstractForm() {
		this.errors = new HashMap<>();
	}

	/**
	 * Provjerava dali je mapa pogrešaka prazna.
	 * 
	 * @return <code>true</code> ako nema spremljenih grešaka, inače
	 *         <code>false</code>
	 */
	public boolean hasErrors() {
		return !errors.isEmpty();
	}

	/**
	 * Čisti mapu s pogreškama.
	 */
	public void clearErrors() {
		errors.clear();
	}

	/**
	 * Provjerava dali postoji pogreška za određeni kluč formulara.
	 * 
	 * @param key
	 *            ključ dotične pogreške
	 * @return <code>true</code> ako pogreška postoji, inače <code>false</code>.
	 */
	public boolean hasError(String key) {
		return errors.containsKey(key);
	}

	/**
	 * Vraća poruku koja opisuje dotičnu pogrešku u formularu.
	 * 
	 * @param key
	 *            ključ pogreške.
	 * @return Poruka koja opisuje pogrešku.
	 */
	public String getError(String key) {
		return errors.get(key);
	}

	/**
	 * Vraća mapu pogrešaka za formular.
	 * 
	 * @return mapa pogrešaka za formular.
	 */
	public Map<String, String> getErrors() {
		return this.errors;
	}

	/**
	 * Briše pogrešku pod određenim ključem iz mape.
	 * 
	 * @param key
	 *            željeni ključ pogreške.
	 */
	public void clearError(String key) {
		if (errors.containsKey(key)) {
			errors.remove(key);
		}
	}

	/**
	 * metoda koja provjerava dali je specificirani sadržaj pofrešan, te ako
	 * jest, stavlja ga u mapu pogrešaka pod specificiranim klučem.
	 * 
	 * @param key
	 *            željeni ključ eventualne pogreške
	 * @param content
	 *            sadržaj za provjeru.
	 * @return <code>true</code> ako je sadržaj korektan, inače
	 *         <code>false</code>.
	 */
	protected boolean check(String key, String content) {
		if (content == null || content.isEmpty()) {
			errors.put(key, key + " cannot be a null-reference or empty");
			return false;
		}

		clearError(key);
		return true;
	}
}
