package hr.fer.zemris.java.tecaj_13.forms;

import java.io.UnsupportedEncodingException;
import javax.servlet.http.HttpServletRequest;

import hr.fer.zemris.java.tecaj_13.model.BlogUser;

/**
 * Ovaj razred predstavlja model podataka za formular pri unosu podaka za login
 * korisnika na web aplikaciji. naslijeđuje {@link AbstractForm} razred.
 * 
 * @author Matej Balun
 *
 */
public class FormLogin extends AbstractForm {

	/**
	 * nadimak modela
	 */
	private String modelNick;

	/**
	 * lozinka modela
	 */
	private String modelPassword;

	/**
	 * nadimak formulara
	 */
	private String nick;

	/**
	 * lozinka formulara
	 */
	private String password;

	/**
	 * Default konstruktor koji poziva konstruktor nad-razreda
	 * {@link AbstractForm}.
	 */
	public FormLogin() {
		super();
	}

	/**
	 * vraća nadimak u loginu.
	 * 
	 * @return nadimak korisnika.
	 */
	public String getNick() {
		return nick;
	}

	/**
	 * Postavlja nadimak u loginu
	 * 
	 * @param nick
	 *            nadimak
	 */
	public void setNick(String nick) {
		this.nick = nick;
	}

	/**
	 * Vraća lozinku formulara
	 * 
	 * @return lozinka formulara.
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Postavlja lozinku formulara.
	 * 
	 * @param password
	 *            lozinka formulara.
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Puni atribute formulara pomoću {@link HttpServletRequest} primjerka
	 * razreda.
	 * 
	 * @param request
	 *            primjerak razreda {@link HttpServletRequest}.
	 */
	public void fillfromRequest(HttpServletRequest request) {
		try {
			nick = new String(request.getParameter("nick").getBytes(), ENCODING);
			password = new String(request.getParameter("password").getBytes(), ENCODING);
		} catch (UnsupportedEncodingException e) {
		}
	}

	/**
	 * Provjerava postoje li pogreške u formularu.
	 * 
	 * @return <code>true</code> ako postoje pogreške, inače <code>false</code>.
	 */
	public boolean checkErrors() {
		if (check("nick", nick)) {
			modelNick = nick;
		}

		if (check("password", password)) {
			modelPassword = password;
		}

		return hasErrors();
	}

	/**
	 * Puni atribute formulara iz primjerka {@link BlogUser} razreda.
	 * 
	 * @param user
	 *            primjerak {@link BlogUser} razreda.
	 */
	public void createFromModel(BlogUser user) {
		modelNick = user.getNick();
		modelPassword = user.getPassword();

		if (modelNick != null) {
			nick = new String(modelNick);
		}

		if (modelPassword != null) {
			password = new String(modelPassword);
		}
	}

	/**
	 * Puni primjerak {@link BlogUser} razreda pomoću atributa formulara.
	 * 
	 * @param user
	 *            primjerak {@link BlogUser} razreda.
	 */
	public void toModel(BlogUser user) {
		user.setNick(modelNick);
		user.setPassword(modelPassword);
	}
}
