package hr.fer.zemris.java.tecaj_13.dao;

import hr.fer.zemris.java.tecaj_13.DAO;
import hr.fer.zemris.java.tecaj_13.dao.jpa.JPADAOImpl;

/**
 * Ovaj razred predstavlja singleton obrazac za dohvat primjerka razreda koji
 * implementira sučelje {@link DAO} te nudi operacije u sloju za perzistenciju
 * podataka.
 * 
 * @author Matej Balun
 *
 */
public class DAOProvider {

	/**
	 * Primjerak singleton objekta koji implementira sučelje {@link DAO}.
	 */
	private static DAO dao = new JPADAOImpl();

	/**
	 * Vraća referencu na primjera razreda koji implementira sučelje {@link DAO}
	 * .
	 * 
	 * @return priomjerak razreda koji implementira sučelje {@link DAO}.
	 */
	public static DAO getDAO() {
		return dao;
	}

}