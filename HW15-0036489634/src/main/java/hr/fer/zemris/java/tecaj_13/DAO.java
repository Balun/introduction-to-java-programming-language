package hr.fer.zemris.java.tecaj_13;

import java.util.List;

import hr.fer.zemris.java.tecaj_13.dao.DAOException;
import hr.fer.zemris.java.tecaj_13.model.BlogComment;
import hr.fer.zemris.java.tecaj_13.model.BlogEntry;
import hr.fer.zemris.java.tecaj_13.model.BlogUser;

/**
 * Ovo sučelje predstavlja strukturu operacija za dohvat i spremanje podata što
 * čini sloj za perzistenciju podataka blog aplikacije. Funkcionalnost je
 * ostvarena pomoću JPA.
 * 
 * @author Matej Balun
 *
 */
public interface DAO {

	/**
	 * Dohvaća entry sa zadanim <code>id</code>-em. Ako takav entry ne postoji,
	 * vraća <code>null</code>.
	 * 
	 * @param id
	 *            ključ zapisa
	 * @return entry ili <code>null</code> ako entry ne postoji
	 * @throws DAOException
	 *             ako dođe do pogreške pri dohvatu podataka
	 */
	public BlogEntry getBlogEntry(Long id) throws DAOException;

	/**
	 * Vraća listu zapisa na blogu iz baze podataka.
	 * 
	 * @param user
	 *            vlasnik zapisa
	 * @return lista zapisa iz baze podataka
	 * @throws DAOException
	 *             ako je došlo do pogreške u dohvatu.
	 */
	public List<BlogEntry> getBlogEntries(BlogUser user) throws DAOException;

	/**
	 * Stvara novog korisnika u bazi podataka.
	 * 
	 * @param user
	 *            novi korisnik
	 * @throws DAOException
	 *             ako je došlo do pogreške u stvaranju
	 */
	public void createUser(BlogUser user) throws DAOException;

	/**
	 * Stvara novi zapis u bazi podataka
	 * 
	 * @param entry
	 *            novi zapis
	 * @throws DAOException
	 *             ako je došlo do pogreške u stvaranju
	 */
	public void createEntry(BlogEntry entry) throws DAOException;

	/**
	 * Stvara novi komentar u bazi podataka
	 * 
	 * @param comment
	 *            novi komentar
	 * @throws DAOException
	 *             ako je došlo do pšogreške u stvaranju
	 */
	public void createComment(BlogComment comment) throws DAOException;

	/**
	 * Vraća korisnika iz baze podataka
	 * 
	 * @param nick
	 *            nadimak korisnika
	 * @return željeni korisnik
	 * @throws DAOException
	 *             ako je došlo do pogreške u dohvatu
	 */
	public BlogUser getUser(String nick) throws DAOException;

	/**
	 * Vraća korisnika iz baze podataka
	 * 
	 * @param nick
	 *            nadimak korisnika
	 * @param password
	 *            lozinka korisnika
	 * @return željeni korisnik
	 * @throws DAOException
	 *             ako je došlo do pogreške u dohvatu
	 */
	public BlogUser getUser(String nick, String password) throws DAOException;

	/**
	 * Vraća listu svih korisnika iz baze podataka
	 * 
	 * @return svi korisnici iz baze podataka
	 * @throws DAOException
	 *             ako je došlo do pogreške u dohvatu.
	 */
	public List<BlogUser> getUsers() throws DAOException;
}