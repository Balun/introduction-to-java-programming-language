package hr.fer.zemris.java.tecaj_13.web.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Osnovni servlet koji preusmjerava zahtjev na lavnu stranicu blog aplikacije.
 * 
 * @author Matej Balun
 *
 */
@WebServlet(name = "index", urlPatterns = { "/index.jsp", "/index", "/" })
public class IndexServlet extends HttpServlet {

	/**
	 * UID zapisa
	 */
	private static final long serialVersionUID = -9087457223658852582L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.sendRedirect(req.getContextPath() + "/servleti/main");
	}
}
