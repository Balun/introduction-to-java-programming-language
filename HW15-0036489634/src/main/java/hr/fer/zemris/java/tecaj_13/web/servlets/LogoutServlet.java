package hr.fer.zemris.java.tecaj_13.web.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet koji služi za odjavu dotičnog korisnika sa bloga.
 * 
 * @author Matej Balun
 *
 */
@WebServlet(name = "logout", urlPatterns = "/servleti/logout")
public class LogoutServlet extends HttpServlet {

	/**
	 * UID za ovaj razred.
	 */
	private static final long serialVersionUID = -1770316178727453773L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getSession().removeAttribute("current.user.id");
		req.getSession().removeAttribute("current.user.name");
		req.getSession().removeAttribute("current.user.surname");
		req.getSession().removeAttribute("current.user.nick");

		resp.sendRedirect(req.getContextPath() + "/servleti/main");
	}
}
