package hr.fer.zemris.java.tecaj_13.forms;

import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;

import hr.fer.zemris.java.tecaj_13.model.BlogUser;

/**
 * Ovaj razred predstavlja model podataka za formular pri unosu podaka za
 * registraciji korisnika na web aplikaciji. naslijeđuje {@link AbstractForm}
 * razred.
 * 
 * @author Matej Balun
 *
 */
public class FormRegister extends AbstractForm {

	/**
	 * ime modela
	 */
	private String modelName;

	/**
	 * prezime modela
	 */
	private String modelSurname;

	/**
	 * email modela
	 */
	private String modelEmail;

	/**
	 * nadimak modela
	 */
	private String modelNick;

	/**
	 * lozinka modela
	 */
	private String modelPassword;

	/**
	 * ime u formularu
	 */
	private String name;

	/**
	 * prezime u formularu
	 */
	private String surname;

	/**
	 * email u formularu
	 */
	private String email;

	/**
	 * nadimak u formularu
	 */
	private String nick;

	/**
	 * lozinka u formularu
	 */
	private String password;

	/**
	 * Default konstruktor koji poziva konstruktor nad-razreda
	 * {@link AbstractForm}.
	 */
	public FormRegister() {
		super();
	}

	/**
	 * Vraća ime u formularu.
	 * 
	 * @return ime u formularu
	 */
	public String getName() {
		return name;
	}

	/**
	 * Postavlja ime u formularu.
	 * 
	 * @param name
	 *            ime u fromularu
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Vraća prezime formulara.
	 * 
	 * @return prezime formulara
	 */
	public String getSurname() {
		return surname;
	}

	/**
	 * Postavlja prezime formulara.
	 * 
	 * @param surname
	 *            prezime formulara
	 */
	public void setSurname(String surname) {
		this.surname = surname;
	}

	/**
	 * Vraća email formulara.
	 * 
	 * @return email formulara
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Postavlja emial formulara.
	 * 
	 * @param email
	 *            email formulara
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * vraća nadimak formulara.
	 * 
	 * @return nadimak formulara
	 */
	public String getNick() {
		return nick;
	}

	/**
	 * Postalvja nadimak formulara.
	 * 
	 * @param nick
	 *            nadimak formulara.
	 */
	public void setNick(String nick) {
		this.nick = nick;
	}

	/**
	 * Vraća lozinku formulara.
	 * 
	 * @return lozinka formulara.
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Postavlja lozinku formulara.
	 * 
	 * @param password
	 *            lozinka formulara.
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Puni atribute formulara iz primjerka {@link HttpServletRequest} razreda.
	 * 
	 * @param request
	 *            primjerak {@link HttpServletRequest} razreda.
	 */
	public void fillFromRequest(HttpServletRequest request) {
		try {
			name = new String(request.getParameter("name").getBytes(), ENCODING);
			surname = new String(request.getParameter("surname").getBytes(), ENCODING);
			email = new String(request.getParameter("email").getBytes(), ENCODING);
			nick = new String(request.getParameter("nick").getBytes(), ENCODING);
			password = new String(request.getParameter("password").getBytes(), ENCODING);
		} catch (UnsupportedEncodingException e) {
		}
	}

	/**
	 * Provjerava postoje li pogreške u formularu.
	 * 
	 * @return <code>true</code> ako postoje pogreške, inalče <code>false</code>
	 *         .
	 */
	public boolean checkErrors() {
		if (check("name", name)) {
			modelName = name;
		}

		if (check("surname", surname)) {
			modelSurname = surname;
		}

		if (check("nick", nick)) {
			modelNick = nick;
		}

		if (check("password", password)) {
			modelPassword = password;
		}

		if (check("email", email)) {
			modelEmail = email;
		}

		return hasErrors();
	}

	/**
	 * Puni atribute formulara iz modela koji predstavlja primjerak razreda
	 * {@link BlogUser}.
	 * 
	 * @param user
	 *            primjerak razreda {@link BlogUser}.
	 */
	public void createfromModel(BlogUser user) {
		modelEmail = user.getEmail();
		modelName = user.getFirstName();
		modelNick = user.getLastName();
		modelPassword = user.getPassword();
		modelSurname = user.getLastName();

		if (modelName != null) {
			name = modelName;
		}

		if (modelSurname != null) {
			surname = modelSurname;
		}

		if (modelEmail != null) {
			email = modelEmail;
		}

		if (modelPassword != null) {
			password = modelPassword;
		}

		if (modelNick != null) {
			nick = modelNick;
		}
	}

	/**
	 * Puni model koji predstavlja primjerak razreda {@link BlogUser}.
	 * 
	 * @param user
	 *            primjerak razreda {@link BlogUser}.
	 */
	public void toModel(BlogUser user) {
		user.setEmail(modelEmail);
		user.setFirstName(modelName);
		user.setNick(modelNick);
		user.setPassword(modelPassword);
		user.setLastName(modelSurname);
	}
}
