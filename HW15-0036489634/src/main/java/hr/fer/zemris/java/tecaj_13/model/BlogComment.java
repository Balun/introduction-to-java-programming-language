package hr.fer.zemris.java.tecaj_13.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Ovaj razred predstavlja model podataka za komentar na zapis bloga na blog
 * aplikaciji. Model koristi anotacije JPA protokola za upravljanje bazom
 * podataka, naznačavajući njegove atribute kao stupce jedne sql relacije.
 * 
 * @author Matej Balun
 *
 */
@Entity
@Table(name = "blog_comments")
public class BlogComment {

	/**
	 * id komentara
	 */
	private Long id;

	/**
	 * roditelj blog zapis komentara
	 */
	private BlogEntry blogEntry;

	/**
	 * email korisnika koji je napisao komentar
	 */
	private String userEmail;

	/**
	 * Sadržaj komentara
	 */
	private String message;

	/**
	 * vrijeme objavljivanja komentara.
	 */
	private Date postedOn;

	/**
	 * Default konstruktor.
	 */
	public BlogComment() {
	}

	/**
	 * Vraća Id komentara, primarni kluč relacije.
	 * 
	 * @return id komentara
	 */
	@Id
	@GeneratedValue
	public Long getId() {
		return id;
	}

	/**
	 * Postavlja id komentara.
	 * 
	 * @param id
	 *            id komentara.
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Vraća Matični zapis komentara.
	 * 
	 * @return matični zapis komenntara.
	 */
	@ManyToOne
	@JoinColumn(nullable = false)
	public BlogEntry getBlogEntry() {
		return blogEntry;
	}

	/**
	 * Postavlja matični zapis komentara.
	 * 
	 * @param blogEntry
	 *            matični zapis komentara.
	 */
	public void setBlogEntry(BlogEntry blogEntry) {
		this.blogEntry = blogEntry;
	}

	/**
	 * Vraća email korisnika autora komentara.
	 * 
	 * @return email autora komentara.
	 */
	@Column(length = 100, nullable = false)
	public String getUsersEMail() {
		return userEmail;
	}

	/**
	 * Postavlja email korisnika autora.
	 * 
	 * @param usersEMail
	 *            email korisnika autora
	 */
	public void setUsersEMail(String usersEMail) {
		this.userEmail = usersEMail;
	}

	/**
	 * Vraća sadržaj komentara.
	 * 
	 * @return sadržaj komentara.
	 */
	@Column(length = 4096, nullable = false)
	public String getMessage() {
		return message;
	}

	/**
	 * Postavlja sadržaj komentara
	 * 
	 * @param message
	 *            sadržaj komentara
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * Vraća datum objave komentara.
	 * 
	 * @return datum objave komentara.
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false)
	public Date getPostedOn() {
		return postedOn;
	}

	/**
	 * Postavlja datum objave komentara.
	 * 
	 * @param postedOn
	 *            datum objave komentara
	 */
	public void setPostedOn(Date postedOn) {
		this.postedOn = postedOn;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BlogComment other = (BlogComment) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}