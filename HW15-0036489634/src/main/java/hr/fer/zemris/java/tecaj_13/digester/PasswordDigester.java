package hr.fer.zemris.java.tecaj_13.digester;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Ovaj razred pruži metode za generiranje i provjeru generiranog sažetka za
 * generiranje lozinki u aplikaciji sa blogovima. Sažetak je generiran sa SHA-1
 * algoritmom.
 */
public class PasswordDigester {

	/**
	 * Primjerak razreda {@link MessageDigest} ta generiranje SHA-1 sažetka.
	 */
	private static MessageDigest algorithm;

	static {
		try {
			algorithm = MessageDigest.getInstance("SHA-1");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Privatni konstruktor koji zabranjuje instanciranje razreda.
	 */
	private PasswordDigester() {
	}

	/**
	 * Metoda koja provjerava konzistentnost lozinske pomoću generiranog
	 * sažetka.
	 * 
	 * @param message
	 *            sadržaj koji želimo sažeti algoritmom.
	 * @param expected
	 *            očekivani SHA-1 sažetak.
	 * @return <code>true</code> ako predpostavljeni sažetak odgovara
	 *         generiranom, inače <code>false</code>.
	 */
	public static boolean checkDigest(String message, String expected) {
		if (MessageDigest.isEqual(hexToByte(digest(message)), hexToByte(expected))) {
			return true;
		}

		return false;
	}

	/**
	 * Ova metoda generira sažetak poslanog sadržaja pomoću SHA-1 algoritma.
	 * 
	 * @param message
	 *            sadržaj za koji se želi generirati sažetak.
	 * @return polje bajtova koje predstavlja generirani sažetak.
	 */
	public static String digest(String message) {
		algorithm.update(message.getBytes(), 0, message.length());
		byte[] digest = algorithm.digest();
		algorithm.reset();

		return byteToHex(digest);
	}

	/**
	 * Pretvara heksadekadski string u polje bajtova.
	 * 
	 * @param hex
	 *            heksadekadski {@link String}
	 * @return pretvoreno polje bajtova
	 */
	private static byte[] hexToByte(String hex) {

		if (hex.length() % 2 == 1) {
			throw new IllegalArgumentException(
					"The hex string must be a valid hexadecimal format, with even number of characters.");
		}

		int length = hex.length();
		byte[] data = new byte[length / 2];
		for (int i = 0; i < length; i += 2) {
			data[i / 2] = (byte) ((Character.digit(hex.charAt(i), 16) << 4) + Character.digit(hex.charAt(i + 1), 16));
		}
		return data;
	}

	/**
	 * Metoda koja konvertira polje bajtova u string u heksadekadskom zapisu..
	 * 
	 * @param a
	 *            polje bajtova
	 * @return pretvoreni heksadekadski {@link String}
	 */
	private static String byteToHex(byte[] a) {
		StringBuilder sb = new StringBuilder(a.length * 2);
		for (byte b : a)
			sb.append(String.format("%02x", b & 0xff));
		return sb.toString();
	}
}
