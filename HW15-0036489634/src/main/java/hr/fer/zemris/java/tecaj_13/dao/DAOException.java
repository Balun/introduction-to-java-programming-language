package hr.fer.zemris.java.tecaj_13.dao;

/**
 * Ovaj razred prestavlja generalnu iznimku namijenjenu za pogreške u
 * operacijama vezan za sloj za perzistenciju podataka.
 * 
 * @author Matej Balun
 *
 */
public class DAOException extends RuntimeException {

	/**
	 * UID za ovaj razred.
	 */
	private static final long serialVersionUID = 1L;

	
	@SuppressWarnings("javadoc")
	public DAOException(String message, Throwable cause) {
		super(message, cause);
	}

	@SuppressWarnings("javadoc")
	public DAOException(String message) {
		super(message);
	}
}