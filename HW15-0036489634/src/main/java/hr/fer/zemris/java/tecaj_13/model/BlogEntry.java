package hr.fer.zemris.java.tecaj_13.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Ovaj razred predstavlja model podataka za zapis bloga na blog aplikaciji.
 * Model koristi anotacije JPA protokola za upravljanje bazom podataka,
 * naznačavajući njegove atribute kao stupce jedne sql relacije.
 * 
 * @author Matej Balun
 *
 */
@Entity
@Table(name = "blog_entries")
@Cacheable(true)
public class BlogEntry {

	/**
	 * id blog zapisa, primarni ključ relacije.
	 */
	private Long id;

	/**
	 * komentari zapisa.
	 */
	private List<BlogComment> comments = new ArrayList<>();

	/**
	 * Datum kreiranja zapisa.
	 */
	private Date createdAt;

	/**
	 * Datum zadnje modifikacije zapisa.
	 */
	private Date lastModifiedAt;

	/**
	 * Naslov zapisa.
	 */
	private String title;

	/**
	 * Sadržaj zapisa.
	 */
	private String text;

	/**
	 * Korisnik tvorac zapisa.
	 */
	private BlogUser creator;

	/**
	 * Default konstruktor.
	 */
	public BlogEntry() {
	}

	/**
	 * vraća id zapisa.
	 * 
	 * @return id zapisa.
	 */
	@Id
	@GeneratedValue
	public Long getId() {
		return id;
	}

	/**
	 * Postavlja id zapisa.
	 * 
	 * @param id
	 *            id zapisa
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Vraća komentare zapisa.
	 * 
	 * @return komentari zapisa.
	 */
	@OneToMany(mappedBy = "blogEntry", fetch = FetchType.LAZY, cascade = CascadeType.PERSIST, orphanRemoval = true)
	@OrderBy("postedOn")
	public List<BlogComment> getComments() {
		return comments;
	}

	/**
	 * Postavlja komentare zapisa.
	 * 
	 * @param comments
	 *            komentari zapisa.
	 */
	public void setComments(List<BlogComment> comments) {
		this.comments = comments;
	}

	/**
	 * Vraća datum kreiranja zapisa.
	 * 
	 * @return datum kreiranja zapsa.
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false)
	public Date getCreatedAt() {
		return createdAt;
	}

	/**
	 * postavlja datum kriranja zapisa.
	 * 
	 * @param createdAt
	 *            datum kriranja zapisa
	 */
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	/**
	 * Vraća datum zadnje promjene zapisa.
	 * 
	 * @return datum zadnje promjene zapisa.
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = true)
	public Date getLastModifiedAt() {
		return lastModifiedAt;
	}

	/**
	 * Postavlja datum zadnje promjene zapisa.
	 * 
	 * @param lastModifiedAt
	 *            datum zadnje promjene zapisa.
	 */
	public void setLastModifiedAt(Date lastModifiedAt) {
		this.lastModifiedAt = lastModifiedAt;
	}

	/**
	 * Vraća naslov zapisa
	 * 
	 * @return nasov zapisa
	 */
	@Column(length = 200, nullable = false)
	public String getTitle() {
		return title;
	}

	/**
	 * Postavlja naslov zapisa.
	 * 
	 * @param title
	 *            naslov zapisa
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Vraća sadržaj zapisa.
	 * 
	 * @return sadržaj zapisa.
	 */
	@Column(length = 4096, nullable = false)
	public String getText() {
		return text;
	}

	/**
	 * Postavlja sadržaj zaspisa.
	 * 
	 * @param text
	 *            sadržaj zapisa.
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * Vraća vlasnika zapisa.
	 * 
	 * @return vlasnik zapisa.
	 */
	@ManyToOne
	@JoinColumn(nullable = false)
	public BlogUser getCreator() {
		return creator;
	}

	/**
	 * Postavlja vlasnika zapisa.
	 * 
	 * @param creator
	 *            vlasnik zapisa.
	 */
	public void setCreator(BlogUser creator) {
		this.creator = creator;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BlogEntry other = (BlogEntry) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}