package hr.fer.zemris.java.cstr;

import java.util.regex.Pattern;

import org.junit.experimental.theories.internal.SpecificDataPointsSupplier;

/**
 * This class represents a custom class for operating with string type of data.
 * It offers similar methods as java {@link String} class, but the methods like
 * substring() have O(1) time complexity. The class operates with internally
 * shared array of characters, an all methods that change the content of the
 * string return the new string with the changed content.
 * 
 * @author Matej Balun (lunba)
 *
 */
public class CString {

	/**
	 * The internal character array.
	 */
	private char[] data;

	/**
	 * The initial offset in the character array for this string.
	 */
	private int offset;

	/**
	 * Length of this string.
	 */
	private int length;

	/**
	 * This constructor initializes this string with specified character array,
	 * offset and specified length.
	 * 
	 * @param data
	 *            - the specified character array
	 * @param offset
	 *            - the initial offset of the string
	 * @param length
	 *            - length of the string
	 */
	public CString(char[] data, int offset, int length) {
		if (data == null) {
			throw new NullPointerException("The data must not be a null-reference.");
		} else if (offset < 0 || offset >= data.length) {
			throw new IndexOutOfBoundsException(
					"The offset must be a value greater or equal than zero and less or equal to length - 1.");
		} else if (length > data.length) {
			throw new IndexOutOfBoundsException("The length cannot be grater than the length of the data array.");
		}

		this.data = data;
		this.offset = offset;
		this.length = length;
	}

	/**
	 * This constructor initializes the string with specified character array.
	 * 
	 * @param data
	 *            - the specified character array
	 */
	public CString(char data[]) {
		if (data == null) {
			throw new NullPointerException("The data must not be a null-reference.");
		}

		this.data = data;
		this.offset = 0;
		this.length = data.length;
	}

	/**
	 * This constructor initializes this string with specified other
	 * {@link CString}.
	 * 
	 * @param original
	 *            - the specified {@link CString} object.
	 */
	public CString(CString original) {
		if (original == null) {
			throw new NullPointerException("The original must not be a null-reference");
		}

		if (original.data.length > original.length) {
			this.data = new char[original.length];
			System.arraycopy(original.data, offset, this.data, 0, data.length);
		} else {
			this.data = original.data;
		}

		this.length = original.length;
		this.offset = 0;
	}

	/**
	 * Constructs the new {@link CString} based on the given {@link String}
	 * object.
	 * 
	 * @param s
	 *            - the specified {@link String} object
	 * @return the new {@link CString} object
	 */
	public static CString fromString(String s) {
		return new CString(s.toCharArray(), 0, s.length());
	}

	/**
	 * Returns the length of this {@link CString}.
	 * 
	 * @return new {@link CString} object
	 */
	public int length() {
		return this.length;
	}

	/**
	 * Returns the character on the specified position in the string.
	 * 
	 * @param index
	 *            - the specified index
	 * @return the specified character
	 */
	public char charAt(int index) {
		return this.data[index + offset];
	}

	/**
	 * Returns the representation of this {@link CString} int character array.
	 * 
	 * @return the character array of this {@link CString}
	 */
	public char[] toCharArray() {
		char[] array = new char[this.length];
		System.arraycopy(this.data, offset, array, 0, this.length);
		return array;
	}

	/**
	 * Returns the string representation of this {@link CString}.
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(this.length);
		for (int i = offset; i < length + offset; i++) {
			builder.append(data[i]);
		}

		return builder.toString();
	}

	/**
	 * Returns the index of the specified character in the string.
	 * 
	 * @param c
	 *            - the specified character
	 * @return index of the character
	 */
	public int indexOf(char c) {
		for (int i = offset; i < length + offset; i++) {
			if (data[i] == c) {
				return i - offset;
			}
		}

		return -1;
	}

	/**
	 * Checks if this {@link CString} starts with the specified {@link CString}.
	 * 
	 * @param s
	 *            - the specified {@link CString}
	 * @return true if this {@link CString} begins with the specified
	 *         {@link CString}, otherwise false.
	 */
	public boolean startsWith(CString s) {
		if (s.length > this.length) {
			return false;
		}

		for (int i = offset, j = 0; j < s.length; i++, j++) {
			if (data[i] != s.charAt(j)) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Checks if this {@link CString} ends with the specified {@link CString}...
	 * 
	 * @param s
	 *            - the specified {@link CString}
	 * @return true if this {@link CString} ends with the specified
	 *         {@link CString}, otherwise false.
	 */
	public boolean endsWith(CString s) {
		if (s.length > this.length) {
			return false;
		}

		for (int i = length - 1, j = s.length - 1; j >= 0; i--, j--) {
			if (data[i] != s.charAt(j)) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Returns true if this {@link CString} contains the specified
	 * {@link CString}.
	 * 
	 * @param s
	 *            - the specified {@link CString}
	 * @return true if this {@link CString} contains the specified
	 *         {@link CString}, otherwise false.
	 */
	public boolean contains(CString s) {
		if (s.length > this.length) {
			return false;
		}

		if (this.indexOf(s.charAt(0)) == -1) {
			return false;
		}

		boolean isSubstring = false;
		int i = 0;
		while (i < this.length) {

			isSubstring = true;
			if (data[i + offset] == s.charAt(0)) {
				for (int j = 0; j < s.length && i < length; j++, i++) {
					if (data[i + offset] != s.charAt(j)) {
						isSubstring = false;
						break;
					}
				}

				if (isSubstring == true && i < length) {
					return true;
				}
			} else {
				isSubstring = false;
				i++;
			}
		}

		return false;
	}

	/**
	 * Returns the substring of this {@link CString} specified with the start
	 * index, and end index - 1.
	 * 
	 * @param startIndex
	 *            - the start index of the substring
	 * @param endIndex
	 *            - the end index of the substring
	 * @return the new {@link CString} object
	 */
	public CString subString(int startIndex, int endIndex) {
		if (startIndex < offset || endIndex > length) {
			throw new IndexOutOfBoundsException(
					"The startIndex must be a value greater or equal to offset, and endIndex must"
							+ "be a value less or equal to length of the string.");
		}

		return new CString(this.data, startIndex, endIndex - startIndex);
	}

	/**
	 * Returns the new {@link CString} based on first "n" characters of this
	 * {@link CString}.
	 * 
	 * @param n
	 *            - the specified number of characters
	 * @return the new {@link CString}
	 */
	public CString left(int n) {
		if (n > this.length || n < 0) {
			throw new IndexOutOfBoundsException(
					"The specified length must be grater than zero and less than length of this string.");
		}

		return new CString(this.data, 0, n);
	}

	/**
	 * Returns the new {@link CString} based on the last n characters of this
	 * {@link CString}.
	 * 
	 * @param n
	 *            - the specified number of characters
	 * @return the new {@link CString}
	 */
	public CString right(int n) {
		if (n > this.length || n < 0) {
			throw new IndexOutOfBoundsException(
					"The specified length must be grater than zero and less than length of this string.");
		}

		return new CString(this.data, this.length - n, n);
	}

	/**
	 * Concatenates the specified {@link CString} to this {@link CString}.
	 * 
	 * @param s
	 *            - the specified {@link CString}
	 * @return the new concatenated {@link CString}
	 */
	public CString add(CString s) {
		if (s == null) {
			throw new NullPointerException("The string must not be a null-reference");
		}

		char[] array = new char[this.length + s.length];
		System.arraycopy(this.data, offset, array, 0, this.length);
		System.arraycopy(s.data, s.offset, array, this.length, s.length);

		return new CString(array);
	}

	/**
	 * Replaces all of the specified characters in this {@link CString} with the
	 * new specified character.
	 * 
	 * @param oldChar
	 *            - the specified character to replace
	 * @param newChar
	 *            - the new character
	 * @return the new {@link CString}
	 */
	public CString replaceAll(char oldChar, char newChar) {
		char[] array = new char[this.length];

		for (int i = 0; i < this.length; i++) {
			if (data[i + offset] == oldChar) {
				array[i] = newChar;
			} else {
				array[i] = data[i + offset];
			}
		}

		return new CString(array);
	}

	/**
	 * Replaces all specified substrings of this {@link CString} with new
	 * {@link SpecificDataPointsSupplier} {@link CString}s'.
	 * 
	 * @param oldString
	 *            - the specified ol substring
	 * @param newString
	 *            - the new {@link CString} to replace
	 * @return the new {@link CString}
	 */
	public CString replaceAll(CString oldString, CString newString) {
		if (oldString == null || newString == null) {
			throw new NullPointerException("The parameters must not be null-references");
		}

		String replaced = Pattern.compile(oldString.toString()).matcher(this.toString())
				.replaceAll(newString.toString());
		return fromString(replaced);
	}

}
