package hr.fer.zemris.java.graphics.raster;

/**
 * This interfaces specifies methods for operating with the blank and white
 * raster.
 * 
 * @author Matej Balun
 *
 */
public interface BWRaster {

	/**
	 * Returns the width of the raster
	 * 
	 * @return width of the raster
	 */
	int getWidth();

	/**
	 * Returns the height of the raster.
	 * 
	 * @return height of the raster
	 */
	int getHeight();

	/**
	 * Clears the raster, setting all the pixels to false.
	 */
	void clear();

	/**
	 * Turns on the specified pixel
	 * 
	 * @param x
	 *            - x-coordinate of the pixel
	 * @param y
	 *            - y-coordinate of the pixel
	 */
	void turnOn(int x, int y);

	/**
	 * Turns off the specified pixel
	 * 
	 * @param x
	 *            - x-coordinate of the pixel
	 * @param y
	 *            - y-coordinate of the pixel
	 */
	void turnOff(int x, int y);

	/**
	 * Enables the flip mode of the raster
	 */
	void enableFlipMode();

	/**
	 * Disables the flip mode of the raster.
	 */
	void disableFlipMode();

	/**
	 * Checks if the specified pixel is turned on.
	 * 
	 * @param x
	 *            - x-coordinate of the pixel
	 * @param y
	 *            - y-coordinate of the pixel
	 * @return true if the pixel is turned on, otherwise false
	 */
	boolean isTurnedOn(int x, int y);

	/**
	 * Returns true if the flip mode is enabled.
	 * 
	 * @return - true if the flip mode is enabled, otherwise false
	 */
	boolean getMode();
}
