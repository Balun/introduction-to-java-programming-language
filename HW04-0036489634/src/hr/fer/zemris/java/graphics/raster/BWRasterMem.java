package hr.fer.zemris.java.graphics.raster;

/**
 * This class implements the {@link BWRaster} interface with its methods for
 * manipulating the black and white raster,
 * 
 * @author Matej Balun
 *
 */
public class BWRasterMem implements BWRaster {

	/**
	 * Width of the raster
	 */
	private int width;

	/**
	 * Height of the raster
	 */
	private int height;

	/**
	 * Boolean array representing the raster
	 */
	private boolean[][] raster;

	/**
	 * The flip mode of the raster
	 */
	private boolean filpMode;

	/**
	 * This constructor initializes the raster with specified width and height.
	 * 
	 * @param width
	 *            - the specified width
	 * @param height
	 *            - the specified height
	 */
	public BWRasterMem(int width, int height) {
		if (width < 1 || height < 1) {
			throw new IllegalArgumentException("The width and height must be greater or equal to 1");
		}

		this.filpMode = false;
		this.height = height;
		this.width = width;
		this.raster = new boolean[width][height];

		for (int i = 0; i < this.width; i++) {
			for (int j = 0; j < this.height; j++) {
				this.raster[i][j] = false;
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean getMode() {
		return this.filpMode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getWidth() {
		return this.width;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getHeight() {
		return this.height;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void clear() {
		for (int i = 0; i < this.width; i++) {
			for (int j = 0; j < this.height; j++) {
				this.raster[i][j] = false;
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void turnOn(int x, int y) {
		checkCoordinates(x, y);

		if (!this.filpMode) {
			this.raster[x][y] = true;

		} else {

			if (this.raster[x][y]) {
				this.raster[x][y] = false;
			} else {
				this.raster[x][y] = true;
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void turnOff(int x, int y) {
		checkCoordinates(x, y);
		this.raster[x][y] = false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void enableFlipMode() {
		this.filpMode = true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void disableFlipMode() {
		this.filpMode = false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isTurnedOn(int x, int y) {
		checkCoordinates(x, y);
		return this.raster[x][y];
	}

	/**
	 * Checks if the coordinates are valid.
	 * 
	 * @param x
	 *            - the x-coordinate
	 * @param y
	 *            - the y-coordinate
	 */
	private void checkCoordinates(int x, int y) {
		if (x < 0 || x > this.width) {
			System.out.println(this.width);
			throw new IllegalArgumentException(
					"The x-coordinate must be greater or equal to zero and less or equal to width");
		} else if (y < 0 || y > this.height) {
			throw new IllegalArgumentException(
					"The y-coordinate must be greater or equal to zero and less or equal to height");
		}
	}

}
