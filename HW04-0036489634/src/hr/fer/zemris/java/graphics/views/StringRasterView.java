package hr.fer.zemris.java.graphics.views;

import hr.fer.zemris.java.graphics.raster.BWRaster;

/**
 * This class implements the {@link RasterView} interface, representing the
 * raster view returned as a {@link String} object.
 * 
 * @author Matej Balun
 *
 */
public class StringRasterView implements RasterView {

	/**
	 * The on character
	 */
	private char on;

	/**
	 * The off character
	 */
	private char off;

	/**
	 * Initializes the raster, with its on and off character parameters.
	 * 
	 * @param on
	 *            - the on character
	 * @param off
	 *            - the off character
	 */
	public StringRasterView(char on, char off) {
		this.on = on;
		this.off = off;
	}

	/**
	 * The default constructor for this view.
	 */
	public StringRasterView() {
		this('*', '.');
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @return string representation of the view.
	 */
	@Override
	public Object produce(BWRaster raster) {
		StringBuilder builder = new StringBuilder();

		for (int x = 0; x < raster.getWidth(); x++) {
			for (int y = 0; y < raster.getHeight(); y++) {
				if (raster.isTurnedOn(x, y)) {
					builder.append(on);
				} else {
					builder.append(off);
				}
			}
			builder.append('\n');
		}

		return builder.toString();
	}

}
