package hr.fer.zemris.java.graphics.views;

import hr.fer.zemris.java.graphics.raster.BWRaster;

/**
 * This interface specifies the view for the specified raster.
 * 
 * @author Matej Balun
 *
 */
public interface RasterView {

	/**
	 * Produces the image of the specified raster.
	 * 
	 * @param raster
	 *            - the specified raster
	 */
	Object produce(BWRaster raster);

}
