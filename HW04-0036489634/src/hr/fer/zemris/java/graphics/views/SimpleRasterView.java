package hr.fer.zemris.java.graphics.views;

import hr.fer.zemris.java.graphics.raster.BWRaster;

/**
 * This class implements the {@link RasterView} interface specifying the produce
 * method with drawing a picture to the standard output.
 * 
 * @author MAtej Balun
 *
 */
public class SimpleRasterView implements RasterView {

	/**
	 * The character for pixels that are on
	 */
	private char on;

	/**
	 * The character for pixels that are off
	 */
	private char off;

	/**
	 * Initializes the raster, with its on and off character parameters.
	 * 
	 * @param on
	 *            - the on character
	 * @param off
	 *            - the off character
	 */
	public SimpleRasterView(char on, char off) {
		this.off = off;
		this.on = on;
	}

	/**
	 * The default constructor for this raster.
	 */
	public SimpleRasterView() {
		this('*', '.');
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object produce(BWRaster raster) {
		for (int x = 0; x < raster.getWidth(); x++) {
			for (int y = 0; y < raster.getHeight(); y++) {
				if (raster.isTurnedOn(x, y)) {
					System.out.print(on);
				} else {
					System.out.print(off);
				}
			}
			System.out.println();
		}

		return null;
	}

}
