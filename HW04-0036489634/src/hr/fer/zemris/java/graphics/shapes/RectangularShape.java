package hr.fer.zemris.java.graphics.shapes;

import hr.fer.zemris.java.graphics.raster.BWRaster;

/**
 * This class represents the general rectangular geometric shape. It extends the
 * {@link GeometricShape} abstract class.
 * 
 * @author Matej Balun
 */
public abstract class RectangularShape extends GeometricShape {

	/**
	 * The x-coordinate of the upper left corner
	 */
	private int x;

	/**
	 * the y-coordinate of the upper left corner
	 */
	private int y;

	/**
	 * Width of the rectangular shape
	 */
	private int width;

	/**
	 * Height of the rectangular shape
	 */
	private int height;

	/**
	 * Initializes the rectangular shape.
	 * 
	 * @param x
	 *            - x-coordinate of the upper left corner
	 * @param y
	 *            - y-coordinate of the upper left corner
	 * @param width
	 *            - width of the shape
	 * @param height
	 *            - height of the shape
	 */
	public RectangularShape(int x, int y, int width, int height) {
		if (width < 1 || height < 1) {
			throw new IllegalArgumentException("the width or height must be greater than zero.");
		}

		this.height = height;
		this.width = width;
		this.x = x;
		this.y = y;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean containsPoint(int x, int y) {
		if (x < this.x) {
			return false;
		} else if (y < this.y) {
			return false;
		} else if (x >= this.x + this.width) {
			return false;
		} else if (y >= this.y + this.height) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void draw(BWRaster raster) {
		for (int i = this.x; i < this.x + width; i++) {
			for (int j = this.y; j < this.y + height; j++) {
				if (i > 0 && i < raster.getWidth() && j > 0 && j < raster.getHeight()) {
					if (this.containsPoint(i, j)) {
						raster.turnOn(i, j);
					}
				}
			}
		}
	}

	/**
	 * Returns the x-coordinate of the upper left corner.
	 * 
	 * @return x-coordinate of the upper left corner
	 */
	public int getX() {
		return x;
	}

	/**
	 * Sets the x-coordinate of the upper left corner.
	 * 
	 * @param x
	 *            - the x-coordinate of the upper left corner
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * Returns the y-coordinate of the upper left corner
	 * 
	 * @return - y-coordinate of the upper left corner
	 */
	public int getY() {
		return y;
	}

	/**
	 * Sets the y-coordinate of the upper left corner.
	 * 
	 * @param y
	 *            - y-coordinate of the upper left corenr
	 */
	public void setY(int y) {
		this.y = y;
	}

	/**
	 * Returns the width of the rectangular shape
	 * 
	 * @return width of the shape
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * Sets the width of the rectangular shape.
	 * 
	 * @param width
	 *            - width of the shape
	 */
	public void setWidth(int width) {
		if (width < 1) {
			throw new IllegalArgumentException("The width must be greater than zero");
		}

		this.width = width;
	}

	/**
	 * Returns the height of the rectangular shape.
	 * 
	 * @return height of the shape
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * Sets the height of the rectangular shape.
	 * 
	 * @param height
	 *            - height of the shape
	 */
	public void setHeight(int height) {
		if (height < 1) {
			throw new IllegalArgumentException("The height must be greater than zero");
		}

		this.height = height;
	}

}
