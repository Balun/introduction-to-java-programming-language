package hr.fer.zemris.java.graphics.shapes;

/**
 * This abstract class represents the general geometric shape, 
 * offering the general method for drawing the shape. The classes that
 * inherit this class must implement the containsPoint() method.
 * 
 * @author Matej Balun
 */
import hr.fer.zemris.java.graphics.raster.BWRaster;

public abstract class GeometricShape {

	/**
	 * Checks if this geometric shape contains the specified point
	 * 
	 * @param x
	 *            - x-coordinate of the point
	 * @param y
	 *            - y-coordinate of the point
	 * @return true if this shape contains the point, false otherwise
	 */
	public abstract boolean containsPoint(int x, int y);

	/**
	 * This method draws this geometric shape on the specified raster.
	 * 
	 * @param raster
	 *            - the specified raster
	 */
	public void draw(BWRaster raster) {
		for (int x = 0; x < raster.getWidth(); x++) {
			for (int y = 0; y < raster.getHeight(); y++) {
				if (this.containsPoint(x, y)) {
					raster.turnOn(x, y);
				}
			}
		}
	}

}
