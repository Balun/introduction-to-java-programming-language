package hr.fer.zemris.java.graphics.shapes;

/**
 * This class represents the ellipse geometric shape. It extends the abstract
 * class {@link Oval}.
 * 
 * @author Matej Balun
 *
 */
public class Ellipse extends Oval {

	/**
	 * This constructor passes the parameters of the ellipse to the super
	 * constructor.
	 * 
	 * @param x
	 *            - x-coordinate of the center.
	 * @param y
	 *            - y-coordinate of the center
	 * @param horizontal
	 *            - horizontal radius of the oval
	 * @param vertical
	 *            - vertical radius of the center
	 */
	public Ellipse(int x, int y, int horizontal, int vertical) {
		super(x, y, horizontal, vertical);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean containsPoint(int x, int y) {
		double equation = Math.round(Math.pow(x - getX(), 2) / (getHorizontal() * getHorizontal())
				+ Math.pow(y - getY(), 2) / (getVertical() * getVertical()));

		if (equation <= 1) {
			return true;
		} else {
			return false;
		}
	}

}
