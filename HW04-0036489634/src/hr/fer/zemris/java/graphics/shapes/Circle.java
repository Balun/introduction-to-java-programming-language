package hr.fer.zemris.java.graphics.shapes;

/**
 * This class represents the circle geometric shape. It extends the abstract
 * class {@link Oval}.
 * 
 * @author Matej Balun
 *
 */
public class Circle extends Oval {

	/**
	 * Initializes the circle passing its parameters to the super constructor.
	 * 
	 * @param x
	 *            - x-coordinate of the center
	 * @param y
	 *            - y-coordinate of the center
	 * @param radius
	 *            - radius of the circle
	 */
	public Circle(int x, int y, int radius) {
		super(x, y, radius, radius);
	}

	/**
	 * Returns the radius of the circle
	 * 
	 * @return radius of the circle
	 */
	public int getRadius() {
		return getHorizontal();
	}

	/**
	 * Sets the radius of the circle.
	 * 
	 * @param radius
	 *            radius of the circle
	 */
	public void setRadius(int radius) {
		if (radius < 1) {
			throw new IllegalArgumentException("The radius must be greater than zero.");
		}

		setHorizontal(radius);
		setVertical(radius);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHorizontal(int horizontal) {
		super.setHorizontal(horizontal);
		super.setVertical(horizontal);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setVertical(int vertical) {
		super.setVertical(vertical);
		super.setHorizontal(vertical);
	}

	@Override
	public boolean containsPoint(int x, int y) {
		if (Math.sqrt(Math.pow(x - getX(), 2)) + Math.pow(y - getY(), 2) <= getHorizontal()) {
			return true;
		}

		return false;
	}

}
