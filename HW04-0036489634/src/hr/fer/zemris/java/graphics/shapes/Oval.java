package hr.fer.zemris.java.graphics.shapes;

import hr.fer.zemris.java.graphics.raster.BWRaster;

/**
 * This class represents the oval geometric shape. It specifies the x and y
 * coordinates of the center of the oval, as well as the horizontal and vertical
 * radius of the oval.
 * 
 * @author Matej Balun
 *
 */
public abstract class Oval extends GeometricShape {

	/**
	 * x-coordinate of the center
	 */
	private int x;

	/**
	 * y-cordinae of the center
	 */
	private int y;

	/**
	 * horizontal radius of the oval
	 */
	private int horizontal;

	/**
	 * vertical radius of the oval
	 */
	private int vertical;

	/**
	 * This constructor initializes the oval.
	 * 
	 * @param x
	 *            - x-coordinate of the center
	 * @param y
	 *            - y-coordinate of the oval
	 * @param horizontal
	 *            - horizontal radius of the oval
	 * @param vertical
	 *            - vertical radius of the oval<
	 */
	public Oval(int x, int y, int horizontal, int vertical) {
		if (horizontal < 1 || vertical < 1) {
			throw new IllegalArgumentException("The radius must be greater than 0");
		}

		this.horizontal = horizontal;
		this.vertical = vertical;
		this.x = x;
		this.y = y;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void draw(BWRaster raster) {
		for (int i = getX() - getHorizontal(); i < getX() + getHorizontal(); i++) {
			for (int j = getY() - getVertical(); j < getY() + getVertical(); j++) {
				if (i > 0 && i < raster.getWidth() && j > 0 && j < raster.getHeight()) {
					if (containsPoint(i, j)) {
						raster.turnOn(i, j);
					}
				}
			}
		}
	}

	/**
	 * Returns the x-coordinate of the center
	 * 
	 * @return x-coordinate of the center
	 */
	public int getX() {
		return x;
	}

	/**
	 * Sets x-coordinate of the center
	 * 
	 * @param x
	 *            - x-coordinate of the center
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * Returns the y-coordinate of the center
	 * 
	 * @return - y-coordinate of the center
	 */
	public int getY() {
		return y;
	}

	/**
	 * Sets the y-coordinate of the center.
	 * 
	 * @param y
	 *            - y-coordinate of the center
	 */
	public void setY(int y) {
		this.y = y;
	}

	/**
	 * Returns the horizontal radius of the oval.
	 * 
	 * @return - the horizontal radius
	 */
	public int getHorizontal() {
		return this.horizontal;
	}

	/**
	 * Sets the horizontal radius of the oval.
	 * 
	 * @param horizontal
	 *            - horizontal radius of the oval
	 */
	public void setHorizontal(int horizontal) {
		if (horizontal < 1) {
			throw new IllegalArgumentException("The horizontal radius mus be greater than zero");
		}

		this.horizontal = horizontal;
	}

	/**
	 * Returns the vertical radius of the oval
	 * 
	 * @return - vertical radius of the oval
	 */
	public int getVertical() {
		return this.vertical;
	}

	/**
	 * Sets the vertical radius of the oval.
	 * 
	 * @param vertical
	 *            - vertical radius of the oval
	 */
	public void setVertical(int vertical) {
		if (vertical < 1) {
			throw new IllegalArgumentException("The vertical radius mus be greater than zero");
		}

		this.vertical = vertical;
	}

}
