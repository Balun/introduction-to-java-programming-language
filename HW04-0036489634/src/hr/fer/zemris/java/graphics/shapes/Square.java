package hr.fer.zemris.java.graphics.shapes;

/**
 * This class represents the square geometric shape. It inherits the abstract
 * class {@link RectangularShape}
 * 
 * @author Matej Balun
 *
 */
public class Square extends RectangularShape {

	/**
	 * Initializes the square, passing its parameters to the super constructor.
	 * 
	 * @param x
	 *            - x-coordinate of the square
	 * @param y
	 *            - y-coordinate of the square
	 * @param size
	 *            - soze of the square
	 */
	public Square(int x, int y, int size) {
		super(x, y, size, size);
	}

	/**
	 * Returns the size of the square.
	 * 
	 * @return size of the square
	 */
	public int getSize() {
		return this.getHeight();
	}

	/**
	 * Sets the size of the square
	 * 
	 * @param size
	 *            - size of the square
	 */
	public void setSize(int size) {
		super.setWidth(size);
		super.setHeight(size);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHeight(int height) {
		super.setHeight(height);
		super.setWidth(height);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setWidth(int width) {
		super.setHeight(width);
		super.setWidth(width);
	}

}
