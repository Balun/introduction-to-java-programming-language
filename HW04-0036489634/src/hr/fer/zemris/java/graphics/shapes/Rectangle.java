package hr.fer.zemris.java.graphics.shapes;

/**
 * This class represents the rectangle shape. It inherits the abstract class
 * {@link RectangularShape}.
 * 
 * @author Matej Balun
 *
 */
public class Rectangle extends RectangularShape {

	/**
	 * Initializes the rectangle, passing its parameters to the super
	 * constructor.
	 * 
	 * @param x
	 *            - x-coordinate of the upper left corner
	 * @param y
	 *            - y-coordinate of the upper left corner
	 * @param width
	 *            - width of the rectangle
	 * @param height
	 *            - height of the rectangle
	 */
	public Rectangle(int x, int y, int width, int height) {
		super(x, y, width, height);
	}

}
