package hr.fer.zemris.java.graphics.demo;

import java.util.Scanner;

import hr.fer.zemris.java.graphics.raster.BWRaster;
import hr.fer.zemris.java.graphics.raster.BWRasterMem;
import hr.fer.zemris.java.graphics.shapes.Circle;
import hr.fer.zemris.java.graphics.shapes.Ellipse;
import hr.fer.zemris.java.graphics.shapes.GeometricShape;
import hr.fer.zemris.java.graphics.shapes.Rectangle;
import hr.fer.zemris.java.graphics.shapes.Square;
import hr.fer.zemris.java.graphics.views.RasterView;
import hr.fer.zemris.java.graphics.views.SimpleRasterView;

/**
 * This is the demonstration program for demonstrating the work with various
 * types of geometric shapes. The program takes one or two command line
 * arguments: width and height of the specified raster (if only one argument is
 * provided the raster has the same width and height). The legal geometric
 * shapes are: Rectangle, square, ellipse and circle. The program draws the
 * shapes on the specified raster.
 * 
 * @author Matej Balun
 * @param args
 *            - the specified dimension(s) of the raster
 */
public class Demo {

	/**
	 * The main method that executes the program.
	 */
	public static void main(String[] args) {
		BWRaster raster = null;
		Scanner sc = new Scanner(System.in);

		switch (args.length) {

		case 1:
			raster = new BWRasterMem(Integer.parseInt(args[0]), Integer.parseInt(args[0]));
			break;

		case 2:
			raster = new BWRasterMem(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
			break;

		default:
			System.err.println("Invalid number of arguments.");
			System.exit(1);
			break;
		}

		System.out.println("please provide the number of elements:");

		int n = sc.nextInt();
		sc.nextLine();
		GeometricShape[] list = new GeometricShape[n];

		for (int i = 0; i < n; i++) {
			System.out.println("please provide a shape:");
			String[] line = sc.nextLine().split(" ");

			switch (line[0].toUpperCase().trim()) {

			case "FLIP":
				list[i] = null;
				break;

			case "RECTANGLE":
				if (line.length != 5) {
					System.err.println("Invalid number of arguments for rectangle.");
					System.exit(1);
				} else {
					list[i] = new Rectangle(Integer.parseInt(line[1]), Integer.parseInt(line[2]),
							Integer.parseInt(line[3]), Integer.parseInt(line[4]));
				}
				break;

			case "SQUARE":
				if (line.length != 4) {
					System.err.println("Invalid number of arguments for square.");
					System.exit(1);
				} else {
					list[i] = new Square(Integer.parseInt(line[1]), Integer.parseInt(line[2]),
							Integer.parseInt(line[3]));
				}
				break;

			case "ELLIPSE":
				if (line.length != 5) {
					System.err.println("Invalid number of arguments for ellipse.");
					System.exit(1);
				} else {
					list[i] = new Ellipse(Integer.parseInt(line[1]), Integer.parseInt(line[2]),
							Integer.parseInt(line[3]), Integer.parseInt(line[4]));
				}
				break;

			case "CIRCLE":
				if (line.length != 4) {
					System.err.println("Invalid number of arguments for circle.");
					System.exit(1);
				} else {
					list[i] = new Circle(Integer.parseInt(line[1]), Integer.parseInt(line[2]),
							Integer.parseInt(line[3]));
				}
				break;

			default:
				System.err.println("Invalid geometric shape or command.");
				System.exit(1);
				break;
			}

		}

		for (GeometricShape shape : list) {
			if (shape == null) {
				if (raster.getMode()) {
					raster.disableFlipMode();
				} else {
					raster.enableFlipMode();
				}
			} else {
				shape.draw(raster);
			}
		}

		RasterView view = new SimpleRasterView('*', '.');
		view.produce(raster);

		System.out.println();
		sc.close();
	}

}
