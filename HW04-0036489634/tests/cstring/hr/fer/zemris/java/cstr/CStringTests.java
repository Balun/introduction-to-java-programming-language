package hr.fer.zemris.java.cstr;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CStringTests {

	@Test
	public void testCreation() {
		CString string = new CString(String.valueOf("Imperator").toCharArray(), 0, 5);
		assertEquals("strings are not equal.", "Imper", string.toString());
		assertEquals("Wrong length", 5, string.length());
		assertEquals("Wrong character.", 'm', string.charAt(1));

		CString string2 = new CString(String.valueOf("Imperator").toCharArray());
		assertEquals("strings are not equal.", "Imperator", string2.toString());

		CString string3 = new CString(string2);
		assertEquals("strings are not equal", "Imperator", string3.toString());

	}

	@Test(expected = NullPointerException.class)
	public void testException1() {
		new CString(null, 3, 8);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testBounds() {
		new CString("Slavica".toCharArray(), 2, 12);
	}

	@Test
	public void testCharAt() {
		CString string = new CString("Imperator".toCharArray(), 0, 5);
		assertEquals("Wrong string.", "Imper", string.toString());
		assertEquals("invalid character.", 'I', string.charAt(0));
		assertEquals("invalid character.", 'p', string.charAt(2));
		assertEquals("invalid character.", 'r', string.charAt(4));

	}

	@Test
	public void testFromString() {
		CString string = CString.fromString("Dječak");
		assertEquals("wrong string.", "Dječak", string.toString());
	}

	@Test
	public void tetsLength() {
		CString string = new CString("Imperator".toCharArray(), 5, 4);
		assertEquals("Invalid length.", 4, string.length());
	}

	@Test
	public void testToCharArray() {
		char[] array = "sin".toCharArray();
		CString string = new CString("sin".toCharArray());
		assertEquals("Invalid array.", String.valueOf(array), String.valueOf(string.toCharArray()));
	}

	@Test
	public void testToString() {
		CString string = new CString("mačak".toCharArray(), 1, 3);
		assertEquals("Wrong string.", "ača", string.toString());
	}

	@Test
	public void testIndexOf() {
		CString string = new CString("Matej".toCharArray());
		assertEquals("Invalid index", -1, string.indexOf('k'));
		assertEquals("Invalid index", 3, string.indexOf('e'));
		assertEquals("Invalid index", 4, string.indexOf('j'));
	}

	@Test
	public void testStartsWith() {
		CString string = new CString("lenovo".toCharArray());
		assertEquals("Invalid statement", true, string.startsWith(new CString("len".toCharArray())));
		assertEquals("Invalid statement", false, string.startsWith(new CString("lenovo računalo".toCharArray())));
		assertEquals("Invalid statement", false, string.startsWith(new CString("kin".toCharArray())));
		assertEquals("Invalid statement", true, string.startsWith(new CString("lenovo".toCharArray())));
	}

	@Test
	public void testEndsWith() {
		CString string = new CString("lenovo".toCharArray());
		assertEquals("Invalid statement", true, string.endsWith(new CString("ovo".toCharArray())));
		assertEquals("Invalid statement", false, string.endsWith(new CString("lenovo računalo".toCharArray())));
		assertEquals("Invalid statement", false, string.endsWith(new CString("kin".toCharArray())));
		assertEquals("Invalid statement", true, string.endsWith(new CString("lenovo".toCharArray())));
	}

	@Test
	public void testContains() {
		CString string = new CString("lenovo".toCharArray());
		assertEquals("Invalid statement", true, string.contains(new CString("len".toCharArray())));
		assertEquals("Invalid statement", false, string.contains(new CString("ovor".toCharArray())));
		assertEquals("Invalid statement", false, string.contains(new CString("lenovo računalo".toCharArray())));
		assertEquals("Invalid statement", true, string.contains(new CString("nov".toCharArray())));
	}

	@SuppressWarnings("unused")
	@Test(expected = IndexOutOfBoundsException.class)
	public void testSubstring() {
		CString string = new CString("lenovo".toCharArray());
		assertEquals("Invalid string.", "len", string.subString(0, 3).toString());
		assertEquals("Invalid string.", "ovo", string.subString(3, 6).toString());
		assertEquals("Invalid string.", "nov", string.subString(2, 5).toString());

		CString string2 = string.subString(0, 9);
		CString string3 = string.subString(-1, 4);
	}

	@SuppressWarnings("unused")
	@Test(expected = IndexOutOfBoundsException.class)
	public void testLeft() {
		CString string = new CString("lenovo".toCharArray());
		assertEquals("Invalid string", "len", string.left(3).toString());
		assertEquals("Invalid string", "l", string.left(1).toString());
		assertEquals("Invalid string", "lenovo", string.left(6).toString());

		CString string2 = string.left(0);
		CString string3 = string.left(string.length() + 1);
	}

	@SuppressWarnings("unused")
	@Test(expected = IndexOutOfBoundsException.class)
	public void testRight() {
		CString string = new CString("lenovo".toCharArray());
		assertEquals("Invalid string", "ovo", string.right(3).toString());
		assertEquals("Invalid string", "lenovo", string.right(6).toString());
		assertEquals("Invalid string", "o", string.right(1).toString());

		CString string2 = string.right(0);
		CString string3 = string.right(string.length() + 1);
	}

	@SuppressWarnings("unused")
	@Test(expected = NullPointerException.class)
	public void testAdd() {
		CString string = new CString("lenovo".toCharArray());
		assertEquals("Invalid string", "lenovo računalo",
				string.add(new CString(" računalo".toCharArray())).toString());
		assertEquals("Invalid string", "lenovo", string.add(new CString("".toCharArray())).toString());

		CString string2 = string.add(null);
	}

	@Test
	public void testReplaceAll() {
		CString string = new CString("lenovo".toCharArray());
		assertEquals("Invalid string.", "len?v?", string.replaceAll('o', '?').toString());
		assertEquals("Invalid string.", "lenovo", string.replaceAll('o', 'o').toString());
		assertEquals("Invalid string.", "len v ", string.replaceAll('o', ' ').toString());
	}

	@SuppressWarnings("unused")
	@Test(expected = NullPointerException.class)
	public void testReplaceAll2() {
		CString string = new CString("lenovo".toCharArray());
		assertEquals("Invalid string.", "len123",
				string.replaceAll(new CString("ovo".toCharArray()), new CString("123".toCharArray())).toString());
		assertEquals("Invalid string.", "lea",
				string.replaceAll(new CString("novo".toCharArray()), new CString("a".toCharArray())).toString());

		CString string2 = new CString("ababab".toCharArray());
		assertEquals("Invalid string.", "abababababab",
				string2.replaceAll(new CString("ab".toCharArray()), new CString("abab".toCharArray())).toString());

		CString string3 = string.replaceAll(new CString("nov".toCharArray()), null);
	}

}
