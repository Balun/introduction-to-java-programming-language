package hr.fer.zemris.java.tecaj.hw3.prob1;

/**
 * This class represents a simple custom lexer. The lexer consumes the given
 * string into character array and produces tokens based on a specified set of
 * rules. The lexer defines several states acting as a deterministic formal
 * automata in some way. There are also several types of tokens specified for
 * more precise token verification. There are a lot of JUnit test available for
 * testing the lexer.
 * 
 * @author Matej Balun
 *
 */
public class Lexer {

	/**
	 * the character array for production of tokens.
	 */
	private char[] data;

	/**
	 * The current token.
	 */
	private Token token;

	/**
	 * The current index of the character array.
	 */
	private int currentIndex;

	/**
	 * The current state of the lexer.
	 */
	private LexerState state;

	/**
	 * This constructor initializes the lexer with the specified text and sets
	 * the lexer to the basic state.
	 * 
	 * @param text
	 *            - the specified text for production of tokens.
	 * @throws IllegalArgumentException
	 *             if the input string is a null-reference.
	 */
	public Lexer(String text) {
		if (text == null) {
			throw new IllegalArgumentException("The input string must not be a null-reference");
		}
		this.data = text.toCharArray();
		this.currentIndex = 0;
		this.state = LexerState.BASIC;
	}

	/**
	 * Sets the current state of the lexer.
	 * 
	 * @param state
	 *            - the specified state of the lexer.
	 * @throws IllegalArgumentException
	 *             if the specified state is a null-reference.
	 */
	public void setState(LexerState state) {
		if (state == null) {
			throw new IllegalArgumentException("The specified state cannot be a null-reference");
		}
		this.state = state;
	}

	/**
	 * Returns the current state of the lexer.
	 * 
	 * @return the current state of the lexer
	 */
	public LexerState getState() {
		return this.state;
	}

	/**
	 * This method consumes the next character in the array, determines the next
	 * lexer state, delegates the production of specified token and return the
	 * token
	 * 
	 * @return - the next specified token
	 * @throws LexerException
	 *             - if the lexer has already reached the end of file.
	 */
	public Token nextToken() {

		if (token != null && token.getType().equals(TokenType.EOF)) {
			throw new LexerException("The lexer has reached the end of file.");
		}

		if (currentIndex == data.length) {
			token = new Token(TokenType.EOF, null);
			return token;
		}

		if (this.state.equals(LexerState.EXTENDED)) {
			return getExtendedToken();
		}

		char current = data[currentIndex];

		if (Character.isLetter(current) || current == '\\') {
			return letterToken(current);

		} else if (Character.isDigit(current)) {
			return numberToken(current);

		} else if (Character.isWhitespace(current)) {
			return blankSpaceToken(current);

		} else {
			return symbolToken(current);
		}

	}

	/**
	 * Returns the current token of the lexer.
	 * 
	 * @return the current token
	 */
	public Token getToken() {
		return token;
	}

	/**
	 * This method produces the token in the specified extended state. It
	 * delegates the production of potential more complicated tokens to other
	 * methods.
	 * 
	 * @return the specified token in the extended state
	 */
	private Token getExtendedToken() {
		char current = data[currentIndex];

		if (current == '#') {
			currentIndex++;
			return new Token(TokenType.SYMBOL, current);
		}

		if (Character.isWhitespace(current)) {
			return blankSpaceToken(current);
		} else {
			return universalToken(current);
		}
	}

	/**
	 * Returns the next token consuming the characters and by legal expression
	 * into a word token.
	 * 
	 * @param current
	 *            - the current character of the string
	 * @return the new word token
	 */
	private Token universalToken(char current) {
		StringBuilder word = new StringBuilder();

		word.append(current);
		currentIndex++;

		while (currentIndex < data.length) {
			current = data[currentIndex];
			if (Character.isWhitespace(current)) {
				break;
			} else if (current == '#') {
				return new Token(TokenType.WORD, word.toString());
			} else {
				word.append(current);
				currentIndex++;
			}
		}

		return new Token(TokenType.WORD, word.toString());
	}

	/**
	 * This method consumes the characters and groups them into a word token
	 * consuming letters. The method consumes the legal escape sequence
	 * delegating the production to other method.
	 * 
	 * @param current
	 *            - the current character
	 * @return the new word token
	 */
	private Token letterToken(char current) {
		StringBuilder word = new StringBuilder();

		if (current == '\\') {
			word.append(escapeSequenceToken());
		} else {
			word.append(current);
		}

		currentIndex++;

		while (currentIndex < data.length) {
			current = data[currentIndex];
			if (Character.isLetter(current)) {
				word.append(current);
				currentIndex++;
			} else if (current == '\\') {
				word.append(escapeSequenceToken());
				currentIndex++;
			} else {
				break;
			}
		}

		token = new Token(TokenType.WORD, word.toString());
		return token;
	}

	/**
	 * This method consumes the characters and groups them into number token,
	 * depending on the correct number format.
	 * 
	 * @param current
	 *            - the current character
	 * @return the new number token
	 * @throws LexerException
	 *             if there is an error in number formatting.
	 */
	private Token numberToken(char current) {
		StringBuilder number = new StringBuilder();
		number.append(current);
		currentIndex++;

		while (currentIndex < data.length) {
			current = data[currentIndex];
			if (Character.isDigit(current)) {
				number.append(current);
				currentIndex++;
			} else {
				break;
			}
		}

		try {
			token = new Token(TokenType.NUMBER, Long.parseLong(number.toString()));
			return token;
		} catch (NumberFormatException e) {
			throw new LexerException("Error in number formatting.");
		}

	}

	/**
	 * Consumes the blank spaces in the character array and calls the nextToken
	 * method when the blank space sequence finishes.
	 * 
	 * @param current
	 *            - the current character
	 * @return the new delegated token
	 */
	private Token blankSpaceToken(char current) {
		currentIndex++;

		while (currentIndex < data.length) {
			current = data[currentIndex];
			if (Character.isWhitespace(current)) {
				currentIndex++;
			} else {
				break;
			}
		}

		return nextToken();
	}

	/**
	 * Returns the new symbol token based on a current character.
	 * 
	 * @param current
	 *            - the current character
	 * @return the new symbol token
	 */
	private Token symbolToken(char current) {
		currentIndex++;
		token = new Token(TokenType.SYMBOL, Character.valueOf(current));
		return token;
	}

	/**
	 * Consumes the possible escape sequences and returns the string
	 * representation of number or a backslash following the legal escape
	 * sequence symbol.
	 * 
	 * @return a string representation of the escape sequence content.
	 * @throws LexerException
	 *             if the escape sequence is invalid.
	 */
	private String escapeSequenceToken() {
		currentIndex++;

		if (currentIndex == data.length) {
			throw new LexerException("Invalid escape sequence.");

		} else if (Character.toString(data[currentIndex]).matches("[0-9]")) {
			return Character.toString(data[currentIndex]);

		} else if (data[currentIndex] == '\\') {
			return Character.toString(data[currentIndex]);

		} else {
			throw new LexerException("Invalid escape sequence.");
		}
	}

}
