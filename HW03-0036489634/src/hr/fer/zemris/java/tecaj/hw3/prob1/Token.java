package hr.fer.zemris.java.tecaj.hw3.prob1;

/**
 * This class represents the basic token for the custom {@link Lexer}. The token
 * consists of its type and value.
 * 
 * @author Matej Balun
 *
 */
public class Token {

	/**
	 * Type of the token.
	 */
	private TokenType type;

	/**
	 * Value of the token.
	 */
	private Object value;

	/**
	 * Initializes this token by specified type and value.
	 * 
	 * @param type
	 *            - type of the token
	 * @param value
	 *            - value of the token
	 */
	public Token(TokenType type, Object value) {
		this.type = type;
		this.value = value;
	}

	/**
	 * Return the type of the token.
	 * 
	 * @return type of the token.
	 */
	public Object getValue() {
		return value;
	}

	/**
	 * Returns the value of the token.
	 * 
	 * @return value of the token
	 */
	public TokenType getType() {
		return type;
	}

}
