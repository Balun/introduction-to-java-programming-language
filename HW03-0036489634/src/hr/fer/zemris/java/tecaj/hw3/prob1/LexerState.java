package hr.fer.zemris.java.tecaj.hw3.prob1;

/**
 * This enumeration defines the states for the custom {@link Lexer}.
 * 
 * @author Matej Balun
 *
 */
public enum LexerState {

	/**
	 * The basic state, set by default in lexer.
	 */
	BASIC,

	/**
	 * The extended state, set by the user when the specified symbol occurs.
	 */
	EXTENDED;

}
