package hr.fer.zemris.java.tecaj.hw3.prob1;

/**
 * This enumeration defines several token types for the custom {@link Lexer}.
 * 
 * @author Matej Balun
 *
 */
public enum TokenType {

	/**
	 * The end of file token.
	 */
	EOF,

	/**
	 * The word token.
	 */
	WORD,

	/**
	 * The number token.
	 */
	NUMBER,

	/**
	 * The symbol token.
	 */
	SYMBOL;

}
