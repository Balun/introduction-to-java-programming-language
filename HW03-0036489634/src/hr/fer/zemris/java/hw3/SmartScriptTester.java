package hr.fer.zemris.java.hw3;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import hr.fer.zemris.java.custom.scripting.nodes.DocumentNode;
import hr.fer.zemris.java.custom.scripting.nodes.ForLoopNode;
import hr.fer.zemris.java.custom.scripting.nodes.Node;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParserException;

/**
 * This program demonstrates the work of cutom implemented
 * {@link SmartScriptParser} for the Smart Script language. The program takes a
 * single command-line argument - a string containing the path to the specified
 * test text document. After building a parse tree, the program resolves the
 * nodes of the tree back to the original text (with some possible errors in
 * white spaces etc.).
 * 
 * (In the main directory of the project there are two sample files; doc1.txt
 * and doc2.txt. They are the actual documents given in the task, with some
 * blank spaces removed or added for extra testing of the parser.)
 * 
 * @param args
 *            - a single command line argument containing a path to the example
 *            document.
 * 
 * @author Matej Balun
 *
 */
public class SmartScriptTester {

	/**
	 * The main method that executes the program.
	 */
	public static void main(String[] args) {

		if (args.length != 1) {
			System.out.println("The argument should be a single string containing a path to the example file.");
			System.exit(-1);
		}

		String docBody = args[0].trim();
		SmartScriptParser parser = null;

		try {
			docBody = new String(Files.readAllBytes(Paths.get(docBody)), StandardCharsets.UTF_8);
			parser = new SmartScriptParser(docBody);
		} catch (IOException e) {
			System.err.println("could not open file.");
			System.exit(-1);
		} catch (SmartScriptParserException e) {
			System.err.println("Unable to parse document.");
			System.exit(-1);
		} catch (Exception e) {
			System.out.println("If this line ever executes, I have failed this class...");
		}

		System.out.println("\nThe original text:\n");
		DocumentNode document = parser.getDocumentNode();
		String originalDocumentBody = createOriginalDocumentBody(document);
		System.out.println(originalDocumentBody);
	}

	/**
	 * This method creates the original textual document body by traversing the
	 * parse tree and appending the textual representations of the nodes.
	 * 
	 * @param document
	 *            - the main {@link DocumentNode} of the parse tree
	 * @return the original document body
	 */
	public static String createOriginalDocumentBody(DocumentNode document) {
		StringBuilder originalDocument = new StringBuilder();

		for (int i = 0; i < document.numberOfChildren(); i++) {
			Node node = document.getChild(i);
			if (node instanceof ForLoopNode) {
				originalDocument.append(node.toString());
				walkForTree(node, originalDocument);
				originalDocument.append("{$END$}");
			} else {
				originalDocument.append(node.toString());
			}
		}

		return originalDocument.toString();
	}

	/**
	 * A recursive method that traverses the contents of a {@link ForLoopNode}
	 * node, appending their textual representations to the original document
	 * body.
	 * 
	 * @param node
	 *            - the specified {@link ForLoopNode}
	 * @param document
	 *            - String builder for appending the strings.
	 */
	private static void walkForTree(Node node, StringBuilder document) {
		ForLoopNode forNode = (ForLoopNode) node;

		for (int i = 0; i < forNode.numberOfChildren(); i++) {
			Node child = forNode.getChild(i);
			document.append(child.toString());
			if (child instanceof ForLoopNode) {
				document.append(child.toString());
				walkForTree(child, document);
				document.append("{$END$}");
			}
		}
	}

}
