package hr.fer.zemris.java.custom.scripting.lexer;

/**
 * This enumeration specifies the legal states of the lexer. The states can (and
 * should be) interpreted as the legal states of the deterministic final
 * automata. As this is a simple lexer, There are only three legal states that
 * help the lexer to produce valid tokens for the parser.
 * 
 * @author Matej Balun
 *
 */
public enum SmartScriptLexerState {

	/**
	 * A basic state of the lexer, specified for producing textual tokens
	 */
	BASIC,

	/**
	 * A state specified for producing tokens inside the tags (words, strings)
	 */
	EXTENDED,

	/**
	 * A state specified for simple production of string tokens, as strings have
	 * different rules.
	 */
	STRING;

}
