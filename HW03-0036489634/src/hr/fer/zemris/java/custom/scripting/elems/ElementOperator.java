package hr.fer.zemris.java.custom.scripting.elems;

/**
 * This class represents the mathematical operator element of the parser tree
 * node. It extends the {@link Element} class.
 * 
 * @author Matej Balun
 *
 */
public class ElementOperator extends Element {

	/**
	 * The value of this operator element.
	 */
	private String symbol;

	/**
	 * This constructor initializes this node operator element with its symbol
	 * value.
	 * 
	 * @param symbol
	 *            - the value of this operator element.
	 */
	public ElementOperator(String symbol) {
		this.symbol = symbol;
	}

	/**
	 * Return the textual representation of this element.
	 */
	@Override
	public String asText() {
		return this.symbol;
	}

}
