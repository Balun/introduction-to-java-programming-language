package hr.fer.zemris.java.custom.scripting.parser;

/**
 * This class implements the exception dor indicating errors in parsing of
 * {@link SmartScriptParser}. It extends the {@link RuntimeException} class.
 * 
 * @author Matej Balun
 *
 */
public class SmartScriptParserException extends RuntimeException {

	/**
	 * The serial version UID for this exception.
	 */
	private static final long serialVersionUID = 981395893165535867L;

	/**
	 * The default constructor.
	 */
	public SmartScriptParserException() {
		super();
	}

	/**
	 * This constructor passes a given message to the super class constructor.
	 * 
	 * @param message
	 *            - the specified message
	 */
	public SmartScriptParserException(String message) {
		super(message);
	}

}
