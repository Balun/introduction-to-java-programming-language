package hr.fer.zemris.java.custom.scripting.parser;

import java.util.Collection;
import java.util.Map;

import hr.fer.zemris.java.custom.scripting.elems.Element;
import hr.fer.zemris.java.custom.scripting.elems.ElementConstantDouble;
import hr.fer.zemris.java.custom.scripting.elems.ElementFunction;
import hr.fer.zemris.java.custom.scripting.elems.ElementOperator;
import hr.fer.zemris.java.custom.scripting.elems.ElementVariable;
import hr.fer.zemris.java.custom.scripting.nodes.EchoNode;

/**
 * This enumeration defines a deterministic final automata. The automata helps
 * the {@link SmartScriptParser} to parse the content of the echo tag searching
 * for legal expressions an creating the {@link Element} of the {@link EchoNode}
 * . Usually, the DFA's are implemented via {@link Map} or some other type of
 * {@link Collection}, but in this example, the implementation is achieved via
 * enumeration (because of the limit of this task). The automata consumes the
 * character from the {@link SmartScriptParser} and determines weather the state
 * should be changed or not.
 * 
 * @author Matej Balun
 *
 */
public enum EchoAutomata {

	/**
	 * A state representing a legal {@link ElementFunction}
	 */
	FUNCTION,

	/**
	 * A state representing a legal {@link ElementVariable}
	 */
	VARIABLE,

	/**
	 * A state representing a legal {@link ElementOperator}
	 */
	OPERATOR,

	/**
	 * A state representing a legal {@link ElementConstantDouble}
	 */
	DOUBLE,

	/**
	 * A state representing a blank space in the echo.
	 */
	BLANK;

	/**
	 * Current state of the automata.
	 */
	private static EchoAutomata currentState;

	/**
	 * Sets the specified state of the automata.
	 * 
	 * @param state
	 *            - state of the automata
	 */
	public static void setState(EchoAutomata state) {
		currentState = state;
	}

	/**
	 * Consumes the character from the {@link SmartScriptParser} and determines
	 * the next state of the automata.
	 * 
	 * @param current
	 *            - the current character
	 * @return the next state of the automata.
	 * @throws SmartScriptParserException
	 *             if there is an error in echo mode fo the automata.
	 */
	public static EchoAutomata consume(char current) {

		if (currentState == EchoAutomata.VARIABLE) {
			return variableState(current);

		} else if (currentState == EchoAutomata.FUNCTION) {
			return functionState(current);

		} else if (currentState == EchoAutomata.OPERATOR) {
			return operatorState(current);

		} else if (currentState == EchoAutomata.DOUBLE) {
			return doubleState(current);

		} else if (currentState == EchoAutomata.BLANK) {
			return blankState(current);
		}

		throw new SmartScriptParserException("Illegal expression in echo mode.");
	}

	/**
	 * 
	 * Determines if the variable state should be changed.
	 * 
	 * @param current
	 *            - the current character
	 * @return the next state
	 * @throws SmartScriptParserException
	 *             if there is an error in echo mode fo the automata.
	 */
	private static EchoAutomata variableState(char current) {
		if (Character.isLetter(current) || Character.isDigit(current) || current == '_') {
			return currentState;
		} else if (current == '@') {
			currentState = EchoAutomata.FUNCTION;
			return currentState;
		} else if (current == '/' || current == '*' || current == '+' || current == '-' || current == '^') {
			currentState = EchoAutomata.OPERATOR;
			return currentState;
		} else if (Character.isWhitespace(current)) {
			currentState = EchoAutomata.BLANK;
			return currentState;
		}

		throw new SmartScriptParserException("Illegal expression in echo mode.");
	}

	/**
	 * Determines if the function state should be changed.
	 * 
	 * @param current
	 *            - the current character
	 * @return the next state
	 * @throws SmartScriptParserException
	 *             if there is an error in echo mode fo the automata.
	 */
	private static EchoAutomata functionState(char current) {
		if (Character.isLetter(current) || Character.isDigit(current) || current == '_' || current == '@') {
			return currentState;
		} else if (current == '/' || current == '*' || current == '+' || current == '-' || current == '^') {
			currentState = EchoAutomata.OPERATOR;
			return currentState;
		} else if (Character.isWhitespace(current)) {
			currentState = EchoAutomata.BLANK;
			return currentState;
		}

		throw new SmartScriptParserException("Illegal expression in echo mode.");
	}

	/**
	 * Determines if the operator state should be changed.
	 * 
	 * @param current
	 *            - the current character
	 * @return the next state
	 * @throws SmartScriptParserException
	 *             if there is an error in echo mode fo the automata.
	 */
	private static EchoAutomata operatorState(char current) {
		if (current == '/' || current == '*' || current == '+' || current == '-' || current == '^') {
			return currentState;
		} else if (Character.isLetter(current)) {
			currentState = EchoAutomata.VARIABLE;
			return currentState;
		} else if (Character.isDigit(current)) {
			currentState = EchoAutomata.DOUBLE;
			return currentState;
		} else if (current == '@') {
			currentState = EchoAutomata.FUNCTION;
			return currentState;
		} else if (Character.isWhitespace(current)) {
			currentState = EchoAutomata.BLANK;
			return currentState;
		}

		throw new SmartScriptParserException("Illegal expression in echo mode.");
	}

	/**
	 * Determines if the double state should be changed.
	 * 
	 * @param current
	 *            - the current character
	 * @return - the next state
	 * @throws SmartScriptParserException
	 *             if there is an error in echo mode fo the automata.
	 */
	private static EchoAutomata doubleState(char current) {
		if (Character.isDigit(current) || current == '.') {
			return currentState;
		} else if (current == '/' || current == '*' || current == '+' || current == '-' || current == '^') {
			currentState = EchoAutomata.OPERATOR;
			return currentState;
		} else if (current == '@') {
			currentState = EchoAutomata.FUNCTION;
			return currentState;
		} else if (Character.isLetter(current)) {
			currentState = EchoAutomata.VARIABLE;
			return currentState;
		} else if (Character.isWhitespace(current)) {
			currentState = EchoAutomata.BLANK;
			return currentState;
		}

		throw new SmartScriptParserException("Illegal expression in echo mode.");
	}

	/**
	 * Determines if the blank state should be changed.
	 * 
	 * @param current
	 *            - the current character.
	 * @return the next state
	 * @throws SmartScriptParserException
	 *             if there is an error in echo mode fo the automata.
	 */
	private static EchoAutomata blankState(char current) {
		if (Character.isDigit(current) || current == '.') {
			currentState = EchoAutomata.DOUBLE;
			return currentState;
		} else if (current == '/' || current == '*' || current == '+' || current == '-' || current == '^') {
			currentState = EchoAutomata.OPERATOR;
			return currentState;
		} else if (current == '@') {
			currentState = EchoAutomata.FUNCTION;
			return currentState;
		} else if (Character.isLetter(current)) {
			currentState = EchoAutomata.VARIABLE;
			return currentState;
		} else if (Character.isWhitespace(current)) {
			return currentState;
		}

		throw new SmartScriptParserException("Illegal expression in echo mode.");
	}

}
