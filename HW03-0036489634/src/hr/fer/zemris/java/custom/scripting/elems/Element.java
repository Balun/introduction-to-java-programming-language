package hr.fer.zemris.java.custom.scripting.elems;

/**
 * The base interface-like class that represent the element of a parser tree
 * node. Other element classes extend this class and contain the actual
 * functionality.
 * 
 * @author Matej Balun
 *
 */
public class Element {

	/**
	 * The default constructor.
	 */
	public Element() {
		super();
	}

	/**
	 * Return the textual representation of the element.
	 * 
	 * @return textual representation of the element.
	 */
	public String asText() {
		return "";
	}

}
