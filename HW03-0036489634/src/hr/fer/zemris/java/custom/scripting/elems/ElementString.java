package hr.fer.zemris.java.custom.scripting.elems;

/**
 * This class represents the string element of the parser tree node. It extends
 * {@link Element} class;
 * 
 * @author Matej Balun
 *
 */
public class ElementString extends Element {

	/**
	 * Value of this string element.
	 */
	private String value;

	/**
	 * This constructor initializes this string element.
	 * 
	 * @param value
	 *            - a string value of the element.
	 */
	public ElementString(String value) {
		this.value = value;
	}

	/**
	 * Return the textual representation of this element.
	 */
	@Override
	public String asText() {
		return "\"" + this.value + "\"";
	}

}
