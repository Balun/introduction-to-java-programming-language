package hr.fer.zemris.java.custom.scripting.nodes;

import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;

/**
 * The main node of {@link SmartScriptParser} tree. It contains all other nodes
 * as its children. It extends the {@link Node} class.
 * 
 * @author Matej Balun
 *
 */
public class DocumentNode extends Node {

	/**
	 * A default constructor.
	 */
	public DocumentNode() {
		super();
	}

}
