package hr.fer.zemris.java.custom.scripting.elems;

/**
 * This class represents the variable element of the parser tree node. It
 * extends the {@link Element} class.
 * 
 * @author Matej Balun
 *
 */
public class ElementVariable extends Element {

	/**
	 * The name of the variable.
	 */
	private String name;

	/**
	 * This constructor initializes this variable element.
	 * 
	 * @param name
	 *            - name of the variable.
	 */
	public ElementVariable(String name) {
		this.name = name;
	}

	/**
	 * Return the textual representation of this element.
	 */
	@Override
	public String asText() {
		return this.name;
	}

}
