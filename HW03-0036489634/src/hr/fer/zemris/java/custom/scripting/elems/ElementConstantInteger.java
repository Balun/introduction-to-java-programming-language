package hr.fer.zemris.java.custom.scripting.elems;

/**
 * This class represents the integer element of the parser tree node. It extends
 * the {@link Element} class;
 * 
 * @author Matej Balun
 *
 */
public class ElementConstantInteger extends Element {

	/**
	 * The actual value of this integer element.
	 */
	private int value;

	/**
	 * This constructor initializes the integer element.
	 * 
	 * @param value
	 *            - value of the element.
	 */
	public ElementConstantInteger(int value) {
		this.value = value;
	}

	/**
	 * Returns the textual representation of this integer element.
	 */
	@Override
	public String asText() {
		return Integer.toString(this.value);
	}

}
