package hr.fer.zemris.java.custom.scripting.lexer;

import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;

/**
 * This enumeration specifies the types of the {@link SmartScriptLexer} tokens.
 * 
 * @author Matej Balun
 *
 */
public enum SmartScriptTokenType {

	/**
	 * This token is produced inside a tag and consists of everything that is
	 * not a string, leaving {@link SmartScriptParser} the actual interpretation
	 * of the token.
	 */
	WORD,

	/**
	 * This token is produced when special symbols or tags occur ("{$", "$}"
	 * etc.). It helps the {@link SmartScriptParser} to interpret what should be
	 * the following sequence of tokens.
	 */
	SYMBOL,

	/**
	 * A textual token, representing everything that is outside the tags.
	 */
	TEXT,

	/**
	 * A special token produced when the {@link SmartScriptLexer} has reached
	 * the end of file.
	 */
	EOF,

	/**
	 * A token produced when the {@link SmartScriptLexer} is in string state.
	 */
	STRING;

}
