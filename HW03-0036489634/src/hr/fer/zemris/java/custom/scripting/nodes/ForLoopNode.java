package hr.fer.zemris.java.custom.scripting.nodes;

import hr.fer.zemris.java.custom.scripting.elems.Element;
import hr.fer.zemris.java.custom.scripting.elems.ElementVariable;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;

/**
 * This node represents the for-loop in the {@link SmartScriptParser}. The
 * for-loop consists of a {@link ElementVariable}, {@link Element} start
 * expression, {@link Element} end expression and the {@link Element} step
 * expression (which can be a null-reference).
 * 
 * @author Matej Balun
 *
 */
public class ForLoopNode extends Node {

	/**
	 * A variable of the for-loop.
	 */
	private ElementVariable variable;

	/**
	 * The start expression of the loop.
	 */
	private Element startExpression;

	/**
	 * The end expression of the loop.
	 */
	private Element endExpression;

	/**
	 * The possible step expression of the loop.
	 */
	private Element stepExpression;

	/**
	 * This constructor initializes the {@link ForLoopNode} with its variable,
	 * start expression and end expression, without the step expression.
	 * 
	 * @param variable
	 *            - a for loop variable
	 * @param startExpression
	 *            - the start expression
	 * @param endExpression
	 *            - the end expression
	 * @throws IllegalArgumentException
	 *             if any of the parameters is a null-reference
	 */
	public ForLoopNode(ElementVariable variable, Element startExpression, Element endExpression) {
		this(variable, startExpression, endExpression, null);
	}

	/**
	 * This constructor initializes the {@link ForLoopNode} with its variable,
	 * start expression, end expression and its step expression.
	 * 
	 * @param variable
	 *            - a for loop variable
	 * @param startExpression
	 *            - the start expression
	 * @param endExpression
	 *            - the end expression
	 * @param stepExpression
	 *            - the step expression
	 * @throws IllegalArgumentException
	 *             if any of the parameters is a null-reference
	 */
	public ForLoopNode(ElementVariable variable, Element startExpression, Element endExpression,
			Element stepExpression) {

		super();

		if (variable == null) {
			throw new IllegalArgumentException("The variable cannot be a null-reference");

		} else if (startExpression == null) {
			throw new IllegalArgumentException("The start expression must not be null-reference");

		} else if (endExpression == null) {
			throw new IllegalArgumentException("The end expression must not be a null-reference");
		}

		this.variable = variable;
		this.startExpression = startExpression;
		this.endExpression = endExpression;
		this.stepExpression = stepExpression;
	}

	/**
	 * Returns the variable of the loop.
	 * 
	 * @return variable of the loop.
	 */
	public ElementVariable getVariable() {
		return variable;
	}

	/**
	 * Returns the start expression of the loop.
	 * 
	 * @return start expression of the loop
	 */
	public Element getStartExpression() {
		return startExpression;
	}

	/**
	 * Returns the end expression of the loop.
	 * 
	 * @return end expression
	 */
	public Element getEndExpression() {
		return endExpression;
	}

	/**
	 * Returns the step expression of the loop.
	 * 
	 * @return step expression of the loop
	 */
	public Element getStepExpression() {
		return stepExpression;
	}

	/**
	 * Returns a string representation of the for-loop, appending all of the
	 * loop's parameters into a single string terminated by tags.
	 */
	@Override
	public String toString() {
		if (stepExpression != null) {
			return "{$ FOR " + variable.asText() + " " + startExpression.asText() + " " + endExpression.asText() + " "
					+ stepExpression.asText() + " $}";
		} else {
			return "{$ FOR " + variable.asText() + " " + startExpression.asText() + " " + endExpression.asText()
					+ " $}";
		}
	}

}
