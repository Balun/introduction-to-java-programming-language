package hr.fer.zemris.java.tecaj.hw5.collections;

import java.util.Iterator;

import hr.fer.zemris.java.tecaj.hw5.collections.SimpleHashTable.TableEntry;

/**
 * This program demonstrates manipulation with the custom collection
 * {@link SimpleHashTable}. The program puts few entities in the collection,
 * tests its iterator along with the remove method and other utilities of the
 * class.
 * 
 * @author Matej Balun
 *
 */
public class Demo {

	/**
	 * The main method that starts the program.
	 */
	public static void main(String[] args) {
		SimpleHashTable<String, Integer> examMarks = new SimpleHashTable<>(2);

		examMarks.put("Ivana", 2);
		examMarks.put("Ante", 2);
		examMarks.put("Jasna", 2);
		examMarks.put("Kristina", 5);
		examMarks.put("Ivana", 5);
		examMarks.put("Balun", null);

		for (TableEntry<String, Integer> pair : examMarks) {
			System.out.printf("%s => %d%n", pair.getKey(), pair.getValue());
		}

		Iterator<TableEntry<String, Integer>> iter = examMarks.iterator();
		while (iter.hasNext()) {
			TableEntry<String, Integer> pair = iter.next();
			if (pair.getKey().equals("Ivana")) {
				iter.remove();
			}
		}

		System.out.println(examMarks.toString());
	}

}
