package hr.fer.zemris.java.tecaj.hw5.parsing;

/**
 * This enumeration describes the legal {@link QueryLexer} states.
 * 
 * @author Matej Balun
 *
 */
public enum QueryLexerState {

	/**
	 * The index query command state.
	 */
	INDEX_QUERY,

	/**
	 * The query command state.
	 */
	QUERY;

}
