package hr.fer.zemris.java.tecaj.hw5.db;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

import hr.fer.zemris.java.tecaj.hw5.filter.IndexQueryFilter;
import hr.fer.zemris.java.tecaj.hw5.filter.QueryFilter;

/**
 * This program demonstrates the manipulation with the simple database
 * {@link StudentDatabase}. The user inputs the query commands on the standard
 * input for filtering the database and obtaining the specified elements. There
 * are two main commands: "query" and "indexquery". The commands contain the
 * {@link ConditionalExpression}s, specifying the arguments for filtration. The
 * comparison condition must contain a value field, comparison operator and the
 * {@link String} literal for comparison. The program is terminated withe the
 * "exit" command. The content of the database is specified via textual file.
 * 
 * @author Matej Balun
 *
 */
public class StudentDB {

	/**
	 * The {@link String} constant fo the exit command.
	 */
	private static final String EXIT = "exit";

	/**
	 * The {@link String} constant for the query command.
	 */
	private static final String QUERY = "query";

	/**
	 * The {@link String} constant for the index query command.
	 */
	private static final String INDEX_QUERRY = "indexquery";

	/**
	 * length of the {@link StudentRecord} jmbag field.
	 */
	private static final int JMBAG_LENGTH = 10;

	/**
	 * length of the {@link StudentRecord}s grade.
	 */
	private static final int GRADE_LENGTH = 1;

	/**
	 * The main method that starts the program.
	 */
	public static void main(String[] args) {

		List<String> lines = null;

		try {
			lines = Files.readAllLines(Paths.get("database.txt"), StandardCharsets.UTF_8);
		} catch (IOException e) {
			System.err.println("Could not read the database file.");
		}

		StudentDatabase database = new StudentDatabase(lines);

		System.out.println("Welcome to querry console. Please type your commands:\n");

		initiateConsole(database);

	}

	/**
	 * This method initializes the console fo the query commands. It requests
	 * the input from the user, writing potential error messages and terminating
	 * the program on first input of string "exit".
	 * 
	 * @param database
	 *            the specified {@link StudentDatabase} for the operations.
	 */
	private static void initiateConsole(StudentDatabase database) {
		Scanner sc = new Scanner(System.in);

		while (sc.hasNextLine()) {
			String line = sc.nextLine();
			if (line.equals(EXIT)) {
				break;
			}

			if (line.startsWith(QUERY)) {
				List<StudentRecord> records = database.filter(new QueryFilter(line));
				printRecords(records.toArray());
			} else if (line.startsWith(INDEX_QUERRY)) {
				StudentRecord record = database.forJMBAG(new IndexQueryFilter(line).getRecordIndex(database));
				System.out.println("Using index for record retrival.");
				printRecords(record);
			} else {
				System.out.println("Invalid querry command!");
				continue;
			}
		}

		System.out.println("Goodbye!");
		sc.close();
	}

	/**
	 * This method prints the specified objects to the standard input, along
	 * with the specified table format.
	 * 
	 * @param records
	 *            specified reqord for printing
	 */
	private static void printRecords(Object... records) {

		int maxLastName = 0;
		int maxFirstName = 0;

		for (Object record : records) {
			StudentRecord tmp = (StudentRecord) record;
			if (tmp.getLastName().length() >= maxLastName) {
				maxLastName = tmp.getLastName().length();
			}
			if (tmp.getFirstname().length() >= maxFirstName) {
				maxFirstName = tmp.getFirstname().length();
			}
		}

		printMarginLine(maxFirstName, maxLastName);
		printStudents(maxFirstName, maxLastName, records);
		printMarginLine(maxFirstName, maxLastName);

		System.out.println("Records selected: " + records.length + "\n");
	}

	/**
	 * This method prints the margins of the table for the filtered elements.
	 * 
	 * @param maxNameWidth
	 *            the longest name in the list
	 * @param maxSurnameWidth
	 *            the longest surname in the list
	 */
	private static void printMarginLine(int maxNameWidth, int maxSurnameWidth) {
		System.out.print("+");
		for (int i = 0; i < JMBAG_LENGTH + 2; i++) {
			System.out.print("=");
		}
		System.out.print("+");
		for (int i = 0; i < maxSurnameWidth + 2; i++) {
			System.out.print("=");
		}
		System.out.print("+");
		for (int i = 0; i < maxNameWidth + 2; i++) {
			System.out.print("=");
		}
		System.out.print("+");
		for (int i = 0; i < GRADE_LENGTH + 2; i++) {
			System.out.print("=");
		}
		System.out.print("+");
		System.out.println();

	}

	/**
	 * This method prints the collection of specified students on the standard
	 * input, in boundaries of the specified margins.
	 * 
	 * @param maxNameWidth
	 *            the longest name in the list
	 * @param maxSurnameWidth
	 *            the longest surname in the list
	 * @param students
	 *            collection of specified students
	 */
	private static void printStudents(int maxNameWidth, int maxSurnameWidth, Object... students) {
		for (Object student : students) {
			StudentRecord tmp = (StudentRecord) student;
			int surNameDiff = maxSurnameWidth - tmp.getLastName().length();
			int firstNameDiff = maxNameWidth - tmp.getFirstname().length();
			System.out.print("| " + tmp.getJmbag() + " | ");
			System.out.print(tmp.getLastName());

			for (int i = 0; i < surNameDiff; i++) {
				System.out.print(" ");
			}

			System.out.print(" | ");
			System.out.print(tmp.getFirstname());

			for (int i = 0; i < firstNameDiff; i++) {
				System.out.print(" ");
			}

			System.out.print(" | ");
			System.out.print(tmp.getFinalGrade() + " |");
			System.out.println();
		}
	}

}
