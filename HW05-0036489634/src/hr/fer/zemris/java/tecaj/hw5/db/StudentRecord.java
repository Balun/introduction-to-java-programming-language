package hr.fer.zemris.java.tecaj.hw5.db;

/**
 * This class represents the basic entity of the {@link StudentDatabase}. It
 * contains the attributes of the student, offering methods for retrieval (the
 * attributes are read-only).
 * 
 * @author Matej Balun
 *
 */
public class StudentRecord {

	/**
	 * jmbag of the student.
	 */
	private String jmbag;

	/**
	 * last name of the student.
	 */
	private String lastName;

	/**
	 * first name of the student.
	 */
	private String firstname;

	/**
	 * final grade of the student.
	 */
	private int finalGrade;

	/**
	 * This constructor initializes the {@link StudentRecord}.
	 * 
	 * @param jmbag
	 *            jmbag of the student
	 * @param lastName
	 *            last name of the student
	 * @param firstname
	 *            first name of the student
	 * @param finalGrade
	 *            final grade of the student
	 * 
	 * @throws IllegalArgumentException
	 *             if jmbag, first name or last name are null-references, or if
	 *             the final grade is not 2 - 5.
	 */
	public StudentRecord(String jmbag, String lastName, String firstname, int finalGrade) {
		super();

		if (jmbag == null || lastName == null || firstname == null) {
			throw new IllegalArgumentException("jmbag, last name or first name must not be a null-references.");
		} else if (finalGrade < 2 || finalGrade > 5) {
			throw new IllegalArgumentException("The final grade muust be in range from 2 - 5.");
		}

		this.jmbag = jmbag;
		this.lastName = lastName;
		this.firstname = firstname;
		this.finalGrade = finalGrade;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((jmbag == null) ? 0 : jmbag.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StudentRecord other = (StudentRecord) obj;
		if (jmbag == null) {
			if (other.jmbag != null)
				return false;
		} else if (!jmbag.equals(other.jmbag))
			return false;
		return true;
	}

	/**
	 * Returns the jmbag of the student.
	 * 
	 * @return the student's jmbag
	 */
	public String getJmbag() {
		return jmbag;
	}

	/**
	 * Returns the last name of the student.
	 * 
	 * @return last name of the student
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Returns the first name of the student.
	 * 
	 * @return first name of the student
	 */
	public String getFirstname() {
		return firstname;
	}

	/**
	 * Returns the final grade of the student.
	 * 
	 * @return final grade of the student
	 */
	public int getFinalGrade() {
		return finalGrade;
	}
}
