package hr.fer.zemris.java.tecaj.hw5.conditions;

import java.text.Collator;
import java.util.Locale;

/**
 * This class represents the equal condition for the query. I implements the
 * {@link IComparisonOperator}.
 * 
 * @author Matej Balun
 *
 */
public class EqualCondition implements IComparisonOperator {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean satisfied(String value1, String value2) {
		int result = Collator.getInstance(new Locale("hr", "HR")).compare(value1, value2);
		return result == 0;
	}

}
