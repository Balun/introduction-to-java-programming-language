package hr.fer.zemris.java.tecaj.hw5.conditions;

import hr.fer.zemris.java.tecaj.hw5.parsing.ParserException;

/**
 * This class represents the wild card operator ("LIKE") for the querry. It
 * implements the {@link IComparisonOperator} interface.
 * 
 * @author Matej Balun
 *
 */
public class WildCardEqualsCondition implements IComparisonOperator {

	/**
	 * {@inheritDoc}
	 * 
	 * @throws ParserException
	 *             if the given string for comparing has more than one "*"
	 *             characters.
	 */
	@Override
	public boolean satisfied(String value1, String value2) {
		if (value2.indexOf('*') != value2.lastIndexOf('*')) {
			throw new ParserException("The wildcard must contain only one '*' character");
		}
		value2 = value2.replaceAll("\\*", ".*");
		return value1.matches(value2);
	}

}
