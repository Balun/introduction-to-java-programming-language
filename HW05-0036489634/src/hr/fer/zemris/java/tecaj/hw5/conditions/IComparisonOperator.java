package hr.fer.zemris.java.tecaj.hw5.conditions;

/**
 * This interface represents the general comparison operator fo the query. The
 * legal operator are ">", "<", ">=", "<=", "!=" and "LIKE". The classes that
 * implements this interface must implement the comparison method.
 * 
 * @author Matej Balun
 *
 */
public interface IComparisonOperator {

	/**
	 * This method compares the two values for the query. The classes that
	 * implement the {@link IComparisonOperator} interface must implement this
	 * method with specified comparison operation.
	 * 
	 * @param value1
	 *            the specified argument for comparison
	 * @param value2
	 *            the argument for comparing to
	 * @return true if the specified comparison is satisfied, otherwise false
	 */
	public boolean satisfied(String value1, String value2);

}
