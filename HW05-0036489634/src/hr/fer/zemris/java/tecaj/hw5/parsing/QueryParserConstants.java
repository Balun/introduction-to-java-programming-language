package hr.fer.zemris.java.tecaj.hw5.parsing;

/**
 * This enumeration represents the {@link String} constant for parsing the
 * query.
 * 
 * @author Matej Balun
 *
 */
public enum QueryParserConstants {

	/**
	 * The {@link String} constant for first name field.
	 */
	FIRST_NAME("firstName"),

	/**
	 * The {@link String} constant for last name field.
	 */
	LAST_NAME("lastName"),

	/**
	 * The {@link String} constant for jmbag field.
	 */
	JMBAG("jmbag"),

	/**
	 * The {@link String} constant for less-than operator.
	 */
	LESS_THAN("<"),

	/**
	 * The {@link String} constan for greater-than operator.
	 */
	GREATER_THAN(">"),

	/**
	 * The {@link String} constant for less-or-equal operator.
	 */
	LESS_EQUAL("<="),

	/**
	 * The {@link String} constant for greater-or-equal operator.
	 */
	GREATER_EQUAL(">="),

	/**
	 * The {@link String} constant for not-equal operator.
	 */
	NOT_EQUAL("!="),

	/**
	 * The {@link String} constant for equal operator.
	 */
	EQUAL("="),

	/**
	 * The string constant for LIKE operator.
	 */
	LIKE("LIKE");

	/**
	 * The value of the {@link String}.
	 */
	private String value;

	/**
	 * The private constructor for the enumeration.
	 * 
	 * @param value
	 */
	private QueryParserConstants(String value) {
		this.value = value;
	}

	/**
	 * Returns the value.
	 * 
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
}
