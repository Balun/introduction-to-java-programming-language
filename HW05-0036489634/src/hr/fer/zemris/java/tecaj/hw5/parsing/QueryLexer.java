package hr.fer.zemris.java.tecaj.hw5.parsing;

/**
 * This class represents a simple lexer for production of tokens from the query
 * line. There are several token types, defined in enumeration
 * {@link QueryTokenType}. The lexer has two states: query and index query
 * state, defined in {@link QueryLexerState} enumeration.
 * 
 * @author Matej Balun
 *
 */
public class QueryLexer {

	/**
	 * The character array of a query line.
	 */
	private char[] data;

	/**
	 * The current token.
	 */
	private QueryToken token;

	/**
	 * The current index in the character array.
	 */
	private int currentIndex;

	/**
	 * The current state of the lexer.
	 */
	private QueryLexerState state;

	/**
	 * The {@link String} constant for the "LIKE" operator.
	 */
	private static final String LIKE_OPERATOR = "LIKE";

	/**
	 * The {@link String} constant for the "AND" logical operator.
	 */
	private static final String AND = "AND";

	/**
	 * This constructor initializes the lexer with the {@link String} text
	 * representing the query line command, and sets the default lexer state to
	 * index query state.
	 * 
	 * @param text
	 *            the specified query line {@link String}
	 * 
	 * @throws IllegalArgumentException
	 *             if the specified {@link String} is a null-reference.
	 */
	public QueryLexer(String text) {
		if (text == null) {
			throw new IllegalArgumentException("The specified query text must not be a null-reference");
		}

		this.data = text.trim().toCharArray();
		this.currentIndex = 0;
		this.state = QueryLexerState.INDEX_QUERY;
	}

	/**
	 * Returns the current {@link QueryToken} of the lexer.
	 * 
	 * @return
	 */
	public QueryToken getToken() {
		return this.token;
	}

	/**
	 * Sets the current state of the {@link QueryLexer}.
	 * 
	 * @param state
	 *            the specified {@link QueryLexerState}.
	 */
	public void setState(QueryLexerState state) {
		this.state = state;
	}

	/**
	 * Returns the current {@link QueryLexerState}.
	 * 
	 * @return the current {@link QueryLexerState}
	 */
	public QueryLexerState getState() {
		return this.state;
	}

	/**
	 * Produces and returns the next token of the lexer. If the lexer has
	 * reacher the end of line, it generates the EOL token. IIf the EOL token
	 * has already been produces, it throws an exception. In general the method
	 * delegates the work to the other private methods.
	 * 
	 * @return the next token of the lexer.
	 * @throws LexerException
	 *             if the control has reached the end of line
	 */
	public QueryToken nextToken() {
		if (this.token != null && this.token.getType().equals(QueryTokenType.EOL)) {
			throw new LexerException("The lexer has already reached the ond of line.");
		} else if (currentIndex == data.length) {
			this.token = new QueryToken(QueryTokenType.EOL, null);
		}

		return getQueryToken();
	}

	/**
	 * Produces the new query token based on the content of consumed input. The
	 * tokens are specified by the {@link QueryTokenType} enumeration.
	 * 
	 * @return the next {@link QueryToken} of the lexer.
	 */
	private QueryToken getQueryToken() {
		char current;

		if (currentIndex < data.length) {
			current = data[currentIndex];
		} else {
			return new QueryToken(QueryTokenType.EOL, null);
		}

		current = removeBlanks(current);

		if (isOperator(current)) {
			String operator = extractOperator(current);
			this.token = new QueryToken(QueryTokenType.OPERATOR, operator);

		} else if (current == '"') {
			String literal = extractStringLiteral(current);
			if (literal == null) {
				this.token = new QueryToken(QueryTokenType.EOL, literal);
			} else {
				this.token = new QueryToken(QueryTokenType.STRING_LITERAL, literal);
			}
		} else if (isAnd(current)) {
			String and = consumeAnd(current);
			this.token = new QueryToken(QueryTokenType.AND, and);

		} else {

			StringBuilder fieldName = new StringBuilder();

			while (!isOperator(current) && current != '"' && !Character.isWhitespace(current)
					&& currentIndex < data.length - 1) {
				fieldName.append(current);
				current = data[++currentIndex];
			}
			this.token = new QueryToken(QueryTokenType.FIELD_NAME, fieldName.toString());
		}

		return this.token;
	}

	/**
	 * Consumes the "AND" operator for the next token
	 * 
	 * @param current
	 *            the current character in the input array.
	 * @return the "AND" string.
	 */
	private String consumeAnd(char current) {
		currentIndex += 3;
		return AND;
	}

	/**
	 * Checks if the character field is and "AND" operator.
	 * 
	 * @param current
	 *            character in the query
	 * @return current true if the next token is "AND", otherwise false
	 */
	private boolean isAnd(char current) {
		StringBuilder and = new StringBuilder();

		for (int i = 0; i <= 2 && (currentIndex + i) < data.length - 1; i++) {
			current = data[currentIndex + i];
			and.append(current);

		}

		if (and.toString().equalsIgnoreCase(AND)) {
			return true;
		}

		return false;
	}

	/**
	 * This method extracts the string literal field from the query.
	 * 
	 * @param current
	 *            the current character in the query.
	 * @return the string literal
	 */
	private String extractStringLiteral(char current) {
		if (currentIndex == data.length - 1) {
			return null;
		}

		current = data[++currentIndex];
		StringBuilder literal = new StringBuilder();

		while (current != '"' && currentIndex < data.length - 1) {
			if (Character.isWhitespace(current)) {
				throw new LexerException("White spaces are not allowed in string literals");
			}

			literal.append(current);
			current = data[++currentIndex];
		}

		currentIndex++;
		return literal.toString();
	}

	/**
	 * This method extracts the new operator {@link String} for the next token.
	 * 
	 * @param current
	 *            the current character in the query {@link String}
	 * @return the new operator {@link String}
	 */
	private String extractOperator(char current) {
		StringBuilder operator = new StringBuilder();

		if (current == '=') {
			currentIndex++;
			return Character.toString(current);
		} else if (current == 'L') {
			while (!Character.isWhitespace(current) && currentIndex < data.length - 1) {
				operator.append(current);
				current = data[++currentIndex];
			}

			if (operator.toString().equals(LIKE_OPERATOR)) {
				return operator.toString();
			}

			operator.delete(0, operator.length());

		} else if (current == '<' || current == '>' || current == '!') {
			while (isOperator(current) && currentIndex < data.length - 1) {
				operator.append(current);
				current = data[++currentIndex];
			}

			return operator.toString();
		}

		throw new LexerException("Illegal operator type.");
	}

	/**
	 * Checks if the current character (sequence) is query operator.
	 * 
	 * @param current
	 *            the current character
	 * @return true if the current character (sequence) is query operator
	 */
	private boolean isOperator(char current) {
		StringBuilder operator = new StringBuilder();
		int index = currentIndex;

		if (current == '=' || current == '<' || current == '>' || current == '!') {
			return true;
		} else if (current == 'L') {
			while (!Character.isWhitespace(current) && index < data.length - 1) {
				operator.append(current);
				current = data[++index];
			}

			if (operator.toString().equals(LIKE_OPERATOR)) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Removes the blank spaces from the query {@link String}
	 * 
	 * @param current
	 *            the current character in the query {@link String}
	 * @return the first non-whitespace character
	 */
	private char removeBlanks(char current) {
		while (Character.isWhitespace(current) && currentIndex < data.length - 1) {
			current = data[++currentIndex];
		}

		return current;
	}
}
