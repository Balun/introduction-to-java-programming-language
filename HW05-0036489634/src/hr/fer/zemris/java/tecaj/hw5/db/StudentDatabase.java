package hr.fer.zemris.java.tecaj.hw5.db;

import java.util.ArrayList;
import java.util.List;

import hr.fer.zemris.java.tecaj.hw5.collections.SimpleHashTable;
import hr.fer.zemris.java.tecaj.hw5.filter.*;

/**
 * This class represents the general stuedent database for this course. The
 * database contains the {@link StudentRecord} elements. It keeps the elements
 * in the {@link List}, and in the {@link SimpleHashTable} for the O(1) time
 * complexity of the retrieval.
 * 
 * @author Matej Balun
 *
 */
public class StudentDatabase {

	/**
	 * The internal list of {@link String}s containing the text representation
	 * of the database.
	 */
	private List<String> content;

	/**
	 * The internal list of {@link StudentRecord}s for the database.
	 */
	private List<StudentRecord> records;

	/**
	 * The internal hash table of the database for fast retrieval.
	 */
	private SimpleHashTable<String, StudentRecord> indexedRecords;

	/**
	 * This constructor initializes the database with the textual representation
	 * of the database content. It delegates the initialization of
	 * {@link StudentRecord}s to other method.
	 * 
	 * @param content
	 *            the textual representation of the database.
	 */
	public StudentDatabase(List<String> content) {
		if (content == null) {
			throw new IllegalArgumentException("The content must not be a null-reference.");
		}

		this.content = content;
		this.records = new ArrayList<>();
		this.indexedRecords = new SimpleHashTable<>();

		initializeDatabase();
	}

	/**
	 * Initializes the {@link StudentDatabase} based on the textual content,
	 * creating the {@link StudentRecord} entities and inserting them into the
	 * list and hash table.
	 */
	private void initializeDatabase() {
		for (String line : content) {
			String[] params = line.trim().split("\t");
			StudentRecord record = new StudentRecord(params[0], params[1], params[2], Integer.parseInt(params[3]));
			this.records.add(record);
			this.indexedRecords.put(record.getJmbag(), record);
		}
	}

	/**
	 * A fast retrieval of the {@link StudentRecord} based on the student's
	 * jmbag, with O(1) time complexity.
	 * 
	 * @param jmbag
	 *            the jmbag of the specified student
	 * @return {@link String} of the specified student.
	 */
	public StudentRecord forJMBAG(String jmbag) {
		return this.indexedRecords.get(jmbag);
	}

	/**
	 * Traverses the database, filtering the {@link StudentRecord}s based on the
	 * specified filter. Returns the list of the filtered {@link StudentRecord}
	 * s.
	 * 
	 * @param filter
	 *            the specified filter
	 * @return {@link List} of the filtered elements.
	 */
	public List<StudentRecord> filter(IFilter filter) {
		List<StudentRecord> tmp = new ArrayList<>();

		for (StudentRecord record : this.records) {
			if (filter.accepts(record)) {
				tmp.add(record);
			}
		}

		return tmp;
	}

}
