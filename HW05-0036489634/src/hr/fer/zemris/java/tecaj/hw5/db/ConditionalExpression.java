package hr.fer.zemris.java.tecaj.hw5.db;

import hr.fer.zemris.java.tecaj.hw5.conditions.*;
import hr.fer.zemris.java.tecaj.hw5.getters.*;

/**
 * This class represents the conditional expression for the database query. The
 * legal conditional expression contains the {@link IFieldValueGetter} field
 * value, {@link IComparisonOperator} comparison operator and the {@link String}
 * literal for the comparison.
 * 
 * @author Matej Balun
 *
 */
public class ConditionalExpression {

	/**
	 * The value field for the condition.
	 */
	private IFieldValueGetter fieldValueGetter;

	/**
	 * The comparison operator for the condition.
	 */
	private IComparisonOperator comparisonOperator;

	/**
	 * The {@link String} literal for the condition.
	 */
	private String literal;

	/**
	 * This constructor initializes the {@link ConditionalExpression}, along
	 * with {@link IFieldValueGetter} value field, {@link IComparisonOperator}
	 * comparison operator and the {@link String} literal.
	 * 
	 * @param fieldValueGetter
	 *            the value field of the condition
	 * @param comparisonOperator
	 *            the comparison operator for the condition
	 * @param literal
	 *            the {@link String} literal for the condition
	 */
	public ConditionalExpression(IFieldValueGetter fieldValueGetter, IComparisonOperator comparisonOperator,
			String literal) {
		this.literal = literal;
		this.fieldValueGetter = fieldValueGetter;
		this.comparisonOperator = comparisonOperator;
	}

	/**
	 * Returns the value field of the {@link ConditionalExpression}.
	 * 
	 * @return the value field
	 */
	public IFieldValueGetter getFieldValueGetter() {
		return fieldValueGetter;
	}

	/**
	 * Returns the comparison operator for the {@link ConditionalExpression}.
	 * 
	 * @return the comparison operator
	 */
	public IComparisonOperator getComparisonOperator() {
		return comparisonOperator;
	}

	/**
	 * Returns the {@link String} literal for the {@link ConditionalExpression}.
	 * 
	 * @return the {@link String} literal.
	 */
	public String getLiteral() {
		return literal;
	}

}
