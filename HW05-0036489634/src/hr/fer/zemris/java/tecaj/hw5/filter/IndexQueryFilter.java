package hr.fer.zemris.java.tecaj.hw5.filter;

import java.util.List;

import hr.fer.zemris.java.tecaj.hw5.db.ConditionalExpression;
import hr.fer.zemris.java.tecaj.hw5.db.StudentDatabase;
import hr.fer.zemris.java.tecaj.hw5.db.StudentRecord;
import hr.fer.zemris.java.tecaj.hw5.parsing.QueryParser;

/**
 * This class represents the implementation of the index query command. The
 * command retrieves a single {@link StudentRecord} specified by its jmbag with
 * the O(1) average time complexity. It implements the {@link IFilter}
 * interface.
 * 
 * @author Matej Balun
 *
 */
public class IndexQueryFilter implements IFilter {

	/**
	 * The string of the index query.
	 */
	private String indexQueryString;

	/**
	 * The parser for the query line.
	 */
	private QueryParser parser;

	/**
	 * The string constant for the correct query command.
	 */
	private static final String CORRECT = "indexquery";

	/**
	 * This method consumes the specified index query string for parsing. It
	 * initializes the {@link QueryParser} for easier parsing of the querry
	 * conditions.
	 * 
	 * @param indexQueryString
	 *            the {@link String} representing the index query.
	 */
	public IndexQueryFilter(String indexQueryString) {
		if (indexQueryString == null) {
			throw new IllegalArgumentException("The index query string must not be a null - reference");
		} else if (!indexQueryString.startsWith(CORRECT)) {
			throw new IllegalArgumentException(
					"The index querry expression must begin with the \"indexQuery\" keyword");
		}

		this.indexQueryString = indexQueryString;
		this.parser = new QueryParser(this.indexQueryString);
		this.parser.parse();
	}

	/**
	 * {@inheritDoc} The {@link QueryParser} parser is used for easier parsing
	 * of the query line. the parser returns the {@link ConditionalExpression}
	 * of the query.
	 */
	@Override
	public boolean accepts(StudentRecord record) {
		List<ConditionalExpression> conditions = this.parser.getConditionalExpressions();

		for (ConditionalExpression condition : conditions) {
			if (!condition.getComparisonOperator().satisfied(condition.getFieldValueGetter().get(record),
					condition.getLiteral())) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Returns the {@link String} literal for the index query retrieve
	 * condtition
	 * 
	 * @param database
	 *            the specified {@link StudentDatabase}
	 * @return literal of the {@link ConditionalExpression}
	 */
	public String getRecordIndex(StudentDatabase database) {
		List<ConditionalExpression> conditions = this.parser.getConditionalExpressions();

		for (ConditionalExpression condition : conditions) {
			return condition.getLiteral();
		}

		return null;
	}

}
