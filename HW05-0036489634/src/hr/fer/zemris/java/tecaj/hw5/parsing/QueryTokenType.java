package hr.fer.zemris.java.tecaj.hw5.parsing;

/**
 * This enumeration describes the legal {@link QueryToken} types.
 * 
 * @author Matej Balun
 *
 */
public enum QueryTokenType {

	/**
	 * The name field token.
	 */
	FIELD_NAME,

	/**
	 * The operator token.
	 */
	OPERATOR,

	/**
	 * the {@link String} literal token.
	 */
	STRING_LITERAL,

	/**
	 * The "AND" keyword token.
	 */
	AND,

	/**
	 * The end of line token.
	 */
	EOL;

}
