package hr.fer.zemris.java.tecaj.hw5.parsing;

/**
 * This class represents the token of the {@link QueryLexer}. The legal token
 * types are specified in {@link QueryTokenType} enumeration
 * 
 * @author Matej Balun
 *
 */
public class QueryToken {

	/**
	 * The type of the token.
	 */
	private QueryTokenType type;

	/**
	 * {@link String} value of the token.
	 */
	private String value;

	/**
	 * This constructor initializes the token, with its {@link QueryTokenType}
	 * and {@link String} value.
	 * 
	 * @param type
	 *            type of the token
	 * @param value
	 *            value of the token
	 * @throws IllegalArgumentException
	 *             if the value is a null reference (except if the type is EOL).
	 */
	public QueryToken(QueryTokenType type, String value) {
		if (value == null && type != QueryTokenType.EOL) {
			throw new IllegalArgumentException("The value for the token must not be a null-reference.");
		}

		this.type = type;
		this.value = value;
	}

	/**
	 * Returns the type of the token
	 * 
	 * @return type of the token
	 */
	public QueryTokenType getType() {
		return type;
	}

	/**
	 * Returns the value of the token
	 * 
	 * @return value of the token
	 */
	public String getValue() {
		return value;
	}

}
