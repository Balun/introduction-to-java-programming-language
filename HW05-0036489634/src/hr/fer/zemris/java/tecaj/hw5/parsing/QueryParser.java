package hr.fer.zemris.java.tecaj.hw5.parsing;

import java.util.ArrayList;
import java.util.List;

import hr.fer.zemris.java.tecaj.hw5.conditions.EqualCondition;
import hr.fer.zemris.java.tecaj.hw5.conditions.GreaterOrEqualCondition;
import hr.fer.zemris.java.tecaj.hw5.conditions.GreaterThanCondition;
import hr.fer.zemris.java.tecaj.hw5.conditions.IComparisonOperator;
import hr.fer.zemris.java.tecaj.hw5.conditions.LessOrEqualCondition;
import hr.fer.zemris.java.tecaj.hw5.conditions.LessThanCondition;
import hr.fer.zemris.java.tecaj.hw5.conditions.NotEqualCondition;
import hr.fer.zemris.java.tecaj.hw5.conditions.WildCardEqualsCondition;
import hr.fer.zemris.java.tecaj.hw5.db.ConditionalExpression;
import hr.fer.zemris.java.tecaj.hw5.getters.FirstNameFieldGetter;
import hr.fer.zemris.java.tecaj.hw5.getters.IFieldValueGetter;
import hr.fer.zemris.java.tecaj.hw5.getters.JmbagFieldGetter;
import hr.fer.zemris.java.tecaj.hw5.getters.LastNameFieldGetter;
import static hr.fer.zemris.java.tecaj.hw5.parsing.QueryParserConstants.*;

/**
 * This class represents the simple parser for the query command line. The
 * parser uses the {@link QueryLexer} object for generating tokens, and consumes
 * the tokens by the specified rules, creating the {@link List} of
 * {@link ConditionalExpression}s.
 * 
 * @author Matej Balun
 *
 */
public class QueryParser {

	/**
	 * The lexer for production of tokens.
	 */
	private QueryLexer lexer;

	/**
	 * The {@link List} of generated {@link ConditionalExpression}s.
	 */
	private List<ConditionalExpression> expressions;

	/**
	 * The text representation of the query.
	 */
	private String text;

	/**
	 * This constructor initializes the parser, with its input query text.
	 * 
	 * @param text
	 *            the input query text
	 * @throws IllegalArgumentException
	 *             if the input text is a null-reference.
	 */
	public QueryParser(String text) {
		if (text == null) {
			throw new IllegalArgumentException("The input text must not be a null reference");
		}

		this.text = text;
		this.expressions = new ArrayList<>();
	}

	/**
	 * Sets the query text of the parser.
	 * 
	 * @param text
	 *            the query text
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * Returns the query text of the parser.
	 * 
	 * @return the query text
	 */
	public String getText() {
		return this.text;
	}

	/**
	 * Returns the {@link List} of the generated {@link ConditionalExpression}s.
	 * 
	 * @return
	 */
	public List<ConditionalExpression> getConditionalExpressions() {
		return this.expressions;
	}

	/**
	 * This method parses the specified {@link String} query text, depending on
	 * the type of the command (query or index query). After parsing process is
	 * finished, the parser generates the {@link List} of
	 * {@link ConditionalExpression}s of the query.
	 * 
	 * @throws ParserException
	 *             if the specified query command is invalid.
	 */
	public void parse() {
		if (this.text.startsWith("query")) {
			String query = this.text.replaceAll("query", "");
			parseQuery(query);

		} else if (this.text.startsWith("indexquery")) {
			String indexQuery = this.text.replace("indexquery", "");
			parseIndexQuery(indexQuery);

		} else {
			throw new ParserException("Invalid query command");
		}
	}

	/**
	 * This method consumes the query tokens of the lexer while there are legal
	 * tokens. It delegates the actual consummation of tokens to the private
	 * methods.
	 * 
	 * @param query
	 *            the {@link String} representing the query line.
	 */
	private void parseQuery(String query) {
		this.lexer = new QueryLexer(query);
		this.lexer.setState(QueryLexerState.QUERY);

		QueryToken token;
		List<QueryToken> tokens = new ArrayList<>();

		do {
			token = this.lexer.nextToken();
			tokens.add(token);
		} while (this.lexer.getToken().getType() != QueryTokenType.EOL);

		consumeTokens(tokens);
	}

	/**
	 * This method consumes the index-query tokens of the lexer while there are
	 * legal tokens. It delegates the actual consummation of tokens to the
	 * private methods.
	 * 
	 * @param query
	 *            the {@link String} representing the query line.
	 */
	private void parseIndexQuery(String indexQuery) {
		this.lexer = new QueryLexer(indexQuery);
		this.lexer.setState(QueryLexerState.INDEX_QUERY);

		QueryToken token;
		List<QueryToken> tokens = new ArrayList<>();

		do {
			token = this.lexer.nextToken();
			tokens.add(token);
		} while (this.lexer.getToken().getType() != QueryTokenType.EOL);

		createIndexedConditionalExpression(tokens);
	}

	/**
	 * This method creates the actual {@link ConditionalExpression} of the index
	 * query based on the generated {@link QueryToken}s.
	 * 
	 * @param tokens
	 *            the generated tokens of the lexer
	 * 
	 * @throws ParserException
	 *             if token order or concept is invalid
	 */
	private void createIndexedConditionalExpression(List<QueryToken> tokens) {
		IFieldValueGetter fieldValueGetter = null;
		IComparisonOperator comparisonOperator = null;
		String literal = null;

		for (QueryToken currentToken : tokens) {
			switch (currentToken.getType()) {

			case FIELD_NAME:
				if (currentToken.getValue().equals(JMBAG) && tokens.indexOf(currentToken) == 0) {
					fieldValueGetter = new JmbagFieldGetter();
				} else {
					throw new ParserException("The field value in index query must be jmbag ont he first position");
				}
				break;

			case OPERATOR:
				if (currentToken.getValue().equals(EQUAL) && tokens.indexOf(currentToken) == 1) {
					comparisonOperator = new EqualCondition();
				} else {
					throw new ParserException(
							"The operator in index query must be an equals operator on the second position");
				}
				break;

			case STRING_LITERAL:
				if (tokens.indexOf(currentToken) != 2) {
					throw new ParserException("The string literal must be on the third position in index query");
				} else {
					literal = currentToken.getValue();
				}
				break;

			case EOL:
				break;

			default:
				throw new ParserException("Error in parsing index query");
			}
		}

		this.expressions.add(new ConditionalExpression(fieldValueGetter, comparisonOperator, literal));

	}

	/**
	 * Consumes the tokens of the query, creating the actual
	 * {@link ConditionalExpression}.
	 * 
	 * @param tokens
	 *            the generated tokens of the lexer
	 */
	private void consumeTokens(List<QueryToken> tokens) {
		List<QueryToken> expression = new ArrayList<>();

		for (QueryToken token : tokens) {
			if (token.getType() == QueryTokenType.EOL) {
				createConditionalExpression(expression);
				break;
			} else if (token.getType() == QueryTokenType.AND || token.getType().equals(QueryTokenType.EOL)) {
				createConditionalExpression(expression);
				expression.clear();
			} else {
				expression.add(token);
			}
		}
	}

	/**
	 * Creates the new {@link ConditionalExpression} based on the real values of
	 * comparison fields and operators.
	 * 
	 * @param expression
	 *            the generated tokens of the lexer.
	 * 
	 * @throws ParserException
	 *             if token order or concept is invalid
	 */
	private void createConditionalExpression(List<QueryToken> expression) {
		if (expression.size() != 3) {
			throw new ParserException("Invalid number of elements in conditional expression");
		}

		IFieldValueGetter fieldValueGetter = null;
		IComparisonOperator comparisonOperator = null;
		String literal = null;

		for (QueryToken token : expression) {
			switch (token.getType()) {

			case FIELD_NAME:
				if (expression.indexOf(token) != 0) {
					throw new ParserException("Invalid position of field value");
				} else {
					fieldValueGetter = extractFieldName(token);
				}
				break;

			case OPERATOR:
				if (expression.indexOf(token) != 1) {
					throw new ParserException("Invalid position of operator value");
				} else {
					comparisonOperator = extractOperator(token);
				}
				break;

			case STRING_LITERAL:
				if (expression.indexOf(token) != 2) {
					throw new ParserException("Invalid position of literal value");
				} else {
					literal = token.getValue();
				}
				break;

			default:
				throw new ParserException("Error in parsing query");
			}
		}

		this.expressions.add(new ConditionalExpression(fieldValueGetter, comparisonOperator, literal));
	}

	/**
	 * Extracts the name of the field based on the specified {@link QueryToken}.
	 * 
	 * @param token
	 *            the specified token of the lexer
	 * @return the new {@link IFieldValueGetter} field value getter
	 * 
	 * @throws ParserException
	 *             if the name of the field is invalid
	 */
	private IFieldValueGetter extractFieldName(QueryToken token) {

		if (token.getValue().equals(LAST_NAME)) {
			return new LastNameFieldGetter();

		} else if (token.getValue().equals(FIRST_NAME)) {
			return new FirstNameFieldGetter();

		} else if (token.getValue().equals(JMBAG)) {
			return new JmbagFieldGetter();

		} else {
			throw new ParserException("invalid field name;");
		}
	}

	/**
	 * This method extracts the {@link IComparisonOperator} type from the given
	 * {@link QueryToken}.
	 * 
	 * @param token
	 *            the specified {@link QueryToken}
	 * @return the new {@link IComparisonOperator} instance
	 * @throws ParserException
	 *             if there was an invalid operator.
	 */
	private IComparisonOperator extractOperator(QueryToken token) {

		if (token.getValue().equals(LESS_THAN)) {
			return new LessThanCondition();

		} else if (token.getValue().equals(GREATER_THAN)) {
			return new GreaterThanCondition();

		} else if (token.getValue().equals(LESS_EQUAL)) {
			return new LessOrEqualCondition();

		} else if (token.getValue().equals(GREATER_EQUAL)) {
			return new GreaterOrEqualCondition();

		} else if (token.getValue().equals(EQUAL)) {
			return new EqualCondition();

		} else if (token.getValue().equals(NOT_EQUAL)) {
			return new NotEqualCondition();

		} else if (token.getValue().equals(LIKE)) {
			return new WildCardEqualsCondition();

		} else {
			throw new ParserException("Invalid operator");
		}
	}
}
