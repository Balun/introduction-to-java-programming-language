package hr.fer.zemris.java.tecaj.hw5.filter;

import hr.fer.zemris.java.tecaj.hw5.db.*;

/**
 * This interface represents a general filter for the {@link StudentDatabase}.
 * Classes that implements this interface must give the algorithms and
 * conditions for filtering the elements in the database.
 * 
 * @author Matej Balun
 *
 */
public interface IFilter {

	/**
	 * Checks if the specified {@link StudentRecord} satisfies the specified
	 * filtering condition.
	 * 
	 * @param record
	 *            the specified {@link StudentRecord}
	 * @return true if the record is accepted, otherwise false
	 */
	public boolean accepts(StudentRecord record);
}
