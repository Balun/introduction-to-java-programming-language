package hr.fer.zemris.java.tecaj.hw5.collections;

import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * This class represents the simple map-like collection {@link SimpleHashTable}.
 * The class is generic, offering the user to determine the type of the key and
 * value. The class offers the all the methods expected for the map-type
 * collection. Average time Complexity of most of the methods is O(1). The class
 * implements {@link Iterable} interface, offering the iteration through its
 * elements.
 * 
 * @author Matej Balun
 *
 * @param <K>
 *            type of the key
 * @param <V>
 *            type of the value
 */
public class SimpleHashTable<K, V> implements Iterable<SimpleHashTable.TableEntry<K, V>> {

	/**
	 * Number of elements in the table.
	 */
	private int size;

	/**
	 * Number of slotas in the table.
	 */
	private int slots;

	/**
	 * The inner reference to the table.
	 */
	private TableEntry<K, V>[] table;

	/**
	 * Count of the all issued modifications.
	 */
	private int modificationCount;

	/**
	 * The default number of slots.
	 */
	private static final int DEFAULT_SIZE = 16;

	/**
	 * Minimal number of slots.
	 */
	private static final int MIN_SIZE = 1;

	/**
	 * The default constructor. It initializes the {@link SimpleHashTable} with
	 * the default number of slots.
	 */
	@SuppressWarnings("unchecked")
	public SimpleHashTable() {
		this.slots = DEFAULT_SIZE;
		this.size = 0;
		this.table = new SimpleHashTable.TableEntry[16];
		this.modificationCount = 0;
	}

	/**
	 * This constructor initializes the {@link SimpleHashTable} with the number
	 * of slot equivalent to first power of number 2 greater or equal to the
	 * specified capacity.
	 * 
	 * @param capacity
	 *            the specified capacity
	 * @throws IllegalArgumentException
	 *             if the capacity is less than 1
	 */
	@SuppressWarnings("unchecked")
	public SimpleHashTable(int capacity) {
		if (capacity < MIN_SIZE) {
			throw new IllegalArgumentException("The capacity muste be greater or equal to 1.");
		}

		int i = 1;
		while (true) {
			if (i >= capacity) {
				this.slots = i;
				break;
			} else {
				i = i * 2;
			}
		}

		this.slots = i;
		this.table = new SimpleHashTable.TableEntry[i];
		this.modificationCount = 0;
	}

	/**
	 * Inserts the specified element in the table on the calculated slot based
	 * on the specified key. The average time complexity is O(1). The value can
	 * be a null-reference.
	 * 
	 * @param key
	 *            the specified key
	 * @param value
	 *            the specified value
	 * 
	 * @throws IllegalArgumentException
	 *             if the key is a null-reference
	 */
	public void put(K key, V value) {
		if (key == null) {
			throw new IllegalArgumentException("The key must not be a null-reference.");
		}

		TableEntry<K, V> newElement = new TableEntry<>(key, value, null);
		if (!containsKey(key)) {
			this.size++;
			modificationCount++;
		}
		putEntry(newElement);
		checkCapacity();
	}

	/**
	 * Returns the object with specified key. The average time complexity of the
	 * method is O(1).
	 * 
	 * @param key
	 *            the specified key
	 * @return the value of the specified key
	 */
	public V get(Object key) {
		int calcSlot = calculateSlot(key);
		TableEntry<K, V> list = this.table[calcSlot];

		while (list != null) {
			if (list.key.equals(key)) {
				return list.value;
			}
			list = list.next;
		}

		return null;
	}

	/**
	 * Returns the number of elements in the table.
	 * 
	 * @return number of elements in the table
	 */
	public int size() {
		return this.size;
	}

	/**
	 * Checks if the table contains the entry with the specified key.
	 * 
	 * @param key
	 *            the specified key
	 * @return true if the table contains the key, otherwise false
	 */
	public boolean containsKey(Object key) {
		if (key == null) {
			return false;
		}

		for (TableEntry<K, V> list : this.table) {
			while (list != null) {
				if (list.key.equals(key)) {
					return true;
				} else {
					list = list.next;
				}
			}
		}

		return false;
	}

	/**
	 * Checks if the table contains the specified value.
	 * 
	 * @param value
	 *            the specified value
	 * @return true if the table contains the value, otherwise false.
	 */
	public boolean containsValue(Object value) {
		for (TableEntry<K, V> list : this.table) {

			while (list != null) {
				if (value == null && list.value == null) {
					return true;
				} else if (list.value.equals(value)) {
					return true;
				} else {
					list = list.next;
				}
			}
		}

		return false;
	}

	/**
	 * Removes the entry with the specified key from the table. The average time
	 * complexity of the method is O(1).
	 * 
	 * @param key
	 *            the specified key for removal
	 */
	public void remove(Object key) {
		if (key == null) {
			return;
		}

		int calcSlot = calculateSlot(key);
		TableEntry<K, V> listPrev = this.table[calcSlot];
		TableEntry<K, V> listCurr = listPrev.next;

		removeEntry(listPrev, listCurr, key, calcSlot);
		modificationCount++;
	}

	/**
	 * Checks if the table has no elements
	 * 
	 * @return true if the table is empty, otherwise false.
	 */
	public boolean isEmpty() {
		if (this.size == 0) {
			return true;
		}

		return false;
	}

	/**
	 * Clears the table of all elements.
	 */
	@SuppressWarnings({ "unchecked" })
	public void clear() {
		this.size = 0;
		this.table = new SimpleHashTable.TableEntry[slots];
		this.modificationCount = 0;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		if (isEmpty()) {
			return "";
		}

		StringBuilder builder = new StringBuilder();
		builder.append("[");

		for (TableEntry<K, V> list : this.table) {
			while (list != null) {
				builder.append(list.toString() + ", ");
				list = list.next;
			}
		}

		builder.replace(builder.length() - 2, builder.length(), "");
		builder.append("]");
		return builder.toString();
	}

	/**
	 * Calculates the slot for the specified key
	 * 
	 * @param key
	 *            the specified key
	 * @return the calculated slot of the key
	 */
	private int calculateSlot(Object key) {
		return (int) (Math.abs(key.hashCode()) % this.slots);
	}

	/**
	 * Inserts the complete {@link TableEntry} in the table.
	 * 
	 * @param entry
	 *            - the specified instance of {@link TableEntry}
	 */
	private void putEntry(TableEntry<K, V> entry) {
		TableEntry<K, V> list;
		int calcSlot = calculateSlot(entry.key);
		list = this.table[calcSlot];

		while (list != null) {
			if (list.key.equals(entry.key)) {
				list.value = entry.value;
				return;
			} else {
				if (list.next == null) {
					break;
				} else {
					list = list.next;
				}
			}
		}

		if (list == null) {
			this.table[calcSlot] = entry;
		} else {
			list.next = entry;
		}
	}

	/**
	 * Helper method for removal of the specified entry in the table. The method
	 * uses the references to the current and previous element in the table.
	 * 
	 * @param listPrev
	 *            reference to the previous value
	 * @param listCurr
	 *            reference to the current value
	 * @param key
	 *            the specified key
	 * @param calcSlot
	 *            calculated slot for removal
	 */
	private void removeEntry(TableEntry<K, V> listPrev, TableEntry<K, V> listCurr, Object key, int calcSlot) {
		if (this.table[calcSlot] == null) {
			return;
		}

		if (listPrev.key.equals(key)) {
			this.table[calcSlot] = listCurr;
			this.size--;
			return;
		} else {
			while (listCurr != null) {
				if (listCurr.key.equals(key)) {
					listPrev.next = listCurr.next;
					listCurr = listPrev.next;
					this.size--;
					return;
				} else {
					listCurr = listCurr.next;
					listPrev = listPrev.next;
				}
			}
		}
	}

	/**
	 * Checks the capacity of the table and if needed, resizes the table with
	 * double number of slots.
	 */
	@SuppressWarnings("unchecked")
	private void checkCapacity() {
		if (size < 0.75 * slots) {
			return;
		}

		TableEntry<K, V>[] tmp = this.table;
		this.slots *= 2;
		this.table = new SimpleHashTable.TableEntry[this.slots];

		for (TableEntry<K, V> entry : tmp) {
			if (entry == null) {
				continue;
			} else if (entry != null && entry.next != null) {
				while (entry != null) {
					putEntry(entry);
					entry = entry.next;
				}
			} else {
				putEntry(entry);
				continue;
			}
		}

		modificationCount++;
	}

	/**
	 * This nested static class represents the entry of the
	 * {@link SimpleHashTable}. The {@link TableEntry} contains the key an the
	 * value of the entry.
	 * 
	 * @author Matej Balun
	 *
	 * @param <K>
	 *            type of the key
	 * @param <V>
	 *            type of the value
	 */
	public static class TableEntry<K, V> {

		private K key;
		private V value;
		private TableEntry<K, V> next;

		public TableEntry(K key, V value, TableEntry<K, V> next) {
			this.key = key;
			this.value = value;
			this.next = next;
		}

		/**
		 * Returns the key of the {@link TableEntry}.
		 * 
		 * @return key of the {@link TableEntry}
		 */
		public Object getKey() {
			return this.key;
		}

		/**
		 * Returns the value of the {@link TableEntry}.
		 * 
		 * @return value of the {@link TableEntry}
		 */
		public Object getValue() {
			return this.value;
		}

		/**
		 * Sets the value of the {@link TableEntry}.
		 * 
		 * @param the
		 *            specified value
		 */
		public void setValue(V value) {
			this.value = value;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public String toString() {
			return this.key.toString() + "=" + this.value.toString();
		}
	}

	/**
	 * Returns the new instance of the table's {@link Iterator}.
	 */
	public Iterator<TableEntry<K, V>> iterator() {
		return new IteratorImpl();
	}

	/**
	 * This class represents the iterator for the {@link SimpleHashTable}. It
	 * implements the {@link Iterator} interface, offering the optional method
	 * for removal.
	 * 
	 * @author Matej Balun
	 *
	 */
	private class IteratorImpl implements Iterator<SimpleHashTable.TableEntry<K, V>> {

		/**
		 * The current slot index.
		 */
		private int slotIndex = 0;

		/**
		 * A reference to the table entry.
		 */
		private TableEntry<K, V> current = table[0];

		/**
		 * Number of remaining elements for iteration
		 */
		private int remaining = size;

		/**
		 * The return value for hasNext and fo removal.
		 */
		private TableEntry<K, V> value;

		/**
		 * Number of iterators's modifications, initially set to the table's
		 * number of modifications.
		 */
		private int currentModification = modificationCount;

		/**
		 * {@inheritDoc}
		 */
		@Override
		public boolean hasNext() {
			if (currentModification != modificationCount) {
				throw new ConcurrentModificationException();
			}
			return remaining > 0;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public TableEntry<K, V> next() {
			if (currentModification != modificationCount) {
				throw new ConcurrentModificationException();
			}

			if (!hasNext()) {
				throw new NoSuchElementException();
			}

			while (current == null) {
				current = table[++slotIndex];
			}

			value = current;

			if (current.next != null) {
				current = current.next;
			} else if (slotIndex < slots - 1) {
				current = table[++slotIndex];
			}

			remaining--;
			return value;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void remove() {
			if (currentModification != modificationCount) {
				throw new ConcurrentModificationException();
			}

			if (value == null) {
				throw new IllegalStateException("Cannot remove the current object twice.");
			}

			int calcSlot = calculateSlot(value.key);
			removeEntry(table[calcSlot], table[calcSlot].next, value.key, calcSlot);
			modificationCount++;
			currentModification++;
			value = null;
		}
	}
}
