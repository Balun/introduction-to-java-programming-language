package hr.fer.zemris.java.tecaj.hw5.getters;

import hr.fer.zemris.java.tecaj.hw5.db.*;

/**
 * This interface represents a strategy patterns for retrieving the attributes
 * of the {@link StudentRecord}. The classes that implement this interface must
 * implement the Strategy desing pattern and retrieve the concrete attributes of
 * the {@link StudentRecord}s.
 * 
 * @author Matej Balun
 *
 */
public interface IFieldValueGetter {

	/**
	 * This Methods returns the specified value field of the
	 * {@link StudentRecord}.
	 * 
	 * @param the
	 *            specified {@link StudentRecord}.
	 * @return the specified value field
	 */
	public String get(StudentRecord record);

}
