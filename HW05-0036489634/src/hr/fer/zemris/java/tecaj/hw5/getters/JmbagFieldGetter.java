package hr.fer.zemris.java.tecaj.hw5.getters;

import hr.fer.zemris.java.tecaj.hw5.db.*;

/**
 * This class represents the strategy pattern getter for the jmbag value field
 * of the {@link StudentRecord}. It implements the {@link IFieldValueGetter}
 * interface
 * 
 * @author Matej Balu
 *
 */
public class JmbagFieldGetter implements IFieldValueGetter {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String get(StudentRecord record) {
		return record.getJmbag();
	}

}
