package hr.fer.zemris.java.tecaj.hw5.filter;

import java.util.List;

import hr.fer.zemris.java.tecaj.hw5.db.ConditionalExpression;
import hr.fer.zemris.java.tecaj.hw5.db.StudentRecord;
import hr.fer.zemris.java.tecaj.hw5.parsing.QueryParser;

/**
 * This class represents the implementation of the query command. The command
 * retrieves a number of {@link StudentRecord} specified by one or more
 * {@link ConditionalExpression}s. It implements the {@link IFilter} interface.
 * The {@link ConditionalExpression}s must have the "AND" operator inside the
 * query and all the {@link ConditionalExpression} of the query must be
 * satisfied for data retrieval.
 * 
 * @author Matej Balun
 *
 */
public class QueryFilter implements IFilter {

	/**
	 * a {@link String} representing the query line.
	 */
	private String queryString;

	/**
	 * The {@link QueryParser} for parsing the query.
	 */
	private QueryParser parser;

	/**
	 * This constructor initializes the {@link QueryFilter} with the string
	 * representing the query.
	 * 
	 * @param queryString
	 *            the query {@link String} representation.
	 */
	public QueryFilter(String queryString) {
		if (queryString == null) {
			throw new IllegalArgumentException("The querry line must not be a null - reference");
		}

		this.queryString = queryString;
		this.parser = new QueryParser(this.queryString);
	}

	/**
	 * {@inheritDoc} It parses the query via {@link QueryParser}, checking if
	 * the {@link List} of entities from the database satisfies the specified
	 * {@link ConditionalExpression}s.
	 */
	@Override
	public boolean accepts(StudentRecord record) {
		this.parser.parse();
		List<ConditionalExpression> conditions = parser.getConditionalExpressions();

		for (ConditionalExpression condition : conditions) {
			if (!condition.getComparisonOperator().satisfied(condition.getFieldValueGetter().get(record),
					condition.getLiteral())) {
				return false;
			}
		}

		return true;
	}

}
