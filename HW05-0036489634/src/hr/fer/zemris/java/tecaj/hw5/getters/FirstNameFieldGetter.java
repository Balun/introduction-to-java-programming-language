package hr.fer.zemris.java.tecaj.hw5.getters;

import hr.fer.zemris.java.tecaj.hw5.db.*;

/**
 * This class represents a strategy pattern getter for the first name of the
 * {@link StudentRecord}. It implements the {@link IFieldValueGetter} interface.
 * 
 * @author Matej Balun
 *
 */
public class FirstNameFieldGetter implements IFieldValueGetter {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String get(StudentRecord record) {
		return record.getFirstname();
	}

}
