
package hr.fer.zemris.java.tecaj.hw5.db;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.junit.Test;

import hr.fer.zemris.java.tecaj.hw5.conditions.EqualCondition;
import hr.fer.zemris.java.tecaj.hw5.conditions.GreaterOrEqualCondition;
import hr.fer.zemris.java.tecaj.hw5.conditions.GreaterThanCondition;
import hr.fer.zemris.java.tecaj.hw5.conditions.IComparisonOperator;
import hr.fer.zemris.java.tecaj.hw5.conditions.LessOrEqualCondition;
import hr.fer.zemris.java.tecaj.hw5.conditions.LessThanCondition;
import hr.fer.zemris.java.tecaj.hw5.conditions.NotEqualCondition;
import hr.fer.zemris.java.tecaj.hw5.conditions.WildCardEqualsCondition;
import hr.fer.zemris.java.tecaj.hw5.filter.IFilter;
import hr.fer.zemris.java.tecaj.hw5.filter.QueryFilter;
import hr.fer.zemris.java.tecaj.hw5.getters.FirstNameFieldGetter;
import hr.fer.zemris.java.tecaj.hw5.getters.IFieldValueGetter;
import hr.fer.zemris.java.tecaj.hw5.getters.JmbagFieldGetter;
import hr.fer.zemris.java.tecaj.hw5.getters.LastNameFieldGetter;
import hr.fer.zemris.java.tecaj.hw5.parsing.ParserException;

public class TestsDatabase {

	StudentRecord record = new StudentRecord("0000000003", "Bengalka", "Krešo", 5);

	@Test
	public void testRegularCreation() {
		assertEquals("Invalid string.", "0000000003", record.getJmbag());
		assertEquals("Invalid string.", "Krešo", record.getFirstname());
		assertEquals("Invalid string.", "Bengalka", record.getLastName());
		assertEquals("Invalid string.", 5, record.getFinalGrade());
	}

	@Test
	public void testEqual() {
		IFilter filter1 = new QueryFilter("query firstName   = \"Krešo\" ");
		IFilter filter2 = new QueryFilter("query firstName   = \"Vojko\" ");
		assertEquals("Invalid string.", true, filter1.accepts(record));
		assertEquals("Invalid string.", false, filter2.accepts(record));
	}

	@Test
	public void testNotEqual() {
		IFilter filter1 = new QueryFilter("query firstName   != \"Krešo\" ");
		IFilter filter2 = new QueryFilter("query firstName   != \"Vojko\" ");
		assertEquals("Invalid string.", false, filter1.accepts(record));
		assertEquals("Invalid string.", true, filter2.accepts(record));
	}

	@Test
	public void testGreater() {
		IFilter filter1 = new QueryFilter("query firstName   > \"Krešo\" ");
		IFilter filter2 = new QueryFilter("query firstName   > \"Vojko\" ");
		assertEquals("Invalid string.", false, filter1.accepts(record));
		assertEquals("Invalid string.", false, filter2.accepts(record));
	}

	@Test
	public void testGreaterEqual() {
		IFilter filter1 = new QueryFilter("query firstName   >= \"Krešo\" ");
		IFilter filter2 = new QueryFilter("query firstName   >= \"Vojko\" ");
		assertEquals("Invalid string.", true, filter1.accepts(record));
		assertEquals("Invalid string.", false, filter2.accepts(record));
	}

	@Test
	public void testLess() {
		IFilter filter1 = new QueryFilter("query firstName   < \"Krešo\" ");
		IFilter filter2 = new QueryFilter("query firstName   < \"Vojko\" ");
		assertEquals("Invalid string.", false, filter1.accepts(record));
		assertEquals("Invalid string.", true, filter2.accepts(record));
	}

	@Test
	public void testLessEqual() {
		IFilter filter1 = new QueryFilter("query firstName   <= \"Krešo\" ");
		IFilter filter2 = new QueryFilter("query firstName   <= \"Vojko\" ");
		assertEquals("Invalid string.", true, filter1.accepts(record));
		assertEquals("Invalid string.", true, filter2.accepts(record));
	}

	@Test
	public void testWildcard() {
		IFilter filter1 = new QueryFilter("query lastName LIKE \"Be*ka\" ");
		IFilter filter2 = new QueryFilter("query lastName LIKE \"Be*vka\" ");
		IFilter filter3 = new QueryFilter("query jmbag LIKE \"*3\" ");
		IFilter filter4 = new QueryFilter("query jmbag LIKE \"123*\" ");
		IFilter filter5 = new QueryFilter("query lastName LIKE \"*\" ");
		IFilter filter6 = new QueryFilter("query lastName LIKE \"Bengalka\" ");
		assertEquals("Invalid string.", true, filter1.accepts(record));
		assertEquals("Invalid string.", false, filter2.accepts(record));
		assertEquals("Invalid string.", true, filter3.accepts(record));
		assertEquals("Invalid string.", false, filter4.accepts(record));
		assertEquals("Invalid string.", true, filter5.accepts(record));
		assertEquals("Invalid string.", true, filter6.accepts(record));
	}

	@Test
	public void testMultipleExpressions() {
		IFilter filter1 = new QueryFilter(
				"query lastName LIKE \"Be*ka\" anD firstName   <= \"Krešo\" aNd  firstName   >= \"Krešo\"  "
						+ " and   firstName   != \"Vojko\" ");
		assertEquals("Invalid string.", true, filter1.accepts(record));
	}

	@Test
	public void testDatabaseMap() throws IOException {
		List<String> lines = Files.readAllLines(Paths.get("database.txt"), StandardCharsets.UTF_8);
		StudentDatabase database = new StudentDatabase(lines);
		assertEquals("Invalid string.", true, database.forJMBAG("0000000003").getLastName().equals("Bosnić"));
	}

	@Test
	public void testRecordEquals() {
		StudentRecord record1 = new StudentRecord("0000000003", "Vojko", "V", 4);
		assertEquals("Invalid string.", true, record.equals(record1));
	}

	@Test(expected = NullPointerException.class)
	public void testNull1() {
		IFieldValueGetter getter = new FirstNameFieldGetter();
		getter.get(null);
	}

	@Test(expected = NullPointerException.class)
	public void testNull2() {
		IFieldValueGetter getter = new LastNameFieldGetter();
		getter.get(null);
	}

	@Test(expected = NullPointerException.class)
	public void testNull3() {
		IFieldValueGetter getter = new JmbagFieldGetter();
		getter.get(null);
	}

	@Test(expected = NullPointerException.class)
	public void testNull4() {
		IComparisonOperator operator = new EqualCondition();
		operator.satisfied(null, "abc");
	}

	@Test(expected = NullPointerException.class)
	public void testNull5() {
		IComparisonOperator operator = new NotEqualCondition();
		operator.satisfied(null, "abc");
	}

	@Test(expected = NullPointerException.class)
	public void testNull6() {
		IComparisonOperator operator = new GreaterOrEqualCondition();
		operator.satisfied(null, "abc");
	}

	@Test(expected = NullPointerException.class)
	public void testNull7() {
		IComparisonOperator operator = new LessOrEqualCondition();
		operator.satisfied(null, "abc");
	}

	@Test(expected = NullPointerException.class)
	public void testNull8() {
		IComparisonOperator operator = new LessThanCondition();
		operator.satisfied(null, "abc");
	}

	@Test(expected = NullPointerException.class)
	public void testNull9() {
		IComparisonOperator operator = new GreaterThanCondition();
		operator.satisfied(null, "abc");
	}

	@Test(expected = NullPointerException.class)
	public void testNull10() {
		IComparisonOperator operator = new WildCardEqualsCondition();
		operator.satisfied(null, "abc");
	}

	@Test(expected = ParserException.class)
	public void testIllArg() {
		IComparisonOperator operator = new WildCardEqualsCondition();
		operator.satisfied("abc", "12*34*56");
	}

}
