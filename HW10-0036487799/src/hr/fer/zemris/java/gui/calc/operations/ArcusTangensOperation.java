package hr.fer.zemris.java.gui.calc.operations;

/**
 * The Class ArcusTangensOperation represents operation arcus tangens.
 * 
 * @author Josip
 */
public class ArcusTangensOperation implements SingleArgumentOperation {

	@Override
	public double doOperation(double number, boolean inverted) {
		double result;
		if (inverted) {
			result = Math.tan(number);
		} else {
			result = Math.atan(number);
		}
		return result;
	}

}
