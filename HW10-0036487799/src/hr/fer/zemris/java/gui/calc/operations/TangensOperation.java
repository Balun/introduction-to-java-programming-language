package hr.fer.zemris.java.gui.calc.operations;

public class TangensOperation implements SingleArgumentOperation {

	@Override
	public double doOperation(double number, boolean inverted) {
		double result;
		if (inverted) {
			result = Math.atan(number);
		} else {
			result = Math.tan(number);
		}
		return result;
	}

}
