package hr.fer.zemris.java.gui.calc.operations;

public class SubtractOperation implements TwoArgumentOperation {

	@Override
	public double doOperation(double firstNumber, double secondNumber) {
		return firstNumber - secondNumber;
	}

}
