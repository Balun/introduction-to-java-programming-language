package hr.fer.zemris.java.gui.calc.operations;

/**
 * The Class DivideOperation represents operation divide.
 * 
 * @author Josip
 */
public class DivideOperation implements TwoArgumentOperation {

	@Override
	public double doOperation(double firstNumber, double secondNumber) {
		return firstNumber / secondNumber;
	}

}
