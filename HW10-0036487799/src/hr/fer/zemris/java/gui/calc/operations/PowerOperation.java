package hr.fer.zemris.java.gui.calc.operations;

public class PowerOperation implements IOperation {

	public double doOperation(double number, double exponent, boolean inverted) {
		double result;
		if (inverted) {
			result = Math.pow(number, 1.0 / exponent);
		} else {
			result = Math.pow(number, exponent);
		}
		return result;
	}

}
