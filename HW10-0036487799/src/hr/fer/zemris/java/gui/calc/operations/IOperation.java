package hr.fer.zemris.java.gui.calc.operations;

public interface IOperation {
	public default double doOperation(double number, boolean inverted) {
		return 0;
	}
	public default double doOperation(double firstNumber, double secondNumber) {
		return 0;
	}
}
