package hr.fer.zemris.java.gui.calc.operations;

public interface TwoArgumentOperation extends IOperation{

	public double doOperation(double firstNumber, double secondNumber);
}
