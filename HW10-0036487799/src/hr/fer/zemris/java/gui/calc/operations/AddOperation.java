package hr.fer.zemris.java.gui.calc.operations;

/**
 * The Class AddOperation represents operation add.
 * 
 * @author Josip
 */
public class AddOperation implements TwoArgumentOperation {

	@Override
	public double doOperation(double number1, double number2) {
		return number1 + number2;
	}
}
