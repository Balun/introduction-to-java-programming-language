package hr.fer.zemris.java.gui.calc;

import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;

import hr.fer.zemris.java.gui.calc.operations.*;
import hr.fer.zemris.java.gui.layouts.CalcLayout;
import hr.fer.zemris.java.gui.layouts.RCPosition;

/**
 * The Class Calculator represents gui for basic calculator operation similar to
 * those on old Windows. Has stack to memory values on display.
 * 
 * @author Josip
 */
public class Calculator extends JFrame {

	/**
	 * Serial number.
	 */
	private static final long serialVersionUID = -6180821893308632997L;

	/** First operand. */
	private double firstNumber;

	/** Second operand. */
	private double secondNumber;

	/** Current operation. */
	private IOperation operation;

	/** Current screen text. */
	private String currentScreenText = "";

	/** Stack of calculator. */
	private List<String> stack = new ArrayList<>();

	/** If inverted button is activated. */
	private boolean inverted = false;

	/**
	 * Instantiates a new calculator.
	 */
	public Calculator() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setTitle("Calculator");
		setSize(700, 400);
		setLocation(20, 20);
		initGUI();
	}

	/**
	 * Inits the gui.
	 */
	private void initGUI() {
		Container cp = this.getContentPane();
		cp.setLayout(new CalcLayout(3));
		JLabel screen = new JLabel();
		screen.setHorizontalAlignment(JLabel.RIGHT);
		screen.setOpaque(true);
		screen.setBackground(Color.YELLOW);

		screen.setFont(new Font(screen.getFont().getName(), Font.BOLD, screen.getFont().getSize() * 2));
		screen.setBorder(BorderFactory.createLineBorder(Color.BLUE));

		cp.add(screen, new RCPosition(1, 1));
		JButton equal = new JButton("=");
		equal.addActionListener(e -> {
			this.secondNumber = Double.parseDouble(screen.getText().replace(",", "."));
			double result;

			if (operation instanceof PowerOperation) {
				result = ((PowerOperation) operation).doOperation(firstNumber, secondNumber, inverted);
			} else {
				result = operation.doOperation(this.firstNumber, this.secondNumber);
			}

			screen.setText(String.format("%.2f", result));
			firstNumber = result;
			currentScreenText = "";
			operation = null;
		});
		cp.add(equal, new RCPosition(1, 6));

		JButton clr = new JButton("clr");
		clr.addActionListener(e -> {
			screen.setText("");
			currentScreenText = "";
		});
		cp.add(clr, new RCPosition(1, 7));

		JButton reciprocally = new JButton("1/x");
		reciprocally.addActionListener(e -> {
			applyOperation(new ReciprocallyOperation(), screen);
		});
		cp.add(reciprocally, new RCPosition(2, 1));

		JButton sin = new JButton("sin");
		sin.addActionListener(e -> {
			applyOperation(new SinusOperation(), screen);
		});
		cp.add(sin, new RCPosition(2, 2));

		JButton seven = new JButton("7");
		seven.addActionListener(e -> {
			currentScreenText += "7";
			screen.setText(currentScreenText);
		});
		cp.add(seven, new RCPosition(2, 3));

		JButton eight = new JButton("8");
		eight.addActionListener(e -> {
			currentScreenText += "8";
			screen.setText(currentScreenText);
		});
		cp.add(eight, new RCPosition(2, 4));

		JButton nine = new JButton("9");
		nine.addActionListener(e -> {
			currentScreenText += "9";
			screen.setText(currentScreenText);
		});
		cp.add(nine, new RCPosition(2, 5));

		JButton div = new JButton("/");
		div.addActionListener(e -> {
			applyOperation(new DivideOperation(), screen);
		});
		cp.add(div, new RCPosition(2, 6));

		JButton res = new JButton("res");
		res.addActionListener(e -> {
			firstNumber = 0;
			secondNumber = 0;
			operation = null;
			currentScreenText = "";
			screen.setText(currentScreenText);
			stack.clear();
			inverted = false;
		});
		cp.add(res, new RCPosition(2, 7));

		JButton log = new JButton("log");
		log.addActionListener(e -> {
			applyOperation(new LogOperation(), screen);
		});
		cp.add(log, new RCPosition(3, 1));

		JButton cos = new JButton("cos");
		cos.addActionListener(e -> {
			applyOperation(new CosinusOperation(), screen);
		});
		cp.add(cos, new RCPosition(3, 2));

		JButton four = new JButton("4");
		four.addActionListener(e -> {
			currentScreenText += "4";
			screen.setText(currentScreenText);
		});
		cp.add(four, new RCPosition(3, 3));

		JButton five = new JButton("5");
		five.addActionListener(e -> {
			currentScreenText += "5";
			screen.setText(currentScreenText);
		});
		cp.add(five, new RCPosition(3, 4));

		JButton six = new JButton("6");
		six.addActionListener(e -> {
			currentScreenText += "6";
			screen.setText(currentScreenText);
		});
		cp.add(six, new RCPosition(3, 5));

		JButton times = new JButton("*");
		times.addActionListener(e -> {
			applyOperation(new MultiplyOperation(), screen);
		});
		cp.add(times, new RCPosition(3, 6));

		JButton push = new JButton("push");
		push.addActionListener(e -> {
			if(!currentScreenText.equals(""))
			stack.add(currentScreenText.trim());
		});
		cp.add(push, new RCPosition(3, 7));

		JButton ln = new JButton("ln");
		ln.addActionListener(e -> {
			applyOperation(new LnOperation(), screen);
		});
		cp.add(ln, new RCPosition(4, 1));

		JButton tan = new JButton("tan");
		tan.addActionListener(e -> {
			applyOperation(new TangensOperation(), screen);
		});
		cp.add(tan, new RCPosition(4, 2));

		JButton one = new JButton("1");
		one.addActionListener(e -> {
			currentScreenText += "1";
			screen.setText(currentScreenText);
		});
		cp.add(one, new RCPosition(4, 3));

		JButton two = new JButton("2");
		two.addActionListener(e -> {
			currentScreenText += "2";
			screen.setText(currentScreenText);
		});
		cp.add(two, new RCPosition(4, 4));

		JButton three = new JButton("3");
		three.addActionListener(e -> {
			currentScreenText += "3";
			screen.setText(currentScreenText);
		});
		cp.add(three, new RCPosition(4, 5));

		JButton minus = new JButton("-");
		minus.addActionListener(e -> {
			applyOperation(new SubtractOperation(), screen);
		});
		cp.add(minus, new RCPosition(4, 6));

		JButton pop = new JButton("pop");
		pop.addActionListener(e -> {
			if (stack.size() == 0) {
				screen.setText("Stack empty.");
			} else {
				screen.setText(stack.get(stack.size() - 1));
				stack.remove(stack.size() - 1);
			}
		});
		cp.add(pop, new RCPosition(4, 7));

		JButton power = new JButton("x^n");
		power.addActionListener(e -> {
			this.firstNumber = Double.parseDouble(screen.getText().replace(",", "."));
			this.operation = new PowerOperation();
			currentScreenText = "";
		});
		cp.add(power, new RCPosition(5, 1));

		JButton ctg = new JButton("ctg");
		ctg.addActionListener(e -> {
			applyOperation(new ArcusTangensOperation(), screen);
		});
		cp.add(ctg, new RCPosition(5, 2));

		JButton zero = new JButton("0");
		zero.addActionListener(e -> {
			screen.setText(screen.getText() + "0");
		});
		cp.add(zero, new RCPosition(5, 3));

		JButton negate = new JButton("+/-");
		negate.addActionListener(e -> {
			currentScreenText = "-" + currentScreenText;
			screen.setText(currentScreenText);
		});
		cp.add(negate, new RCPosition(5, 4));

		JButton dot = new JButton(".");
		dot.addActionListener(e -> {
			currentScreenText += ",";
			screen.setText(currentScreenText);
		});
		cp.add(dot, new RCPosition(5, 5));

		JButton plus = new JButton("+");
		plus.addActionListener(e -> {
			applyOperation(new AddOperation(), screen);
		});

		cp.add(plus, new RCPosition(5, 6));
		JCheckBox inv = new JCheckBox("Inv");
		inv.addActionListener(e -> {
			inverted = inv.isSelected();
		});
		cp.add(inv, new RCPosition(5, 7));

	}

	/**
	 * Applies given operation and puts result to given screen.
	 *
	 * @param operation
	 *            Given operation.
	 * @param screen
	 *            Given screen.
	 */
	private void applyOperation(IOperation operation, JLabel screen) {

		if (operation instanceof SingleArgumentOperation) {
			double currentScreenValue = Double.parseDouble(screen.getText().replace(",", "."));
			this.firstNumber = operation.doOperation(currentScreenValue, inverted);
			screen.setText(String.format("%.2f", firstNumber));
			this.operation = null;
			currentScreenText = "";
			return;
		}

		if (this.operation == null) {
			this.firstNumber = Double.parseDouble(screen.getText().replace(",", "."));
			this.operation = operation;
			currentScreenText = "";
		} else {
			secondNumber = Double.parseDouble(screen.getText().replace(",", "."));
			double previous = this.operation.doOperation(firstNumber, secondNumber);
			firstNumber = previous;
			this.operation = operation;
			currentScreenText = "";
		}
	}

	/**
	 * The main method is called when program starts.
	 *
	 * @param args
	 *            No arguments needed.
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> {
			new Calculator().setVisible(true);
		});
	}
}
