package hr.fer.zemris.java.gui.calc.operations;

/**
 * The Class CosinusOperation represents operation cosinus.
 * 
 * @author Josip
 */
public class CosinusOperation implements SingleArgumentOperation {

	@Override
	public double doOperation(double number, boolean inverted) {
		double result;
		if (inverted) {
			result = Math.acos(number);
		} else {
			result = Math.cos(number);
		}
		return result;
	}

}
