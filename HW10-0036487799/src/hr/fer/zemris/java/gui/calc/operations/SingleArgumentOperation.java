package hr.fer.zemris.java.gui.calc.operations;

public interface SingleArgumentOperation extends IOperation {
	public double doOperation(double number, boolean inverted);
}
