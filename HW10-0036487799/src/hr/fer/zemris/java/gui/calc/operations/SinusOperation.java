package hr.fer.zemris.java.gui.calc.operations;

public class SinusOperation implements SingleArgumentOperation {

	@Override
	public double doOperation(double number, boolean inverted) {
		double result;
		if (inverted) {
			result = Math.asin(number);
		} else {
			result = Math.sin(number);
		}
		return result;
	}

}
