package hr.fer.zemris.java.gui.calc.operations;

public class LogOperation implements SingleArgumentOperation {

	public static final int TEN = 10;

	@Override
	public double doOperation(double number, boolean inverted) {
		double result;

		if (inverted) {
			result = Math.pow(TEN, number);
		} else {
			result = Math.log10(number);
		}
		return result;
	}

}
