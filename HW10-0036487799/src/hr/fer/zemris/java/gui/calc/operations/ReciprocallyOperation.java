package hr.fer.zemris.java.gui.calc.operations;

public class ReciprocallyOperation implements SingleArgumentOperation {

	@Override
	public double doOperation(double number, boolean inverted) {
		return 1.0 / number;
	}

}
