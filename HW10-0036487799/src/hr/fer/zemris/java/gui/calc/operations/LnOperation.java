package hr.fer.zemris.java.gui.calc.operations;

public class LnOperation implements SingleArgumentOperation {

	@Override
	public double doOperation(double number, boolean inverted) {
		double result;
		if (inverted) {
			result = Math.pow(Math.E, number);
		} else {
			result = Math.log(number);
		}
		return result;
	}

}
