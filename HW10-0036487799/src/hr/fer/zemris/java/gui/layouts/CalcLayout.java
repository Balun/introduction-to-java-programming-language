package hr.fer.zemris.java.gui.layouts;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager2;

/**
 * The Class CalcLayout represents layout in java swing. Determines where
 * components should be placed. Best use of this layout is for calculator. It is
 * possible to add elements in specified position. Position is specified as
 * instance of RCPosition or String in form "r,c" with index.
 * 
 * @author Josip
 */
public class CalcLayout implements LayoutManager2 {

	/** Maximal row. */
	public static final int MAX_ROW = 5;

	/** Maximal column. */
	public static final int MAX_COLUMNS = 7;

	/** The number of added elements in each row. */
	public int numberOfElementsInRow[] = new int[MAX_ROW];

	/** The number of elements in each column. */
	public int numberOfElementsInColumn[] = new int[MAX_COLUMNS];

	/** Gap between each added component. */
	private int gap;

	/** Added components. */
	Component components[] = new Component[MAX_ROW * MAX_COLUMNS];

	/**
	 * Instantiates a new calc layout with specified gap.
	 *
	 * @param spacesBetweenRowsAndColumns
	 *            Spaces between rows and columns.
	 */
	public CalcLayout(int spacesBetweenRowsAndColumns) {
		this.gap = spacesBetweenRowsAndColumns;
	}

	/**
	 * Instantiates a new calc layout with ne initial gap.
	 */
	public CalcLayout() {
		this(0);
	}

	@Override
	public void addLayoutComponent(String name, Component comp) {
		throw new UnsupportedOperationException("Operation unsupported.");
	}

	@Override
	public void removeLayoutComponent(Component comp) {
	}

	@Override
	public Dimension preferredLayoutSize(Container parent) {
		Dimension preferredDimension = new Dimension();
		Insets insets = parent.getInsets();
		double height = 0;
		double width = 0;

		for (Component c : components) {
			if (c == null) {
				continue;
			}
			height = c.getPreferredSize().getHeight() > height ? c.getPreferredSize().getHeight() : height;
		}

		for (int i = 1; i < components.length; i++) {
			if (components[i] == null) {
				continue;
			}
			width = components[i].getPreferredSize().getWidth() > width ? components[i].getPreferredSize().getWidth()
					: width;
		}

		preferredDimension.setSize(width * MAX_COLUMNS + insets.right + insets.right + (MAX_COLUMNS - 1) * gap,
				height * MAX_ROW + insets.bottom + insets.top + (MAX_ROW - 1) * gap);
		return preferredDimension;
	}

	@Override
	public Dimension minimumLayoutSize(Container parent) {
		Insets insets = parent.getInsets();
		Dimension minimumLayoutDimension = new Dimension();
		double width = 0;
		double height = 0;

		if (components.length != 0) {
			width = Integer.MAX_VALUE;
			height = Integer.MAX_VALUE;
		}

		for (Component c : components) {
			if (c == null) {
				continue;
			}
			if (c.getMinimumSize() != null) {
				width = width < c.getMinimumSize().getWidth() ? width : c.getMinimumSize().getWidth();
				height = height < c.getMinimumSize().getHeight() ? height : c.getMinimumSize().getHeight();
			}
		}

		minimumLayoutDimension.setSize(width * MAX_COLUMNS + insets.right + insets.right + (MAX_COLUMNS - 1) * gap,
				height * MAX_ROW + insets.bottom + insets.top + (MAX_ROW - 1) * gap);
		return minimumLayoutDimension;
	}

	@Override
	public void layoutContainer(Container parent) {
		Insets insets = parent.getInsets();
		int ncomponents = parent.getComponentCount();

		if (ncomponents == 0) {
			return;
		}

		double widthRatio = ((double) parent.getWidth() - (MAX_COLUMNS - 1) * gap - insets.left - insets.right)
				/ (this.minimumLayoutSize(parent).width * MAX_COLUMNS);
		double heightRatio = ((double) parent.getHeight() - (MAX_ROW - 1) * gap - insets.top - insets.bottom)
				/ (this.minimumLayoutSize(parent).height * MAX_ROW);

		int widthOnComponent = (int) (this.minimumLayoutSize(parent).width * widthRatio);
		int heightOnComponent = (int) ((double) this.minimumLayoutSize(parent).height * heightRatio);

		int offsetForFirstRow = 0;
		// for first row
		final int numberOfElementsInFirstRow = 3;
		for (int c = 0, x = insets.left; c < numberOfElementsInFirstRow; c++, x += widthOnComponent + gap) {
			int i = c;

			try {
				parent.getComponent(i + offsetForFirstRow);
			} catch (ArrayIndexOutOfBoundsException exc) {

				continue;
			}

			if (i == 0) {
				if (components[i] != null) {
					parent.getComponent(i + offsetForFirstRow).setBounds(x, insets.top, widthOnComponent * 5 + 4 * gap,
							heightOnComponent);
				} else {
					offsetForFirstRow--;
				}

				x += (widthOnComponent + gap) * 4;
				continue;
			} else {

				parent.getComponent(i + offsetForFirstRow).setBounds(x, insets.top, widthOnComponent,
						heightOnComponent);

			}

		}
		// ensures that layout will work properly if there is less elements in
		// first row
		int offset = 2;
		for (int i = 0; i < 3; i++) {
			offset += components[i] == null ? -1 : 0;
		}
		// counts element from second row and so on
		int counter = 3;
		for (int r = 0, y = insets.top; r < MAX_ROW; r++, y += heightOnComponent + gap) {
			if (r == 0) {
				continue;
			}
			for (int c = 0, x = insets.left; c < MAX_COLUMNS; c++, x += widthOnComponent + gap) {
				int i = r * MAX_COLUMNS + c;
				try {
					if (components[i] == null) {
						continue;
					} else {
						counter++;
						parent.getComponent(counter - 1 + offset).setBounds(x, y, widthOnComponent, heightOnComponent);
					}
				} catch (ArrayIndexOutOfBoundsException exc) {
					continue;
				}

			}
		}

	}

	@Override
	public void addLayoutComponent(Component comp, Object constraints) {
		int row = 0;
		int column = 0;

		if (constraints == null) {
			throw new IllegalArgumentException("Constraints expected.");
		}

		if (constraints.getClass().equals(RCPosition.class)) {
			row = ((RCPosition) constraints).getRow();
			column = ((RCPosition) constraints).getColumn();
		} else if (constraints.getClass().equals(String.class)) {
			String indexes = (String) constraints;
			if (indexes.matches("[1-5],[1-7]")) {
				row = Integer.parseInt(indexes.charAt(0) + "");
				column = Integer.parseInt(indexes.charAt(2) + "");
			} else {
				throw new IllegalArgumentException(
						"Invalid string constraints : \"" + constraints + "\" given. Should be like \"2,4\"");
			}
		} else {
			throw new IllegalArgumentException("Invalid constraints given.");
		}

		if ((row - 1) == 0 && (column - 1) > 0 && (column - 1) < 5) {
			throw new IllegalArgumentException("( " + row + "," + column + " ) not valid (row,column) index.");
		}

		if (row > 5 || row < 1 || column > 7 || column < 1) {
			throw new IllegalArgumentException(
					"Indexed row and column : (" + row + "," + column + ") out of bounds. Valid are ([1-5],[1-7]).");
		}

		if (components[MAX_COLUMNS * (row - 1) + (column - 1)] != null) {
			throw new IllegalArgumentException("Component at ( " + row + "," + column + " ) already exist.");
		}
		components[MAX_COLUMNS * (row - 1) + column - 1] = comp;
	}

	@Override
	public Dimension maximumLayoutSize(Container target) {
		Insets insets = target.getInsets();
		Dimension maximumLayoutDimension = new Dimension();
		double width = 0;
		double height = 0;

		for (Component c : components) {
			if (c == null) {
				continue;
			}
			height = c.getMaximumSize().getHeight() > height ? c.getMaximumSize().getHeight() : height;
		}
		for (int i = 1; i < components.length; i++) {
			if (components[i] == null) {
				continue;
			}
			width = components[i].getMaximumSize().getWidth() > width ? components[i].getMaximumSize().getWidth()
					: width;
		}

		maximumLayoutDimension.setSize(width * MAX_COLUMNS + insets.right + insets.right + (MAX_COLUMNS - 1) * gap,
				height * MAX_ROW + insets.bottom + insets.top + (MAX_ROW - 1) * gap);
		return maximumLayoutDimension;
	}

	@Override
	public float getLayoutAlignmentX(Container target) {
		return 0;
	}

	@Override
	public float getLayoutAlignmentY(Container target) {
		return 0;
	}

	@Override
	public void invalidateLayout(Container target) {

	}

}
