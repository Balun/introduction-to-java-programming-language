package hr.fer.zemris.java.gui.layouts;

/**
 * The Class RCPosition represents position in CalcLayout grid with defined
 * values of row and column.
 * 
 * @author Josip
 */
public class RCPosition {

	/** The row. */
	private int row;

	/** The column. */
	private int column;

	/**
	 * Instantiates a new RC position with given parameters.
	 *
	 * @param row
	 *            Given row.
	 * @param column
	 *            Given column.
	 */
	public RCPosition(int row, int column) {
		this.row = row;
		this.column = column;
	}

	/**
	 * Gets defined row.
	 *
	 * @return Row.
	 */
	public int getRow() {
		return row;
	}

	/**
	 * Gets defined column.
	 *
	 * @return Column.
	 */
	public int getColumn() {
		return column;
	}

}
