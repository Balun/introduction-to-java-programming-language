package hr.fer.zemris.java.gui.prim;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

public class PrimDemo extends JFrame {

	public PrimDemo() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setTitle("PrimeDemo");
		setLocation(20, 20);
		setSize(500, 300);
		initGUI();
	}

	private void initGUI() {
		Container cp = getContentPane();
		cp.setLayout(new BorderLayout());
		JButton next = new JButton("sljedeći");
		JPanel northPanel = new JPanel(new GridLayout(1, 2));

		PrimListModel model = new PrimListModel();
		JList<Integer> list1 = new JList<>(model);
		JList<Integer> list2 = new JList<>(model);
		JScrollPane scrollableList1 = new JScrollPane(list1);
		JScrollPane scrollableList2 = new JScrollPane(list2);

		northPanel.add(scrollableList1);
		northPanel.add(scrollableList2);
		cp.add(next, BorderLayout.SOUTH);
		cp.add(northPanel, BorderLayout.CENTER);

		next.addActionListener(e -> {
			model.next();
		});
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> {
			new PrimDemo().setVisible(true);
		});
	}
}
