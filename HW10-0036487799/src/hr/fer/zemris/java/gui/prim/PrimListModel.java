package hr.fer.zemris.java.gui.prim;

import java.util.ArrayList;
import java.util.List;

import javax.swing.ListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

public class PrimListModel implements ListModel<Integer> {

	private ArrayList<Integer> list;
	private List<ListDataListener> listeners = new ArrayList<>();
	private Integer currentPrime;

	public PrimListModel() {
		list = new ArrayList<>();
		currentPrime = 1;
		this.list.add(currentPrime);
		ListDataEvent event = new ListDataEvent(this, ListDataEvent.INTERVAL_ADDED, list.size(), list.size());
		for (ListDataListener l : listeners) {
			l.intervalAdded(event);
		}
	}

	@Override
	public int getSize() {
		return list.size();
	}

	public void next() {
		currentPrime = nextPrime(currentPrime);
		this.list.add(currentPrime);
		ListDataEvent event = new ListDataEvent(this, ListDataEvent.INTERVAL_ADDED, list.size(), list.size());
		for (ListDataListener l : listeners) {
			l.intervalAdded(event);
		}
	}

	private Integer nextPrime(Integer currentPrime) {
		int j, count;
		for (int i = currentPrime + 1;; i++) {
			for (j = 2, count = 0; j <= i; j++) {
				if (i % j == 0) {
					count++;
				}
			}
			if (count == 1) {
				return i;
			}
		}
	}

	@Override
	public Integer getElementAt(int index) {

		return list.get(index);
	}

	@Override
	public void addListDataListener(ListDataListener l) {
		listeners = new ArrayList<>(listeners);
		listeners.add(l);
	}

	@Override
	public void removeListDataListener(ListDataListener l) {
		listeners = new ArrayList<>(listeners);
		listeners.remove(l);
	}

}
