package hr.fer.zemris.java.gui.charts;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

public class BarChartDemo extends JFrame {
	private static XYValue[] values;
	private static String descriptionXAxis;
	private static String descriptionYAxis;
	private static Integer minimumY;
	private static Integer maximumY;
	private static Integer step;
	private static Path filePath;

	public BarChartDemo() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setTitle("Bar Chart");
		setLocation(20, 20);
		setSize(500, 200);
		initGUI();
	}

	private void initGUI() {
		Container cp = getContentPane();
		cp.setLayout(new BorderLayout());
		BarChart bcData = new BarChart(values, descriptionXAxis, descriptionYAxis, minimumY, maximumY, step);
		BarChartComponent chart = new BarChartComponent(bcData);

		chart.setOpaque(true);
		chart.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, 2));

		chart.setBackground(Color.WHITE);
		cp.add(chart, BorderLayout.CENTER);

		JLabel pathLabel = new JLabel("path to file...", SwingConstants.CENTER);
		pathLabel.setText(filePath.toAbsolutePath().toString());
		pathLabel.setOpaque(true);
		pathLabel.setBackground(Color.WHITE);
		cp.add(pathLabel, BorderLayout.NORTH);
	}

	public static void main(String[] args) {
		if (args.length != 1) {
			throw new IllegalArgumentException("Expected single path to file with chart description.");
		}
		try (BufferedReader br = new BufferedReader(new FileReader(args[0]));) {
			filePath = Paths.get(args[0]);
			descriptionXAxis = br.readLine();
			if (descriptionXAxis == null) {
				throw new IllegalArgumentException("Description of x-axis can not be null.");
			}
			descriptionYAxis = br.readLine();
			if (descriptionYAxis == null) {
				throw new IllegalArgumentException("Description of y-axis can not be null.");
			}
			String xyValues[] = br.readLine().trim().split(" ");
			List<XYValue> xyValuesList = new ArrayList<>();
			for (String value : xyValues) {
				if (!value.matches("[0-9,,]+")) {
					throw new IllegalArgumentException("Can not read x-y value " + value);
				}
				int x = Integer.parseInt(String.valueOf(value.substring(0, value.indexOf(","))));
				int y = Integer.parseInt(String.valueOf(value.substring(value.indexOf(",") + 1)));
				xyValuesList.add(new XYValue(x, y));
			}
			values = xyValuesList.toArray(new XYValue[0]);
			minimumY = Integer.parseInt(br.readLine());
			maximumY = Integer.parseInt(br.readLine());
			step = Integer.parseInt(br.readLine());

		} catch (FileNotFoundException e) {
			throw new IllegalArgumentException("File \"" + args[0] + "\" doesn't exist.");

		} catch (Exception e) {
			System.err.println("Invalid decription of chart given.");
			System.exit(0);
		}

		SwingUtilities.invokeLater(() -> {
			new BarChartDemo().setVisible(true);
		});
	}
}
