package hr.fer.zemris.java.gui.charts;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.HeadlessException;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;

import javax.swing.JComponent;

public class BarChartComponent extends JComponent {

	public static final int CONSTANT_SPACE = 5;
	private BarChart chartData;

	public BarChartComponent(BarChart chartData) throws HeadlessException {
		super();
		this.chartData = chartData;
	}

	// dodat strelice,ako se mora
	@Override
	protected void paintComponent(Graphics g) {
		Insets insets = getInsets();
		Dimension dim = getSize();
		int width = (int) dim.getWidth() - insets.left - insets.right;
		int height = (int) dim.getHeight() - insets.top - insets.bottom;
		// this.setSize(width, height);

		Rectangle r = new Rectangle(insets.left, insets.top, dim.width - insets.left - insets.right,
				dim.height - insets.top - insets.bottom);

		if (isOpaque()) {
			g.setColor(getBackground());
			g.fillRect(r.x, r.y, r.width, r.height);
		}
		// treba pocetne, tamnije crte nacrtati
		g.setColor(Color.BLACK);
		FontMetrics fm = g.getFontMetrics();
		// X axis description
		g.drawString(chartData.getDescriptionXAxis(), width / 2 - fm.stringWidth(chartData.getDescriptionXAxis()) / 2,
				height);

		int numberOfHorizontalLines = (chartData.getMaximumY() - chartData.getMinimumY()) / chartData.getStep() + 1;
		int spaceFromBottom = CONSTANT_SPACE * 2 + fm.getAscent() * 2;
		int spaceFromLeft = CONSTANT_SPACE * 2 + fm.getAscent() + fm.stringWidth(chartData.getMaximumY() + " ");
		int stepLength = (height - spaceFromBottom - CONSTANT_SPACE) / numberOfHorizontalLines;
		int horizontalLineEndOnX = width - spaceFromLeft - CONSTANT_SPACE;
		int lengthOfVerticalLine = insets.bottom - CONSTANT_SPACE * 3 + height - fm.getAscent() * 2;
		int step = chartData.getStep();

		// Y axis description
		Graphics2D g2d = (Graphics2D) g;
		// Graphics2D gg = g2d.create(0, 0, width, height);
		AffineTransform defaultAt = g2d.getTransform();
		// rotates the coordinate by 90 degree counterclockwise
		AffineTransform at = new AffineTransform();
		at.rotate(-Math.PI / 2);
		g2d.setTransform(at);

		int positionYAxisDescription = -lengthOfVerticalLine / 2 - (CONSTANT_SPACE * 2 + stepLength)
				- fm.stringWidth(chartData.getDescriptionYAxis()) / 2;

		g2d.drawString(chartData.getDescriptionYAxis(), positionYAxisDescription, CONSTANT_SPACE * 3);
		g2d.setTransform(defaultAt);

		g.setFont(g.getFont().deriveFont(Font.BOLD));
		// horizontal lines
		int beginVerticalLine = 0;
		for (int i = 0, y = insets.bottom - CONSTANT_SPACE * 3 + height - fm.getAscent() * 2, yIndex = chartData
				.getMinimumY(); i < numberOfHorizontalLines; i++, y -= stepLength, yIndex += step) {

			g.setColor(Color.BLACK);
			String YindexString = String.valueOf(yIndex);
			g.drawString(YindexString, spaceFromLeft - CONSTANT_SPACE - fm.stringWidth(YindexString),
					y + CONSTANT_SPACE);

			g.setColor(Color.LIGHT_GRAY);
			g.drawLine(spaceFromLeft, y, horizontalLineEndOnX, y);
			beginVerticalLine = y;
		}

		int numberOfVerticalLines = chartData.getValue().length;
		int widthOfColumn = (horizontalLineEndOnX - spaceFromLeft) / numberOfVerticalLines;

		g.drawLine(spaceFromLeft, beginVerticalLine, spaceFromLeft, lengthOfVerticalLine);
		// vertical lines
		for (int i = 0, x = spaceFromLeft + widthOfColumn; i < numberOfVerticalLines; i++, x += widthOfColumn) {
			g.setColor(Color.BLACK);
			String XindexString = String.valueOf(chartData.getValue()[i].getX());
			g.drawString(XindexString, x - widthOfColumn / 2, lengthOfVerticalLine + CONSTANT_SPACE * 3);
			g.setColor(Color.LIGHT_GRAY);
			g.drawLine(x, beginVerticalLine, x, lengthOfVerticalLine);
		}

		Color lightRed = new Color(240, 120, 70);
		g.setColor(lightRed);
		// filling columns
		for (int i = 0, x = spaceFromLeft; i < numberOfVerticalLines; i++, x += widthOfColumn) {
			g.fillRect(x, lengthOfVerticalLine, widthOfColumn - CONSTANT_SPACE,
					-(chartData.getValue()[i].getY() - chartData.getMinimumY()) * stepLength / step);
		}
	}
}
