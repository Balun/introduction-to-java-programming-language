package hr.fer.zemris.java.gui.charts;

public class BarChart {

	private XYValue value[];
	private String descriptionXAxis;
	private String descriptionYAxis;
	private int minimumY;
	private int maximumY;
	private int step;

	public BarChart(XYValue value[], String descriptionXAxis, String descriptionYAxis, int minimumY, int maximumY,
			int step) {
		this.value = value;
		this.descriptionXAxis = descriptionXAxis;
		this.descriptionYAxis = descriptionYAxis;
		this.minimumY = minimumY;
		this.maximumY = maximumY;
		this.step = step;
	}

	/**
	 * @return the value
	 */
	public XYValue[] getValue() {
		return value;
	}

	/**
	 * @return the descriptionXAxis
	 */
	public String getDescriptionXAxis() {
		return descriptionXAxis;
	}

	/**
	 * @return the descriptionYAxis
	 */
	public String getDescriptionYAxis() {
		return descriptionYAxis;
	}

	/**
	 * @return the minimumY
	 */
	public int getMinimumY() {
		return minimumY;
	}

	/**
	 * @return the maximumY
	 */
	public int getMaximumY() {
		return maximumY;
	}

	/**
	 * @return the step
	 */
	public int getStep() {
		return step;
	}

}
