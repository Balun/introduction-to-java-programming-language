package hr.fer.zemris.java.tecaj.hw1;

	
	class Quad{
		int a = 5, b = 2, product = 1;
		
		public Quad() {
			a=a++;
			b=b+1;
		}
	}
	
	class Rhombus extends Quad{
		public Rhombus() {
			super();
			product = a*b;
			System.out.println(product);
		}
	}
	
	class Trapezium extends Quad{
		public Trapezium() {
			a = a+1;
			b=++b - 1;
			product = a*b;
			System.out.println(product);
		}
	}
	
class Main {
	
	public static void main(String[] args) {
		Quad q1 = new Rhombus();
		Quad q2 = new Trapezium();
	}

}
