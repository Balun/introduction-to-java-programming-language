package hr.fer.zemris.java.tecaj.hw1;

/**
 * This program demonstrates the decomposition of a specified number into prime
 * factors. The user provides one command line argument - a natural number
 * greater than 1.
 * 
 * @author Matej Balun
 *
 */
public class NumberDecomposition {

	/**
	 * The main method that executes the number decomposition program.
	 * 
	 * @param args
	 *            - A natural number greater than 1 for decomposition onto prime
	 *            factors
	 */
	public static void main(String[] args) {
		if (args.length != 1 || Integer.parseInt(args[0]) <= 1) {
			System.err.println("Invalid number of arguments or invalid argument was provided.");
			return;
		}

		int n = Integer.parseInt(args[0]);
		System.out.println("You requested decomposition of number " + n + " onto prime factors. Here they are:");

		int j = 1;
		for (int i = 2; n != 1;) {
			if (n % i == 0) {
				n /= i;
				System.out.printf("%d. %d\n", j++, i);
			} else
				i++;
		}
	}

}
