package hr.fer.zemris.java.tecaj.hw1;

/**
 * This program creates an empty linked list with nodes described in class
 * CvorListe. The demonstration puts some elements in the list and calls the
 * methods for writing the list on standard output, sorting the list, getting
 * the length of the list and insertion of a new element in the list.
 * 
 * @author Matej Balun
 *
 */
public class ProgramListe {

	/**
	 * The structure of a node in the linked list. Consists of a string that
	 * represents data and a reference to the next node in the list.
	 * 
	 * @author Lumba
	 *
	 */
	static class CvorListe {
		CvorListe sljedeci;
		String podatak;
	}

	/**
	 * The main method that executes the linked list demonstration program.
	 */
	public static void main(String[] args) {
		CvorListe cvor = null;

		cvor = ubaci(cvor, "Jasna");
		cvor = ubaci(cvor, "Ana");
		cvor = ubaci(cvor, "Ivana");

		System.out.println("Ispisujem listu uz originalni poredak:");
		ispisiListu(cvor);

		cvor = sortirajListu(cvor);
		System.out.println("Ispisujem listu nakon sortiranja:");
		ispisiListu(cvor);

		int vel = velicinaListe(cvor);
		System.out.println("Lista sadrzi elemenata: " + vel);
	}

	/**
	 * This method return the size of the linked list.
	 * 
	 * @param cvor
	 *            - A reference to the first node of the list
	 * @return - size of the list.
	 */
	private static int velicinaListe(CvorListe cvor) {
		int length = 0;

		for (CvorListe tmp = cvor; tmp != null; tmp = tmp.sljedeci) {
			length++;
		}

		return length;
	}

	/**
	 * The method for insertion a new element in the list.
	 * 
	 * @param prvi
	 *            - a reference to the first node of the list.
	 * @param podatak
	 *            - the data string of the new node.
	 * @return - a reference to the first node of the list (after insertion).
	 */
	private static CvorListe ubaci(CvorListe prvi, String podatak) {
		CvorListe novi = new CvorListe();
		novi.podatak = podatak;
		novi.sljedeci = null;

		if (prvi == null) {
			prvi = novi;
			return prvi;
		}

		CvorListe tmp = prvi;

		while (tmp.sljedeci != null) {
			tmp = tmp.sljedeci;
		}

		tmp.sljedeci = novi;

		return prvi;
	}

	/**
	 * This method writes the content of the list to the standard output.
	 * 
	 * @param cvor
	 *            - A reference to the first node in the list.
	 */
	private static void ispisiListu(CvorListe cvor) {
		for (CvorListe tmp = cvor; tmp != null; tmp = tmp.sljedeci) {
			System.out.println(tmp.podatak);
		}
	}

	/**
	 * This method sorts the list data in the natural order. The algorithm used
	 * is Bubble sort.
	 * 
	 * @param cvor
	 *            - A reference to the first node in the list.
	 * @return - A reference to the first node in the list (after sorting).
	 */
	private static CvorListe sortirajListu(CvorListe cvor) {
		if (velicinaListe(cvor) < 2) {
			return cvor;
		}

		boolean sorted;

		do {
			sorted = true;
			for (CvorListe tmp = cvor; tmp != null; tmp = tmp.sljedeci) {
				if (tmp.sljedeci != null && tmp.podatak.compareTo(tmp.sljedeci.podatak) > 0) {
					String pom = tmp.podatak;
					tmp.podatak = tmp.sljedeci.podatak;
					tmp.sljedeci.podatak = pom;
					sorted = false;
				}
			}
		} while (!sorted);

		return cvor;
	}

}
