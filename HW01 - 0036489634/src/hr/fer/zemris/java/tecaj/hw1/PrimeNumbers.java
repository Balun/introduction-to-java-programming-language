package hr.fer.zemris.java.tecaj.hw1;

/**
 * This program calculates the first n prime numbers. The user provides the
 * positive integer value for the calculation. The program lists the first n
 * prime numbers on the standard output. It's considered that the number 2 is
 * the first prime number.
 * 
 * @author Matej Balun
 *
 */
public class PrimeNumbers {

	/**
	 * The main method that executes the prime numbers calculation program.
	 * 
	 * @param args
	 *            - A positive integer value for the number of prime numbers.
	 */
	public static void main(String[] args) {
		if (args.length != 1 || Integer.parseInt(args[0]) < 1) {
			System.err.println("Invalid number of arguments or invalid argument was provided.");
			return;
		}

		int n = Integer.parseInt(args[0]);
		System.out.println("you requested calculation of " + n + " prime numbers. Here they are:");
		int i = 1;

		System.out.println(i + ". 2"); // Ako je argument tocno unesen dvojka ce
										// se sigurno ispisati
		n--;
		for (int j = 3; n > 0; j += 2) {
			if (isPrime(j)) {
				i++;
				n--;
				System.out.println(i + ". " + j);
			}
		}
	}

	/**
	 * This method calculates whether the specified number is prime or not.
	 * 
	 * @param n
	 *            - The specified number.
	 * @return - True if the number is prime, otherwise false.
	 */
	private static boolean isPrime(int n) {
		if (n % 2 == 0)
			return false;

		double k = Math.sqrt(n);
		for (int i = 3; i <= k; i += 2) {
			if (n % i == 0)
				return false;
		}

		return true;
	}

}
