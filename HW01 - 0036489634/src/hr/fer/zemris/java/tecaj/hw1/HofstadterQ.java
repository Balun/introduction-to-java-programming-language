package hr.fer.zemris.java.tecaj.hw1;

/**
 * This program demonstrates the Hofstadter Q-sequence. The user must provide a
 * positive integer value via command line. The program calculates the element
 * of the Hofstadter Q-sequence on the specified index.
 * 
 * @author Matej Balun
 *
 */
public class HofstadterQ {

	/**
	 * The main method that executes the Hofstadter Q-sequence demonstration
	 * program.
	 * 
	 * @param args
	 *            - positive integer value for index of the element in the
	 *            sequence.
	 */
	public static void main(String[] args) {
		if (args.length != 1) {
			System.out.println("Invalid number of arguments.");
			return;
		}

		int index = Integer.parseInt(args[0].trim());
		if (index < 1) {
			System.out.println("The argument must be positive.");
			return;
		}

		System.out.printf("You requested the calculation of %d. number of Hofstadter's Q-sequence."
				+ "The requested number is %d.\n", index, hofstadterQ(index));
	}

	/**
	 * This method calculates the value of Hofstadter Q-sequence of the
	 * specified index.
	 * 
	 * @param index
	 *            - index of the element in the sequence.
	 * @return - value of the element in the sequence with the specified index.
	 */
	private static long hofstadterQ(long index) {
		if (index <= 2) {
			return 1;
		}

		return hofstadterQ(index - hofstadterQ(index - 1)) + hofstadterQ(index - hofstadterQ(index - 2));
	}

}
