package hr.fer.zemris.java.tecaj.hw1;

/**
 * This program calculates n roots of a complex number. The user provides three
 * arguments via command line: real part, imaginary part and required root to
 * calculate. The program prints the roots on the standard output.
 * 
 * @author Matej Balun
 *
 */
public class Roots {

	/**
	 * The main method that executes the complex number roots calculation
	 * program.
	 * 
	 * @param args
	 *            - real part, imaginary part and required root to calculate.
	 */
	public static void main(String[] args) {
		double a = Double.parseDouble(args[0]); // z = a + b*i
		double b = Double.parseDouble(args[1]);
		double n = Double.parseDouble(args[2]);

		System.out.printf("You requested calculation of %.0f. roots. Solutions are:\n", n);

		double radius = Math.sqrt(a * a + b * b);
		double angle = Math.atan(b / a);

		for (int i = 0; i < n; i++) {
			a = Math.pow(radius, 1 / n) * Math.cos((angle + 2 * i * Math.PI) / n);
			b = Math.pow(radius, 1 / n) * Math.sin((angle + 2 * i * Math.PI) / n);
			if (b < 0) {
				System.out.printf("%d) %d  - %di\n", i + 1, Math.round(a), -Math.round(b));
			} else {
				System.out.printf("%d) %d  - %di\n", i + 1, Math.round(a), Math.round(b));
			}
		}

	}

}
