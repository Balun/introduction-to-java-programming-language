package hr.fer.zemris.java.tecaj.hw1;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * This program calculates the area of the specified rectangle with accuracy of
 * one decimal. The parameters can be read from command prompt when running the
 * program, or from standard input during the execution. The width and height
 * must not be negative. The user can type either both two arguments in command
 * prompt or none. Anything else will be treated as an error.
 * 
 * @author Matej Balun
 *
 */
public class Rectangle {

	/**
	 * The method that executes the rectangle calculation program.
	 * 
	 * @param args
	 *            - optional arguments for width and height.
	 */
	public static void main(String[] args) {
		double width;
		double height;
		BufferedReader reader = new BufferedReader(new InputStreamReader(new BufferedInputStream(System.in)));

		switch (args.length) {

		case 0:
			do {
				System.out.println("please provide width: ");
				width = readFromStandardInput(reader, "width");
			} while (width < 0);

			do {
				System.out.println("please provide height: ");
				height = readFromStandardInput(reader, "height");
			} while (height < 0);

			System.out.printf(
					"You have specified a rectangle with width %.1f and height %.1f. "
							+ "Its area is %.1f and its perimiter is %.1f.\n",
					width, height, width * height, 2 * (width + height));
			break;

		case 2:
			width = Double.parseDouble(args[0].trim());
			height = Double.parseDouble(args[1].trim());

			if (width < 0 && height < 0) {
				System.out.println("The width and height must not be negative.");
			} else if (height < 0) {
				System.out.println("The height must not be negative.");
			} else if (width < 0) {
				System.out.println("The width must not be negative.");
			} else {
				System.out.printf(
						"You have specified a rectangle with width %.1f and height %.1f. "
								+ "Its area is %.1f and its perimiter is %.1f.\n",
						width, height, width * height, 2 * (width + height));
			}
			break;

		default:
			System.out.println("Invalid number of arguments was provided.");
			break;
		}
	}

	/**
	 * The method that reads the specified argument from the standard input and
	 * converts it into a double value if the conversion is possible. If the
	 * arguments from standard input is blank, the method returns a negative
	 * value.
	 * 
	 * @param reader
	 *            - Buffered reader for reading from the standard input.
	 * @param message
	 *            - The name of the specified argument ("width" or "height").
	 * @return - decimal value of the specified argument.
	 */
	private static double readFromStandardInput(BufferedReader reader, String message) {
		double value;
		String line = null;

		try {
			line = reader.readLine().trim();
		} catch (IOException e) {
		}

		if (line.isEmpty()) {
			System.out.println("Nothing was given.");
			return -1;
		}

		value = Double.parseDouble(line);
		if (value < 0) {
			System.out.println(message + " is negative");
		}

		return value;
	}

}
