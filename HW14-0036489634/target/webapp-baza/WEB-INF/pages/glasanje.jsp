<%@page import="hr.fer.zemris.java.HW14.model.Option"%>
<%@page import="hr.fer.zemris.java.HW14.model.Poll"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Enumeration"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<body>
	<%
		Poll poll = (Poll) request.getAttribute("poll");
		List<Option> options = (List<Option>) request.getAttribute("options");
	%>
	<h1><%=poll.getTitle()%></h1>
	<p>
		<%=poll.getMessage()%></p>
	<ol>
		<%
			for (Option option : options) {
		%><li><a href="glasanje-glasaj?id=<%=option.getId()%>"><%=option.getOptionTitle()%></a></li>
		<%
			}
		%>
	</ol>
</body>
</html>