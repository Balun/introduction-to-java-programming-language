package hr.fer.zemris.java.HW14.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.HW14.dao.DAOProvider;

/**
 * ovaj razred predstavlja servlet za inskrementiranje specificirane anketne
 * opcije čiji je id dobiven preko parametara. Dalje proslijeđuje posao na
 * prikaz rezultata glasanja dotične ankete.
 * 
 * @author matej Balun
 *
 */
@WebServlet(name = "glasanjeRedirect", urlPatterns = { "/glasanje-glasaj" })
public class GlasanjeRedirectServlet extends HttpServlet {

	/**
	 * UID za ovaj servlet
	 */
	private static final long serialVersionUID = 1438417749430122820L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		long id = Long.parseLong(req.getParameter("id"));
		DAOProvider.getDao().incrementOptionVotes(id);

		resp.sendRedirect(req.getContextPath() + "/glasanje-rezultati");
	}

}
