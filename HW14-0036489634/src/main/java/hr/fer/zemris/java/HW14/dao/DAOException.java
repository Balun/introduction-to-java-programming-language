package hr.fer.zemris.java.HW14.dao;

/**
 * Iznimka koja se baca u slučaju općenitih pogrešaka u dohvatu podataka iz baze
 * podataka.
 * 
 * @author Matej Balun
 *
 */
public class DAOException extends RuntimeException {

	/**
	 * UID za ovaj razred.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Default konstruktor.
	 */
	public DAOException() {
	}

	@SuppressWarnings("javadoc")
	public DAOException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	@SuppressWarnings("javadoc")
	public DAOException(String message, Throwable cause) {
		super(message, cause);
	}

	@SuppressWarnings("javadoc")
	public DAOException(String message) {
		super(message);
	}

	@SuppressWarnings("javadoc")
	public DAOException(Throwable cause) {
		super(cause);
	}
}
