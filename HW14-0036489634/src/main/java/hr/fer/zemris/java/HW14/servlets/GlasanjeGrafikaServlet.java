package hr.fer.zemris.java.HW14.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import org.jfree.util.Rotation;

import hr.fer.zemris.java.HW14.dao.DAOProvider;
import hr.fer.zemris.java.HW14.model.Option;

/**
 * This class represents a simple servlet for displaying graphical results of
 * the music voting in a pie-chart format. It extends the {@link HttpServlet}
 * class.
 * 
 * @author Matej Balun
 *
 */
@WebServlet(name = "glasanjeGrafika", urlPatterns = { "/glasanje-grafika" })
public class GlasanjeGrafikaServlet extends HttpServlet {

	/**
	 * The serial version UID for this class.
	 */
	private static final long serialVersionUID = 7769545304191066178L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		long pollID = (Long) req.getSession().getAttribute("pollID");
		List<Option> options = DAOProvider.getDao().getOptionsList(pollID);

		PieDataset dataset = createDataset(options);
		JFreeChart chart = createChart(dataset, "Rezultati glasanja");
		byte[] bi = ChartUtilities.encodeAsPNG(chart.createBufferedImage(500, 500));
		resp.setContentType("image/png");
		resp.getOutputStream().write(bi);
	}

	/**
	 * THis method creates the data set for the music chart.
	 * 
	 * @param options
	 *            list of options for pie chart
	 * 
	 * @return {@link PieDataset} representation of the vote data
	 */
	private PieDataset createDataset(List<Option> options) {
		DefaultPieDataset result = new DefaultPieDataset();

		for (Option option : options) {
			result.setValue(option.getOptionTitle(), option.getVotesCount());
		}
		return result;

	}

	/**
	 * This method creates a simple pie char based on the {@link PieDataset}
	 * object.
	 * 
	 * @param dataset
	 *            the specified {@link PieDataset} for the chart
	 * @param title
	 *            title of the chart
	 * @return new instance of {@link JFreeChart}
	 */
	private JFreeChart createChart(PieDataset dataset, String title) {

		JFreeChart chart = ChartFactory.createPieChart3D(title, // chart title
				dataset, // data
				true, // include legend
				true, false);

		PiePlot3D plot = (PiePlot3D) chart.getPlot();
		plot.setStartAngle(290);
		plot.setDirection(Rotation.CLOCKWISE);
		plot.setForegroundAlpha(0.5f);
		return chart;
	}

}
