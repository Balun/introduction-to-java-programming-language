package hr.fer.zemris.java.HW14.model;

/**
 * Ovaj razred opisuje model podataka vezan za anketu u bazi podataka. Javno
 * sučelje i atributi razreda odgovaraju tablici u bazi podataka pod nazivom
 * "Polls".
 * 
 * @author Matej Balun
 *
 */
public class Poll {

	/**
	 * ID ankete.
	 */
	private long id;

	/**
	 * NAziv ankete.
	 */
	private String title;

	/**
	 * Poruka ankete.
	 */
	private String message;

	/**
	 * Ovaj konstruktor inicijalizira razred {@link Poll} sa njegovim
	 * atributima.
	 * 
	 * @param id
	 *            id ankete
	 * @param title
	 *            naslov ankete
	 * @param message
	 *            poruka ankete
	 */
	public Poll(long id, String title, String message) {
		super();
		this.id = id;
		this.message = message;
		this.title = title;
	}

	/**
	 * Vraća id ankete.
	 * 
	 * @return id ankete
	 */
	public long getId() {
		return id;
	}

	/**
	 * Vraća poruku ankete.
	 * 
	 * @return poruka ankete
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Vraća naslov ankete.
	 * 
	 * @return naslov ankete
	 */
	public String getTitle() {
		return title;
	}

}
