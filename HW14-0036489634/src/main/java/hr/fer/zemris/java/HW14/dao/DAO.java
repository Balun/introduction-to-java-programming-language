package hr.fer.zemris.java.HW14.dao;

import java.util.List;

import hr.fer.zemris.java.HW14.model.Poll;
import hr.fer.zemris.java.HW14.model.Option;

/**
 * Sučelje prema podsustavu za perzistenciju podataka.
 * 
 * @author Matej Balun
 *
 */
public interface DAO {

	/**
	 * Dohvaća sve postojeće unose u bazi specifične ankete.
	 * 
	 * @param pollID
	 *            id željene ankete
	 * 
	 * @return listu unosa
	 * @throws DAOException
	 *             u slučaju pogreške
	 */
	public List<Option> getOptionsList(long pollID) throws DAOException;

	/**
	 * Dohvaća opciju za zadani id ankete. Ako opcija ne postoji, vraća
	 * <code>null</code>.
	 * 
	 * @param id
	 *            id željene opcije
	 * @return željeni opcija tipa {@link Option}
	 * @throws DAOException
	 *             Ako je došlo do pogreške u dohvatu
	 */
	public Option getOption(long id) throws DAOException;

	/**
	 * Vraća željenu anketu iz baze podataka.
	 * 
	 * @param id
	 *            id željene ankete
	 * @return željena anketa tipa {@link Poll}
	 * @throws DAOException
	 *             ako je došlo do pogreške u dohvatu iz baze
	 */
	public Poll getPoll(long id) throws DAOException;

	/**
	 * Vraća listu definiranih anketa iz baze podataka.
	 * 
	 * @return {@link List}a definiranih anketa.
	 * @throws DAOException
	 *             ako je došlo do pogreške u dohvatu iz baze.
	 */
	public List<Poll> getPollsList() throws DAOException;

	/**
	 * Inkrementira broj glasova specifične anketne opcije.
	 * 
	 * @param id
	 *            id željene anketne opcije
	 */
	public void incrementOptionVotes(long id);

}