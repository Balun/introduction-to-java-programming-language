package hr.fer.zemris.java.HW14.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import hr.fer.zemris.java.HW14.dao.DAOProvider;
import hr.fer.zemris.java.HW14.model.Option;

/**
 * This class represents a simple servlet for downloading the results of the
 * voting application in an Microsoft excel document form.
 * 
 * @author Matej Balun
 *
 */
@WebServlet(name = "glasanjeXLS", urlPatterns = { "/glasanje-xls" })
public class GlasanjeXLSServlet extends HttpServlet {

	/**
	 * The serial version UID for this class.
	 */
	private static final long serialVersionUID = -3234834091565020346L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		long pollID = (Long) req.getSession().getAttribute("pollID");
		List<Option> options = DAOProvider.getDao().getOptionsList(pollID);

		HSSFWorkbook book = new HSSFWorkbook();
		HSSFSheet sheet = book.createSheet("Rezultati glasanja");

		int k = 0;
		HSSFRow rowhead = sheet.createRow(k++);
		rowhead.createCell(0).setCellValue("Ime opcije");
		rowhead.createCell(1).setCellValue("Bodovi");

		for (Option option : options) {
			HSSFRow row = sheet.createRow(k++);
			row.createCell(0).setCellValue(option.getOptionTitle());
			row.createCell(1).setCellValue(option.getVotesCount());
		}

		resp.setContentType("application/vnd.ms-excel");
		book.write(resp.getOutputStream());
		book.close();

	}

}
