package hr.fer.zemris.java.HW14.model;

import java.text.Collator;
import java.util.Locale;

/**
 * Razred koji opisuje model podataka vezan za anketnu opciju spremljenu u bazi
 * podataka. Javno sučelje razreda i atributi odgovaraju tablici "PollOptions"
 * iza baze podataka.
 * 
 * @author Matej Balun
 *
 */
public class Option implements Comparable<Option> {

	/**
	 * ID opcije.
	 */
	private long id;

	/**
	 * Naziv opcije.
	 */
	private String optionTitle;

	/**
	 * Link na opis ili primjerak opcije.
	 */
	private String optionLink;

	/**
	 * id ankete kojoj opcija pripada.
	 */
	private long pollID;

	/**
	 * Broj trenutnih glasova opcije.
	 */
	private long votesCount;

	/**
	 * Ovaj kosntruktor inicijalizira razred {@link Option} sa njegovim dotičnim
	 * atributima.
	 * 
	 * @param id
	 *            id anketne opcije
	 * @param optionTitle
	 *            naziv opcije
	 * @param optionLink
	 *            link na primjer opcije
	 * @param pollID
	 *            id ankete kojoj opcija pripada
	 * @param votesCount
	 *            broj glasova opcije
	 */
	public Option(long id, String optionTitle, String optionLink, long pollID, long votesCount) {
		super();
		this.id = id;
		this.optionTitle = optionTitle;
		this.optionLink = optionLink;
		this.pollID = pollID;
		this.votesCount = votesCount;
	}

	/**
	 * Vraća id opcije.
	 * 
	 * @return id opcije
	 */
	public long getId() {
		return id;
	}

	/**
	 * Vraća naziv opcije.
	 * 
	 * @return naziv opcije
	 */
	public String getOptionTitle() {
		return optionTitle;
	}

	/**
	 * Vraća primjer opcije.
	 * 
	 * @return primjer opcije
	 */
	public String getOptionLink() {
		return optionLink;
	}

	/**
	 * Vraća id ankee kojoj opcija pripada.
	 * 
	 * @return id ankete
	 */
	public long getPollID() {
		return pollID;
	}

	/**
	 * Vraća broj glasva opcije.
	 * 
	 * @return broj gčasova opcije
	 */
	public long getVotesCount() {
		return votesCount;
	}

	@Override
	public int compareTo(Option o) {
		int r = -Long.compare(this.votesCount, o.votesCount);

		if (r == 0) {
			r = Collator.getInstance(new Locale("en")).compare(this.optionTitle, o.optionTitle);
		}

		return r;
	}

}
