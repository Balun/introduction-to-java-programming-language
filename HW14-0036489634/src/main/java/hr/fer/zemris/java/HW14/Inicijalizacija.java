package hr.fer.zemris.java.HW14;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.mchange.v2.c3p0.DataSources;

import hr.fer.zemris.java.HW14.dao.DAOException;
import hr.fer.zemris.java.HW14.dao.NoSuchTableException;
import hr.fer.zemris.java.HW14.model.Option;
import hr.fer.zemris.java.HW14.model.Poll;

/**
 * Listener koji sluša promjene konteksta servleta za web aplikaciju anketnog
 * glasanja.
 * 
 * @author Matej Balun
 *
 */
@WebListener
public class Inicijalizacija implements ServletContextListener {

	/**
	 * Ime tablica sa anketama.
	 */
	public static final String POOLS = "Polls";

	/**
	 * Ključ baze za ankete.
	 */
	public static final String POOL_KEY = "pool";

	/**
	 * Ime tablice sa anketnim opcijama.
	 */
	public static final String POOLS_OPTIONS = "PollOptions";

	/**
	 * Naslov opcije za glasanje za omiljeni bend.
	 */
	private static final String POOL_TITLE = "Glasanje za omiljeni bend";

	/**
	 * SQL naredba za stvaranje tablice anketa.
	 */
	private static final String CREATE_POOLS = "CREATE TABLE Polls "
			+ "(id BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, " + "title VARCHAR(150) NOT NULL,"
			+ "message CLOB(2048) NOT NULL)";

	/**
	 * SQL naredba za stvaranje tablice anketnih opcija.
	 */
	private static final String CREATE_POOL_OPTIONS = "CREATE TABLE PollOptions "
			+ "(id BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, " + "optionTitle VARCHAR(100) NOT NULL, "
			+ "optionLink VARCHAR(150) NOT NULL, " + "pollID BIGINT, " + "votesCount BIGINT, "
			+ "FOREIGN KEY (pollID) REFERENCES Polls(id))";

	/**
	 * SQL kod za nepostojeću tablicu.
	 */
	private final static String NO_SUCH_TABLE_CODE = "42X05";

	/**
	 * Konekcija za dohvata podataka iz baze.
	 */
	private Connection connection;

	/**
	 * {@link ServletContextEvent} ovog listenera.
	 */
	private ServletContextEvent sce;

	@Override
	public void contextInitialized(ServletContextEvent sce) {

		String connectionURL = null;
		Properties dbProps = new Properties();
		this.sce = sce;

		try {
			dbProps.load(Files
					.newInputStream(Paths.get(sce.getServletContext().getRealPath("/WEB-INF/dbsettings.properties"))));

			connectionURL = "jdbc:derby://" + dbProps.getProperty("host") + ":" + dbProps.getProperty("port") + "/"
					+ dbProps.getProperty("name") + ";user=" + dbProps.getProperty("user") + ";password="
					+ dbProps.getProperty("password");

		} catch (IOException e) {
			e.printStackTrace();
		}

		ComboPooledDataSource cpds = new ComboPooledDataSource();
		try {
			cpds.setDriverClass("org.apache.derby.jdbc.ClientDriver");
		} catch (PropertyVetoException e1) {
			throw new RuntimeException("Pogreška prilikom inicijalizacije poola.", e1);
		}
		cpds.setJdbcUrl(connectionURL);

		sce.getServletContext().setAttribute("hr.fer.zemris.dbpool", cpds);

		try {
			connection = cpds.getConnection();
			createMissingTables();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Metoda koja stvara potrebne tablice u bazi podataka ako one ne postoje.
	 * 
	 * @throws SQLException
	 *             ako je došlo do poreške u baratanju s bazom podataka
	 */
	private void createMissingTables() throws SQLException {
		boolean haveToLoad = false;
		try {
			ResultSet set = select(connection, "SELECT COUNT(*) FROM " + POOLS);
			set.next();
			if (set.getLong(1) == 0) {
				// table is empty
				haveToLoad = true;
			}
		} catch (NoSuchTableException e) {
			// table doesn't exists
			update(connection, CREATE_POOLS);
			haveToLoad = true;
		}

		try {
			select(connection, "SELECT COUNT(*) FROM " + POOLS_OPTIONS);
		} catch (NoSuchTableException e) {
			// table doesn't exists
			update(connection, CREATE_POOL_OPTIONS);
			haveToLoad = true;
		}

		if (haveToLoad) {
			// first deletes all tables
			update(connection, "DELETE FROM " + POOLS);
			update(connection, "DELETE FROM " + POOLS_OPTIONS);

			insertPolls();
			insertPollOptions();
		}

	}

	/**
	 * Metoda koja unosi par primjernih anketa u bazu podataka.
	 */
	private void insertPolls() {
		insertPoll(new Poll(1, "Glasanje za omiljeni bend",
				"Od sljedećih bendova, koji Vam je bend najdraži? Kliknite na link kako biste glasali!"));
		insertPoll(new Poll(2, "Koja je najbolja momčad na EURU 2016?",
				"Kliknite link na po Vama najbolju momčad na europskom prvenstvu"));
		insertPoll(new Poll(3, "Glasanje za najbollji kolegij na FER-u",
				"Kliknite na link koji je za Vas najbolji kolegij na fakultetu elektrotehnike i računarstva"));

	}

	/**
	 * Metoda koja unosi par anketnih opcija u bazu podataka pod anketom za
	 * glasanje za najbolji bend.
	 */
	private void insertPollOptions() {

		long key = 0;

		try (PreparedStatement pst = connection.prepareStatement("SELECT * FROM Polls ORDER BY id")) {

			try (ResultSet rs = pst.executeQuery()) {
				while (rs != null && rs.next()) {
					Poll poll = new Poll(rs.getLong(1), rs.getString(2), rs.getString(3));
					if (poll.getTitle().trim().equalsIgnoreCase(POOL_TITLE)) {
						key = poll.getId();
						break;
					}
				}
			}
		} catch (Exception e) {
			throw new DAOException("Pogreška u dohvatu popisa anketa.", e);
		}

		try {
			List<String> options = Files
					.readAllLines(Paths.get(sce.getServletContext().getRealPath("/WEB-INF/glasanje-definicija.txt")));

			for (String line : options) {
				insertPollOption(new Option(Long.parseLong(line.split("\t")[0]), line.split("\t")[1],
						line.split("\t")[2], key, 0));
			}
		} catch (IOException e) {
			throw new DAOException("Pogreška u dohvatu popisa opcija", e);
		}
	}

	/**
	 * Metoda koja služi za unos ankete u bazu podataka.
	 * 
	 * @param poll
	 *            model podataka ankete
	 * @throws DAOException
	 *             ako je došlo do pogreške u unosu
	 */
	public void insertPoll(Poll poll) throws DAOException {

		try (PreparedStatement pst = connection.prepareStatement("INSERT INTO Polls (title, message) values (?, ?)")) {

			pst.setString(1, poll.getTitle());
			pst.setString(2, poll.getMessage());

			pst.executeUpdate();

		} catch (Exception e) {
			throw new DAOException("Poreška u dohvatu ankete.", e);
		}
	}

	/**
	 * Metoda za unos anketne opcije u baz podataka.
	 * 
	 * @param option
	 *            model podataka koji opsisuje anketnu opciju.
	 * @throws DAOException
	 *             ako je došlo do pogreške u unosu u bazu podataka.
	 */
	public void insertPollOption(Option option) throws DAOException {

		try (PreparedStatement pst = connection.prepareStatement(
				"INSERT INTO PollOptions (optionTitle, optionLink, pollID, votesCount) values (?, ?, ?, ?)")) {

			System.out.println(option.getPollID());

			pst.setString(1, option.getOptionTitle());
			pst.setString(2, option.getOptionLink());
			pst.setLong(3, option.getPollID());
			pst.setLong(4, option.getVotesCount());

			pst.executeUpdate();

		} catch (Exception e) {
			throw new DAOException("Poreška u dohvatu ankete.", e);
		}
	}

	/**
	 * Metoda koja služi za selektiranje skupa podataka iz baze podataka.
	 * 
	 * @param con
	 *            konekcija s bazom podataka
	 * @param command
	 *            SQL naredba
	 * @return {@link ResultSet} skup pdataka
	 */
	public static ResultSet select(Connection con, String command) {
		PreparedStatement pst = null;
		ResultSet result = null;
		try {
			pst = con.prepareStatement(command);
			result = pst.executeQuery();
		} catch (SQLException ex) {
			if (ex.getSQLState().equals(NO_SUCH_TABLE_CODE)) {
				throw new NoSuchTableException();
			}
		}
		return result;
	}

	/**
	 * Metoda za promjenu podataka u bazi podataka
	 * 
	 * @param con
	 *            konekcija s bazom podataka
	 * @param command
	 *            SQL naredba
	 * @return broj promijenjenih redala
	 */
	public static int update(Connection con, String command) {
		PreparedStatement pst = null;
		int result = 0;
		try {
			pst = con.prepareStatement(command);
			result = pst.executeUpdate();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}

		return result;
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		ComboPooledDataSource cpds = (ComboPooledDataSource) sce.getServletContext()
				.getAttribute("hr.fer.zemris.dbpool");
		if (cpds != null) {
			try {
				DataSources.destroy(cpds);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}