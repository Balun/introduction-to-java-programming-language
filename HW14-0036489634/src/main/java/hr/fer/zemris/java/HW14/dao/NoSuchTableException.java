package hr.fer.zemris.java.HW14.dao;

/**
 * Inimka koja se baca u slučaju pokušaja dohvata nepostojeće tablice iz baze
 * podataka.
 * 
 * @author Matej Balun
 *
 */
public class NoSuchTableException extends RuntimeException {

	/**
	 * UID za ovaj razred.
	 */
	private static final long serialVersionUID = -6759787659168782458L;

	/**
	 * Default konstruktor.
	 */
	public NoSuchTableException() {
	}

	@SuppressWarnings("javadoc")
	public NoSuchTableException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	@SuppressWarnings("javadoc")
	public NoSuchTableException(String message, Throwable cause) {
		super(message, cause);
	}

	@SuppressWarnings("javadoc")
	public NoSuchTableException(String message) {
		super(message);
	}

	@SuppressWarnings("javadoc")
	public NoSuchTableException(Throwable cause) {
		super(cause);
	}
}
