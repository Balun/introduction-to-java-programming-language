package hr.fer.zemris.java.HW14.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.HW14.dao.DAOProvider;
import hr.fer.zemris.java.HW14.model.Option;

/**
 * This class represents a simple servlet for the displaying the results of the
 * music voting in table, graphical and excel form. It also provides the links
 * to the winners' songs.
 * 
 * @author Matej Balun
 *
 */

@WebServlet(name = "glasanjeRezultati", urlPatterns = { "/glasanje-rezultati" })
public class GlasanjeRezultatiServlet extends HttpServlet {

	/**
	 * The serial version UID for this class.
	 */
	private static final long serialVersionUID = 8856357268158616660L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		long pollID = (Long) req.getSession().getAttribute("pollID");
		List<Option> options = DAOProvider.getDao().getOptionsList(pollID);

		req.setAttribute("options", options);
		req.setAttribute("pollID", pollID);

		req.getRequestDispatcher("/WEB-INF/pages/glasanjeRez.jsp").forward(req, resp);
	}

}
