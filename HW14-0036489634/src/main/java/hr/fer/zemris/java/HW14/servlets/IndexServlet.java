package hr.fer.zemris.java.HW14.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.HW14.dao.DAOProvider;
import hr.fer.zemris.java.HW14.model.Poll;

/**
 * Ovaj razred predstavlja osnovni (index) servlet web aplikacije za glasanje.
 * On dohvaća listu raspoloživih anketa iz baze podataka te ju proslijeđuje
 * dalje na index.jsp stranicu kako bi korisnik odabrao kojoj anketi želi
 * prisustvovati.
 * 
 * @author Matej Balun
 *
 */
@WebServlet(name = "index", urlPatterns = { "/index.html", "/index", "/" })
public class IndexServlet extends HttpServlet {

	/**
	 * UID za ovaj servlet
	 */
	private static final long serialVersionUID = 3798443809756001824L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		List<Poll> polls = DAOProvider.getDao().getPollsList();
		req.setAttribute("polls", polls);

		req.getRequestDispatcher("/WEB-INF/pages/index.jsp").forward(req, resp);
	}

}
