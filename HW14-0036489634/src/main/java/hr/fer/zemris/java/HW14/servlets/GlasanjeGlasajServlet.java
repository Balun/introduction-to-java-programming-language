package hr.fer.zemris.java.HW14.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.HW14.dao.DAOProvider;
import hr.fer.zemris.java.HW14.model.Poll;
import hr.fer.zemris.java.HW14.model.Option;

/**
 * Ova servlet služi za dohvat opcija dotične ankete čiji id servlet dobiva
 * preko parametara, te proslijeđuje podatke o anketi dalje na glasanje.jsp za
 * grafički prikaz.
 * 
 * @author Matej Balun
 *
 */
@WebServlet(name = "glasanje", urlPatterns = { "/glasanje" })
public class GlasanjeGlasajServlet extends HttpServlet {

	/**
	 * UID za ovaj servlet. 
	 */
	private static final long serialVersionUID = 2441952367635931388L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		long n = Long.parseLong(req.getParameter("pollID"));
		req.getSession().setAttribute("pollID", n);

		List<Option> options = DAOProvider.getDao().getOptionsList(n);
		Poll poll = DAOProvider.getDao().getPoll(n);

		req.setAttribute("options", options);
		req.setAttribute("poll", poll);

		req.getRequestDispatcher("/WEB-INF/pages/glasanje.jsp").forward(req, resp);
	}

}
