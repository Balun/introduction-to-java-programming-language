package hr.fer.zemris.java.HW14.dao.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import hr.fer.zemris.java.HW14.dao.DAO;
import hr.fer.zemris.java.HW14.dao.DAOException;
import hr.fer.zemris.java.HW14.model.Option;
import hr.fer.zemris.java.HW14.model.Poll;

/**
 * Ovo je implementacija podsustava DAO uporabom tehnologije SQL. Ova konkretna
 * implementacija očekuje da joj veza stoji na raspolaganju preko
 * {@link SQLConnectionProvider} razreda, što znači da bi netko prije no što
 * izvođenje dođe do ove točke to trebao tamo postaviti. U web-aplikacijama
 * tipično rješenje je konfigurirati jedan filter koji će presresti pozive
 * servleta i prije toga ovdje ubaciti jednu vezu iz connection-poola, a po
 * zavrsetku obrade je maknuti.
 * 
 * @author Matej Balun
 */
public class SQLDAO implements DAO {

	@Override
	public List<Option> getOptionsList(long pollID) throws DAOException {
		List<Option> options = new ArrayList<>();
		Connection con = SQLConnectionProvider.getConnection();

		try (PreparedStatement pst = con
				.prepareStatement("SELECT * FROM PollOptions WHERE pollID=" + pollID + " ORDER BY id")) {

			try (ResultSet rs = pst.executeQuery()) {
				while (rs != null && rs.next()) {
					Option option = new Option(rs.getLong(1), rs.getString(2), rs.getString(3), rs.getLong(4),
							rs.getLong(5));
					options.add(option);
				}
			}
		} catch (Exception e) {
			throw new DAOException("Pogreška prilikom dohvata liste opcija ankete.", e);
		}

		return options;
	}

	@Override
	public Option getOption(long id) throws DAOException {
		Option option = null;
		Connection con = SQLConnectionProvider.getConnection();

		try (PreparedStatement pst = con.prepareStatement("SELECT * FROM PollOptions WHERE id =" + id)) {

			try (ResultSet rs = pst.executeQuery()) {
				rs.next();
				option = new Option(rs.getLong(1), rs.getString(2), rs.getString(3), rs.getLong(4), rs.getLong(5));
			}
		} catch (Exception e) {
			throw new DAOException("Pogreška prilikom dohvata anketne opcije.", e);
		}

		return option;
	}

	@Override
	public Poll getPoll(long id) throws DAOException {
		Poll poll = null;
		Connection con = SQLConnectionProvider.getConnection();

		try (PreparedStatement pst = con.prepareStatement("SELECT * FROM Polls where id=?")) {
			pst.setLong(1, Long.valueOf(id));

			try (ResultSet rs = pst.executeQuery()) {
				rs.next();
				poll = new Poll(rs.getLong(1), rs.getString(2), rs.getString(3));
			}
		} catch (Exception e) {
			throw new DAOException("Poreška u dohvatu ankete.", e);
		}

		return poll;
	}

	@Override
	public List<Poll> getPollsList() throws DAOException {
		List<Poll> polls = new ArrayList<>();
		Connection con = SQLConnectionProvider.getConnection();

		try (PreparedStatement pst = con.prepareStatement("SELECT * FROM Polls ORDER BY id")) {

			try (ResultSet rs = pst.executeQuery()) {
				while (rs != null && rs.next()) {
					Poll poll = new Poll(rs.getLong(1), rs.getString(2), rs.getString(3));
					polls.add(poll);
				}
			}
		} catch (Exception e) {
			throw new DAOException("Pogreška u dohvatu popisa anketa.", e);
		}

		return polls;
	}

	@Override
	public void incrementOptionVotes(long id) {
		Connection con = SQLConnectionProvider.getConnection();

		try (PreparedStatement pst = con
				.prepareStatement("UPDATE PollOptions SET votesCount=votesCount+1 WHERE id=?")) {
			pst.setLong(1, Long.valueOf(id));

			pst.executeUpdate();

		} catch (Exception e) {
			throw new DAOException("Poreška u dohvatu ankete.", e);
		}
	}

}
