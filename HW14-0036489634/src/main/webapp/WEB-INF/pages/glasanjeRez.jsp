<%@page import="hr.fer.zemris.java.HW14.model.Option"%>
<%@page import="java.util.TreeSet"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.List"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<style type="text/css">
table.rez td {
	text-align: center;
}
</style>
</head>
<body style="background-color: <%=session.getAttribute("pickedBgCol")%>">
	<h1>Rezultati glasanja</h1>
	<p>Ovo su rezultati glasanja.</p>
	<table border="1" cellspacing="0" class="rez">
		<thead>
			<tr>
				<th>Opcija</th>
				<th>Broj glasova</th>
			</tr>
		</thead>
		<tbody>
			<%
				List<Option> options = (List<Option>) request.getAttribute("options");

				for (Option option : new TreeSet<Option>(options)) {
			%><tr>
				<td><%=option.getOptionTitle()%></td>
				<td><%=option.getVotesCount()%></td>
			</tr>
			<%
				}
			%>
		</tbody>
	</table>

	<h2>Grafički prikaz rezultata</h2>
	<img alt="Pie-chart" src=glasanje-grafika width="400" height="400" />

	<h2>Rezultati u XLS formatu</h2>
	<p>
		Rezultati u XLS formatu dostupni su <a href=glasanje-xls>ovdje</a>
	</p>

	<h2>Razno</h2>
	<p>Primjeri pjesama pobjedničkih bendova:</p>
	<ul>
		<%
			long max = 0;
			for (Option option : options) {
				if (option.getVotesCount() > max) {
					max = option.getVotesCount();
				}
			}

			for (Option option : options) {
				if (option.getVotesCount() == max) {
		%><li><a href=<%=option.getOptionLink()%> target="_blank"><%=option.getOptionTitle()%></a></li>
		<%
			}
			}
		%>
	</ul>

</body>
</html>