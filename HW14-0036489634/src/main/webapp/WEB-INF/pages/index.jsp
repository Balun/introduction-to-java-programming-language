<%@page import="hr.fer.zemris.java.HW14.model.Poll"%>
<%@page import="java.awt.Point"%>
<%@page import="java.util.List"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<body>
	<h1>Odaberite željenu anketu od ponuđenih:</h1>
	<%
		List<Poll> polls = (List<Poll>) request.getAttribute("polls");
	%>
	<c:forEach var="e" items="${polls}">
		<p>
			<a href="glasanje?pollID=${e.id}">${e.title}</a>
		</p>
		<br>
	</c:forEach>
</body>
</html>