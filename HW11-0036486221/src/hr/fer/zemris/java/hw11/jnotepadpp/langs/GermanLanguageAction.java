package hr.fer.zemris.java.hw11.jnotepadpp.langs;

import java.awt.event.ActionEvent;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.local.LocalizableAction;
import hr.fer.zemris.java.hw11.jnotepadpp.local.LocalizationProvider;

public class GermanLanguageAction extends LocalizableAction {

	public GermanLanguageAction(JNotepadPP notepad, ILocalizationProvider lzProvider) {
		super("deLangKey", "deLangDesc", lzProvider);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		LocalizationProvider.getInstance().setLanguage("de");

	}

}
