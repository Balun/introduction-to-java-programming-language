package hr.fer.zemris.java.hw11.jnotepadpp;

import java.awt.event.ActionEvent;
import java.util.Map;

import javax.swing.JTabbedPane;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP.AreaPath;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.local.LocalizableAction;

public class CutDocumentAction extends LocalizableAction {
	private JNotepadPP notepad;
	private JTabbedPane tabbedPane;
	private Map<Integer, AreaPath> editors;

	public CutDocumentAction(JNotepadPP notepad, ILocalizationProvider lzProvider) {
		super("cutKey", "cutDesc", lzProvider);
		this.notepad = notepad;
		this.tabbedPane = notepad.getTabbedPane();
		this.editors = notepad.getEditors();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		int index = tabbedPane.getSelectedIndex();
		editors.get(index).getArea().cut();

	}

}
