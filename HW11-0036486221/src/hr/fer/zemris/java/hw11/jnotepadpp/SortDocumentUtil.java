package hr.fer.zemris.java.hw11.jnotepadpp;

import java.awt.event.ActionEvent;
import java.text.Collator;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.swing.JTabbedPane;
import javax.swing.JTextArea;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP.AreaPath;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.local.LocalizableAction;

public abstract class SortDocumentUtil extends LocalizableAction {
	protected Collator collator;
	private JNotepadPP notepad;
	private JTabbedPane tabbedPane;
	private Map<Integer, AreaPath> editors;

	public SortDocumentUtil(JNotepadPP notepad, ILocalizationProvider lzProvider) {
		super("exitKey", "exitDesc", lzProvider);
		this.notepad = notepad;
		this.tabbedPane = notepad.getTabbedPane();
		this.editors = notepad.getEditors();
		this.collator = Collator.getInstance(new Locale("hr"));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		List<String> linesList = new LinkedList<>();
		int index = tabbedPane.getSelectedIndex();
		JTextArea editor = editors.get(index).getArea();
		try {
			int dot = editor.getCaret().getDot();
			int mark = editor.getCaret().getMark();
			if (mark < dot) {
				int temp = dot;
				dot = mark;
				mark = temp;
			}
			int first = editor.getLineOfOffset(dot);
			int last = editor.getLineOfOffset(mark);

			int removeLen = 0;
			for (int i = first; i < last + 1; i++) {
				int stop = editor.getLineEndOffset(i);
				int begin = editor.getLineStartOffset(i);
				int len = stop - begin;
				String line = editor.getDocument().getText(begin, len);
				removeLen += line.length();
				if (line.contains("\n")) {
					int newRow = line.indexOf("\n");
					line = line.substring(0, newRow);
				}
				linesList.add(line);
			}
			editor.getDocument().remove(editor.getLineStartOffset(first), removeLen);
			Collections.sort(linesList, comparator());
			int begin = editor.getLineStartOffset(first);
			String stringToInsert = "";
			for (String line : linesList) {
				stringToInsert += line + "\n";
			}
			editor.getDocument().insertString(begin, stringToInsert, null);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	protected abstract Comparator<String> comparator();

	protected abstract String getKey();

	protected abstract String getDesc();

}
