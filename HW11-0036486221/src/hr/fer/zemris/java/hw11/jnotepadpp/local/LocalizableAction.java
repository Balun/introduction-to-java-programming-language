package hr.fer.zemris.java.hw11.jnotepadpp.local;

import javax.swing.AbstractAction;
import javax.swing.Action;

public abstract class LocalizableAction extends AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ILocalizationProvider lzProvider;
	private String name;
	private String description;

	public LocalizableAction(String name, String desc, ILocalizationProvider lp) {
		super();
		this.lzProvider = lp;
		this.name = name;
		this.description = desc;
		this.lzProvider.addLocalizationListener(new ILocalizationListener() {

			@Override
			public void localizationChanged() {
				putValue(Action.NAME, lzProvider.getString(name));
				putValue(Action.SHORT_DESCRIPTION, lzProvider.getString(description));

			}
		});

	}

}
