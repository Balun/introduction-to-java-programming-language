package hr.fer.zemris.java.hw11.jnotepadpp;

import java.awt.event.ActionEvent;
import java.util.Map;

import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP.AreaPath;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.local.LocalizableAction;

public class StatisticalDocumentAction extends LocalizableAction {
	private JNotepadPP notepad;
	private JTabbedPane tabbedPane;
	private Map<Integer, AreaPath> editors;

	public StatisticalDocumentAction(JNotepadPP notepad, ILocalizationProvider lzProvider) {
		super("statisticKey", "statisticDesc", lzProvider);
		this.notepad = notepad;
		this.tabbedPane = notepad.getTabbedPane();
		this.editors = notepad.getEditors();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		int index = tabbedPane.getSelectedIndex();
		JTextArea editor = editors.get(index).getArea();
		int lines = editor.getLineCount();
		String characters = editor.getText();
		int chars = characters.length();
		characters = characters.replaceAll("\\s+", "");
		int noSpaces = characters.length();

		String message = "Your document has " + chars + " characters, " + noSpaces + " non-blank characters and "
				+ lines + " lines.";
		JOptionPane.showMessageDialog(notepad, message, "Info", JOptionPane.INFORMATION_MESSAGE);
	}

}
