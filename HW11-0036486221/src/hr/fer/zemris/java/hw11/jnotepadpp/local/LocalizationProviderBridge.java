package hr.fer.zemris.java.hw11.jnotepadpp.local;

public class LocalizationProviderBridge extends AbstractLocalizationProvider {
	private boolean connected;
	private String language;
	private String addedLanguage;
	private ILocalizationProvider lzProvider;

	ILocalizationListener lzListener = new ILocalizationListener() {
		@Override
		public void localizationChanged() {
			fire();
		}
	};

	public LocalizationProviderBridge(ILocalizationProvider provider) {
		this.lzProvider = provider;
	}

	@Override
	public String getString(String key) {
		return lzProvider.getString(key);
	}

	public void disconnect() {
		if (connected) {
			connected = false;
			lzProvider.removeLocalizationListener(lzListener);
		}
	}

	public void connect() {
		if (!connected) {
			connected = true;
			lzProvider.addLocalizationListener(lzListener);
			lzListener.localizationChanged();
		}

	}
}
