package hr.fer.zemris.java.hw11.jnotepadpp;

public enum ToolType {
	OPEN, SAVE, SAVEAS, NEW, CLOSE, STATISTICS, EXIT, CUT, COPY, PASTE;
}
