package hr.fer.zemris.java.hw11.jnotepadpp.langs;

import java.awt.event.ActionEvent;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.local.LocalizableAction;
import hr.fer.zemris.java.hw11.jnotepadpp.local.LocalizationProvider;

public class EnglishLanguageAction extends LocalizableAction {

	public EnglishLanguageAction(JNotepadPP notepad, ILocalizationProvider lzProvider) {
		super("engLangKey", "engLangDesc", lzProvider);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		LocalizationProvider.getInstance().setLanguage("en");

	}

}
