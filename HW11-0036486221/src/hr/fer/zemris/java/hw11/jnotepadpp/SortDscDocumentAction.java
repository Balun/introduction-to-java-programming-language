package hr.fer.zemris.java.hw11.jnotepadpp;

import java.util.Comparator;

import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;

public class SortDscDocumentAction extends SortDocumentUtil {

	public SortDscDocumentAction(JNotepadPP notepad, ILocalizationProvider lzProvider) {
		super(notepad, lzProvider);
	}

	@Override
	protected Comparator<String> comparator() {
		return (s1, s2) -> -collator.compare(s1, s2);
	}

	@Override
	protected String getKey() {
		return "dscKey";
	}

	@Override
	protected String getDesc() {
		return "dscDesc";
	}

}
