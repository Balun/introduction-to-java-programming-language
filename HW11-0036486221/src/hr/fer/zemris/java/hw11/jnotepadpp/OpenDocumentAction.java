package hr.fer.zemris.java.hw11.jnotepadpp;

import java.awt.event.ActionEvent;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP.AreaPath;
import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP.TabPanel;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.local.LocalizableAction;

public class OpenDocumentAction extends LocalizableAction {
	private final Path UNSAVED_DOCUMENT = null;
	private final int STARTING_AREA_INDEX = 0;
	private final String TITLE = " JNotepad++";

	private JNotepadPP notepad;
	private JTabbedPane tabbedPane;
	private Map<Integer, AreaPath> editors;

	public OpenDocumentAction(JNotepadPP notepad, ILocalizationProvider lzProvider) {
		super("openKey", "openDesc", lzProvider);
		this.notepad = notepad;
		this.tabbedPane = notepad.getTabbedPane();
		this.editors = notepad.getEditors();

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		int selectedIndex = tabbedPane.getSelectedIndex();
		AreaPath areaAndPath = editors.get(selectedIndex);
		JTextArea editor = new JTextArea();
		Path currentPath = null;
		editor = areaAndPath.getArea();
		currentPath = areaAndPath.getPath();

		String currentAreaText = editor.getText().trim();

		JFileChooser fc = new JFileChooser();
		fc.setDialogTitle("Open file");
		if (fc.showOpenDialog(notepad) != JFileChooser.APPROVE_OPTION)
			return;
		File fileName = fc.getSelectedFile();
		Path filePath = fileName.toPath();

		if (checkIfOppened(filePath))
			return;
		if (!currentAreaText.equals("") || currentPath != UNSAVED_DOCUMENT
				|| !editor.equals(notepad.getStartingEditor()) || editors.size() > 1) {
			selectedIndex = tabbedPane.getComponentCount() - 1;
			notepad.addNewTab(filePath.getFileName().toString(), new JTextArea());
			notepad.setLastNewTabIndex(notepad.getLastNewTabIndex() - 1);
		} else {
			selectedIndex = STARTING_AREA_INDEX;
		}
		editor = new JTextArea();
		if (!Files.isReadable(filePath)) {
			JOptionPane.showConfirmDialog(notepad, "File " + fileName.getAbsolutePath() + " doesn't exist.", "Error",
					JOptionPane.ERROR_MESSAGE);
			return;
		}

		byte[] okteti;

		try {
			okteti = Files.readAllBytes(filePath);
		} catch (Exception ex) {
			JOptionPane.showMessageDialog(notepad, "Error while loading " + fileName.getAbsolutePath(), "Error",
					JOptionPane.ERROR_MESSAGE);
			return;
		}
		JScrollPane scrollPane = new JScrollPane(editor);
		notepad.addSaveIconListener(editor, scrollPane);
		notepad.addCaretListener(editor);
		JLabel title = new JLabel(filePath.getFileName().toString());
		title.setBorder(BorderFactory.createEmptyBorder(0, 1, 0, 3));
		TabPanel tabPanel = notepad.new TabPanel(title);
		tabbedPane.setComponentAt(selectedIndex, scrollPane);
		tabbedPane.setTabComponentAt(selectedIndex, tabPanel);
		tabbedPane.setSelectedIndex(selectedIndex);
		String text = new String(okteti, StandardCharsets.UTF_8);
		editor.setText(text);
		notepad.setTitle(filePath.toString() + TITLE);
		editors.put(selectedIndex, notepad.new AreaPath(editor, filePath));
		tabbedPane.setSelectedIndex(selectedIndex);
		notepad.setSaveIcon(selectedIndex, "icons/save4.png");

	}

	private boolean checkIfOppened(Path path) {
		for (Map.Entry<Integer, AreaPath> entry : editors.entrySet()) {
			AreaPath ap = entry.getValue();
			if (ap.getPath() == null || path == null)
				continue;
			if (ap.getPath().equals(path)) {
				return true;
			}
		}
		return false;
	}

}
