package hr.fer.zemris.java.hw11.jnotepadpp;

import java.awt.event.ActionEvent;

import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.local.LocalizableAction;

public class NewTabAction extends LocalizableAction {
	private final String NEW_TAB;
	int lastNewTabIndex;
	private JNotepadPP notepad;

	public NewTabAction(JNotepadPP notepad, ILocalizationProvider lzProvider) {
		super("newKey", "newDesc", lzProvider);
		this.NEW_TAB = notepad.getNEW_TAB();
		this.notepad = notepad;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		this.lastNewTabIndex = notepad.getLastNewTabIndex();
		notepad.addNewTab(NEW_TAB + lastNewTabIndex, null);

	}
}
