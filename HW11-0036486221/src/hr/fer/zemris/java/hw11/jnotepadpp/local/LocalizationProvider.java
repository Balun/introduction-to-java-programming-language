package hr.fer.zemris.java.hw11.jnotepadpp.local;

import java.util.Locale;
import java.util.ResourceBundle;

public class LocalizationProvider extends AbstractLocalizationProvider {

	private ResourceBundle bundle;
	private String language;
	private static LocalizationProvider localizationProvider = null;

	private LocalizationProvider() {
		setLanguage("hr");
	}

	@Override
	public String getString(String key) {
		return bundle.getString(key);
	}

	public void setLanguage(String strLng) {
		language = strLng;
		Locale locale = Locale.forLanguageTag(this.language);
		bundle = ResourceBundle.getBundle("hr.fer.zemris.java.hw11.jnotepadpp.translates.translates", locale);
		fire();
	}

	public static LocalizationProvider getInstance() {
		if (localizationProvider == null) {
			localizationProvider = new LocalizationProvider();
		}
		return localizationProvider;
	}

}
