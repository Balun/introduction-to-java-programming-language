package hr.fer.zemris.java.hw11.jnotepadpp;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.WindowConstants;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.plaf.basic.BasicButtonUI;
import javax.swing.text.BadLocationException;

import hr.fer.zemris.java.hw11.jnotepadpp.langs.CroatianLanguageAction;
import hr.fer.zemris.java.hw11.jnotepadpp.langs.EnglishLanguageAction;
import hr.fer.zemris.java.hw11.jnotepadpp.langs.GermanLanguageAction;
import hr.fer.zemris.java.hw11.jnotepadpp.local.FormLocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.local.LocalizationProvider;

public class JNotepadPP extends JFrame {

	private final Path UNSAVED_DOCUMENT = null;
	private final int STARTING_AREA_INDEX = 0;
	private final String NEW_TAB = "new ";
	private final String SAVE_AS_IDENTIFICATOR = "Save as";
	private final String SAVE_IDENTIFICATOR = "Save";
	private final String TITLE = " JNotepad++";
	private final String ICON_SAVED = "icons/save1.png";
	private final String ICON_SAVED_TAB = "icons/save4.png";
	private final String ICON_UNSAVED_TAB = "icons/save3.png";
	private final String ICON_UNSAVED = "icons/save2.png";
	private final String ICON_NEW = "icons/newDoc.png";
	private final String ICON_OPEN = "icons/open.png";
	private final String ICON_SAVEAS = "icons/saveas.png";
	private final String ICON_CLOSE = "icons/close.png";
	private final String ICON_COPY = "icons/copy.png";
	private final String ICON_CUT = "icons/cut.png";
	private final String ICON_PASTE = "icons/paste.png";
	private final String ICON_STATISTICS = "icons/statistics.png";
	private final String ICON_EXIT = "icons/exit.png";
	private String buttonText;
	private int lastNewTabIndex;
	private Map<Integer, AreaPath> editors;
	private JTextArea startingEditor;
	private JTabbedPane tabbedPane;
	private JButton saveButton;
	private JButton closeButton;
	private JButton copyButton;
	private JButton cutButton;
	private JMenuItem invertCase;
	private JMenuItem upperCase;
	private JMenuItem lowerCase;
	private Action openDocumentAction;
	private Action saveDocumentAction;
	private Action saveAsDocumentAction;
	private Action newDocumentAction;
	private Action statisticalDocumentAction;
	private Action copyDocumentAction;
	private Action cutDocumentAction;
	private Action pasteDocumentAction;
	private Action closeDocumentAction;
	private Action invertDocumentAction;
	private Action upperDocumentAction;
	private Action lowerDocumentAction;
	private Action sortAscDocumentAction;
	private Action sortDscDocumentAction;
	private Action uniqueDocumentAction;
	private Action croLangAction;
	private Action engLangAction;
	private Action deLangAction;
	private Action exitAction;
	private JLabel lenghtLabel;
	private JLabel linesLabel;
	private JLabel colLabel;
	private JLabel selLabel;
	private JLabel timeLabel;
	private Timer clock;
	private ILocalizationProvider lzProvider = new FormLocalizationProvider(LocalizationProvider.getInstance(), this);

	public JNotepadPP(String str) {
		super(str);
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		editors = new HashMap<>();
		tabbedPane = new JTabbedPane();
		setLocation(50, 50);
		setSize(750, 599);
		setLayout(new BorderLayout());
		lastNewTabIndex = 1;

		initGUI();
	}

	private void initGUI() {
		addClosingOperation();
		initializeActions();
		saveButton = new JButton(saveDocumentAction);
		cutButton = new JButton(cutDocumentAction);
		copyButton = new JButton(copyDocumentAction);
		saveButton.setEnabled(false);
		cutButton.setEnabled(false);
		copyButton.setEnabled(false);
		closeButton = new JButton(closeDocumentAction);

		startingEditor = new JTextArea();
		JScrollPane scrollPane = new JScrollPane(startingEditor);
		JNotepadPP.this.setTitle(NEW_TAB + (STARTING_AREA_INDEX + 1) + TITLE);
		editors.put(STARTING_AREA_INDEX, new AreaPath(startingEditor, UNSAVED_DOCUMENT));
		tabbedPane.addTab(NEW_TAB, scrollPane);
		JLabel title = new JLabel(NEW_TAB + lastNewTabIndex);
		title.setBorder(BorderFactory.createEmptyBorder(0, 1, 0, 3));
		TabPanel tabPanel = new TabPanel(title);
		tabbedPane.setTabComponentAt(lastNewTabIndex++ - 1, tabPanel);

		addSaveIconListener(startingEditor, scrollPane);
		addCaretListener(startingEditor);
		tabbedPane.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				int selectedIndex = tabbedPane.getSelectedIndex();
				String tittle = null;
				AreaPath areaPath = editors.get(selectedIndex);
				try {
					tittle = areaPath.getPath().toString();
				} catch (NullPointerException np) {
					tittle = NEW_TAB + ++selectedIndex;
				}
				tittle += TITLE;
				JNotepadPP.this.setTitle(tittle);
				lenghtLabel.setText("lenght : " + areaPath.getArea().getText().length());
				linesLabel.setText("Ln : " + getLineCount(areaPath.getArea()));
				colLabel.setText("Col : " + Integer.valueOf(getColumnCount(areaPath.getArea())).toString());
				selLabel.setText("Sel : " + Integer.valueOf(getSelectedCount(areaPath.getArea())));
				if (areaPath.saved) {
					saveButton.setIcon(loadIcon(ICON_SAVED));
				} else {
					saveButton.setIcon(loadIcon(ICON_UNSAVED));
				}
			}
		});

		this.getContentPane().add(tabbedPane, BorderLayout.CENTER);
		addStatusBar();
		createActions();
		createMenus();
		createToolbars();
	}

	private void addStatusBar() {
		JPanel statusBar = new JPanel(new GridLayout(0, 6));
		int index = tabbedPane.getSelectedIndex();
		JTextArea editor = editors.get(index).getArea();
		lenghtLabel = new JLabel("length : " + editor.getText().length(), SwingConstants.LEFT);
		lenghtLabel.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 0));
		linesLabel = new JLabel("Ln : " + getLineCount(editor), SwingConstants.RIGHT);
		linesLabel.setBorder(BorderFactory.createMatteBorder(0, 1, 0, 0, Color.GRAY));
		colLabel = new JLabel("Col : " + Integer.valueOf(getColumnCount(editor)).toString(), SwingConstants.CENTER);
		selLabel = new JLabel("Sel : " + Integer.valueOf(getSelectedCount(editor)), SwingConstants.LEFT);
		selLabel.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 1, Color.GRAY));
		timeLabel = getTimeCount();
		timeLabel.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 0));
		statusBar.add(lenghtLabel);
		statusBar.add(new JLabel());
		statusBar.add(linesLabel);
		statusBar.add(colLabel);
		statusBar.add(selLabel);
		statusBar.add(timeLabel);
		statusBar.setBorder(BorderFactory.createMatteBorder(1, 0, 0, 0, Color.black));
		this.getContentPane().add(statusBar, BorderLayout.SOUTH);

	}

	private void initializeActions() {
		openDocumentAction = new OpenDocumentAction(JNotepadPP.this, lzProvider);
		saveDocumentAction = new SaveDocumentAction(JNotepadPP.this, lzProvider);
		saveAsDocumentAction = new SaveAsDocumentAction(JNotepadPP.this, lzProvider);
		newDocumentAction = new NewTabAction(JNotepadPP.this, lzProvider);
		statisticalDocumentAction = new StatisticalDocumentAction(JNotepadPP.this, lzProvider);
		copyDocumentAction = new CopyDocumentAction(JNotepadPP.this, lzProvider);
		cutDocumentAction = new CutDocumentAction(JNotepadPP.this, lzProvider);
		pasteDocumentAction = new PasteDocumentAction(JNotepadPP.this, lzProvider);
		exitAction = new ExitAction(JNotepadPP.this, lzProvider);
		closeDocumentAction = new CloseDocumentAction(JNotepadPP.this, lzProvider);
		invertDocumentAction = new InvertDocumentAction(JNotepadPP.this, lzProvider);
		upperDocumentAction = new UpperDocumentAction(JNotepadPP.this, lzProvider);
		lowerDocumentAction = new LowerDocumentAction(JNotepadPP.this, lzProvider);
		sortAscDocumentAction = new SortAscDocumentAction(JNotepadPP.this, lzProvider);
		sortDscDocumentAction = new SortDscDocumentAction(JNotepadPP.this, lzProvider);
		uniqueDocumentAction = new UniqueDocumentAction(JNotepadPP.this, lzProvider);
		croLangAction = new CroatianLanguageAction(JNotepadPP.this, lzProvider);
		engLangAction = new EnglishLanguageAction(JNotepadPP.this, lzProvider);
		deLangAction = new GermanLanguageAction(JNotepadPP.this, lzProvider);
	}

	private void createToolbars() {
		JToolBar toolBar = new JToolBar("Tools");
		toolBar.setFloatable(true);

		addToolBarComponent(toolBar, ToolType.NEW, ICON_NEW, new JButton(newDocumentAction));
		toolBar.addSeparator();
		addToolBarComponent(toolBar, ToolType.OPEN, ICON_OPEN, new JButton(openDocumentAction));
		addToolBarComponent(toolBar, ToolType.SAVE, ICON_SAVED, new JButton(saveDocumentAction));
		addToolBarComponent(toolBar, ToolType.SAVEAS, ICON_SAVEAS, new JButton(saveAsDocumentAction));
		addToolBarComponent(toolBar, ToolType.CLOSE, ICON_CLOSE, new JButton(closeDocumentAction));
		toolBar.addSeparator();
		addToolBarComponent(toolBar, ToolType.COPY, ICON_COPY, new JButton(copyDocumentAction));
		addToolBarComponent(toolBar, ToolType.CUT, ICON_CUT, new JButton(cutDocumentAction));
		addToolBarComponent(toolBar, ToolType.PASTE, ICON_PASTE, new JButton(pasteDocumentAction));
		toolBar.addSeparator();
		addToolBarComponent(toolBar, ToolType.STATISTICS, ICON_STATISTICS, new JButton(statisticalDocumentAction));
		addToolBarComponent(toolBar, ToolType.EXIT, ICON_EXIT, new JButton(exitAction));

		this.getContentPane().add(toolBar, BorderLayout.PAGE_START);

	}

	private void createMenus() {
		JMenuBar menuBar = new JMenuBar();
		JMenu fileMenu = new JMenu(lzProvider.getString("fileKey"));
		menuBar.add(fileMenu);
		fileMenu.add(new JMenuItem(newDocumentAction));
		fileMenu.addSeparator();
		fileMenu.add(new JMenuItem(openDocumentAction));
		fileMenu.add(new JMenuItem(saveDocumentAction));
		fileMenu.add(new JMenuItem(saveAsDocumentAction));
		fileMenu.add(new JMenuItem(closeDocumentAction));
		fileMenu.addSeparator();
		fileMenu.add(new JMenuItem(statisticalDocumentAction));
		fileMenu.addSeparator();
		fileMenu.add(new JMenuItem(exitAction));

		JMenu editMenu = new JMenu(lzProvider.getString("editKey"));
		menuBar.add(editMenu);
		editMenu.add(new JMenuItem(copyDocumentAction));
		editMenu.add(new JMenuItem(cutDocumentAction));
		editMenu.add(new JMenuItem(pasteDocumentAction));

		JMenu toolsMenu = new JMenu(lzProvider.getString("toolsKey"));
		JMenu changeCase = new JMenu(lzProvider.getString("caseKey"));
		invertCase = new JMenuItem(invertDocumentAction);
		upperCase = new JMenuItem(upperDocumentAction);
		lowerCase = new JMenuItem(lowerDocumentAction);
		changeCase.add(invertCase);
		changeCase.add(upperCase);
		changeCase.add(lowerCase);
		toolsMenu.add(changeCase);
		JMenu sortMenu = new JMenu(lzProvider.getString("sortKey"));
		sortMenu.add(new JMenuItem(sortAscDocumentAction));
		sortMenu.add(new JMenuItem(sortDscDocumentAction));
		toolsMenu.add(sortMenu);
		toolsMenu.add(new JMenuItem(uniqueDocumentAction));
		menuBar.add(toolsMenu);

		JMenu langMenu = new JMenu(lzProvider.getString("langsKey"));
		langMenu.add(new JMenuItem(engLangAction));
		langMenu.add(new JMenuItem(croLangAction));
		langMenu.add(new JMenuItem(deLangAction));

		menuBar.add(langMenu);

		lzProvider.addLocalizationListener(() -> {
			fileMenu.setText(lzProvider.getString("fileKey"));
			editMenu.setText(lzProvider.getString("editKey"));
			toolsMenu.setText(lzProvider.getString("toolsKey"));
			changeCase.setText(lzProvider.getString("caseKey"));
			sortMenu.setText(lzProvider.getString("sortKey"));
			langMenu.setText(lzProvider.getString("langsKey"));
		});

		this.setJMenuBar(menuBar);
	}

	private void createActions() {
		openDocumentAction.putValue(Action.NAME, lzProvider.getString("openKey"));
		openDocumentAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control O"));
		openDocumentAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_O);
		openDocumentAction.putValue(Action.SHORT_DESCRIPTION, lzProvider.getString("openDesc"));

		saveDocumentAction.putValue(Action.NAME, lzProvider.getString("saveKey"));
		saveDocumentAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control S"));
		saveDocumentAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_S);
		saveDocumentAction.putValue(Action.SHORT_DESCRIPTION, "Used to save current file to disk.");

		exitAction.putValue(Action.NAME, lzProvider.getString("exitKey"));
		exitAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control F4"));
		exitAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_F4);
		exitAction.putValue(Action.SHORT_DESCRIPTION, lzProvider.getString("exitDesc"));

		newDocumentAction.putValue(Action.NAME, lzProvider.getString("newKey"));
		newDocumentAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control N"));
		newDocumentAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_N);
		newDocumentAction.putValue(Action.SHORT_DESCRIPTION, lzProvider.getString("newDesc"));

		saveAsDocumentAction.putValue(Action.NAME, lzProvider.getString("saveAsKey"));
		saveAsDocumentAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control alt S"));
		saveAsDocumentAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_A);
		saveAsDocumentAction.putValue(Action.SHORT_DESCRIPTION, lzProvider.getString("saveAsDesc"));

		copyDocumentAction.putValue(Action.NAME, lzProvider.getString("copyKey"));
		copyDocumentAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control C"));
		copyDocumentAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_C);
		copyDocumentAction.putValue(Action.SHORT_DESCRIPTION, lzProvider.getString("copyDesc"));

		pasteDocumentAction.putValue(Action.NAME, lzProvider.getString("pasteKey"));
		pasteDocumentAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control V"));
		pasteDocumentAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_V);
		pasteDocumentAction.putValue(Action.SHORT_DESCRIPTION, lzProvider.getString("pasteDesc"));

		cutDocumentAction.putValue(Action.NAME, lzProvider.getString("cutKey"));
		cutDocumentAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control X"));
		cutDocumentAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_X);
		cutDocumentAction.putValue(Action.SHORT_DESCRIPTION, lzProvider.getString("cutDesc"));

		statisticalDocumentAction.putValue(Action.NAME, lzProvider.getString("statisticKey"));
		statisticalDocumentAction.putValue(Action.SHORT_DESCRIPTION, lzProvider.getString("statisticDesc"));

		closeDocumentAction.putValue(Action.NAME, lzProvider.getString("closeKey"));
		closeDocumentAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control W"));
		closeDocumentAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_W);
		closeDocumentAction.putValue(Action.SHORT_DESCRIPTION, lzProvider.getString("closeDesc"));

		invertDocumentAction.putValue(Action.NAME, lzProvider.getString("invertKey"));
		invertDocumentAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control F3"));
		invertDocumentAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_T);
		invertDocumentAction.putValue(Action.SHORT_DESCRIPTION, lzProvider.getString("invertDesc"));

		upperDocumentAction.putValue(Action.NAME, lzProvider.getString("upperKey"));
		upperDocumentAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("shift F3"));
		upperDocumentAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_U);
		upperDocumentAction.putValue(Action.SHORT_DESCRIPTION, lzProvider.getString("upperDesc"));

		lowerDocumentAction.putValue(Action.NAME, lzProvider.getString("lowerKey"));
		lowerDocumentAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("alt F3"));
		lowerDocumentAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_L);
		lowerDocumentAction.putValue(Action.SHORT_DESCRIPTION, lzProvider.getString("lowerDesc"));

		sortAscDocumentAction.putValue(Action.NAME, lzProvider.getString("ascKey"));
		sortAscDocumentAction.putValue(Action.SHORT_DESCRIPTION, lzProvider.getString("ascDesc"));

		sortDscDocumentAction.putValue(Action.NAME, lzProvider.getString("dscKey"));
		sortDscDocumentAction.putValue(Action.SHORT_DESCRIPTION, lzProvider.getString("dscDesc"));

		uniqueDocumentAction.putValue(Action.NAME, lzProvider.getString("uniqueKey"));
		uniqueDocumentAction.putValue(Action.SHORT_DESCRIPTION, lzProvider.getString("uniqueDesc"));

		croLangAction.putValue(Action.NAME, lzProvider.getString("croLangKey"));
		croLangAction.putValue(Action.SHORT_DESCRIPTION, lzProvider.getString("croLangDesc"));
		engLangAction.putValue(Action.NAME, lzProvider.getString("engLangKey"));
		engLangAction.putValue(Action.SHORT_DESCRIPTION, lzProvider.getString("engLangDesc"));
		deLangAction.putValue(Action.NAME, lzProvider.getString("deLangKey"));
		deLangAction.putValue(Action.SHORT_DESCRIPTION, lzProvider.getString("deLangDesc"));
	}

	private void addToolBarComponent(JToolBar toolBar, ToolType type, String path, JButton button) {
		switch (type) {
		case OPEN:
			buttonText = "openDesc";
			break;
		case SAVEAS:
			buttonText = "saveAsDesc";
			break;
		case SAVE:
			buttonText = "saveDesc";
			break;
		case NEW:
			buttonText = "newDesc";
			break;
		case CLOSE:
			buttonText = "closeDesc";
			break;
		case CUT:
			buttonText = "cutDesc";
			break;
		case COPY:
			buttonText = "copyDesc";
			break;
		case PASTE:
			buttonText = "pasteDesc";
			break;
		case STATISTICS:
			buttonText = "statisticDesc";
			break;
		case EXIT:
			buttonText = "exitDesc";
			break;

		}

		button.setText("");
		button.setToolTipText(lzProvider.getString(buttonText));
		button.setIcon(loadIcon(path));

		lzProvider.addLocalizationListener(() -> {
			button.setToolTipText(lzProvider.getString(buttonText));
			button.setText("");
		});

		toolBar.add(button);
	}

	public void addCaretListener(JTextArea editor) {
		editor.addCaretListener(new CaretListener() {

			@Override
			public void caretUpdate(CaretEvent e) {
				selLabel.setText("Sel : " + Integer.valueOf(getSelectedCount(editor)));
				colLabel.setText("Col : " + Integer.valueOf(getColumnCount(editor)).toString());
				linesLabel.setText("Ln : " + getLineCount(editor));
				int dot = e.getDot();
				int mark = e.getMark();
				if (dot == mark) {
					cutButton.setEnabled(false);
					copyButton.setEnabled(false);
					invertCase.setEnabled(false);
					upperCase.setEnabled(false);
					lowerCase.setEnabled(false);
				} else {
					cutButton.setEnabled(true);
					copyButton.setEnabled(true);
					invertCase.setEnabled(true);
					upperCase.setEnabled(true);
					lowerCase.setEnabled(true);
				}

			}
		});
	}

	public void addSaveIconListener(JTextArea editor, JScrollPane pane) {
		editor.getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void removeUpdate(DocumentEvent e) {
				int index1 = JNotepadPP.this.tabbedPane.indexOfComponent(pane);
				editors.get(index1).setSaved(false);
				setSaveIcon(tabbedPane.getSelectedIndex(), ICON_UNSAVED_TAB);
				saveButton.setIcon(loadIcon(ICON_UNSAVED));
				saveButton.setEnabled(true);
				lenghtLabel.setText("lenght : " + editor.getText().length());

			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				int index1 = JNotepadPP.this.tabbedPane.indexOfComponent(pane);
				editors.get(index1).setSaved(false);
				lenghtLabel.setText("lenght : " + editor.getText().length());
				setSaveIcon(tabbedPane.getSelectedIndex(), ICON_UNSAVED_TAB);
				saveButton.setIcon(loadIcon(ICON_UNSAVED));
				saveButton.setEnabled(true);
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				// not supported
			}
		});

	}

	public void setSaveIcon(int index, String path) {
		TabPanel panel = (TabPanel) tabbedPane.getTabComponentAt(index);
		panel.setSaveIcon(path);
	}

	public void addNewTab(String tabName, JTextArea editor) {
		JTextArea area = editor;
		;
		if (editor == null) {
			area = new JTextArea();
		}
		JScrollPane scrollPane = new JScrollPane(area);
		tabbedPane.addTab(NEW_TAB, scrollPane);
		int index = tabbedPane.getComponentCount() - 1;
		addSaveIconListener(area, scrollPane);
		addCaretListener(area);
		JLabel title = new JLabel(tabName);
		title.setBorder(BorderFactory.createEmptyBorder(0, 1, 0, 3));
		TabPanel tabPanel = new TabPanel(title);
		editors.put(index - 1, new AreaPath(area, null));
		lastNewTabIndex++;

		tabbedPane.setTabComponentAt(index - 1, tabPanel);
		tabbedPane.setSelectedIndex(index - 1);

	}

	protected void saveMethod(String methodIndetificator) {

		int selecetedIndex = tabbedPane.getSelectedIndex();
		AreaPath areaAndPath = editors.get(selecetedIndex);
		JTextArea editor = areaAndPath.getArea();
		Path openedFilePath = areaAndPath.getPath();
		if (openedFilePath == UNSAVED_DOCUMENT || methodIndetificator.equals(SAVE_AS_IDENTIFICATOR)) {
			JFileChooser jfc = new JFileChooser();
			jfc.setDialogTitle("Save document");
			if (jfc.showSaveDialog(JNotepadPP.this) != JFileChooser.APPROVE_OPTION) {
				JOptionPane.showMessageDialog(JNotepadPP.this, "Nothing was saved.", "Warning",
						JOptionPane.WARNING_MESSAGE);
				return;
			}
			File oppenedFile = jfc.getSelectedFile();
			openedFilePath = oppenedFile.toPath();
			if (oppenedFile.exists()) {
				int desiciosn = JOptionPane.showConfirmDialog(JNotepadPP.this,
						oppenedFile.getName() + " already exists. \n Do you want to replace it  ?", "Confirm Save",
						JOptionPane.YES_NO_OPTION);
				if (desiciosn == JOptionPane.NO_OPTION || desiciosn == JOptionPane.CLOSED_OPTION) {
					return;
				}
			}
		}
		byte[] podatci = editor.getText().getBytes(StandardCharsets.UTF_8);

		try {
			Files.write(openedFilePath, podatci);
		} catch (IOException el) {
			JOptionPane.showMessageDialog(JNotepadPP.this, "Error while saving.", "Pogreška",
					JOptionPane.ERROR_MESSAGE);
			return;
		}

		JOptionPane.showMessageDialog(JNotepadPP.this, "File was saved.", "Info", JOptionPane.INFORMATION_MESSAGE);
		editors.put(selecetedIndex, new AreaPath(editor, openedFilePath));
		editors.get(selecetedIndex).setSaved(true);
		setSaveIcon(selecetedIndex, ICON_SAVED_TAB);
		tabbedPane.setTitleAt(selecetedIndex, openedFilePath.getFileName().toString());
		saveButton.setIcon(loadIcon(ICON_SAVED));
		saveButton.setEnabled(false);

	}

	private boolean checkIfOppened(Path path) {
		for (Map.Entry<Integer, AreaPath> entry : editors.entrySet()) {
			AreaPath ap = entry.getValue();
			if (ap.getPath() == null || path == null)
				continue;
			if (ap.getPath().equals(path)) {
				return true;
			}
		}
		return false;
	}

	private boolean checkIsSaved(int index) {

		AreaPath areaPath = editors.get(index);
		String path;
		try {
			path = areaPath.getPath().toString();
		} catch (NullPointerException np) {
			path = NEW_TAB + (index + 1);
		}
		if (!areaPath.getSaved()) {
			int desiciosn = JOptionPane.showConfirmDialog(JNotepadPP.this, "Save file \"" + path + "\"", "Save",
					JOptionPane.YES_NO_CANCEL_OPTION);
			if (desiciosn == JOptionPane.YES_OPTION) {
				saveButton.doClick();
				return true;
			}
			if (desiciosn == JOptionPane.NO_OPTION) {
				return true;
			}
			if (desiciosn == JOptionPane.CANCEL_OPTION) {
				return false;
			}
		}
		return true;
	}

	private ImageIcon loadIcon(String path) {
		InputStream is = this.getClass().getResourceAsStream(path);
		if (is == null) {
			System.err.println("error");
		}
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		byte[] bytes = new byte[0xFFFF];
		try {
			for (int len; (len = is.read(bytes)) != -1;)
				os.write(bytes, 0, len);
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			os.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			is.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new ImageIcon(bytes);
	}

	private JLabel getTimeCount() {
		JLabel time = new JLabel();
		SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		clock = new Timer(100, (e) -> {
			time.setText(timeFormat.format(new Date()));
		});
		clock.start();
		return time;
	}

	private int getSelectedCount(JTextArea editor) {
		return Math.abs(editor.getCaret().getDot() - editor.getCaret().getMark());
	}

	private int getColumnCount(JTextArea editor) {
		int col = 1;
		try {
			int position = editor.getCaretPosition();
			int line = editor.getLineOfOffset(position);
			col = position - editor.getLineStartOffset(line);
		} catch (Exception e) {
		}
		return col + 1;
	}

	private int getLineCount(JTextArea editor) {
		int caretpos = editor.getCaretPosition();
		int ln = 1;
		try {
			ln = editor.getLineOfOffset(caretpos);
		} catch (BadLocationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ln + 1;
	}

	private void addClosingOperation() {
		addWindowListener(new WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent e) {
				checkAllSaves();

			}

		});

	}

	protected void checkAllSaves() {
		boolean disposeFlag = true;
		for (Map.Entry<Integer, AreaPath> entry : editors.entrySet()) {
			if (!entry.getValue().getSaved()) {
				tabbedPane.setSelectedIndex(entry.getKey());
				if (!checkIsSaved(entry.getKey())) {
					disposeFlag = false;
					break;
				}
			}
		}
		if (disposeFlag) {
			clock.stop();
			dispose();
		}

	}

	public class AreaPath {
		JTextArea area;
		Path path;
		String areaString;
		String pathString;
		Boolean saved;

		public AreaPath(JTextArea area, Path path) {
			super();
			this.area = area;
			this.path = path;
			this.saved = true;
		}

		public JTextArea getArea() {
			return area;
		}

		public Path getPath() {
			return path;
		}

		public void setPath(Path path) {
			this.path = path;
		}

		public void setArea(JTextArea area) {
			this.area = area;
		}

		public Boolean getSaved() {
			return saved;
		}

		public void setSaved(Boolean saved) {
			this.saved = saved;
		}

	}

	private class TabButton extends JButton implements ActionListener {

		private JPanel panelWhereIAmStored;

		public TabButton(JPanel panel) {
			setPreferredSize(new Dimension(17, 17));
			setToolTipText("Close");
			this.panelWhereIAmStored = panel;
			setUI(new BasicButtonUI());
			setBorderPainted(false);
			addActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			int index = tabbedPane.indexOfTabComponent(panelWhereIAmStored);
			if (checkIsSaved(index)) {
				if (index >= 0 && editors.size() > 1) {
					tabbedPane.remove(index);
					editors.remove(index);
					Iterator<Entry<Integer, AreaPath>> it = editors.entrySet().iterator();
					Map<Integer, AreaPath> valuesToChangeIndex = new HashMap<>();
					while (it.hasNext()) {
						int editorsIndex = it.next().getKey();
						if (editorsIndex > index) {
							AreaPath areaPathTemp = editors.get(editorsIndex);
							it.remove();
							valuesToChangeIndex.put(editorsIndex - 1, areaPathTemp);
						}
					}
					for (Map.Entry<Integer, AreaPath> entry : valuesToChangeIndex.entrySet()) {
						editors.put(entry.getKey(), entry.getValue());
					}
				} else if (editors.get(index).getPath() != null || !editors.get(index).getSaved()) {
					editors.remove(index);
					tabbedPane.remove(index);
					startingEditor = new JTextArea();
					addNewTab(NEW_TAB + "1", startingEditor);
				}
			}
		}

		// source :
		// http://docs.oracle.com
		// paint the cross
		@Override
		protected void paintComponent(Graphics g) {
			super.paintComponent(g);
			Graphics2D g2 = (Graphics2D) g.create();
			if (getModel().isPressed()) {
				g2.translate(1, 1);
			}
			g2.setStroke(new BasicStroke(3));
			g2.setColor(Color.RED);

			int delta = 5;
			g2.drawLine(delta, delta, getWidth() - delta - 1, getHeight() - delta - 1);
			g2.drawLine(getWidth() - delta - 1, delta, delta, getHeight() - delta - 1);
			g2.dispose();
		}
	};

	public class TabPanel extends JPanel {
		JLabel title;
		TabButton closingButton;
		JLabel iconLabel;

		public TabPanel() {

		}

		public TabPanel(JLabel title2) {
			super();
			this.title = title2;
			this.closingButton = new TabButton(TabPanel.this);
			this.iconLabel = new JLabel();
			setSaveIcon(ICON_SAVED_TAB);
			this.add(iconLabel);
			this.add(title);
			this.add(closingButton);
		}

		public void doClick() {
			closingButton.doClick();
		}

		public void setSaveIcon(String path) {
			iconLabel.setIcon(loadIcon(path));
		}

	}

	public Map<Integer, AreaPath> getEditors() {
		return editors;
	}

	public JTabbedPane getTabbedPane() {
		return tabbedPane;
	}

	public int getLastNewTabIndex() {
		return lastNewTabIndex;
	}

	public void setLastNewTabIndex(int lastNewTabIndex) {
		this.lastNewTabIndex = lastNewTabIndex;
	}

	public JTextArea getStartingEditor() {
		return startingEditor;
	}

	public JButton getSaveButton() {
		return saveButton;
	}

	public Path getUNSAVED_DOCUMENT() {
		return UNSAVED_DOCUMENT;
	}

	public int getSTARTING_AREA_INDEX() {
		return STARTING_AREA_INDEX;
	}

	public String getNEW_TAB() {
		return NEW_TAB;
	}

	public String getSAVE_AS_INDETIFICATOR() {
		return SAVE_AS_IDENTIFICATOR;
	}

	public String getSAVE_INDETIFICATOR() {
		return SAVE_IDENTIFICATOR;
	}

	public String getTITLE() {
		return TITLE;
	}

	public static void main(String[] args) {

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				new JNotepadPP("JNotepad++").setVisible(true);
			}
		});

	}

}
