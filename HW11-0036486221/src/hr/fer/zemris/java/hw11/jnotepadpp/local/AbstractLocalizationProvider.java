package hr.fer.zemris.java.hw11.jnotepadpp.local;

import java.util.LinkedList;
import java.util.List;

public abstract class AbstractLocalizationProvider implements ILocalizationProvider {
	private List<ILocalizationListener> list = new LinkedList<>();

	@Override
	public void addLocalizationListener(ILocalizationListener l) {
		if (!list.contains(l)) {
			list.add(l);
		}
	}

	@Override
	public void removeLocalizationListener(ILocalizationListener l) {
		if (!list.contains(l)) {
			list.remove(l);
		}

	}

	protected void fire() {
		for (ILocalizationListener l : list) {
			l.localizationChanged();
		}
	}

}
