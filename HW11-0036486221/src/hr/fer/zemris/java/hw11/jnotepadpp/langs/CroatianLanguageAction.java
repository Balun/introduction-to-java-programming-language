package hr.fer.zemris.java.hw11.jnotepadpp.langs;

import java.awt.event.ActionEvent;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.local.LocalizableAction;
import hr.fer.zemris.java.hw11.jnotepadpp.local.LocalizationProvider;

public class CroatianLanguageAction extends LocalizableAction {

	public CroatianLanguageAction(JNotepadPP notepad, ILocalizationProvider lzProvider) {
		super("croLangKey", "croLangDesc", lzProvider);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		LocalizationProvider.getInstance().setLanguage("hr");

	}

}
