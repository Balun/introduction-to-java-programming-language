package hr.fer.zemris.java.hw11.jnotepadpp;

import java.awt.event.ActionEvent;

import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.local.LocalizableAction;

public class ExitAction extends LocalizableAction {
	JNotepadPP notepad;

	public ExitAction(JNotepadPP notepad, ILocalizationProvider lzProvider) {
		super("exitKey", "exitDesc", lzProvider);
		this.notepad = notepad;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		notepad.checkAllSaves();

	}

}
