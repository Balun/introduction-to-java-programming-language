package hr.fer.zemris.java.hw11.jnotepadpp.actions;

import java.awt.event.ActionEvent;
import java.util.Map;
import java.util.function.Function;

import javax.swing.Action;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;
import hr.fer.zemris.java.hw11.jnotepadpp.tabEntry;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;

/**
 * This class represents the general toggle {@link Action} for the
 * {@link JNotepadPP} application. It extends the {@link AbstractNotepadAction}
 * class.
 * 
 * @author Matej Balun
 *
 */
public abstract class AbstractToggleAction extends AbstractNotepadAction {

	/**
	 * The specified text to toggle
	 */
	protected String text;

	/**
	 * Offset of the toggled part of the document
	 */
	protected int offset;

	/**
	 * Length of the toggled part of the document
	 */
	protected int len;

	/**
	 * The specified document
	 */
	protected Document doc;

	/**
	 * The generated serial version UID of this class.
	 */
	private static final long serialVersionUID = 1046215793685310548L;

	/**
	 * This constructor initialises this {@link Action} with its specified
	 * parameters.
	 * 
	 * @param tabMap
	 *            the {@link Map} containing the basic document attributes
	 * @param tabbedPane
	 *            {@link JTabbedPane} of the {@link JNotepadPP}
	 * @param notepad
	 *            the specified {@link JNotepadPP}
	 * @param key
	 *            the {@link Action}'s name key
	 * @param provider
	 *            the {@link ILocalizationProvider} for internationalisation
	 * @param desc
	 *            the {@link Action}'s short description key
	 */
	public AbstractToggleAction(Map<Integer, tabEntry> tabMap, JTabbedPane tabbedPane, JNotepadPP notepad, String key,
			ILocalizationProvider provider, String desc) {
		super(tabMap, tabbedPane, notepad, key, provider, desc);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		text = getSelectedText();

		if (text == null) {
			return;
		}

		text = changeCase(text);
		try {
			doc.remove(offset, len);
			doc.insertString(offset, text, null);
		} catch (BadLocationException e1) {
			e1.printStackTrace();
		}
	}

	/**
	 * Returns the selected text of the specified document
	 * 
	 * @return the selected text of the document
	 */
	private String getSelectedText() {
		JTextArea editor = tabMap.get(tabbedPane.getSelectedIndex()).getEditor();

		doc = editor.getDocument();
		len = Math.abs(editor.getCaret().getDot() - editor.getCaret().getMark());

		if (len == 0) {
			return null;
		}

		offset = 0;

		if (len != 0) {
			offset = Math.min(editor.getCaret().getDot(), editor.getCaret().getMark());
		} else {
			len = doc.getLength();
		}

		try {
			text = doc.getText(offset, len);
		} catch (BadLocationException e) {
			e.printStackTrace();
		}

		return text;
	}

	/**
	 * Changes the case of the specified text based on the specified toggle
	 * operation
	 * 
	 * @param text
	 *            the specified text
	 * @return the changed text
	 */
	private String changeCase(String text) {
		char[] znakovi = text.toCharArray();
		for (int i = 0; i < znakovi.length; i++) {
			char c = znakovi[i];

			znakovi[i] = getToggleFunction().apply(c);
		}

		return new String(znakovi);
	}

	/**
	 * Returns the {@link Function} for text toggle operation.
	 * 
	 * @return the text toggle operation {@link Function}
	 */
	protected abstract Function<Character, Character> getToggleFunction();
}
