package hr.fer.zemris.java.hw11.jnotepadpp;

import java.nio.file.Path;

import javax.swing.JTextArea;

/**
 * This class represents the simple data structure for keeping the basic
 * Document content of the psecified tab for the {@link JNotepadPP} application.
 * The class consists of {@link JTextArea} representing the body of the document
 * and its file path, which can be null if the file does not exist on the disc.
 * 
 * @author Matej Balun
 *
 */
public class tabEntry {

	/**
	 * The specified document view.
	 */
	private JTextArea editor;

	/**
	 * The document's file path
	 */
	private Path filePath;

	/**
	 * The indicator for unsaved changes
	 */
	private boolean changed;

	/**
	 * This constructor initialises the {@link tabEntry} with its file path and
	 * text view.
	 * 
	 * @param filePath
	 *            path of the file, can be null
	 * @param editor
	 *            {@link JTextArea} representing the view of the document.
	 */
	public tabEntry(Path filePath, JTextArea editor) {
		this.filePath = filePath;
		this.editor = editor;
		this.changed = false;
	}

	/**
	 * Returns the documents view.
	 * 
	 * @return {@link JTextArea} of the document.
	 */
	public JTextArea getEditor() {
		return this.editor;
	}

	/**
	 * Returns the document+s file path
	 * 
	 * @return the document's file path
	 */
	public Path getFilePath() {
		return this.filePath;
	}

	/**
	 * Sets the document's file path
	 * 
	 * @param filePath
	 *            the new file path
	 */
	public void setFilePath(Path filePath) {
		this.filePath = filePath;
	}

	/**
	 * Sets the content change indicator value
	 * 
	 * @param changeStatus
	 *            the the specified content change status.
	 */
	public void setChangedStatus(boolean changeStatus) {
		this.changed = changeStatus;
	}

	/**
	 * Returns the indicator of the content changes.
	 * 
	 * @return true if the contents has changed, otherwise false
	 */
	public boolean getChangedStatus() {
		return this.changed;
	}
}
