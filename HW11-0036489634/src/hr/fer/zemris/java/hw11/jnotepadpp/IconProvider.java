package hr.fer.zemris.java.hw11.jnotepadpp;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.swing.ImageIcon;

/**
 * This class represents the icon provider for the {@link JNotepadPP}
 * application. It generates the icons using the class resource stream with
 * icons in dedicated packages.
 * 
 * @author Matej Balun
 *
 */
public class IconProvider {

	/** new file icon path */
	public static final String NEW_ICON = "icons/new.png";

	/** open file icon path */
	public static final String OPEN_ICON = "icons/open.png";

	/** save as icon path */
	public static final String SAVE_AS_ICON = "icons/save as.png";

	/** save file icon path */
	public static final String SAVE_ICON = "icons/save.png";

	/** copy content icon path */
	public static final String COPY_ICON = "icons/copy.png";

	/** cut icon path */
	public static final String CUT_ICON = "icons/cut.png";

	/** paste icon path */
	public static final String PASTE_ICON = "icons/paste.png";

	/** statistics icon path */
	public static final String STAT_ICON = "icons/statistics.png";

	/** close file icon path */
	public static final String CLOSE_ICON = "icons/close.png";

	/** close all files icon path */
	public static final String CLOSE_ALL_ICON = "icons/close all.png";

	/** delete selected icon path */
	public static final String DELETE_ICON = "icons/delete.png";

	/** invert case icon path */
	public static final String TOGGLE_ICON = "icons/toggle.png";

	/** unsaved content icon path */
	public static final String UNSAVED_ICON = "icons/unsaved.png";

	/** saved content icon path */
	public static final String SAVED_ICON = "icons/saved.png";

	/** upper case icon path */
	public static final String TO_UPPER_ICON = "icons/to upper.png";

	/** lower case icon path */
	public static final String TO_LOWER_ICON = "icons/to lower.png";

	/** sort ascending icon path */
	public static final String ASCENDING_ACTION = "icons/ascending.png";

	/** sort descending icon path */
	public static final String DESCENDING_ORDER = "icons/descending.png";

	/** unique lines icon path */
	public static final String UNIQUE_ICON = "icons/unique.png";

	/**
	 * This method returns the icon with specified path available in
	 * {@link IconProvider} public constants. The method uses class resource
	 * stream for obtaining icon's binary content.
	 * 
	 * @param name
	 *            path of the icon, specified in {@link IconProvider}s constants
	 * @return The new instance of {@link ImageIcon} for {@link JNotepadPP}
	 */
	public ImageIcon getIcon(String name) {

		InputStream input = this.getClass().getResourceAsStream(name);

		if (input == null) {
			System.err.println("Error in loading icons");
			return null;
		}

		byte[] bytes = new byte[4096];
		try (InputStream is = new BufferedInputStream(input)) {
			is.read(bytes);
		} catch (IOException e1) {
			System.err.println("Unable to read icon..");
		} finally {
			try {
				input.close();
			} catch (IOException e) {
			}
		}

		return new ImageIcon(bytes);
	}

}
