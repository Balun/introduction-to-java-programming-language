package hr.fer.zemris.java.hw11.jnotepadpp;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.WindowConstants;
import javax.swing.event.CaretListener;

import hr.fer.zemris.java.hw11.jnotepadpp.actions.BlankDocumentAction;
import hr.fer.zemris.java.hw11.jnotepadpp.actions.CloseAction;
import hr.fer.zemris.java.hw11.jnotepadpp.actions.CloseAllAction;
import hr.fer.zemris.java.hw11.jnotepadpp.actions.CopyAction;
import hr.fer.zemris.java.hw11.jnotepadpp.actions.CutAction;
import hr.fer.zemris.java.hw11.jnotepadpp.actions.DeleteSelectedAction;
import hr.fer.zemris.java.hw11.jnotepadpp.actions.ExitAction;
import hr.fer.zemris.java.hw11.jnotepadpp.actions.OpenDocumentAction;
import hr.fer.zemris.java.hw11.jnotepadpp.actions.PasteAction;
import hr.fer.zemris.java.hw11.jnotepadpp.actions.SaveAction;
import hr.fer.zemris.java.hw11.jnotepadpp.actions.SaveAsAction;
import hr.fer.zemris.java.hw11.jnotepadpp.actions.SortAscendingAction;
import hr.fer.zemris.java.hw11.jnotepadpp.actions.SortDescendingAction;
import hr.fer.zemris.java.hw11.jnotepadpp.actions.StatisticsAction;
import hr.fer.zemris.java.hw11.jnotepadpp.actions.ToLowerCaseAction;
import hr.fer.zemris.java.hw11.jnotepadpp.actions.ToUpperCaseAction;
import hr.fer.zemris.java.hw11.jnotepadpp.actions.ToggleCaseAction;
import hr.fer.zemris.java.hw11.jnotepadpp.actions.UniqueAction;
import hr.fer.zemris.java.hw11.jnotepadpp.listeners.DocumentLengthListener;
import hr.fer.zemris.java.hw11.jnotepadpp.listeners.NotepadCaretListener;
import hr.fer.zemris.java.hw11.jnotepadpp.listeners.TabListener;
import hr.fer.zemris.java.hw11.jnotepadpp.local.FormLocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.local.LocalizationProvider;

/**
 * This program represents the simple text editor application in style of
 * Notepad++ editor. The editor supports multiple tabs, as well as opening,
 * saving, overwriting, sorting and general editing files. The editor is
 * available in three default languages; English, German and Croatian.
 * 
 * @author Matej Balun
 *
 */
public class JNotepadPP extends JFrame {

	/**
	 * The serial version UID fopr this class.
	 */
	private static final long serialVersionUID = -3653750640122725170L;

	/** The general {@link JTabbedPane} of the editor. */
	private JTabbedPane tabbedPane;

	/**
	 * The {@link Map} containing {@link tabEntry} objects with tab documents
	 * and file paths mapped by tab index.
	 */
	private Map<Integer, tabEntry> tabMap;

	/** index of untitled (new) documents. */
	private int untitledCount;

	/** {@link Action} for opening documents. */
	private Action openDocumentAction;

	/** {@link Action} for saving documents */
	private Action saveDocumentAction;

	/** {@link Action} for saving documents with specified name */
	private Action saveAsAction;

	/** {@link Action} for deleting selected part of the document */
	private Action deleteSelectedPartAction;

	/** {@link Action} for creating the new blank document */
	private Action newBlankDocumentAction;

	/** {@link Action} for inverting selected document part case */
	private Action toggleCaseAction;

	/** {@link Action} for exiting the application */
	private Action exitAction;

	/** {@link Action} for pasting */
	private Action pasteAction;

	/** {@link Action} */
	private Action cutAction;

	/** {@link Action} for copying */
	private Action copyAction;

	/** {@link Action} for displaying statistics */
	private Action statisticsAction;

	/** {@link Action} for closing documents */
	private Action closeAction;

	/** {@link Action} to close all documents */
	private Action closeAllAction;

	/** {@link Action} to select upper case */
	private Action toUpperAction;

	/** {@link Action} to select lower case */
	private Action toLowerAction;

	/** {@link Action} to sort document in ascending order */
	private Action sortAscending;

	/** {@link Action} to sort document in descending order */
	private Action sortDescending;

	/** {@link Action} for deleting equal lines */
	private Action uniqueAction;

	/** Status bar {@link JLabel} for document length */
	private JLabel length;

	/** Status bar {@link JLabel} for current line */
	private JLabel line;

	/** Status bar {@link JLabel} for current column */
	private JLabel col;

	/** Status bar {@link JLabel} for selected part of document count */
	private JLabel sel;

	/** The status bar of the current document */
	private JPanel statusBar;

	/** The current date */
	private Date date;

	/** The {@link Timer} for clock */
	private Timer timer;

	/** The {@link JMenu} with tools **/
	private JMenu toolsMenu;

	/** The constant for document font */
	public static final Font TEXT_FONT = new Font(Font.DIALOG, Font.PLAIN, 18);

	/** The constant for tab font */
	public static final Font TAB_FONT = new Font(Font.DIALOG, Font.PLAIN, 19);

	/** The {@link IconProvider} for generating icons */
	private final IconProvider iconProvider = new IconProvider();

	/** The {@link ILocalizationProvider} for dynamic internationalisation */
	private FormLocalizationProvider provider;

	/**
	 * This constructor initialises the {@link JNotepadPP}, setting its default
	 * size and position, as well as default closing operation and other frame
	 * properties.
	 */
	public JNotepadPP() {
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		setLocation(0, 0);
		setSize(1000, 700);
		getContentPane().setLayout(new BorderLayout());

		provider = new FormLocalizationProvider(LocalizationProvider.getInstance(), this);

		setTitle(provider.getString("UNTITLED") + " 1 - JNotepad++");

		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			// nothing
		}

		addWindowListener(new WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent e) {
				if (checkBeforeClosing()) {
					timer.stop();
					dispose();
				}
			}
		});

		tabMap = new HashMap<>();
		untitledCount = 0;

		initGUI();
	}

	/**
	 * This method adds the {@link JTabbedPane} an the first blank document to
	 * the editor, as well as {@link CaretListener} for changing the caret
	 * information.
	 */
	private void initGUI() {
		tabbedPane = new JTabbedPane();

		tabbedPane.setFont(TAB_FONT);

		JTextArea textArea = new JTextArea();
		textArea.setFont(TEXT_FONT);
		textArea.setDragEnabled(true);
		textArea.setLineWrap(true);
		textArea.addCaretListener(new NotepadCaretListener(this, textArea, tabbedPane, tabMap));

		textArea.getDocument().addDocumentListener(new TabListener(tabMap, tabbedPane));
		textArea.getDocument().addDocumentListener(new DocumentLengthListener(tabMap, tabbedPane, this));

		tabMap.put(tabbedPane.getTabCount(), new tabEntry(null, textArea));
		tabbedPane.add(provider.getString("UNTITLED") + " " + ++untitledCount, new JScrollPane(textArea));
		tabbedPane.setToolTipTextAt(tabbedPane.getTabCount() - 1, provider.getString("UNTITLED") + " " + untitledCount);
		tabbedPane.setIconAt(tabbedPane.getSelectedIndex(), iconProvider.getIcon(IconProvider.SAVED_ICON));

		tabbedPane.addChangeListener((e) -> {

			Path current = null;
			try {
				current = tabMap.get(tabbedPane.getSelectedIndex()).getFilePath();
			} catch (NullPointerException ex) {
				JNotepadPP.this.setTitle("JNotepad++");
				return;
			}

			length.setText(
					"length: " + tabMap.get(tabbedPane.getSelectedIndex()).getEditor().getDocument().getLength());

			if (current != null && Files.exists(current)) {
				JNotepadPP.this.setTitle(current.toString() + " - JNotepad++");
			} else {
				JNotepadPP.this.setTitle(tabbedPane.getTitleAt(tabbedPane.getSelectedIndex()) + " - JNotepad++");
			}
		});

		getContentPane().add(tabbedPane, BorderLayout.CENTER);

		initiateActions();
	}

	/**
	 * This method initialises all the specified {@link Action}s of
	 * {@link JNotepadPP} editor.
	 */
	private void initiateActions() {
		openDocumentAction = new OpenDocumentAction(tabMap, tabbedPane, this, "OPEN_FILE", provider, "OPEN_FILE_DESC");

		saveDocumentAction = new SaveAction(tabMap, tabbedPane, this, "SAVE_FILE", provider, "SAVE_FILE_DESC");

		saveAsAction = new SaveAsAction(tabMap, tabbedPane, this, "SAVE_AS", provider, "SAVE_AS_DESC");

		deleteSelectedPartAction = new DeleteSelectedAction(tabMap, tabbedPane, this, "DELETE", provider,
				"DELETE_DESC");

		newBlankDocumentAction = new BlankDocumentAction(tabMap, tabbedPane, this, untitledCount,
				provider.getString("UNTITLED"), "NEW_FILE", provider, "NEW_FILE_DESC");

		toggleCaseAction = new ToggleCaseAction(tabMap, tabbedPane, this, "INVERT_CASE", provider, "INVERT_CASE");

		exitAction = new ExitAction(tabMap, tabbedPane, this, "EXIT", provider, "EXIT_DESC");

		copyAction = new CopyAction(tabMap, tabbedPane, this, "COPY", provider, "COPY_DESC");

		pasteAction = new PasteAction(tabMap, tabbedPane, this, "PASTE", provider, "PASTE_DESC");

		cutAction = new CutAction(tabMap, tabbedPane, this, "CUT", provider, "CUT_DESC");

		statisticsAction = new StatisticsAction(tabMap, tabbedPane, this, "STATISTICS", provider, "STATISTICS_DESC");

		closeAction = new CloseAction(tabMap, tabbedPane, JNotepadPP.this, "CLOSE", provider, "CLOSE_DESC");

		closeAllAction = new CloseAllAction(tabMap, tabbedPane, this, "CLOSE_ALL", provider, "CLOSE_ALL_DESC");

		toUpperAction = new ToUpperCaseAction(tabMap, tabbedPane, this, "TO_UPPER_CASE", provider,
				"TO_UPPER_CASE_DESC");

		toLowerAction = new ToLowerCaseAction(tabMap, tabbedPane, this, "TO_LOWER_CASE", provider,
				"TO_LOWER_CASE_DESC");

		sortAscending = new SortAscendingAction(tabMap, tabbedPane, this, "SORT_ASCENDING", provider,
				"SORT_ASCENDING_DESC");

		sortDescending = new SortDescendingAction(tabMap, tabbedPane, this, "SORT_DESCENDING", provider,
				"SORT_DESCENDING_DESC");

		uniqueAction = new UniqueAction(tabMap, tabbedPane, this, "UNIQUE", provider, "UNIQUE_DESC");

		createActions();
		createMenus();
		createToolbars();
		addStatusBar();
	}

	/**
	 * Returns the {@link Action} for saving documents.
	 * 
	 * @return The {@link Action} for saving documents
	 */
	public Action getSaveAction() {
		return this.saveDocumentAction;
	}

	/**
	 * Returns the {@link Action} for closing documents.
	 * 
	 * @return The {@link Action} for closing documents
	 */
	public Action getCloseAction() {
		return this.closeAction;
	}

	/**
	 * Checks if there are any changes before closing the application and asks
	 * the user whether he would like to save files and close the application.
	 * 
	 * @return true if the application is good for closing, otherwise false.
	 */
	public boolean checkBeforeClosing() {
		for (int i = 0; i < tabbedPane.getTabCount(); i++) {

			tabbedPane.setSelectedIndex(i);

			if (tabMap.get(i).getChangedStatus()) {

				String name;
				if (tabMap.get(i).getFilePath() == null) {
					name = tabbedPane.getTitleAt(i);
				} else {
					name = tabMap.get(i).getFilePath().getFileName().toString();
				}

				int decision = JOptionPane.showConfirmDialog(JNotepadPP.this,
						provider.getString("UNSAVED_1") + " " + name + " " + provider.getString("UNSAVED_2"),
						provider.getString("UNSAVED_TITLE"), JOptionPane.YES_NO_CANCEL_OPTION);

				if (decision == JOptionPane.YES_OPTION) {
					saveDocumentAction.actionPerformed(null);

				} else if (decision == JOptionPane.CANCEL_OPTION) {
					return false;
				}
			}
		}

		return true;
	}

	/**
	 * Adds the status bar with all its components to the bottom of the editor.
	 */
	private void addStatusBar() {
		statusBar = new JPanel(new GridLayout(0, 5));
		statusBar.setBorder(BorderFactory.createMatteBorder(1, 0, 0, 0, Color.DARK_GRAY));

		length = new JLabel(provider.getString("LENGTH") + ": 0", SwingConstants.RIGHT);
		line = new JLabel(provider.getString("LINE") + ": 1", SwingConstants.RIGHT);
		col = new JLabel(provider.getString("COLUMN") + ": 1", SwingConstants.CENTER);
		sel = new JLabel(provider.getString("SELECTION") + ": 0", SwingConstants.LEFT);

		statusBar.add(length);
		statusBar.add(line);
		statusBar.add(col);
		statusBar.add(sel);

		statusBar.add(createClock());

		getContentPane().add(statusBar, BorderLayout.SOUTH);
	}

	/**
	 * Creates the clock with current date and time for the status bar.
	 * 
	 * @return the specified clock
	 */
	private Component createClock() {
		date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		JLabel dateTime = new JLabel(dateFormat.format(date));

		timer = new Timer(100, (e) -> {
			date = new Date();
			dateTime.setText(dateFormat.format(date));
		});
		timer.start();

		return dateTime;
	}

	/**
	 * Creates the tool bar with all specified buttons (with icons) with
	 * {@link Action}s of the {@link JNotepadPP} editor.
	 */
	private void createToolbars() {
		JToolBar toolBar = new JToolBar("Tools");
		toolBar.setFloatable(true);

		JButton newDoc = new JButton(newBlankDocumentAction);
		newDoc.setIcon(iconProvider.getIcon(IconProvider.NEW_ICON));
		newDoc.setToolTipText(provider.getString("NEW_FILE"));
		configureAndAddButton(newDoc, toolBar);

		JButton openDoc = new JButton(openDocumentAction);
		openDoc.setIcon(iconProvider.getIcon(IconProvider.OPEN_ICON));
		openDoc.setToolTipText(provider.getString("OPEN_FILE"));
		configureAndAddButton(openDoc, toolBar);

		JButton saveDoc = new JButton(saveDocumentAction);
		saveDoc.setToolTipText(provider.getString("SAVE_FILE"));
		saveDoc.setIcon(iconProvider.getIcon(IconProvider.SAVE_ICON));
		configureAndAddButton(saveDoc, toolBar);

		JButton saveAs = new JButton(saveAsAction);
		saveAs.setToolTipText(provider.getString("SAVE_AS"));
		saveAs.setIcon(iconProvider.getIcon(IconProvider.SAVE_AS_ICON));
		configureAndAddButton(saveAs, toolBar);

		toolBar.addSeparator();

		JButton close = new JButton(closeAction);
		close.setToolTipText(provider.getString("CLOSE"));
		close.setIcon(iconProvider.getIcon(IconProvider.CLOSE_ICON));
		configureAndAddButton(close, toolBar);

		JButton closeAll = new JButton(closeAllAction);
		closeAll.setToolTipText(provider.getString("CLOSE_ALL"));
		closeAll.setIcon(iconProvider.getIcon(IconProvider.CLOSE_ALL_ICON));
		configureAndAddButton(closeAll, toolBar);

		toolBar.addSeparator();

		JButton copy = new JButton(copyAction);
		copy.setIcon(iconProvider.getIcon(IconProvider.COPY_ICON));
		copy.setToolTipText(provider.getString("COPY"));
		configureAndAddButton(copy, toolBar);

		JButton cut = new JButton(cutAction);
		cut.setIcon(iconProvider.getIcon(IconProvider.CUT_ICON));
		cut.setToolTipText(provider.getString("CUT"));
		configureAndAddButton(cut, toolBar);

		JButton paste = new JButton(pasteAction);
		paste.setIcon(iconProvider.getIcon(IconProvider.PASTE_ICON));
		paste.setToolTipText(provider.getString("PASTE"));
		configureAndAddButton(paste, toolBar);

		toolBar.addSeparator();

		JButton stat = new JButton(statisticsAction);
		stat.setIcon(iconProvider.getIcon(IconProvider.STAT_ICON));
		stat.setToolTipText(provider.getString("STATISTICS"));
		configureAndAddButton(stat, toolBar);

		toolBar.addSeparator();

		JButton delete = new JButton(deleteSelectedPartAction);
		delete.setIcon(iconProvider.getIcon(IconProvider.DELETE_ICON));
		delete.setToolTipText(provider.getString("DELETE"));
		configureAndAddButton(delete, toolBar);

		JButton toggle = new JButton(toggleCaseAction);
		toggle.setIcon(iconProvider.getIcon(IconProvider.TOGGLE_ICON));
		toggle.setToolTipText(provider.getString("INVERT_CASE"));
		configureAndAddButton(toggle, toolBar);

		JButton toUpper = new JButton(toUpperAction);
		toUpper.setIcon(iconProvider.getIcon(IconProvider.TO_UPPER_ICON));
		toUpper.setToolTipText(provider.getString("TO_UPPER_CASE"));
		configureAndAddButton(toUpper, toolBar);

		JButton toLower = new JButton(toLowerAction);
		toLower.setIcon(iconProvider.getIcon(IconProvider.TO_LOWER_ICON));
		toLower.setToolTipText(provider.getString("TO_LOWER_CASE"));
		configureAndAddButton(toLower, toolBar);

		JButton ascending = new JButton(sortAscending);
		ascending.setIcon(iconProvider.getIcon(IconProvider.ASCENDING_ACTION));
		ascending.setToolTipText(provider.getString("SORT_ASCENDING"));
		configureAndAddButton(ascending, toolBar);

		JButton descending = new JButton(sortDescending);
		descending.setIcon(iconProvider.getIcon(IconProvider.DESCENDING_ORDER));
		descending.setToolTipText(provider.getString("SORT_DESCENDING"));
		configureAndAddButton(descending, toolBar);

		JButton unique = new JButton(uniqueAction);
		unique.setIcon(iconProvider.getIcon(IconProvider.UNIQUE_ICON));
		unique.setToolTipText(provider.getString("UNIQUE"));
		configureAndAddButton(unique, toolBar);

		this.getContentPane().add(toolBar, BorderLayout.PAGE_START);

		provider.addLocalizationListener(() -> {
			newDoc.setToolTipText(provider.getString("NEW_FILE"));
			openDoc.setToolTipText(provider.getString("OPEN_FILE"));
			saveDoc.setToolTipText(provider.getString("SAVE_FILE"));
			saveAs.setToolTipText(provider.getString("SAVE_AS"));
			close.setToolTipText(provider.getString("CLOSE"));
			closeAll.setToolTipText(provider.getString("CLOSE_ALL"));
			copy.setToolTipText(provider.getString("COPY"));
			cut.setToolTipText(provider.getString("CUT"));
			paste.setToolTipText(provider.getString("PASTE"));
			stat.setToolTipText(provider.getString("STATISTICS"));
			delete.setToolTipText(provider.getString("DELETE"));
			toggle.setToolTipText(provider.getString("INVERT_CASE"));
			toUpper.setToolTipText(provider.getString("TO_UPPER_CASE"));
			toLower.setToolTipText(provider.getString("TO_LOWER_CASE"));
			ascending.setToolTipText(provider.getString("SORT_ASCENDING"));
			descending.setToolTipText(provider.getString("SORT_DESCENDING"));
			unique.setToolTipText(provider.getString("UNIQUE"));
			for (Component c : toolBar.getComponents()) {
				if (c instanceof JButton) {
					JButton button = (JButton) c;
					button.setText("");
				}
			}
		});
	}

	/**
	 * Creates all the menus for the {@link JNotepadPP} text editor.
	 */
	private void createMenus() {
		JMenuBar menuBar = new JMenuBar();
		JMenu fileMenu = new JMenu(provider.getString("MENU_FILE"));
		menuBar.add(fileMenu);

		fileMenu.add(new JMenuItem(openDocumentAction));
		fileMenu.add(new JMenuItem(saveDocumentAction));
		fileMenu.add(new JMenuItem(saveAsAction));
		fileMenu.add(new JMenuItem(newBlankDocumentAction));
		fileMenu.add(new JMenuItem(closeAction));
		fileMenu.add(new JMenuItem(closeAllAction));

		fileMenu.addSeparator();
		fileMenu.add(new JMenuItem(statisticsAction));

		fileMenu.addSeparator();
		fileMenu.add(new JMenuItem(exitAction));

		JMenu editMenu = new JMenu(provider.getString("MENU_EDIT"));
		menuBar.add(editMenu);

		editMenu.add(new JMenuItem(deleteSelectedPartAction));
		editMenu.add(new JMenuItem(toggleCaseAction));
		editMenu.addSeparator();
		editMenu.add(new JMenuItem(copyAction));
		editMenu.add(new JMenuItem(cutAction));
		editMenu.add(new JMenuItem(pasteAction));

		toolsMenu = new JMenu(provider.getString("MENU_TOOLS"));

		JMenu changeCase = new JMenu(provider.getString("MENU_CHANGE_CASE"));
		toolsMenu.add(changeCase);
		menuBar.add(toolsMenu);

		changeCase.add(new JMenuItem(toUpperAction));
		changeCase.add(new JMenuItem(toLowerAction));
		changeCase.add(new JMenuItem(toggleCaseAction));

		JMenu sortMenu = new JMenu(provider.getString("MENU_SORT"));
		toolsMenu.add(sortMenu);

		sortMenu.add(new JMenuItem(sortAscending));
		sortMenu.add(new JMenuItem(sortDescending));

		toolsMenu.add(new JMenuItem(uniqueAction));

		JMenu langMenu = new JMenu(provider.getString("MENU_LANGUAGES"));

		JMenuItem hr = new JMenuItem(provider.getString("LANG_HR"));
		hr.addActionListener((e) -> LocalizationProvider.getInstance().setLanguage("hr"));
		langMenu.add(hr);

		JMenuItem en = new JMenuItem(provider.getString("LANG_EN"));
		en.addActionListener((e) -> LocalizationProvider.getInstance().setLanguage("en"));
		langMenu.add(en);

		JMenuItem de = new JMenuItem(provider.getString("LANG_DE"));
		de.addActionListener((e) -> LocalizationProvider.getInstance().setLanguage("de"));
		langMenu.add(de);

		menuBar.add(langMenu);

		this.setJMenuBar(menuBar);

		provider.addLocalizationListener(() -> {
			fileMenu.setText(provider.getString("MENU_FILE"));
			editMenu.setText(provider.getString("MENU_EDIT"));
			toolsMenu.setText(provider.getString("MENU_TOOLS"));
			langMenu.setText(provider.getString("MENU_LANGUAGES"));
			hr.setText(provider.getString("LANG_HR"));
			en.setText(provider.getString("LANG_EN"));
			de.setText(provider.getString("LANG_DE"));
			changeCase.setText(provider.getString("MENU_CHANGE_CASE"));
			sortMenu.setText(provider.getString("MENU_SORT"));
		});
	}

	/**
	 * Modifies the specified button for editor.
	 * 
	 * @param button
	 *            the specified button
	 * @param toolBar
	 *            the specified toolbar
	 */
	private void configureAndAddButton(JButton button, JToolBar toolBar) {
		button.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
		toolBar.add(button);
		button.setText("");
	}

	/**
	 * This method creates the actions with their specified names, hot keys,
	 * mnemonic keys and short descriptions.
	 */
	private void createActions() {
		openDocumentAction.putValue(Action.NAME, provider.getString("OPEN_FILE"));
		openDocumentAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control O"));
		openDocumentAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_O);
		openDocumentAction.putValue(Action.SHORT_DESCRIPTION, provider.getString("OPEN_FILE_DESC"));

		saveDocumentAction.putValue(Action.NAME, provider.getString("SAVE_FILE"));
		saveDocumentAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control S"));
		saveDocumentAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_S);
		saveDocumentAction.putValue(Action.SHORT_DESCRIPTION, provider.getString("SAVE_FILE_DESC"));

		deleteSelectedPartAction.putValue(Action.NAME, provider.getString("DELETE"));
		deleteSelectedPartAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control F2"));
		deleteSelectedPartAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_D);
		deleteSelectedPartAction.putValue(Action.SHORT_DESCRIPTION, provider.getString("DELETE_DESC"));

		toggleCaseAction.putValue(Action.NAME, provider.getString("INVERT_CASE"));
		toggleCaseAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control F3"));
		toggleCaseAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_T);
		toggleCaseAction.putValue(Action.SHORT_DESCRIPTION, provider.getString("INVERT_CASE_DESC"));

		exitAction.putValue(Action.NAME, provider.getString("EXIT"));
		exitAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("alt F4"));
		exitAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_F4);
		exitAction.putValue(Action.SHORT_DESCRIPTION, provider.getString("EXIT_DESC"));

		newBlankDocumentAction.putValue(Action.NAME, provider.getString("NEW_FILE"));
		newBlankDocumentAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control N"));
		newBlankDocumentAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_N);
		newBlankDocumentAction.putValue(Action.SHORT_DESCRIPTION, provider.getString("NEW_FILE_DESC"));

		saveAsAction.putValue(Action.NAME, provider.getString("SAVE_AS"));
		saveAsAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control alt S"));
		saveAsAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_A);
		saveAsAction.putValue(Action.SHORT_DESCRIPTION, provider.getString("SAVE_AS_DESC"));

		copyAction.putValue(Action.NAME, provider.getString("COPY"));
		copyAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control C"));
		copyAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_C);
		copyAction.putValue(Action.SHORT_DESCRIPTION, provider.getString("COPY_DESC"));

		cutAction.putValue(Action.NAME, provider.getString("OPEN_FILE"));
		cutAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control X"));
		cutAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_X);
		cutAction.putValue(Action.SHORT_DESCRIPTION, provider.getString("OPEN_FILE_DESC"));

		pasteAction.putValue(Action.NAME, provider.getString("PASTE"));
		pasteAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control V"));
		pasteAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_V);
		pasteAction.putValue(Action.SHORT_DESCRIPTION, provider.getString("PASTE_DESC"));

		statisticsAction.putValue(Action.NAME, provider.getString("STATISTICS"));
		statisticsAction.putValue(Action.SHORT_DESCRIPTION, provider.getString("STATISTICS_DESC"));

		closeAction.putValue(Action.NAME, provider.getString("CLOSE"));
		closeAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control W"));
		closeAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_W);
		closeAction.putValue(Action.SHORT_DESCRIPTION, provider.getString("CLOSE_DESC"));

		closeAllAction.putValue(Action.NAME, provider.getString("CLOSE_ALL"));
		closeAllAction.putValue(Action.SHORT_DESCRIPTION, provider.getString("CLOSE_ALL_DESC"));

		toUpperAction.putValue(Action.NAME, provider.getString("TO_UPPER_CASE"));
		toUpperAction.putValue(Action.SHORT_DESCRIPTION, provider.getString("TO_UPPER_CASE_DESC"));

		toLowerAction.putValue(Action.NAME, provider.getString("TO_LOWER_CASE"));
		toUpperAction.putValue(Action.SHORT_DESCRIPTION, provider.getString("TO_LOWER_CASE_DESC"));

		sortAscending.putValue(Action.NAME, provider.getString("SORT_ASCENDING"));
		sortAscending.putValue(Action.SHORT_DESCRIPTION, provider.getString("SORT_ASCENDING_DESC"));

		sortDescending.putValue(Action.NAME, provider.getString("SORT_DESCENDING"));
		sortDescending.putValue(Action.SHORT_DESCRIPTION, provider.getString("SORT_DESCENDING_DESC"));

		uniqueAction.putValue(Action.NAME, provider.getString("UNIQUE"));
		uniqueAction.putValue(Action.SHORT_DESCRIPTION, provider.getString("UNIQUE_DESC"));
	}

	/**
	 * Returns the {@link JLabel} containing document length information.
	 * 
	 * @return The length {@link JLabel}
	 */
	public JLabel getLength() {
		return length;
	}

	/**
	 * Returns the {@link JLabel} containing the current line index
	 * 
	 * @return The line {@link JLabel}
	 */
	public JLabel getLine() {
		return line;
	}

	/**
	 * Returns the {@link JLabel} containing the current column index.
	 * 
	 * @return The column {@link JLabel}
	 */
	public JLabel getCol() {
		return col;
	}

	/**
	 * Returns the {@link JLabel} containing the selection count.
	 * 
	 * @return The selection {@link JLabel}
	 */
	public JLabel getSel() {
		return sel;
	}

	/**
	 * Returns the editor's tool menu.
	 * 
	 * @return the tool menu
	 */
	public JMenu getToolsMenu() {
		return toolsMenu;
	}

	/**
	 * Returns the specified {@link IconProvider} object
	 * 
	 * @return the {@link IconProvider} object
	 */
	public IconProvider getIconProvider() {
		return iconProvider;
	}

	/**
	 * Returns the specified {@link FormLocalizationProvider} for
	 * internationalisation.
	 * 
	 * @return the {@link FormLocalizationProvider} fo internationalisation
	 */
	public FormLocalizationProvider getProvider() {
		return this.provider;
	}

	/**
	 * The entry point of the {@link JNotepadPP} application.
	 * 
	 * @param args
	 *            command-line arguments are not used.
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> new JNotepadPP().setVisible(true));
	}
}
