package hr.fer.zemris.java.hw11.jnotepadpp.local;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;

/**
 * This class represents the bridge between the {@link FormLocalizationProvider}
 * and the singleton object {@link LocalizationProvider} in the
 * {@link JNotepadPP} application's localisation implementation.
 * 
 * @author Matej Balun
 *
 */
public class LocalizationProviderBridge extends AbstractLocalizationProvider {

	/**
	 * Indicator for connection
	 */
	private boolean connected;

	/**
	 * The specified {@link ILocalizationListener}
	 */
	private ILocalizationListener listener;

	/**
	 * The specified {@link ILocalizationProvider}
	 */
	private ILocalizationProvider provider;

	/**
	 * Initialises the {@link LocalizationProviderBridge} with its specified
	 * {@link ILocalizationProvider}
	 * 
	 * @param provider
	 *            the specified {@link ILocalizationProvider}
	 */
	public LocalizationProviderBridge(ILocalizationProvider provider) {
		this.provider = provider;
		connected = false;

	}

	/**
	 * Connects the {@link FormLocalizationProvider} to the registered
	 * listeners, firing the localisation changed notification.
	 */
	public void connect() {
		if (!connected) {
			connected = true;
			listener = new ILocalizationListener() {

				@Override
				public void localizationChanged() {
					fire();
				}
			};

			provider.addLocalizationListener(listener);
		}
	}

	/**
	 * Disconnects the specified {@link ILocalizationProvider}
	 */
	public void disconnect() {
		if (connected) {
			connected = false;
			provider.removeLocalizationListener(listener);
		}
	}

	@Override
	public String getString(String key) {
		return provider.getString(key);
	}

}
