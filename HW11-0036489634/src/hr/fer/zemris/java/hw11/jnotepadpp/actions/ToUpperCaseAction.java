package hr.fer.zemris.java.hw11.jnotepadpp.actions;

import java.util.Map;
import java.util.function.Function;

import javax.swing.Action;
import javax.swing.JTabbedPane;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;
import hr.fer.zemris.java.hw11.jnotepadpp.tabEntry;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;

/**
 * This class represents the to upper case class for the {@link JNotepadPP}
 * {@link Action} for the {@link JNotepadPP} application. It changes the case of
 * the selected text to upper case. It extends the {@link AbstractToggleAction}
 * class.
 * 
 * @author Matej Balun
 *
 */
public class ToUpperCaseAction extends AbstractToggleAction {

	/**
	 * The serial version UID for this class.
	 */
	private static final long serialVersionUID = 1046215793685310548L;

	/**
	 * This constructor initialises this {@link Action} with its specified
	 * parameters.
	 * 
	 * @param tabMap
	 *            the {@link Map} containing the basic document attributes
	 * @param tabbedPane
	 *            {@link JTabbedPane} of the {@link JNotepadPP}
	 * @param notepad
	 *            the specified {@link JNotepadPP}
	 * @param key
	 *            the {@link Action}'s name key
	 * @param provider
	 *            the {@link ILocalizationProvider} for internationalisation
	 * @param desc
	 *            the {@link Action}'s short description key
	 */

	public ToUpperCaseAction(Map<Integer, tabEntry> tabMap, JTabbedPane tabbedPane, JNotepadPP notepad, String key,
			ILocalizationProvider provider, String desc) {
		super(tabMap, tabbedPane, notepad, key, provider, desc);

	}

	@Override
	protected Function<Character, Character> getToggleFunction() {
		return (c) -> Character.toUpperCase(c);
	}

}
