package hr.fer.zemris.java.hw11.jnotepadpp.local;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;

/**
 * This interface represents the general localisation listener for the
 * {@link JNotepadPP} application. It offers the internationalisation operations
 * for dynamic localisation of the application. The user should register
 * implementations of this interface to the {@link LocalizationProvider}
 * objects.
 * 
 * @author Matej Balun
 *
 */
public interface ILocalizationListener {

	/**
	 * Executes the specified action when the localisation changes
	 */
	public void localizationChanged();
}
