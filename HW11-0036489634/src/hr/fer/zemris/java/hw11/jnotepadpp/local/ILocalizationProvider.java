package hr.fer.zemris.java.hw11.jnotepadpp.local;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;

/**
 * This interface represents the general localisation provider for the
 * {@link JNotepadPP} application. It offers the internationalisation operations
 * for dynamic localisation of the application. The listeners should register to
 * implementations of this class to receive localisation change notifications
 * 
 * @author Matej Balun
 *
 */
public interface ILocalizationProvider {

	/**
	 * Adds the {@link ILocalizationListener} to the listeners collection
	 * 
	 * @param listener
	 *            the {@link ILocalizationListener} to register
	 */
	public void addLocalizationListener(ILocalizationListener listener);

	/**
	 * Removes the {@link ILocalizationListener} from the listeners collection
	 * 
	 * @param listener
	 *            the {@link ILocalizationListener} to unregister.
	 */
	public void removeLocalizationListener(ILocalizationListener listener);

	/**
	 * Returns the localised {@link String} based on the specified key
	 * 
	 * @param key
	 *            the specified key of localisation
	 * @return the specified localised String
	 */
	public String getString(String key);

}
