package hr.fer.zemris.java.hw11.jnotepadpp.actions;

import java.util.Map;
import java.util.function.Function;

import javax.swing.Action;
import javax.swing.JTabbedPane;

import hr.fer.zemris.java.hw11.jnotepadpp.tabEntry;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;

/**
 * This class represents the invert case {@link Action} for {@link JNotepadPP}
 * application. It inverts the case of the selected text. It extends the
 * {@link AbstractToggleAction} class.
 * 
 * @author Matej Balun
 *
 */
public class ToggleCaseAction extends AbstractToggleAction {

	/**
	 * The serial version UID for this class.
	 */
	private static final long serialVersionUID = 1046215793685310548L;

	/**
	 * This constructor initialises this {@link Action} with its specified
	 * parameters.
	 * 
	 * @param tabMap
	 *            the {@link Map} containing the basic document attributes
	 * @param tabbedPane
	 *            {@link JTabbedPane} of the {@link JNotepadPP}
	 * @param notepad
	 *            the specified {@link JNotepadPP}
	 * @param key
	 *            the {@link Action}'s name key
	 * @param provider
	 *            the {@link ILocalizationProvider} for internationalisation
	 * @param desc
	 *            the {@link Action}'s short description key
	 */
	public ToggleCaseAction(Map<Integer, tabEntry> tabMap, JTabbedPane tabbedPane, JNotepadPP notepad, String key,
			ILocalizationProvider provider, String desc) {
		super(tabMap, tabbedPane, notepad, key, provider, desc);
	}

	@Override
	protected Function<Character, Character> getToggleFunction() {
		return (c) -> {
			if (Character.isUpperCase(c)) {
				return Character.toLowerCase(c);
			} else {
				return Character.toUpperCase(c);
			}
		};
	}

}
