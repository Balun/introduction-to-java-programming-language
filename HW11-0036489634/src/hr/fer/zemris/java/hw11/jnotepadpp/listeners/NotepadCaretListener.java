package hr.fer.zemris.java.hw11.jnotepadpp.listeners;

import java.util.Map;

import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.text.BadLocationException;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;
import hr.fer.zemris.java.hw11.jnotepadpp.tabEntry;

/**
 * This class represents the caret listener for the {@link JNotepadPP}
 * application. It updates the status bar information of the currently selected
 * document. It implements the {@link CaretListener} interface.
 * 
 * @author Matej Balun
 *
 */
public class NotepadCaretListener implements CaretListener {

	/**
	 * The specified {@link JNotepadPP}
	 */
	private JNotepadPP notepad;

	/**
	 * The specifed document
	 */
	private JTextArea editor;

	/**
	 * {@link JTabbedPane} of the {@link JNotepadPP}
	 */
	private JTabbedPane tabbedPane;

	/**
	 * Map containing document-tab attributes.
	 */
	private Map<Integer, tabEntry> tabMap;

	/**
	 * This constructor initialises the {@link NotepadCaretListener} with its
	 * specified attributes.
	 * 
	 * @param notepad
	 *            the specified {@link JNotepadPP}
	 * @param editor
	 *            the specified document
	 * @param tabbedPane
	 *            {@link JTabbedPane} of the {@link JNotepadPP}
	 * @param tabMap
	 *            tab {@link Map} with tab-document attributes
	 */
	public NotepadCaretListener(JNotepadPP notepad, JTextArea editor, JTabbedPane tabbedPane,
			Map<Integer, tabEntry> tabMap) {
		if (notepad == null) {
			throw new IllegalArgumentException("The specified notepad cannot be a null-reference.");

		} else if (editor == null) {
			throw new IllegalArgumentException("The specified text area cannot be a null-reference.");

		} else if (tabbedPane == null) {
			throw new IllegalArgumentException("The specified tabbed pane cannot be a null-reference.");

		} else if (tabMap == null) {
			throw new IllegalArgumentException("The specified map for tabs cannot be a null reference.");
		}

		this.notepad = notepad;
		this.editor = editor;
		this.tabbedPane = tabbedPane;
		this.tabMap = tabMap;
	}

	@Override
	public void caretUpdate(CaretEvent e) {
		int dot = e.getDot();
		int mark = e.getMark();
		if (dot == mark) {
			notepad.getToolsMenu().setEnabled(false);
		} else {
			notepad.getToolsMenu().setEnabled(true);
		}

		int end = 0;
		int start = 0;

		start = tabMap.get(tabbedPane.getSelectedIndex()).getEditor().getCaret().getDot();
		end = tabMap.get(tabbedPane.getSelectedIndex()).getEditor().getCaret().getMark();

		int length = Math.abs(end - start);
		notepad.getSel().setText(notepad.getProvider().getString("SELECTION") + ": " + Integer.valueOf(length));

		int caretpos = editor.getCaretPosition();
		try {
			int linenum = editor.getLineOfOffset(caretpos);
			int columnnum = caretpos - editor.getLineStartOffset(linenum);
			notepad.getLine().setText(notepad.getProvider().getString("LINE") + ": " + linenum);
			notepad.getCol().setText(notepad.getProvider().getString("COLUMN") + ": " + columnnum);
		} catch (BadLocationException e1) {
			e1.printStackTrace();
		}
	}
}
