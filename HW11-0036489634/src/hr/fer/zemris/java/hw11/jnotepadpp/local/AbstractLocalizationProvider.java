package hr.fer.zemris.java.hw11.jnotepadpp.local;

import java.util.ArrayList;
import java.util.List;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;

/**
 * This class represents the general localisation provider for the
 * {@link JNotepadPP} application. It implements the
 * {@link ILocalizationProvider} interface along with method fire.
 * 
 * @author Matej Balun
 *
 */
public abstract class AbstractLocalizationProvider implements ILocalizationProvider {

	/**
	 * Collection of registered listeners
	 */
	private List<ILocalizationListener> listeners;

	/**
	 * Initialises the {@link AbstractLocalizationProvider} with its listener
	 * collection.
	 */
	public AbstractLocalizationProvider() {
		this.listeners = new ArrayList<>();
	}

	@Override
	public void addLocalizationListener(ILocalizationListener listener) {
		listeners = new ArrayList<>(listeners);
		listeners.add(listener);
	}

	@Override
	public void removeLocalizationListener(ILocalizationListener listener) {
		listeners = new ArrayList<>(listeners);
		listeners.remove(listener);
	}

	/**
	 * This method notifies all registered listeners when the localisation
	 * changes.
	 */
	public void fire() {
		listeners.forEach(l -> l.localizationChanged());
	}
}
