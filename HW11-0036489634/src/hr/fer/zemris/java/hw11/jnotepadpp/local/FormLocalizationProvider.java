package hr.fer.zemris.java.hw11.jnotepadpp.local;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;

/**
 * This class represents the form localisation listener for the
 * {@link JNotepadPP} application. It extends the
 * {@link LocalizationProviderBridge} connecting itself to the
 * {@link LocalizationProvider} object.
 * 
 * @author Matej Balun
 *
 */
public class FormLocalizationProvider extends LocalizationProviderBridge {

	/**
	 * Adds the window listener to the specified {@link JFrame} connecting
	 * itself to the specified {@link ILocalizationProvider}.
	 * 
	 * @param provider
	 *            the specified {@link ILocalizationProvider}
	 * @param frame
	 *            the specified {@link JFrame}
	 */
	public FormLocalizationProvider(ILocalizationProvider provider, JFrame frame) {
		super(provider);

		frame.addWindowListener(new WindowListener() {

			@Override
			public void windowOpened(WindowEvent e) {
				connect();
			}

			@Override
			public void windowIconified(WindowEvent e) {
			}

			@Override
			public void windowDeiconified(WindowEvent e) {
			}

			@Override
			public void windowDeactivated(WindowEvent e) {
			}

			@Override
			public void windowClosing(WindowEvent e) {
			}

			@Override
			public void windowClosed(WindowEvent e) {
				disconnect();
			}

			@Override
			public void windowActivated(WindowEvent e) {
			}
		});
	}

}
