package hr.fer.zemris.java.hw11.jnotepadpp.actions;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.Action;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;
import hr.fer.zemris.java.hw11.jnotepadpp.tabEntry;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;

/**
 * This class represents the close action for the {@link JNotepadPP}
 * application. It closes the currently selected document. It extends the
 * {@link AbstractNotepadAction} class.
 * 
 * @author Matej Balun
 *
 */
public class CloseAction extends AbstractNotepadAction {

	/**
	 * The serial version UID for this class.
	 */
	private static final long serialVersionUID = 1046215793685310548L;

	/**
	 * This constructor initialises this {@link Action} with its specified
	 * parameters.
	 * 
	 * @param tabMap
	 *            the {@link Map} containing the basic document attributes
	 * @param tabbedPane
	 *            {@link JTabbedPane} of the {@link JNotepadPP}
	 * @param notepad
	 *            the specified {@link JNotepadPP}
	 * @param key
	 *            the {@link Action}'s name key
	 * @param provider
	 *            the {@link ILocalizationProvider} for internationalisation
	 * @param desc
	 *            the {@link Action}'s short description key
	 */
	public CloseAction(Map<Integer, tabEntry> tabMap, JTabbedPane tabbedPane, JNotepadPP notepad, String key,
			ILocalizationProvider provider, String desc) {
		super(tabMap, tabbedPane, notepad, key, provider, desc);
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		int result = JOptionPane.YES_OPTION;
		tabEntry entry = tabMap.get(tabbedPane.getSelectedIndex());

		if (!entry.getChangedStatus()) {
			close();
			return;
		}

		if (entry.getFilePath() != null) {

			result = JOptionPane.showConfirmDialog(notepad,
					notepad.getProvider().getString("SAVE?") + " " + entry.getFilePath().toString() + " ?",
					notepad.getProvider().getString("CLOSE_DIALOG"), JOptionPane.YES_NO_CANCEL_OPTION);

		} else {
			result = JOptionPane.showConfirmDialog(notepad,
					notepad.getProvider().getString("SAVE?") + " "
							+ tabbedPane.getTitleAt(tabbedPane.getSelectedIndex()) + " ?",
					notepad.getProvider().getString("CLOSE_DIALOG"), JOptionPane.YES_NO_CANCEL_OPTION);
		}

		if (result == JOptionPane.YES_OPTION) {
			notepad.getSaveAction().actionPerformed(e);
			close();
		} else if (result == JOptionPane.NO_OPTION) {
			close();
		} else {
			return;
		}
	}

	/**
	 * This method closes the currently selected tab in the editor.
	 */
	private void close() {
		tabMap.remove(tabbedPane.getSelectedIndex());
		tabbedPane.remove(tabbedPane.getSelectedIndex());
		int i = 0;

		List<tabEntry> list = new ArrayList<>();
		for (tabEntry entry : tabMap.values()) {
			list.add(entry);
		}

		tabMap.clear();

		for (tabEntry entry : list) {
			tabMap.put(i, entry);
			i++;
			if (i == tabbedPane.getTabCount()) {
				break;
			}
		}
	}

}
