package hr.fer.zemris.java.hw11.jnotepadpp.actions;

import java.awt.event.ActionEvent;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import javax.swing.Action;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;
import hr.fer.zemris.java.hw11.jnotepadpp.tabEntry;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;

/**
 * This class represents the unique lines {@link Action} for the
 * {@link JNotepadPP} application. It deletes the duplicate lines from selected
 * part of the document. It extends the {@link AbstractNotepadAction} class.
 * 
 * @author Matej Balun
 *
 */
public class UniqueAction extends AbstractNotepadAction {

	/**
	 * The serial version UID for this class.
	 */
	private static final long serialVersionUID = 1046215793685310548L;

	/**
	 * This constructor initialises this {@link Action} with its specified
	 * parameters.
	 * 
	 * @param tabMap
	 *            the {@link Map} containing the basic document attributes
	 * @param tabbedPane
	 *            {@link JTabbedPane} of the {@link JNotepadPP}
	 * @param notepad
	 *            the specified {@link JNotepadPP}
	 * @param key
	 *            the {@link Action}'s name key
	 * @param provider
	 *            the {@link ILocalizationProvider} for internationalisation
	 * @param desc
	 *            the {@link Action}'s short description key
	 */
	public UniqueAction(Map<Integer, tabEntry> tabMap, JTabbedPane tabbedPane, JNotepadPP notepad, String key,
			ILocalizationProvider provider, String desc) {
		super(tabMap, tabbedPane, notepad, key, provider, desc);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		try {
			JTextArea editor = tabMap.get(tabbedPane.getSelectedIndex()).getEditor();
			Document doc = editor.getDocument();
			Set<String> set = new LinkedHashSet<>();

			int startLine = editor.getLineOfOffset(editor.getCaret().getDot());
			int endLine = editor.getLineOfOffset(editor.getCaret().getMark());
			int start = editor.getLineStartOffset(startLine);

			if (endLine < startLine) {
				int tmp = startLine;
				startLine = endLine;
				endLine = tmp;
				start = editor.getLineStartOffset(startLine);
			}

			int removeLength = 0;

			for (int i = startLine; i <= endLine; i++) {
				int startOffset = editor.getLineStartOffset(i);
				int endOffset = editor.getLineEndOffset(i);
				int length = Math.abs(endOffset - startOffset);

				String line = doc.getText(startOffset, length);

				if (line.contains("\n")) {
					line = doc.getText(startOffset, length - 1);
				}

				set.add(line);
				removeLength += length;
			}

			doc.remove(start, removeLength);

			int startOffset = editor.getLineStartOffset(startLine);
			doc.insertString(startOffset, getString(set), null);

		} catch (BadLocationException ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * Returns the text without duplicate lines
	 * 
	 * @param set
	 *            the {@link Set} containing non duplicate lines
	 * @return the text without duplicate lines
	 */
	private String getString(Set<String> set) {
		StringBuilder builder = new StringBuilder();
		set.forEach(l -> builder.append(l + "\n"));
		return builder.toString();
	}

}
