package hr.fer.zemris.java.hw11.jnotepadpp.local;

import java.util.Locale;
import java.util.ResourceBundle;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;

/**
 * This class represents a Singleton pattern class as a localisation provider
 * for the {@link JNotepadPP} application. It extends the
 * {@link AbstractLocalizationProvider} class. The class itself cannot be
 * instantiated via constructor, but with singleton getInstance method.
 * 
 * @author Matej Balun
 *
 */
public class LocalizationProvider extends AbstractLocalizationProvider {

	/**
	 * Default language of the localisation
	 */
	public static final String DEFAULT_LANGUAGE = "en";

	/**
	 * The specified resource path with ppoperties files.
	 */
	public static final String RESOURCE = "hr.fer.zemris.java.hw11.jnotepadpp.preferences.lang";

	/**
	 * The currently selected language.
	 */
	private String language;

	/**
	 * The {@link ResourceBundle} for {@link LocalizationProvider}
	 */
	private ResourceBundle bundle;

	/**
	 * Singleton reference to the {@link LocalizationProvider} instance
	 */
	private static LocalizationProvider instance;

	/**
	 * This private constructor disables the instantiation of the class in
	 * Singleton pattern, initialising the default localisation attributes.
	 */
	private LocalizationProvider() {
		this.language = DEFAULT_LANGUAGE;
		Locale locale = Locale.forLanguageTag(language);
		this.bundle = ResourceBundle.getBundle(RESOURCE, locale);
	}

	/**
	 * Returns the singleton instance of the {@link LocalizationProvider}.
	 * 
	 * @return the singleton instance of the {@link LocalizationProvider}
	 */
	public static LocalizationProvider getInstance() {
		if (instance == null) {
			instance = new LocalizationProvider();

		}
		return instance;
	}

	/**
	 * Sets the currently selected language for localisation and notifies all
	 * registered listeners for the localisation change.
	 * 
	 * @param language
	 *            the new specified language
	 */
	public void setLanguage(String language) {
		this.language = language;
		Locale locale = Locale.forLanguageTag(language);
		this.bundle = ResourceBundle.getBundle(RESOURCE, locale);
		fire();
	}

	@Override
	public String getString(String key) {
		return bundle.getString(key);
	}

}
