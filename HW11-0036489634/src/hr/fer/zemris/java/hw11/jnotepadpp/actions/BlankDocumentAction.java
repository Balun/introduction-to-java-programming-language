package hr.fer.zemris.java.hw11.jnotepadpp.actions;

import java.awt.event.ActionEvent;
import java.util.Map;

import javax.swing.Action;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;

import hr.fer.zemris.java.hw11.jnotepadpp.IconProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.tabEntry;
import hr.fer.zemris.java.hw11.jnotepadpp.listeners.DocumentLengthListener;
import hr.fer.zemris.java.hw11.jnotepadpp.listeners.NotepadCaretListener;
import hr.fer.zemris.java.hw11.jnotepadpp.listeners.TabListener;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;

/**
 * This class represents the new blank document operation for the
 * {@link JNotepadPP} application. It opens the new blank document in the new
 * tab. It extends the {@link AbstractNotepadAction} class.
 * 
 * @author Matej Balun
 *
 */
public class BlankDocumentAction extends AbstractNotepadAction {

	/**
	 * Index of the untitled document
	 */
	private int untitledCount;

	/**
	 * The label of the untitled document
	 */
	private final String UNTITLED;

	/**
	 * The serial version UID for this class.
	 */
	private static final long serialVersionUID = 1046215793685310548L;

	/**
	 * This constructor initialises this {@link Action} with its specified
	 * parameters.
	 * 
	 * @param tabMap
	 *            the {@link Map} containing the basic document attributes
	 * @param tabbedPane
	 *            {@link JTabbedPane} of the {@link JNotepadPP}
	 * @param notepad
	 *            the specified {@link JNotepadPP}
	 * @param untitledCount
	 *            index of the untitled documents
	 * @param untitled
	 *            label of the untitled documents
	 * @param key
	 *            the {@link Action}'s name key
	 * @param provider
	 *            the {@link ILocalizationProvider} for internationalisation
	 * @param desc
	 *            the {@link Action}'s short description key
	 */
	public BlankDocumentAction(Map<Integer, tabEntry> tabMap, JTabbedPane tabbedPane, JNotepadPP notepad,
			int untitledCount, String untitled, String key, ILocalizationProvider provider, String desc) {

		super(tabMap, tabbedPane, notepad, key, provider, desc);

		if (untitledCount < 0) {
			throw new IllegalArgumentException("The number of untitled tabs cannot be less than zero.");
		} else if (untitled == null) {
			throw new IllegalArgumentException("The String label for untitled tabs cannot be a null-reference.");
		}

		this.untitledCount = untitledCount;
		this.UNTITLED = untitled;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JTextArea editor = new JTextArea();
		editor.setLineWrap(true);
		editor.setDragEnabled(true);
		editor.getDocument().addDocumentListener(new DocumentLengthListener(tabMap, tabbedPane, notepad));
		editor.setFont(JNotepadPP.TEXT_FONT);
		editor.addCaretListener(new NotepadCaretListener(notepad, editor, tabbedPane, tabMap));

		editor.addCaretListener((l) -> {
			int dot = l.getDot();
			int mark = l.getMark();
			if (dot == mark) {
				notepad.getToolsMenu().setEnabled(false);
			} else {
				notepad.getToolsMenu().setEnabled(true);
			}

			int start = tabMap.get(tabbedPane.getSelectedIndex()).getEditor().getCaret().getDot();
			int end = tabMap.get(tabbedPane.getSelectedIndex()).getEditor().getCaret().getMark();

			int length = Math.abs(end - start);
			notepad.getSel().setText(notepad.getProvider().getString("SELECTION") + ": " + Integer.valueOf(length));
		});

		JScrollPane scrollPane = new JScrollPane(editor);

		editor.getDocument().addDocumentListener(new TabListener(tabMap, tabbedPane));
		tabEntry newEntry = new tabEntry(null, editor);

		tabMap.put(tabbedPane.getTabCount(), newEntry);
		tabbedPane.add(UNTITLED + " " + ++untitledCount, scrollPane);
		tabbedPane.setToolTipTextAt(tabbedPane.getTabCount() - 1, UNTITLED + " " + untitledCount);
		tabbedPane.setIconAt(tabbedPane.getTabCount() - 1, notepad.getIconProvider().getIcon(IconProvider.SAVED_ICON));
	}

}
