package hr.fer.zemris.java.hw11.jnotepadpp.actions;

import java.util.Comparator;
import java.util.Map;

import javax.swing.Action;
import javax.swing.JTabbedPane;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;
import hr.fer.zemris.java.hw11.jnotepadpp.tabEntry;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;

/**
 * This class represents the sort text ascending {@link Action} for the
 * {@link JNotepadPP} application. It sorts the selected lines in ascending
 * order. It extends the {@link AbstractSortAction} class.
 * 
 * @author Matej Balun
 *
 */
public class SortAscendingAction extends AbstractSortAction {

	/**
	 * The serial version UID for this class.
	 */
	private static final long serialVersionUID = 1046215793685310548L;

	/**
	 * This constructor initialises this {@link Action} with its specified
	 * parameters.
	 * 
	 * @param tabMap
	 *            the {@link Map} containing the basic document attributes
	 * @param tabbedPane
	 *            {@link JTabbedPane} of the {@link JNotepadPP}
	 * @param notepad
	 *            the specified {@link JNotepadPP}
	 * @param key
	 *            the {@link Action}'s name key
	 * @param provider
	 *            the {@link ILocalizationProvider} for internationalisation
	 * @param desc
	 *            the {@link Action}'s short description key
	 */
	public SortAscendingAction(Map<Integer, tabEntry> tabMap, JTabbedPane tabbedPane, JNotepadPP notepad, String key,
			ILocalizationProvider provider, String desc) {
		super(tabMap, tabbedPane, notepad, key, provider, desc);
	}

	@Override
	protected Comparator<String> getSortedList() {
		return (s1, s2) -> collator.compare(s1, s2);
	}

}
