package hr.fer.zemris.java.hw11.jnotepadpp.actions;

import java.awt.event.ActionEvent;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.swing.Action;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;
import hr.fer.zemris.java.hw11.jnotepadpp.tabEntry;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;

/**
 * This abstract class represents the general sorting action for document text
 * in {@link JNotepadPP} application. It extends the
 * {@link AbstractNotepadAction} class.
 * 
 * @author Matej Balun
 *
 */
public abstract class AbstractSortAction extends AbstractNotepadAction {

	/**
	 * The specified locale for text editing
	 */
	private Locale locale;

	/**
	 * The {@link Collator} for text comparing
	 */
	protected Collator collator;

	/**
	 * The generated serial version UID of this class.
	 */
	private static final long serialVersionUID = 1046215793685310548L;

	/**
	 * This constructor initialises this {@link Action} with its specified
	 * parameters.
	 * 
	 * @param tabMap
	 *            the {@link Map} containing the basic document attributes
	 * @param tabbedPane
	 *            {@link JTabbedPane} of the {@link JNotepadPP}
	 * @param notepad
	 *            the specified {@link JNotepadPP}
	 * @param key
	 *            the {@link Action}'s name key
	 * @param provider
	 *            the {@link ILocalizationProvider} for internationalisation
	 * @param desc
	 *            the {@link Action}'s short description key
	 */
	public AbstractSortAction(Map<Integer, tabEntry> tabMap, JTabbedPane tabbedPane, JNotepadPP notepad, String key,
			ILocalizationProvider provider, String desc) {
		super(tabMap, tabbedPane, notepad, key, provider, desc);

		this.locale = new Locale("hr");
		this.collator = Collator.getInstance(locale);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		try {
			JTextArea editor = tabMap.get(tabbedPane.getSelectedIndex()).getEditor();
			Document doc = editor.getDocument();
			List<String> sortList = new ArrayList<>();

			int startLine = editor.getLineOfOffset(editor.getCaret().getDot());
			int endLine = editor.getLineOfOffset(editor.getCaret().getMark());
			int start = editor.getLineStartOffset(startLine);

			if (endLine < startLine) {
				int tmp = startLine;
				startLine = endLine;
				endLine = tmp;
				start = editor.getLineStartOffset(startLine);
			}

			int removeLength = 0;

			for (int i = startLine; i <= endLine; i++) {
				int startOffset = editor.getLineStartOffset(i);
				int endOffset = editor.getLineEndOffset(i);
				int length = Math.abs(endOffset - startOffset);

				String line = doc.getText(startOffset, length);

				if (line.contains("\n")) {
					line = doc.getText(startOffset, length - 1);
				}

				sortList.add(line);
				removeLength += length;
			}

			doc.remove(start, removeLength);
			Collections.sort(sortList, getSortedList());
			int startOffset = editor.getLineStartOffset(startLine);
			doc.insertString(startOffset, getString(sortList), null);

		} catch (BadLocationException ex) {
			ex.printStackTrace();
		}

	}

	/**
	 * Returns the the changed sorted string for selected part of the document.
	 * 
	 * @param list
	 *            the list with sorted lines
	 * @return The sorted text part of the document
	 */
	private String getString(List<String> list) {
		StringBuilder builder = new StringBuilder();
		list.forEach(l -> builder.append(l + "\n"));
		// builder.replace(builder.length() - 1, builder.length(), "");
		return builder.toString();
	}

	/**
	 * Returns the specified comparator for specified sort text operation
	 * 
	 * @return the specified {@link Comparator} for sorting.
	 */
	protected abstract Comparator<String> getSortedList();

}
