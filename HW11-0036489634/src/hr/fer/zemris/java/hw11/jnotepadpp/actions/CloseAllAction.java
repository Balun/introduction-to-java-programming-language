package hr.fer.zemris.java.hw11.jnotepadpp.actions;

import java.awt.event.ActionEvent;
import java.util.Map;

import javax.swing.Action;
import javax.swing.JTabbedPane;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;
import hr.fer.zemris.java.hw11.jnotepadpp.tabEntry;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;

/**
 * This class represents the close all action for the {@link JNotepadPP}
 * application. It closes all documents currently opened in the editor. It
 * extends the {@link AbstractNotepadAction} class
 * 
 * @author Matej Balun
 *
 */
public class CloseAllAction extends AbstractNotepadAction {

	/**
	 * The serial version UID for this class.
	 */
	private static final long serialVersionUID = 1046215793685310548L;

	/**
	 * This constructor initialises this {@link Action} with its specified
	 * parameters.
	 * 
	 * @param tabMap
	 *            the {@link Map} containing the basic document attributes
	 * @param tabbedPane
	 *            {@link JTabbedPane} of the {@link JNotepadPP}
	 * @param notepad
	 *            the specified {@link JNotepadPP}
	 * @param key
	 *            the {@link Action}'s name key
	 * @param provider
	 *            the {@link ILocalizationProvider} for internationalisation
	 * @param desc
	 *            the {@link Action}'s short description key
	 */
	public CloseAllAction(Map<Integer, tabEntry> tabMap, JTabbedPane tabbedPane, JNotepadPP notepad, String key,
			ILocalizationProvider provider, String desc) {
		super(tabMap, tabbedPane, notepad, key, provider, desc);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		notepad.checkBeforeClosing();
		tabbedPane.removeAll();
		tabMap.clear();
	}

}
