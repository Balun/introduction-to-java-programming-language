package hr.fer.zemris.java.hw11.jnotepadpp.listeners;

import java.util.Map;

import javax.swing.JTabbedPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import hr.fer.zemris.java.hw11.jnotepadpp.IconProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;
import hr.fer.zemris.java.hw11.jnotepadpp.tabEntry;

/**
 * This class represents the listener for tabs in {@link JNotepadPP}
 * application. It changes the selected tab names, as well as selected documents
 * saved/unsaved document icons in tabs. It Implements the
 * {@link DocumentListener} interface.
 * 
 * @author Matej Balun
 *
 */
public class TabListener implements DocumentListener {

	/**
	 * {@link Map} containing the tab-document attributes
	 */
	private Map<Integer, tabEntry> tabMap;

	/**
	 * {@link JTabbedPane} of the {@link JNotepadPP}
	 */
	private JTabbedPane tabbedPane;

	/**
	 * The specified {@link IconProvider} for generating icons
	 */
	private final IconProvider provider;

	/**
	 * This constructor initialises the {@link TabListener} with its specified
	 * parameters.
	 * 
	 * @param tabMap
	 *            {@link Map} with tab-document attributes
	 * @param tabbedPane
	 *            {@link JNotepadPP} of the {@link JNotepadPP}
	 */
	public TabListener(Map<Integer, tabEntry> tabMap, JTabbedPane tabbedPane) {
		if (tabMap == null) {
			throw new IllegalArgumentException(
					"The map containing text areas and file paths cannot be a null-reference.");
		} else if (tabbedPane == null) {
			throw new IllegalArgumentException("The editor's tab pane cannot be a null-reference.");
		}
		this.tabbedPane = tabbedPane;
		this.tabMap = tabMap;
		this.provider = new IconProvider();
	}

	@Override
	public void removeUpdate(DocumentEvent e) {
		tabMap.get(tabbedPane.getSelectedIndex()).setChangedStatus(true);
		tabbedPane.setIconAt(tabbedPane.getSelectedIndex(), provider.getIcon(IconProvider.UNSAVED_ICON));
	}

	@Override
	public void insertUpdate(DocumentEvent e) {
		tabMap.get(tabbedPane.getSelectedIndex()).setChangedStatus(true);
		tabbedPane.setIconAt(tabbedPane.getSelectedIndex(), provider.getIcon(IconProvider.UNSAVED_ICON));
	}

	@Override
	public void changedUpdate(DocumentEvent e) {
		tabMap.get(tabbedPane.getSelectedIndex()).setChangedStatus(true);
		tabbedPane.setIconAt(tabbedPane.getSelectedIndex(), provider.getIcon(IconProvider.UNSAVED_ICON));
	}

}
