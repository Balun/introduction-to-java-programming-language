package hr.fer.zemris.java.hw11.jnotepadpp.actions;

import java.awt.event.ActionEvent;
import java.util.Map;

import javax.swing.Action;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;
import hr.fer.zemris.java.hw11.jnotepadpp.tabEntry;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;

/**
 * This class represents the statistics {@link Action} for the
 * {@link JNotepadPP} application. It displays the selected document statistics.
 * It extends the {@link AbstractNotepadAction} class.
 * 
 * @author Matej Balun
 *
 */
public class StatisticsAction extends AbstractNotepadAction {

	/**
	 * The serial version UID for this class.
	 */
	private static final long serialVersionUID = 1046215793685310548L;

	/**
	 * This constructor initialises this {@link Action} with its specified
	 * parameters.
	 * 
	 * @param tabMap
	 *            the {@link Map} containing the basic document attributes
	 * @param tabbedPane
	 *            {@link JTabbedPane} of the {@link JNotepadPP}
	 * @param notepad
	 *            the specified {@link JNotepadPP}
	 * @param key
	 *            the {@link Action}'s name key
	 * @param provider
	 *            the {@link ILocalizationProvider} for internationalisation
	 * @param desc
	 *            the {@link Action}'s short description key
	 */
	public StatisticsAction(Map<Integer, tabEntry> tabMap, JTabbedPane tabbedPane, JNotepadPP notepad, String key,
			ILocalizationProvider provider, String desc) {
		super(tabMap, tabbedPane, notepad, key, provider, desc);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String text = tabMap.get(tabbedPane.getSelectedIndex()).getEditor().getText();
		int lineCount = tabMap.get(tabbedPane.getSelectedIndex()).getEditor().getLineCount();
		int charCount = text.length();
		int letterCount = 0;

		for (int i = 0; i < charCount; i++) {
			if (!Character.isWhitespace(text.charAt(i))) {
				letterCount++;
			}
		}

		StringBuilder builder = new StringBuilder();
		builder.append(notepad.getProvider().getString("CHARS") + " " + charCount + "\n");
		builder.append(notepad.getProvider().getString("LETTERS") + " " + letterCount + "\n");
		builder.append(notepad.getProvider().getString("LINES") + " " + lineCount);

		JOptionPane.showMessageDialog(notepad, builder.toString(), notepad.getProvider().getString("STATISTICS"),
				JOptionPane.NO_OPTION);
	}

}
