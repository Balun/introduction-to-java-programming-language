package hr.fer.zemris.java.hw11.jnotepadpp.listeners;

import java.util.Map;

import javax.swing.JTabbedPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;
import hr.fer.zemris.java.hw11.jnotepadpp.tabEntry;

/**
 * This class represents the length document listener for this
 * {@link JNotepadPP} application. The listener updates the length label of the
 * specified document. This class implements the {@link DocumentListener}
 * interface
 * 
 * @author Matej Balun
 *
 */
public class DocumentLengthListener implements DocumentListener {

	/**
	 * The specified map with tab indexes paths and documents
	 */
	private Map<Integer, tabEntry> tabMap;

	/**
	 * The notepad*s tab pane.
	 */
	private JTabbedPane tabbedPane;

	/**
	 * The specified notepad.
	 */
	private JNotepadPP notepad;

	/**
	 * This constructor initialises the {@link DocumentLengthListener} with its
	 * values.
	 * 
	 * @param tabMap
	 *            The specified map with tab indexes paths and documents
	 * @param tabbedPane
	 *            The notepad*s tab pane.
	 * @param notepad
	 *            The specified notepad.
	 */
	public DocumentLengthListener(Map<Integer, tabEntry> tabMap, JTabbedPane tabbedPane, JNotepadPP notepad) {
		if (tabMap == null) {
			throw new IllegalArgumentException(
					"The map containing text areas and file paths cannot be a null-reference.");
		} else if (tabbedPane == null) {
			throw new IllegalArgumentException("The editor's tab pane cannot be a null-reference.");
		} else if (notepad == null) {
			throw new IllegalArgumentException("The specified notepad cannot be a null reference.");
		}
		this.tabbedPane = tabbedPane;
		this.tabMap = tabMap;
		this.notepad = notepad;
	}

	@Override
	public void removeUpdate(DocumentEvent e) {
		notepad.getLength().setText(notepad.getProvider().getString("LENGTH") + ": "
				+ tabMap.get(tabbedPane.getSelectedIndex()).getEditor().getDocument().getLength());
	}

	@Override
	public void insertUpdate(DocumentEvent e) {
		notepad.getLength().setText(notepad.getProvider().getString("LENGTH") + ": "
				+ tabMap.get(tabbedPane.getSelectedIndex()).getEditor().getDocument().getLength());
	}

	@Override
	public void changedUpdate(DocumentEvent e) {
		notepad.getLength().setText(notepad.getProvider().getString("LENGTH") + ": "
				+ tabMap.get(tabbedPane.getSelectedIndex()).getEditor().getDocument().getLength());
	}

}
