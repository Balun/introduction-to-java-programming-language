package hr.fer.zemris.java.hw11.jnotepadpp.actions;

import java.awt.event.ActionEvent;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;

import javax.swing.Action;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;

import hr.fer.zemris.java.hw11.jnotepadpp.IconProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.tabEntry;
import hr.fer.zemris.java.hw11.jnotepadpp.listeners.DocumentLengthListener;
import hr.fer.zemris.java.hw11.jnotepadpp.listeners.NotepadCaretListener;
import hr.fer.zemris.java.hw11.jnotepadpp.listeners.TabListener;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;

/**
 * This class represents the open existing document {@link Action} for the
 * {@link JNotepadPP} application. It opens the selected document from disc. It
 * extends the {@link AbstractNotepadAction} class.
 * 
 * @author Matej Balun
 *
 */
public class OpenDocumentAction extends AbstractNotepadAction {

	/**
	 * The serial version UID for this class.
	 */
	private static final long serialVersionUID = 1046215793685310548L;

	/**
	 * This constructor initialises this {@link Action} with its specified
	 * parameters.
	 * 
	 * @param tabMap
	 *            the {@link Map} containing the basic document attributes
	 * @param tabbedPane
	 *            {@link JTabbedPane} of the {@link JNotepadPP}
	 * @param notepad
	 *            the specified {@link JNotepadPP}
	 * @param key
	 *            the {@link Action}'s name key
	 * @param provider
	 *            the {@link ILocalizationProvider} for internationalisation
	 * @param desc
	 *            the {@link Action}'s short description key
	 */
	public OpenDocumentAction(Map<Integer, tabEntry> tabMap, JTabbedPane tabbedPane, JNotepadPP notepad, String key,
			ILocalizationProvider provider, String desc) {
		super(tabMap, tabbedPane, notepad, key, provider, desc);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JTextArea editor = new JTextArea();
		editor.setLineWrap(true);
		editor.setDragEnabled(true);
		editor.addCaretListener(new NotepadCaretListener(notepad, editor, tabbedPane, tabMap));

		JFileChooser fc = new JFileChooser();
		fc.setDialogTitle(notepad.getProvider().getString("OPEN_DIALOG"));
		if (fc.showOpenDialog(notepad) != JFileChooser.APPROVE_OPTION) {
			return;
		}

		File fileName = fc.getSelectedFile();
		Path filePath = fileName.toPath();

		if (!tabMap.isEmpty()) {
			for (tabEntry entry : tabMap.values()) {
				if (entry.getFilePath() != null && entry.getFilePath().equals(filePath)) {
					return;
				}
			}
		}

		if (!Files.isReadable(filePath)) {
			JOptionPane.showMessageDialog(notepad,
					notepad.getProvider().getString("FILE") + " " + fileName.getAbsolutePath() + " "
							+ notepad.getProvider().getString("NOT_EXISTS"),
					notepad.getProvider().getString("ERROR"), JOptionPane.ERROR_MESSAGE);
			return;
		}

		byte[] okteti;

		try {
			okteti = Files.readAllBytes(filePath);
		} catch (Exception ex) {
			JOptionPane.showMessageDialog(notepad,
					notepad.getProvider().getString("ERROR_READING") + " " + fileName.getAbsolutePath(),
					notepad.getProvider().getString("ERROR"), JOptionPane.ERROR_MESSAGE);
			return;
		}

		String tekst = new String(okteti, StandardCharsets.UTF_8);
		editor.setText(tekst);
		editor.setFont(JNotepadPP.TEXT_FONT);
		editor.getDocument().addDocumentListener(new DocumentLengthListener(tabMap, tabbedPane, notepad));

		editor.addCaretListener(new NotepadCaretListener(notepad, editor, tabbedPane, tabMap));

		JScrollPane scrollPane = new JScrollPane(editor);

		tabbedPane.add(filePath.getFileName().toString(), scrollPane);
		tabMap.put(tabbedPane.getTabCount() - 1, new tabEntry(filePath, editor));
		tabbedPane.setToolTipTextAt(tabbedPane.getTabCount() - 1, filePath.toAbsolutePath().toString());
		tabbedPane.setIconAt(tabbedPane.getTabCount() - 1, notepad.getIconProvider().getIcon(IconProvider.SAVED_ICON));

		editor.getDocument().addDocumentListener(new TabListener(tabMap, tabbedPane));
	}

}
