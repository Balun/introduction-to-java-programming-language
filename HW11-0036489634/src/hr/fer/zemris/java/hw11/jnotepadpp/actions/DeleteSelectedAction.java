package hr.fer.zemris.java.hw11.jnotepadpp.actions;

import java.awt.event.ActionEvent;
import java.util.Map;

import javax.swing.Action;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

import hr.fer.zemris.java.hw11.jnotepadpp.tabEntry;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;

/**
 * This class represents the delete {@link Action} for the {@link JNotepadPP}
 * application. It deletes the selected text of the specified document. It
 * extends the {@link AbstractNotepadAction} class.
 * 
 * @author Matej Balun
 *
 */
public class DeleteSelectedAction extends AbstractNotepadAction {

	/**
	 * The serial version UID for this class.
	 */
	private static final long serialVersionUID = 1046215793685310548L;

	/**
	 * This constructor initialises this {@link Action} with its specified
	 * parameters.
	 * 
	 * @param tabMap
	 *            the {@link Map} containing the basic document attributes
	 * @param tabbedPane
	 *            {@link JTabbedPane} of the {@link JNotepadPP}
	 * @param notepad
	 *            the specified {@link JNotepadPP}
	 * @param key
	 *            the {@link Action}'s name key
	 * @param provider
	 *            the {@link ILocalizationProvider} for internationalisation
	 * @param desc
	 *            the {@link Action}'s short description key
	 */
	public DeleteSelectedAction(Map<Integer, tabEntry> tabMap, JTabbedPane tabbedPane, JNotepadPP notepad, String key,
			ILocalizationProvider provider, String desc) {
		super(tabMap, tabbedPane, notepad, key, provider, desc);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JTextArea editor = tabMap.get(tabbedPane.getSelectedIndex()).getEditor();

		Document doc = editor.getDocument();
		int len = Math.abs(editor.getCaret().getDot() - editor.getCaret().getMark());
		if (len == 0) {
			return;
		}

		int offset = Math.min(editor.getCaret().getDot(), editor.getCaret().getMark());
		try {
			doc.remove(offset, len);
		} catch (BadLocationException e1) {
			e1.printStackTrace();
		}
	}

}
