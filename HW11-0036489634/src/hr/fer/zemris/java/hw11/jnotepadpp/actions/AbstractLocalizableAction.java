package hr.fer.zemris.java.hw11.jnotepadpp.actions;

import javax.swing.AbstractAction;
import javax.swing.Action;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;

/**
 * This abstract class represents the general {@link Action} for the
 * {@link JNotepadPP} application with the possibility of dynamic
 * internationalisation. It extends the {@link AbstractAction} class.
 * 
 * @author Matej Balun
 *
 */
public abstract class AbstractLocalizableAction extends AbstractAction {

	/**
	 * The serial version UID for this class.
	 */
	private static final long serialVersionUID = -6118598984979468892L;

	/**
	 * This constructor initialises the {@link AbstractLocalizableAction}, by
	 * inserting the localised name and description values into the
	 * {@link Action}'s value map. It also registers the specified action for
	 * change of the localisation specification.
	 * 
	 * @param key
	 *            the localised name key
	 * @param provider
	 *            the specified {@link ILocalizationProvider}
	 * @param desc
	 *            the localised description key
	 */
	public AbstractLocalizableAction(String key, ILocalizationProvider provider, String desc) {
		super();

		putValue(Action.NAME, provider.getString(key));
		putValue(Action.SHORT_DESCRIPTION, provider.getString(desc));

		provider.addLocalizationListener(() -> {
			AbstractLocalizableAction.this.putValue(Action.NAME, provider.getString(key));
			AbstractLocalizableAction.this.putValue(Action.SHORT_DESCRIPTION, provider.getString(desc));
		});
	}

}
