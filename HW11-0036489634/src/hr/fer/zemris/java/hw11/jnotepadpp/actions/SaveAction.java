package hr.fer.zemris.java.hw11.jnotepadpp.actions;

import java.awt.event.ActionEvent;
import java.nio.file.Path;
import java.util.Map;

import javax.swing.Action;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;

import hr.fer.zemris.java.hw11.jnotepadpp.tabEntry;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;

/**
 * This class represents the standard save action for the {@link JNotepadPP}
 * application. It saves the currently selected document to the disc. It extends
 * the {@link AbstractSaveAction} class.
 * 
 * @author Matej Balun
 *
 */
public class SaveAction extends AbstractSaveAction {

	/**
	 * The serial version UID for this class.
	 */
	private static final long serialVersionUID = 1046215793685310548L;

	/**
	 * This constructor initialises this {@link Action} with its specified
	 * parameters.
	 * 
	 * @param tabMap
	 *            the {@link Map} containing the basic document attributes
	 * @param tabbedPane
	 *            {@link JTabbedPane} of the {@link JNotepadPP}
	 * @param notepad
	 *            the specified {@link JNotepadPP}
	 * @param key
	 *            the {@link Action}'s name key
	 * @param provider
	 *            the {@link ILocalizationProvider} for internationalisation
	 * @param desc
	 *            the {@link Action}'s short description key
	 */
	public SaveAction(Map<Integer, tabEntry> tabMap, JTabbedPane tabbedPane, JNotepadPP notepad, String key,
			ILocalizationProvider provider, String desc) {
		super(tabMap, tabbedPane, notepad, key, provider, desc);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		tabEntry entry = tabMap.get(tabbedPane.getSelectedIndex());
		Path openedFilePath = entry.getFilePath();

		if (openedFilePath == null) {
			JFileChooser jfc = new JFileChooser();
			jfc.setDialogTitle(notepad.getProvider().getString("SAVE_DOC"));

			if (jfc.showSaveDialog(notepad) != JFileChooser.APPROVE_OPTION) {
				JOptionPane.showMessageDialog(notepad, notepad.getProvider().getString("SAVE_NOT"),
						notepad.getProvider().getString("WARNING"), JOptionPane.WARNING_MESSAGE);
				return;
			}

			openedFilePath = jfc.getSelectedFile().toPath();
			tabMap.get(tabbedPane.getSelectedIndex()).setFilePath(openedFilePath);
			tabbedPane.setTitleAt(tabbedPane.getSelectedIndex(), openedFilePath.getFileName().toString());

			if (!overwriteWarning(openedFilePath)) {
				return;
			}

		}

		save(openedFilePath, entry, tabbedPane.getSelectedIndex());

	}

}
