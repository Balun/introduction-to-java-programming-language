package hr.fer.zemris.java.hw11.jnotepadpp.actions;

import java.util.Map;

import javax.swing.Action;
import javax.swing.JTabbedPane;

import hr.fer.zemris.java.hw11.jnotepadpp.tabEntry;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;

/**
 * This class represents the general {@link JNotepadPP} {@link Action}. It
 * extends the {@link AbstractLocalizableAction}, offering the dynamic
 * internationalisation specification.
 * 
 * @author Matej Balun
 *
 */
public abstract class AbstractNotepadAction extends AbstractLocalizableAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1046215793685310548L;

	/**
	 * The map containing the document's attributes
	 */
	protected Map<Integer, tabEntry> tabMap;

	/**
	 * The tab pane of the {@link JNotepadPP}
	 */
	protected JTabbedPane tabbedPane;

	/**
	 * The specified {@link JNotepadPP}
	 */
	protected JNotepadPP notepad;

	/**
	 * This constructor initialises this {@link Action} with its specified
	 * parameters.
	 * 
	 * @param tabMap
	 *            the {@link Map} containing the basic document attributes
	 * @param tabbedPane
	 *            {@link JTabbedPane} of the {@link JNotepadPP}
	 * @param notepad
	 *            the specified {@link JNotepadPP}
	 * @param key
	 *            the {@link Action}'s name key
	 * @param provider
	 *            the {@link ILocalizationProvider} for internationalisation
	 * @param desc
	 *            the {@link Action}'s short description key
	 */
	public AbstractNotepadAction(Map<Integer, tabEntry> tabMap, JTabbedPane tabbedPane, JNotepadPP notepad, String key,
			ILocalizationProvider provider, String desc) {

		super(key, provider, desc);
		if (tabMap == null) {
			throw new IllegalArgumentException(
					"The map containing text areas and file paths cannot be a null-reference.");
		} else if (tabbedPane == null) {
			throw new IllegalArgumentException("The editor's tab pane cannot be a null-reference.");
		} else if (notepad == null) {
			throw new IllegalArgumentException("The specified notepad for this action cannot be a null-reference.");
		}

		this.tabbedPane = tabbedPane;
		this.tabMap = tabMap;
		this.notepad = notepad;
	}

}
