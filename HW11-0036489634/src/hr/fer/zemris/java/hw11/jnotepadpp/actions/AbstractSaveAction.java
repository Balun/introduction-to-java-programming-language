package hr.fer.zemris.java.hw11.jnotepadpp.actions;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;

import javax.swing.Action;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;

import hr.fer.zemris.java.hw11.jnotepadpp.tabEntry;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.IconProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;

/**
 * This class Represents the abstract save action for saving documents in
 * {@link JNotepadPP}. It extends the {@link AbstractNotepadAction} class.
 * 
 * @author Matej Balun
 *
 */
public abstract class AbstractSaveAction extends AbstractNotepadAction {

	/**
	 * The serial version UID for this class.
	 */
	private static final long serialVersionUID = 1046215793685310548L;

	/**
	 * This constructor initialises this {@link Action} with its specified
	 * parameters.
	 * 
	 * @param tabMap
	 *            the {@link Map} containing the basic document attributes
	 * @param tabbedPane
	 *            {@link JTabbedPane} of the {@link JNotepadPP}
	 * @param notepad
	 *            the specified {@link JNotepadPP}
	 * @param key
	 *            the {@link Action}'s name key
	 * @param provider
	 *            the {@link ILocalizationProvider} for internationalisation
	 * @param desc
	 *            the {@link Action}'s short description key
	 */
	public AbstractSaveAction(Map<Integer, tabEntry> tabMap, JTabbedPane tabbedPane, JNotepadPP notepad, String key,
			ILocalizationProvider provider, String desc) {
		super(tabMap, tabbedPane, notepad, key, provider, desc);
	}

	/**
	 * This method displays the {@link JOptionPane} for overwriting a file.
	 * 
	 * @param filePath
	 *            the specified file path
	 * @return true if overwrite is permitted.
	 */
	protected boolean overwriteWarning(Path filePath) {
		if (Files.exists(filePath)) {
			int choice = JOptionPane.showConfirmDialog(notepad, notepad.getProvider().getString("EXISTS"),
					notepad.getProvider().getString("EXISTS_TITLE"), JOptionPane.YES_NO_OPTION);
			if (choice != JOptionPane.YES_OPTION) {
				return false;
			}
		}

		return true;
	}

	/**
	 * This method saves the specified file to the disc
	 * 
	 * @param filePath
	 *            the path of the file
	 * @param entry
	 *            the {@link tabEntry} with document attributes
	 * @param index
	 *            the index of document's tab
	 */
	protected void save(Path filePath, tabEntry entry, int index) {
		byte[] podaci = entry.getEditor().getText().getBytes(StandardCharsets.UTF_8);
		try {
			Files.write(filePath, podaci);
		} catch (IOException ex) {
			JOptionPane.showMessageDialog(notepad,
					notepad.getProvider().getString("SAVE_ERROR") + " " + filePath.toString(),
					notepad.getProvider().getString("ERROR"), JOptionPane.ERROR_MESSAGE);
			return;
		}

		entry.setChangedStatus(false);
		tabbedPane.setIconAt(tabbedPane.getSelectedIndex(), notepad.getIconProvider().getIcon(IconProvider.SAVED_ICON));

		JOptionPane.showMessageDialog(notepad, notepad.getProvider().getString("SAVE_OK"),
				notepad.getProvider().getString("INFORMATION"), JOptionPane.INFORMATION_MESSAGE);
	}

}
