package hr.fer.zemris.java.hw16.jvdraw.drawing.actions;

import java.awt.Color;
import java.awt.Point;

import hr.fer.zemris.java.hw16.jvdraw.JVDraw;
import hr.fer.zemris.java.hw16.jvdraw.geom.GeometricalObject;

/**
 * This interface represents the general user interface for retrieval of the
 * geometric objects for the {@link JVDraw} application. It offers a single
 * method which creates the geometric object. Classes that implement this
 * interface must implement this method in order to create concrete geometric
 * objects
 * 
 * @author Matej Balun
 *
 */
public interface DrawingAction {

	/**
	 * This method creates the specified geometric object defined by the class.
	 * The object is created based on the general parameters for the geometric
	 * objects creation.
	 * 
	 * @param firstPoint
	 *            first point for defining an object
	 * @param secondPoint
	 *            second point for defining an object
	 * @param foreground
	 *            foreground of the object
	 * @param background
	 *            background of the object
	 * @param id
	 *            id of the specified object
	 * @return new instance of {@link GeometricalObject}.
	 */
	public GeometricalObject createGeometricObject(Point firstPoint, Point secondPoint, Color foreground,
			Color background, int id);

}
