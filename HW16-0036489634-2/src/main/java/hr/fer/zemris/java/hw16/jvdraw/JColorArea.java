package hr.fer.zemris.java.hw16.jvdraw;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JColorChooser;
import javax.swing.JComponent;

import hr.fer.zemris.java.hw16.jvdraw.listeners.ChangeColorListener;

/**
 * This class represents the basic color area for the {@link JVDraw}
 * application. The class offers registration of {@link ChangeColorListener}s
 * for the area. It extends the {@link JComponent} class overriding some of its
 * methods, as well as implementing the {@link IColorProvider} interface.
 * 
 * @author Matej Balun
 *
 */
public class JColorArea extends JComponent implements IColorProvider {

	/**
	 * The serial version UID for this class.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Current colour of the {@link JColorArea}.
	 */
	private Color currentColor;

	/**
	 * List of registered {@link ChangeColorListener}s
	 */
	private List<ChangeColorListener> listeners;

	/**
	 * This constructor initialises the {@link JColorArea} with its default
	 * colour, as well as registration of the {@link MouseAdapter} for the
	 * {@link JColorChooser} for changing the colour.
	 * 
	 * @param color
	 *            the default colour of the {@link JColorArea}.
	 */
	public JColorArea(Color color) {
		currentColor = color;
		listeners = new ArrayList<>();

		addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {

				Color color = JColorChooser.showDialog(JColorArea.this, "Chose preffered color", Color.BLUE);
				if (color != null) {
					Color old = currentColor;
					currentColor = color;
					repaint();
					fire(old, color);
				}
			}
		});
	}

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(15, 15);
	}

	@Override
	protected void paintComponent(Graphics g) {
		g.setColor(currentColor);
		g.fillRect(getInsets().left, getInsets().right, 15, 15);
	}

	@Override
	public Color getCurrentColor() {
		return currentColor;
	}

	/**
	 * Registers the {@link ChangeColorListener} to the {@link JColorArea}.
	 * 
	 * @param listener
	 *            the specified {@link ChangeColorListener}
	 */
	public void addColorChangeListener(ChangeColorListener listener) {
		if (!listeners.contains(listener)) {
			listeners.add(listener);
		}
	}

	/**
	 * Unregisters the {@link ChangeColorListener} to the {@link JColorArea}.
	 * 
	 * @param listener
	 *            the specified {@link ChangeColorListener}
	 */
	public void removeColorChangeListener(ChangeColorListener listener) {
		listeners.remove(listener);
	}

	/**
	 * Notifies all the registered {@link ChangeColorListener} of the colour
	 * change event
	 * 
	 * @param oldColor
	 *            colour of the {@link JColorArea} before change.
	 * @param newColor
	 *            colour of the {@link JColorArea} after the change.
	 */
	private void fire(Color oldColor, Color newColor) {
		for (ChangeColorListener listener : listeners) {
			listener.newColorSelected(this, oldColor, newColor);
		}
	}

}
