package hr.fer.zemris.java.hw16.jvdraw.drawing.actions;

import java.awt.Color;
import java.awt.Point;

import hr.fer.zemris.java.hw16.jvdraw.JVDraw;
import hr.fer.zemris.java.hw16.jvdraw.geom.GeometricalObject;
import hr.fer.zemris.java.hw16.jvdraw.geom.Line;

/**
 * THis class represents the command for creation of 2D {@link Line} geometric
 * object for the {@link JVDraw} application. It implements the
 * {@link DrawingAction} interface, implementing the method for creation of
 * concrete geometric object.
 * 
 * @author Matej Balun
 *
 */
public class LineAction implements DrawingAction {

	@Override
	public GeometricalObject createGeometricObject(Point firstPoint, Point secondPoint, Color foreground,
			Color background, int id) {

		return new Line("line " + id, firstPoint, secondPoint, foreground);
	}

}
