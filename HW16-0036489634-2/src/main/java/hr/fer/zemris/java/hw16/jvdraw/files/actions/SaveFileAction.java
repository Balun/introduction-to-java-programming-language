package hr.fer.zemris.java.hw16.jvdraw.files.actions;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.nio.file.Files;
import java.nio.file.Paths;

import hr.fer.zemris.java.hw16.jvdraw.JDrawingCanvas;
import hr.fer.zemris.java.hw16.jvdraw.JVDraw;
import hr.fer.zemris.java.hw16.jvdraw.model.DrawingModel;

/**
 * This class represents the save action for the {@link JVDraw} application. It
 * extends the {@link AbstractFileAction} class, offering the save functionality
 * of the current content of the workspace.
 * 
 * @author Matej Balun
 *
 */
public class SaveFileAction extends AbstractFileAction {

	/**
	 * The serial version UID for this class.
	 */
	private static final long serialVersionUID = -393213897990125331L;

	/**
	 * The description of the {@link SaveFileAction}
	 */
	private static final String DESC = "Save the current file";

	/**
	 * This constructor initialises the {@link SaveFileAction} with its
	 * specified parameters and passes name and description to the super
	 * constructor {@link AbstractFileAction}.
	 * 
	 * @param model
	 *            {@link DrawingModel} of the application
	 * @param canvas
	 *            {@link JDrawingCanvas} of the application
	 * @param saveDest
	 *            currently specified save destination of the workspace
	 */
	public SaveFileAction(DrawingModel model, JDrawingCanvas canvas, String saveDest) {
		super("Save", "control S", KeyEvent.VK_S, DESC, model, canvas, saveDest);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (saveDest == null || !Files.exists(Paths.get(saveDest).toAbsolutePath())) {
			saveDest = getSaveDest();
			if (saveDest == null) {
				return;
			}
		}
		save();
		canvas.setChanged(false);
	}
}
