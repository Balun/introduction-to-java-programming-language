package hr.fer.zemris.java.hw16.jvdraw.geom;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import hr.fer.zemris.java.hw16.jvdraw.JColorArea;

/**
 * This class represents the simple 2D line geometric object. It extends the
 * {@link GeometricalObject} class, implementing the methods specific for this
 * geometric object.
 * 
 * @author Matej Balun
 *
 */
public class Line extends GeometricalObject {

	/**
	 * First point of the line.
	 */
	private Point first;

	/**
	 * Second point of the line.
	 */
	private Point second;

	/**
	 * This constructor initialises the {@link Line} with its specified
	 * parameters.
	 * 
	 * @param name
	 *            name of the line
	 * @param first
	 *            first point of the line
	 * @param second
	 *            second point of the line
	 */
	public Line(String name, Point first, Point second) {
		super(name);
		this.first = first;
		this.second = second;
	}

	/**
	 * This constructor initialises the {@link Line} with its specified
	 * parameters and color.
	 * 
	 * @param name
	 *            name of the line
	 * @param first
	 *            first point of the line
	 * @param second
	 *            second point of the line
	 * @param color
	 *            colour of the line
	 */
	public Line(String name, Point first, Point second, Color color) {
		this(name, first, second);
		setColor(color);
	}

	/**
	 * Sets the first point of the {@link Line}.
	 * 
	 * @param first
	 *            specified first point of the line
	 */
	public void setFirstPoint(Point first) {
		this.first = first;
	}

	/**
	 * Sets the second point of the line
	 * 
	 * @param second
	 *            specified second point of the line.
	 */
	public void setSecondPoint(Point second) {
		this.second = second;
	}

	@Override
	public void draw(Graphics g, Bounds bounds) {
		g.setColor(getColor());
		g.drawLine(first.x - bounds.getMinX(), first.y - bounds.getMinY(), second.x - bounds.getMinX(),
				second.y - bounds.getMinY());

	}

	@Override
	public void getBounds(Bounds bounds) {
		bounds.setMinX(Math.min(first.x, second.x));
		bounds.setMaxX(Math.max(first.x, second.x));
		bounds.setMinY(Math.min(first.y, second.y));
		bounds.setMaxY(Math.max(first.y, second.y));
	}

	@Override
	public Bounds getBounds() {
		return new Bounds(Math.min(first.x, second.x), Math.max(first.x, second.x), Math.min(first.y, second.y),
				Math.max(first.y, second.y));
	}

	@Override
	public String getDesc() {
		return "LINE " + first.x + " " + first.y + " " + second.x + " " + second.y + " " + getColor().getRed() + " "
				+ getColor().getGreen() + " " + getColor().getBlue();
	}

	@Override
	public void update() {
		JPanel panel = new JPanel();
		JTextField firstField = new JTextField(first.x + ", " + first.y);
		JTextField secondField = new JTextField(second.x + ", " + second.y);

		JColorArea colorArea = new JColorArea(getColor());

		panel.add(new JLabel("First:"));
		panel.add(firstField);
		panel.add(new JLabel("Second:"));
		panel.add(secondField);
		panel.add(new JLabel("Color"));
		panel.add(colorArea);

		if (JOptionPane.showConfirmDialog(null, panel) == JOptionPane.OK_OPTION) {
			int x1 = 0;
			int y1 = 0;
			int x2 = 0;
			int y2 = 0;

			try {
				x1 = Integer.parseInt(firstField.getText().split(",")[0].trim());
				y1 = Integer.parseInt(firstField.getText().split(",")[1].trim());
				x2 = Integer.parseInt(secondField.getText().split(",")[0].trim());
				y2 = Integer.parseInt(secondField.getText().split(",")[1].trim());

			} catch (NumberFormatException e) {
				return;
			}

			setColor(colorArea.getCurrentColor());
			setFirstPoint(new Point(x1, y1));
			setSecondPoint(new Point(x2, y2));
		}
	}
}
