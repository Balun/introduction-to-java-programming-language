package hr.fer.zemris.java.hw16.jvdraw;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.WindowConstants;

import hr.fer.zemris.java.hw16.jvdraw.drawing.actions.CircleAction;
import hr.fer.zemris.java.hw16.jvdraw.drawing.actions.FilledCircleAction;
import hr.fer.zemris.java.hw16.jvdraw.drawing.actions.LineAction;
import hr.fer.zemris.java.hw16.jvdraw.files.actions.ExitAction;
import hr.fer.zemris.java.hw16.jvdraw.files.actions.ExportFileAction;
import hr.fer.zemris.java.hw16.jvdraw.files.actions.OpenFileAction;
import hr.fer.zemris.java.hw16.jvdraw.files.actions.SaveAsAction;
import hr.fer.zemris.java.hw16.jvdraw.files.actions.SaveFileAction;
import hr.fer.zemris.java.hw16.jvdraw.geom.Circle;
import hr.fer.zemris.java.hw16.jvdraw.geom.FilledCircle;
import hr.fer.zemris.java.hw16.jvdraw.geom.GeometricalObject;
import hr.fer.zemris.java.hw16.jvdraw.geom.Line;
import hr.fer.zemris.java.hw16.jvdraw.model.DrawingModel;
import hr.fer.zemris.java.hw16.jvdraw.model.DrawingModelImpl;
import hr.fer.zemris.java.hw16.jvdraw.model.DrawingObjectsListModel;

/**
 * This program represents a simple GUI paint/drawing application in Java Swing.
 * The application offers drawing of the simple {@link GeometricalObject}s such
 * as {@link Circle}, {@link FilledCircle} and {@link Line}. The application
 * also offers opening and saving the content of the workspace in the custom
 * .jvd files that contains parameters of the drawn objects. It can also export
 * the content of the workspace as an .gif, .jpg or and .png file.
 * 
 * @author Matej Balun
 *
 */
public class JVDraw extends JFrame {

	/**
	 * The serial version UID for this class.
	 */
	private static final long serialVersionUID = -7622214254754846052L;

	/**
	 * The current foreground colour for the objects.
	 */
	private JColorArea foreground;

	/**
	 * The current background colour for the objects.
	 */
	private JColorArea background;

	/**
	 * THe canvas of the application
	 */
	private JDrawingCanvas canvas;

	/**
	 * The model of the application
	 */
	private DrawingModel model;

	/**
	 * The view of the collection of {@link GeometricalObject}s on the workspace
	 */
	private JList<GeometricalObject> geomList;

	/**
	 * Model of the {@link GeometricalObject} {@link JList}
	 */
	private DrawingObjectsListModel listModel;

	/**
	 * The currently specified save destination of the workspace (can be null).
	 */
	private String saveDest;

	/**
	 * Count of the circles currently on the workspace
	 */
	public static int circleCount = 0;

	/**
	 * Count of the filled circles currently on the workspace
	 */
	public static int filledCircleCount = 0;

	/**
	 * Count of the lines currently on the workspace
	 */
	public static int lineCount = 0;

	/**
	 * This constructor initialises the GUI of the application.
	 */
	public JVDraw() {
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setSize(1000, 1000);
		setLayout(new BorderLayout());

		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			// nothing
		}

		initGUI();
	}

	/**
	 * Adds the components to the frame of the aplication
	 */
	private void initGUI() {
		createToolbar();
		createCanvas();
		createMenus();
		createList();
		createBottomLabel();
	}

	/**
	 * Creates the {@link CustomLabel} for displaying the current foreground and
	 * background colour settings.
	 */
	private void createBottomLabel() {
		JPanel panel = new JPanel();
		CustomLabel label = new CustomLabel(foreground, background);

		background.addColorChangeListener(label);
		foreground.addColorChangeListener(label);

		panel.add(label);
		add(panel, BorderLayout.PAGE_END);
	}

	/**
	 * 
	 */
	private void createList() {
		listModel = new DrawingObjectsListModel(model);
		model.addDrawingModelListener(listModel);
		geomList = new JList<>(listModel);

		geomList.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					model.updateObject(geomList.getSelectedIndex());
					geomList.clearSelection();
				}
			}
		});

		JScrollPane listPane = new JScrollPane(geomList);
		listPane.setPreferredSize(new Dimension(150, 450));
		add(listPane, BorderLayout.EAST);
	}

	/**
	 * Creates the {@link JDrawingCanvas} for the application's workspace.
	 */
	private void createCanvas() {
		model = new DrawingModelImpl();
		canvas = new JDrawingCanvas(foreground, background, model);
		model.addDrawingModelListener(canvas);
		add(canvas, BorderLayout.CENTER);
	}

	/**
	 * Creates the toolbar of the application.
	 */
	private void createToolbar() {
		JToolBar tools = new JToolBar(JToolBar.HORIZONTAL);
		tools.setFloatable(true);
		tools.setLayout(new FlowLayout(FlowLayout.LEFT));

		foreground = new JColorArea(Color.ORANGE);
		background = new JColorArea(Color.GREEN);

		tools.add(foreground);
		tools.add(background);

		ButtonGroup group = new ButtonGroup();

		JButton line = new JButton("Line");
		line.addActionListener((e) -> canvas.setAction(new LineAction()));

		JButton circle = new JButton("Circle");
		circle.addActionListener((e) -> canvas.setAction(new CircleAction()));

		JButton filledCircle = new JButton("Filled circle");
		filledCircle.addActionListener((e) -> canvas.setAction(new FilledCircleAction()));

		group.add(filledCircle);
		group.add(circle);
		group.add(line);

		tools.addSeparator();

		tools.add(filledCircle);
		tools.add(line);
		tools.add(circle);

		add(tools, BorderLayout.NORTH);
	}

	/**
	 * Creates the menus of the application.
	 */
	private void createMenus() {
		JMenuBar menuBar = new JMenuBar();
		JMenu fileMenu = new JMenu("File");
		menuBar.add(fileMenu);

		JMenuItem openFile = new JMenuItem(new OpenFileAction(model, canvas, saveDest));
		fileMenu.add(openFile);

		JMenuItem saveFile = new JMenuItem(new SaveFileAction(model, canvas, saveDest));
		fileMenu.add(saveFile);

		JMenuItem saveAs = new JMenuItem(new SaveAsAction(model, canvas, saveDest));
		fileMenu.add(saveAs);

		fileMenu.addSeparator();

		JMenuItem exportFile = new JMenuItem(new ExportFileAction(model, canvas, saveDest));
		fileMenu.add(exportFile);

		fileMenu.addSeparator();

		JMenuItem exit = new JMenuItem(new ExitAction(model, canvas, saveDest));
		fileMenu.add(exit);

		setJMenuBar(menuBar);
	}

	/**
	 * The main method that starts the program.
	 * 
	 * @param args
	 *            not used in this program
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> new JVDraw().setVisible(true));
	}

}
