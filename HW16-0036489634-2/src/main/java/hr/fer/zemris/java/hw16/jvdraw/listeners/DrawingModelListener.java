package hr.fer.zemris.java.hw16.jvdraw.listeners;

import hr.fer.zemris.java.hw16.jvdraw.JVDraw;
import hr.fer.zemris.java.hw16.jvdraw.model.DrawingModel;

/**
 * This interface represents a listener for changes in the {@link DrawingModel}
 * of the {@link JVDraw} application. Classes that implement this interface must
 * implement methods for event actions in changing of the model's content
 * regarding the object array in the model.
 * 
 * @author Matej Balun
 *
 */
public interface DrawingModelListener {

	/**
	 * Defines the action in case of the addition of the new element in the
	 * model.
	 * 
	 * @param model
	 *            {@link DrawingModel} of the application
	 * @param begin
	 *            begin index of the change array
	 * @param end
	 *            end index of the change array
	 */
	public void objectsAdded(DrawingModel model, int begin, int end);

	/**
	 * Defines the action in case of the removal of the element in the model.
	 * 
	 * @param model
	 *            {@link DrawingModel} of the application
	 * @param begin
	 *            begin index of the change array
	 * @param end
	 *            end index of the change array
	 */
	public void objectsRemoved(DrawingModel model, int begin, int end);

	/**
	 * Defines the action in case of the change of the element in the model.
	 * 
	 * @param model
	 *            {@link DrawingModel} of the application
	 * @param begin
	 *            begin index of the change array
	 * @param end
	 *            end index of the change array
	 */
	public void objectsChanged(DrawingModel model, int begin, int end);
}
