package hr.fer.zemris.java.hw16.jvdraw.files.actions;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.rmi.NoSuchObjectException;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import hr.fer.zemris.java.hw16.jvdraw.JDrawingCanvas;
import hr.fer.zemris.java.hw16.jvdraw.JVDraw;
import hr.fer.zemris.java.hw16.jvdraw.geom.GeometricalObject;
import hr.fer.zemris.java.hw16.jvdraw.model.DrawingModel;

/**
 * This class represents the action for opening .jvd files and loading them into
 * the interface via {@link JVParsingUtility} class. It extends the
 * {@link AbstractFileAction} class.
 * 
 * @author Mate Balun
 *
 */
public class OpenFileAction extends AbstractFileAction {

	/**
	 * The serial version UID.
	 */
	private static final long serialVersionUID = -3101378578429183976L;

	/**
	 * Description of the action.
	 */
	private static final String DESC = "Open the specified file";

	/**
	 * 
	 * @param model
	 *            {@link DrawingModel} of the application
	 * @param canvas
	 *            {@link JDrawingCanvas} of the application
	 * @param saveDest
	 *            currently specified save destination of the workspace
	 */
	public OpenFileAction(DrawingModel model, JDrawingCanvas canvas, String saveDest) {
		super("Open", "control O", KeyEvent.VK_O, DESC, model, canvas, saveDest);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (canvas.changed()) {
			int status = JOptionPane.showConfirmDialog(null, "Do you want to save the content?", "Save dialog",
					JOptionPane.YES_NO_OPTION);

			if (status == JOptionPane.YES_OPTION) {
				if (saveDest == null || !Files.exists(Paths.get(saveDest))) {
					saveDest = getSaveDest();
				}
				if (saveDest == null) {
					save();
				}
			}
		}

		openFile();
		saveDest = null;
		canvas.setChanged(false);
	}

	/**
	 * This method prompts the user with the {@link JFileChooser} to select the
	 * file to open.
	 */
	private void openFile() {
		JVDraw.circleCount = 0;
		JVDraw.filledCircleCount = 0;
		JVDraw.lineCount = 0;

		JFileChooser chooser = new JFileChooser("./");
		chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		chooser.setAcceptAllFileFilterUsed(true);
		chooser.addChoosableFileFilter(new FileNameExtensionFilter("JVD (*.jvd)", ".jvd"));
		chooser.setMultiSelectionEnabled(false);

		int status = chooser.showOpenDialog(null);

		if (status == JFileChooser.APPROVE_OPTION) {
			String openDest = chooser.getSelectedFile().getAbsolutePath();
			try {
				read(openDest);
			} catch (IOException e) {
				JOptionPane.showMessageDialog(null, "Error in reading file.");
			}
		}

		saveDest = chooser.getSelectedFile().getAbsolutePath();
	}

	/**
	 * This method reads the content of the .jvd file and bounds the created
	 * {@link GeometricalObject}s to the {@link DrawingModel}.
	 * 
	 * @param openDest
	 *            path to the destination file
	 * @throws IOException
	 *             if there is an error in reading the file.
	 */
	private void read(String openDest) throws IOException {
		List<String> lines = Files.readAllLines(Paths.get(openDest).toAbsolutePath());
		model.clear();

		int id = 0;
		for (String line : lines) {
			try {

				if (line.startsWith("CIRCLE")) {
					id = ++JVDraw.circleCount;

				} else if (line.startsWith("FCIRCLE")) {
					id = ++JVDraw.filledCircleCount;

				} else if (line.startsWith("LINE"))
					id = ++JVDraw.lineCount;

				GeometricalObject object = JVParsingUtility.getGeomObject(line, id);
				model.add(object);
			} catch (NoSuchObjectException | NumberFormatException e) {
				continue;
			}
		}
	}
}
