package hr.fer.zemris.java.hw16.jvdraw;

import java.awt.Color;

/**
 * This interface specifies the basic object that has a colour option defined.
 * Classes that implements this interface must implements the method for current
 * colour retrieval.
 * 
 * @author Matej Balun
 *
 */
public interface IColorProvider {

	/**
	 * Returns the current colour of the object
	 * 
	 * @return {@link Color} of the object.
	 */
	public Color getCurrentColor();
}
