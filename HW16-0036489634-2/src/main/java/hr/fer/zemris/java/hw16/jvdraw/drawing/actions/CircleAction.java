package hr.fer.zemris.java.hw16.jvdraw.drawing.actions;

import java.awt.Color;
import java.awt.Point;

import hr.fer.zemris.java.hw16.jvdraw.JVDraw;
import hr.fer.zemris.java.hw16.jvdraw.geom.Circle;
import hr.fer.zemris.java.hw16.jvdraw.geom.GeometricalObject;

/**
 * This class represents the command for drawing a plain circle on the canvas
 * for the {@link JVDraw} application. It implements the {@link DrawingAction}
 * interface, offering a method for creation of a circle geometric object.
 * 
 * @author Matej Balun
 *
 */
public class CircleAction implements DrawingAction {

	@Override
	public GeometricalObject createGeometricObject(Point firstPoint, Point secondPoint, Color foreground,
			Color background, int id) {

		double radius = calculateRadius(firstPoint, secondPoint);
		return new Circle("circle " + id, radius, firstPoint, foreground);
	}

	/**
	 * Calculates the radius for circle-like geometric objects based on the
	 * specified points in the 2D space.
	 * 
	 * @param firstPoint
	 *            first point
	 * @param secondPoint
	 *            second point
	 * @return the calculated radius
	 */
	protected double calculateRadius(Point firstPoint, Point secondPoint) {
		return Math.sqrt(Math.pow(firstPoint.x - secondPoint.x, 2) + Math.pow(firstPoint.y - secondPoint.y, 2));
	}
}
