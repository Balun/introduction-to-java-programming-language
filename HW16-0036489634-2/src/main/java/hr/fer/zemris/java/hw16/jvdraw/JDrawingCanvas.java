package hr.fer.zemris.java.hw16.jvdraw;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JComponent;

import hr.fer.zemris.java.hw16.jvdraw.drawing.actions.CircleAction;
import hr.fer.zemris.java.hw16.jvdraw.drawing.actions.DrawingAction;
import hr.fer.zemris.java.hw16.jvdraw.drawing.actions.FilledCircleAction;
import hr.fer.zemris.java.hw16.jvdraw.geom.Bounds;
import hr.fer.zemris.java.hw16.jvdraw.geom.GeometricalObject;
import hr.fer.zemris.java.hw16.jvdraw.listeners.DrawingModelListener;
import hr.fer.zemris.java.hw16.jvdraw.model.DrawingModel;

/**
 * This class represents the canvas of the {@link JVDraw} application. The
 * canvas serves as a container for all graphical objects in the application,
 * representing the graphical view of the model. It Extends the
 * {@link JComponent} class and implements the {@link DrawingModelListener}
 * interface.
 * 
 * @author Matej Balun
 *
 */
public class JDrawingCanvas extends JComponent implements DrawingModelListener {

	/**
	 * The serial version UID.
	 */
	private static final long serialVersionUID = -4400487729058092665L;

	/**
	 * First point of the current {@link GeometricalObject}.
	 */
	private Point firstPoint;

	/**
	 * Second point of the current {@link GeometricalObject}.
	 */
	private Point secondPoint;

	/**
	 * Currently selected {@link GeometricalObject}.
	 */
	private GeometricalObject currentObject;

	/**
	 * Model of the application.
	 */
	private DrawingModel model;

	/**
	 * The foreground {@link IColorProvider}
	 */
	private IColorProvider foreground;

	/**
	 * The background {@link IColorProvider}
	 */
	private IColorProvider background;

	/**
	 * Flag for change status of the canvas
	 */
	private boolean changed;

	/**
	 * Currently selected {@link DrawingAction}.
	 */
	private DrawingAction action;

	/**
	 * Id of the current {@link GeometricalObject}.
	 */
	private int id;

	/**
	 * This constructor initialises the {@link JDrawingCanvas} with its
	 * specified parameters. It also registers {@link MouseListener} and
	 * {@link MouseMotionListener} for the drawing of the
	 * {@link GeometricalObject}.
	 * 
	 * @param foreground
	 *            default foreground colour
	 * @param background
	 *            default background colour
	 * @param model
	 *            {@link DrawingModel} of the canvas
	 */
	public JDrawingCanvas(IColorProvider foreground, IColorProvider background, DrawingModel model) {
		this.foreground = foreground;
		this.background = background;
		this.model = model;
		this.id = 0;
		this.changed = false;

		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (action != null) {
					if (firstPoint == null) {
						firstPoint = e.getPoint();

					} else {
						secondPoint = e.getPoint();
						createGeomObject(true);
					}
				}
			}
		});

		addMouseMotionListener(new MouseAdapter() {
			@Override
			public void mouseMoved(MouseEvent e) {
				if (firstPoint != null) {
					secondPoint = e.getPoint();
					createGeomObject(false);
				}
			}
		});
	}

	@Override
	protected void paintComponent(Graphics graphics) {
		graphics.setColor(Color.WHITE);
		graphics.fillRect(0, 0, getWidth(), getHeight());

		if (model != null) {
			for (int i = 0; i < model.getSize(); i++) {
				model.getObject(i).draw(graphics, new Bounds(0, 0, 0, 0));
			}
		}

		if (currentObject != null) {
			currentObject.draw(graphics, new Bounds(0, 0, 0, 0));
		}
	}

	/**
	 * Creates the {@link GeometricalObject} via the specified
	 * {@link DrawingAction}.
	 * 
	 * @param isFinal
	 *            true if the creation of {@link GeometricalObject} is final,
	 *            otherwise false.
	 */
	private void createGeomObject(boolean isFinal) {
		if (isFinal) {

			if (action instanceof FilledCircleAction) {
				JVDraw.filledCircleCount++;
				id = JVDraw.filledCircleCount;

			} else if (action instanceof CircleAction) {
				JVDraw.circleCount++;
				id = JVDraw.circleCount;

			} else {
				JVDraw.lineCount++;
				id = JVDraw.lineCount;
			}
		}

		currentObject = action.createGeometricObject(firstPoint, secondPoint, foreground.getCurrentColor(),
				background.getCurrentColor(), id);

		if (isFinal) {
			model.add(currentObject);
			firstPoint = null;
			secondPoint = null;
			currentObject = null;

		} else {
			repaint();
		}
	}

	@Override
	public void objectsAdded(DrawingModel model, int begin, int end) {
		changed = true;
		repaint();
	}

	@Override
	public void objectsRemoved(DrawingModel model, int begin, int end) {
		changed = true;
		repaint();
	}

	@Override
	public void objectsChanged(DrawingModel odel, int begin, int end) {
		changed = true;
		repaint();
	}

	/**
	 * Sets the currently selected {@link DrawingAction} of the canvas.
	 * 
	 * @param action
	 *            the specified {@link DrawingAction}
	 */
	public void setAction(DrawingAction action) {
		this.action = action;
		firstPoint = null;
		secondPoint = null;
		currentObject = null;
		id = 0;
	}

	/**
	 * Returns the change status of the canvas for saving the workspace.
	 * 
	 * @return true if the canvas has changed, otherwise false.
	 */
	public boolean changed() {
		return changed;
	}

	/**
	 * Sets the change status of the canvas.
	 * 
	 * @param changed
	 *            the specified change status
	 */
	public void setChanged(boolean changed) {
		this.changed = changed;
	}
}
