package hr.fer.zemris.java.hw16.jvdraw.files.actions;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.filechooser.FileNameExtensionFilter;

import hr.fer.zemris.java.hw16.jvdraw.JDrawingCanvas;
import hr.fer.zemris.java.hw16.jvdraw.JVDraw;
import hr.fer.zemris.java.hw16.jvdraw.geom.GeometricalObject;
import hr.fer.zemris.java.hw16.jvdraw.model.DrawingModel;

/**
 * This class represents the general action for manipulating with files and data
 * for the {@link JVDraw} application. It extends the {@link AbstractAction}
 * class, creating the package protected attributes for the class that implement
 * the concrete implementation of the application's actions. It offers also
 * package protected methods for saving options.
 * 
 * @author Matej Balun
 *
 */
public abstract class AbstractFileAction extends AbstractAction {

	/**
	 * The serial version UID for this class.
	 */
	private static final long serialVersionUID = 8761708524246469145L;

	/**
	 * Reference to the {@link DrawingModel} of the application.
	 */
	protected DrawingModel model;

	/**
	 * The {@link JDrawingCanvas} of the application.
	 */
	protected JDrawingCanvas canvas;

	/**
	 * The currently specified save destination for the application.
	 */
	protected String saveDest;

	/**
	 * The default extension for the files of the application.
	 */
	protected static final String EXTENSION = ".jvd";

	/**
	 * This constructor initialises the {@link AbstractFileAction} with its
	 * specified parameters.
	 * 
	 * @param name
	 *            name of the command
	 * @param keyStroke
	 *            the default key stroke for the command
	 * @param keyEvent
	 *            the default key event for the command
	 * @param desc
	 *            description of the command
	 * @param model
	 *            {@link DrawingModel} of the application
	 * @param canvas
	 *            {@link JDrawingCanvas} of the application
	 * @param saveDest
	 *            currently specified save destination for the application.
	 */
	public AbstractFileAction(String name, String keyStroke, int keyEvent, String desc, DrawingModel model,
			JDrawingCanvas canvas, String saveDest) {

		putValue(Action.NAME, name);
		putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(keyStroke));
		putValue(Action.MNEMONIC_KEY, keyEvent);
		putValue(Action.SHORT_DESCRIPTION, desc);

		this.model = model;
		this.canvas = canvas;
		this.saveDest = saveDest;
	}

	/**
	 * This method saves the current content of the workspace of the
	 * {@link JVDraw} application. It saves the content of the workspace based
	 * on the {@link GeometricalObject} descriptions and parameters.
	 */
	protected void save() {
		List<String> lines = new ArrayList<>();

		for (int i = 0; i < model.getSize(); i++) {
			String desc = model.getObject(i).getDesc();
			lines.add(desc);
		}

		try {
			Files.write(Paths.get(saveDest), lines, StandardCharsets.UTF_8);
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Unable to save file");
		}
	}

	/**
	 * This method prompts the user to select the save destination for the
	 * current content of the workspace.
	 * 
	 * @return path to the selected save destination
	 */
	protected String getSaveDest() {
		JFileChooser chooser = new JFileChooser("./");
		chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		chooser.setAcceptAllFileFilterUsed(true);
		chooser.addChoosableFileFilter(new FileNameExtensionFilter("JVD (*.jvd)", ".jvd"));
		chooser.setMultiSelectionEnabled(false);

		int status = chooser.showSaveDialog(null);

		if (status == JFileChooser.APPROVE_OPTION) {
			if (overwriteWarning(chooser.getSelectedFile().toPath())) {
				return generateExtension(chooser.getSelectedFile().getAbsolutePath(), EXTENSION);
			}
		}

		return null;
	}

	/**
	 * This method takes the file path of the specified file and checks if he
	 * file has the extension specified in the parameter. If not, it
	 * concatenates the extension on the file name.
	 * 
	 * @param fileName
	 *            the specified
	 * @param extension
	 *            the specified extension for checking
	 * @return the correct file name
	 */
	protected String generateExtension(String fileName, String extension) {
		if (fileName.endsWith(extension) || fileName.endsWith(extension.toUpperCase())) {
			return fileName;
		}

		return fileName + extension;
	}

	/**
	 * This method checks if there is a potential overwriting of the file with
	 * the specified path. If it is, it prompts the user for the overwrite
	 * approval.
	 * 
	 * @param filePath
	 *            the specified file path
	 * @return true if its' approved to save file, otherwise false.
	 */
	protected boolean overwriteWarning(Path filePath) {
		if (Files.exists(filePath)) {
			int choice = JOptionPane.showConfirmDialog(null, "The specified file already exists, overwrite?",
					"Overwrite warning", JOptionPane.YES_NO_OPTION);
			if (choice != JOptionPane.YES_OPTION) {
				return false;
			}
		}

		return true;
	}
}
