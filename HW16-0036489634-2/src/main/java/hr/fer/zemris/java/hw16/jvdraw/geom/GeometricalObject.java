package hr.fer.zemris.java.hw16.jvdraw.geom;

import java.awt.Color;
import java.awt.Graphics;

import hr.fer.zemris.java.hw16.jvdraw.JVDraw;

/**
 * This abstract class represents a general geometric object model for the
 * {@link JVDraw} application. Classes the extend this class must implement the
 * specific method for drawing and updating the object, along with dimensios of
 * the objects.
 * 
 * @author Matej Balun
 *
 */
public abstract class GeometricalObject {

	/**
	 * Name of the geometric object
	 */
	private String name;

	/**
	 * Default colour of the {@link GeometricalObject} object
	 */
	private Color color;

	/**
	 * This constructor initialises the object with its name.
	 * 
	 * @param name
	 *            name of the {@link GeometricalObject}
	 */
	public GeometricalObject(String name) {
		this.name = name;
	}

	/**
	 * This constructor initialises the {@link GeometricalObject} with its name
	 * and default colour.
	 * 
	 * @param name
	 *            name of the object
	 * @param color
	 *            colour of the object
	 */
	public GeometricalObject(String name, Color color) {
		this(name);
		this.color = color;
	}

	/**
	 * Sets the colour of the {@link GeometricalObject} object.
	 * 
	 * @param color
	 *            the specified colour
	 */
	public void setColor(Color color) {
		this.color = color;
	}

	/**
	 * Returns the colour of the object
	 * 
	 * @return colour of the object
	 */
	public Color getColor() {
		return color;
	}

	@Override
	public String toString() {
		return name;
	}

	/**
	 * This method draws the current sate of the {@link GeometricalObject} on
	 * the {@link Graphics} with the specified {@link Bounds}.
	 * 
	 * @param graphics
	 *            the specified {@link Graphics}
	 * @param bounds
	 *            the specified bounding box
	 */
	public abstract void draw(Graphics graphics, Bounds bounds);

	/**
	 * This method updates the graphical representation of the object.
	 */
	public abstract void update();

	/**
	 * initialises the specified bounding box of the object.
	 * 
	 * @param bounds
	 *            the specified bounding box
	 */
	public abstract void getBounds(Bounds bounds);

	/**
	 * Returns the bounding box of this {@link GeometricalObject}.
	 * 
	 * @return the bounding box of the object
	 */
	public abstract Bounds getBounds();

	/**
	 * Returns the description of the {@link GeometricalObject}.
	 * 
	 * @return description of the {@link GeometricalObject}.
	 */
	public abstract String getDesc();
}
