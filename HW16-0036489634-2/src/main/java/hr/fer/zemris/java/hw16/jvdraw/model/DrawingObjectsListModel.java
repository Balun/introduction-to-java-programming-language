package hr.fer.zemris.java.hw16.jvdraw.model;

import javax.swing.AbstractListModel;
import javax.swing.JList;

import hr.fer.zemris.java.hw16.jvdraw.JVDraw;
import hr.fer.zemris.java.hw16.jvdraw.geom.GeometricalObject;
import hr.fer.zemris.java.hw16.jvdraw.listeners.DrawingModelListener;

/**
 * This class represents the model for the {@link JList} view object in the
 * {@link JVDraw} application. The model holds a reference to the
 * {@link DrawingModel} of the application, extending the
 * {@link AbstractListModel} class and {@link DrawingModelListener},
 * implementing all their methods.
 * 
 * @author Matej Balun
 *
 */
public class DrawingObjectsListModel extends AbstractListModel<GeometricalObject> implements DrawingModelListener {

	/**
	 * The serial version UID for this class
	 */
	private static final long serialVersionUID = 8601407411097619934L;
	/**
	 * Reference to the drawing model of the application
	 */
	private DrawingModel model;

	/**
	 * Initialises the {@link DrawingObjectsListModel} with its specified
	 * {@link DrawingModel} object.
	 * 
	 * @param model
	 *            {@link DrawingModel} of the application
	 */
	public DrawingObjectsListModel(DrawingModel model) {
		this.model = model;
	}

	public void objectsAdded(DrawingModel model, int begin, int end) {
		fireIntervalAdded(this, begin, end);
	}

	public void objectsRemoved(DrawingModel model, int begin, int end) {
		fireIntervalRemoved(this, begin, end);
	}

	public void objectsChanged(DrawingModel odel, int begin, int end) {
		fireContentsChanged(this, begin, end);
	}

	public int getSize() {
		return model.getSize();
	}

	public GeometricalObject getElementAt(int index) {
		return model.getObject(index);
	}

}
