package hr.fer.zemris.java.hw16.jvdraw.files.actions;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.nio.file.Files;
import java.nio.file.Paths;

import hr.fer.zemris.java.hw16.jvdraw.JDrawingCanvas;
import hr.fer.zemris.java.hw16.jvdraw.JVDraw;
import hr.fer.zemris.java.hw16.jvdraw.model.DrawingModel;

/**
 * This class represents the standard exit action for the {@link JVDraw}
 * application. It exits the application when fired. It extends the
 * {@link AbstractFileAction}
 * 
 * @author Matej Balun
 *
 */
public class ExitAction extends AbstractFileAction {

	/**
	 * The serial version UID for this class.
	 */
	private static final long serialVersionUID = -8896082221779434428L;

	/**
	 * Descripton of the action.
	 */
	private static final String DESC = "Exit the application.";

	/**
	 * Initialises the {@link ExitAction} with its parameters and passes name,
	 * hot keys and description to the super constructor
	 * {@link AbstractFileAction}.
	 * 
	 * @param model
	 *            {@link DrawingModel} of the application.
	 * @param canvas
	 *            {@link JDrawingCanvas} of the application
	 * @param saveDest
	 *            the currently specified save destination of the workspace
	 */
	public ExitAction(DrawingModel model, JDrawingCanvas canvas, String saveDest) {
		super("Exit", "alt F4", KeyEvent.VK_F4, DESC, model, canvas, saveDest);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (canvas.changed()) {
			if (saveDest == null || !Files.exists(Paths.get(saveDest))) {
				saveDest = getSaveDest();
			}
		}
		System.exit(0);
	}
}
