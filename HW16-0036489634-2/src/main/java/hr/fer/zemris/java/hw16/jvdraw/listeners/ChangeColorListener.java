package hr.fer.zemris.java.hw16.jvdraw.listeners;

import java.awt.Color;

import hr.fer.zemris.java.hw16.jvdraw.IColorProvider;
import hr.fer.zemris.java.hw16.jvdraw.JVDraw;

/**
 * This interface specifies the listener for changing of the colour in colour
 * objects in the {@link JVDraw} application. Classes this interface must
 * implement the method for determine the action in the colour change event.
 * 
 * @author Matej Balun
 *
 */
public interface ChangeColorListener {

	/**
	 * This method executes the defined action in the case of the color change
	 * event by the {@link IColorProvider} object.
	 * 
	 * @param provider
	 *            {@link IColorProvider} source of the colour change
	 * @param oldColor
	 *            old colour of the provider
	 * @param newColor
	 *            new colour of the provider
	 */
	public void newColorSelected(IColorProvider provider, Color oldColor, Color newColor);

}
