package hr.fer.zemris.java.hw16.jvdraw.model;

import hr.fer.zemris.java.hw16.jvdraw.JVDraw;
import hr.fer.zemris.java.hw16.jvdraw.geom.GeometricalObject;
import hr.fer.zemris.java.hw16.jvdraw.listeners.DrawingModelListener;

/**
 * This interface represents the general model of the {@link JVDraw} application
 * regarding the MVC of the application. It defines methods for addition,
 * removal and change of the elements in the model, as well as for registration
 * of the {@link DrawingModelListener}s as the view components.
 * 
 * @author Matej Balun
 *
 */
public interface DrawingModel {

	/**
	 * Returns the size of the model's collection.
	 * 
	 * @return size of the object array
	 */
	public int getSize();

	/**
	 * Returns the {@link GeometricalObject} with the specified index
	 * 
	 * @param index
	 *            index of the object
	 * @return the specified object
	 */
	public GeometricalObject getObject(int index);

	/**
	 * Removes the object with the specified index from the model's collection.
	 * 
	 * @param index
	 *            index of the specified model.
	 */
	public void removeObject(int index);

	/**
	 * This method updates the object on the specified index in the model's
	 * collection.
	 * 
	 * @param index
	 *            index of the specified {@link GeometricalObject}.
	 */
	public void updateObject(int index);

	/**
	 * This method clears the model's collection of the
	 * {@link GeometricalObject}s
	 */
	public void clear();

	/**
	 * Adds the specified {@link GeometricalObject} to the model's collection.
	 * 
	 * @param object
	 *            the specified {@link GeometricalObject}
	 */
	public void add(GeometricalObject object);

	/**
	 * Unregisters the specified listener from the model.
	 * 
	 * @param listener
	 *            the specified {@link DrawingModelListener}
	 */
	public void addDrawingModelListener(DrawingModelListener listener);

	/**
	 * Registers a listener to this model.
	 * 
	 * @param listener
	 *            specified {@link DrawingModelListener}
	 */
	public void removeDrawingModelListener(DrawingModelListener listener);
}
