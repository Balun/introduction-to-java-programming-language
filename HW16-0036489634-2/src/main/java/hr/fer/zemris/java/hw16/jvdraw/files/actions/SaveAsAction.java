package hr.fer.zemris.java.hw16.jvdraw.files.actions;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import hr.fer.zemris.java.hw16.jvdraw.JDrawingCanvas;
import hr.fer.zemris.java.hw16.jvdraw.model.DrawingModel;

/**
 * This class represents the action for saving the current content of the
 * workspace under a specific file name. It extends the
 * {@link AbstractFileAction} class.
 * 
 * @author Matej Balun
 *
 */
public class SaveAsAction extends AbstractFileAction {

	/**
	 * The serial version UID for this class.
	 */
	private static final long serialVersionUID = -8517796710873888712L;

	/**
	 * Description of the action
	 */
	private static final String DESC = "Save the file under specified name";

	/**
	 * This constructor initialises the {@link SaveAsAction} with its specified
	 * parameters.
	 * 
	 * @param model
	 *            {@link DrawingModel} of the application
	 * @param canvas
	 *            {@link JDrawingCanvas} of the application
	 * @param saveDest
	 *            currently specified save destination of the workspace
	 */
	public SaveAsAction(DrawingModel model, JDrawingCanvas canvas, String saveDest) {
		super("Save as", "control alt S", KeyEvent.VK_A, DESC, model, canvas, saveDest);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		saveDest = getSaveDest();

		if (saveDest == null) {
			return;
		}
		save();
		canvas.setChanged(false);
	}
}
