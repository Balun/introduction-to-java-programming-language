package hr.fer.zemris.java.hw16.jvdraw.model;

import java.util.ArrayList;
import java.util.List;

import hr.fer.zemris.java.hw16.jvdraw.JVDraw;
import hr.fer.zemris.java.hw16.jvdraw.geom.GeometricalObject;
import hr.fer.zemris.java.hw16.jvdraw.listeners.DrawingModelListener;

/**
 * This class represents the model in the {@link JVDraw} application in the MVC
 * pattern. It implements the {@link DrawingModel} interface, implementing all
 * its methods for {@link GeometricalObject} and {@link DrawingModelListener}
 * manipulation.
 * 
 * @author atej Balun
 *
 */
public class DrawingModelImpl implements DrawingModel {

	/**
	 * List of the model's {@link GeometricalObject}s.
	 */
	private List<GeometricalObject> objects;

	/**
	 * list of the registered {@link DrawingModelListener}s.
	 */
	private List<DrawingModelListener> listeners;

	/**
	 * Initialises the {@link DrawingModelImpl}.
	 */
	public DrawingModelImpl() {
		this.objects = new ArrayList<>();
		this.listeners = new ArrayList<>();
	}

	public GeometricalObject getObject(int index) {
		return objects.get(index);
	}

	public void removeObject(int index) {
		objects.remove(index);
		intervalRemoved(index, index);
	}

	/**
	 * Informs all the registered listeners of the removal interval of
	 * {@link GeometricalObject} in the model.
	 * 
	 * @param index1
	 *            begin of the interval
	 * @param index2
	 *            end of the interval
	 */
	private void intervalRemoved(int index1, int index2) {
		for (DrawingModelListener listener : listeners) {
			listener.objectsRemoved(this, index1, index2);
		}
	}

	public void updateObject(int index) {
		objects.get(index).update();
		intervalChanged(index, index);
	}

	/**
	 * Informs all the registered listeners of the change interval of
	 * {@link GeometricalObject} in the model.
	 * 
	 * @param index1
	 *            begin of the interval
	 * @param index2
	 *            end of the interval
	 */
	private void intervalChanged(int index1, int index2) {
		for (DrawingModelListener listener : listeners) {
			listener.objectsChanged(this, index1, index2);
		}
	}

	public void clear() {
		objects.clear();
	}

	public void add(GeometricalObject object) {
		int index = objects.size();
		objects.add(object);
		intervalAdded(index, index);
	}

	/**
	 * Informs all the registered listeners of the addition interval of
	 * {@link GeometricalObject} in the model.
	 * 
	 * @param index1
	 *            begin of the interval
	 * @param index2
	 *            end of the interval
	 */
	private void intervalAdded(int index1, int index2) {
		for (DrawingModelListener listener : listeners) {
			listener.objectsAdded(this, index1, index2);
		}
	}

	public void addDrawingModelListener(DrawingModelListener listener) {
		if (!listeners.contains(listener)) {
			listeners.add(listener);
		}
	}

	public int getSize() {
		return objects.size();
	}

	@Override
	public void removeDrawingModelListener(DrawingModelListener listener) {
		if (listeners.contains(listener)) {
			listeners.remove(listener);
		}
	}

}
