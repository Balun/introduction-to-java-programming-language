package hr.fer.zemris.java.hw16.jvdraw.files.actions;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import hr.fer.zemris.java.hw16.jvdraw.JDrawingCanvas;
import hr.fer.zemris.java.hw16.jvdraw.geom.Bounds;
import hr.fer.zemris.java.hw16.jvdraw.model.DrawingModel;

/**
 * This class represents the action for exporting current content of the
 * workspace to the specified picture formats. The provided picture formats are
 * .jpg, .png, .gif. It extends the {@link AbstractFileAction} class.
 * 
 * @author Matej Balun
 *
 */
public class ExportFileAction extends AbstractFileAction {

	/**
	 * The serial version UID for this class.
	 */
	private static final long serialVersionUID = 3611248165278778016L;

	/**
	 * Description of the action
	 */
	private static final String DESC = "Export the current file";

	/**
	 * The .png file extension
	 */
	private static final FileNameExtensionFilter PNG = new FileNameExtensionFilter("PNG (*.png)", ".png");

	/**
	 * The .gif file extension
	 */
	private static final FileNameExtensionFilter GIF = new FileNameExtensionFilter("GIF (*.gif)", ".gif");

	/**
	 * The .jpg file extension
	 */
	private static final FileNameExtensionFilter JPG = new FileNameExtensionFilter("JPG (*.jpg)", ".jpg");

	/**
	 * This constructor initialises the {@link ExportFileAction} with its
	 * specified parameters. It passes the name and the description with hot
	 * keys to the super constructor {@link AbstractFileAction}.
	 * 
	 * @param model
	 *            {@link DrawingModel} of the application
	 * @param canvas
	 *            {@link JDrawingCanvas} of the application
	 * @param saveDest
	 *            currently specified save destination of the workspace
	 */
	public ExportFileAction(DrawingModel model, JDrawingCanvas canvas, String saveDest) {
		super("Export", "control E", KeyEvent.VK_E, DESC, model, canvas, saveDest);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Bounds boundBox = new Bounds(canvas.getWidth(), 0, canvas.getWidth(), 0);

		for (int i = 0; i < model.getSize(); i++) {
			model.getObject(i).getBounds(boundBox);
		}

		JFileChooser chooser = new JFileChooser("./");

		chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		chooser.setAcceptAllFileFilterUsed(true);
		chooser.addChoosableFileFilter(PNG);
		chooser.addChoosableFileFilter(GIF);
		chooser.addChoosableFileFilter(JPG);
		chooser.setMultiSelectionEnabled(false);
		chooser.setFileFilter(PNG);

		chooser.setDialogTitle("Export file");
		int status = chooser.showDialog(null, "export");

		if (status == JFileChooser.APPROVE_OPTION) {

			String fileName = chooser.getSelectedFile().getAbsolutePath();

			if (overwriteWarning(Paths.get(fileName).toAbsolutePath())) {

				int width = Math.abs(boundBox.getMaxX() - boundBox.getMinX());
				int height = Math.abs(boundBox.getMaxY() - boundBox.getMinY());

				BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_3BYTE_BGR);
				Graphics g = image.createGraphics();
				g.setColor(Color.WHITE);
				g.fillRect(0, 0, width, height);

				for (int i = 0; i < model.getSize(); i++) {
					model.getObject(i).draw(g, boundBox);
				}

				g.dispose();

				createImage(chooser, fileName, image);
			}
		}
	}

	/**
	 * This method creates the image based on the extension specified in the
	 * {@link JFileChooser} object with the specified file path.
	 * 
	 * @param chooser
	 *            the specified {@link JFileChooser}
	 * @param fileName
	 *            name of the image.
	 * @param image
	 *            the specified image.
	 */
	private void createImage(JFileChooser chooser, String fileName, BufferedImage image) {
		try {
			if (chooser.getFileFilter().equals(PNG)) {
				fileName = generateExtension(fileName, ".png");
				ImageIO.write(image, "png", new File(fileName));

			} else if (chooser.getFileFilter().equals(GIF)) {
				fileName = generateExtension(fileName, ".gif");
				ImageIO.write(image, "gif", new File(fileName));

			} else if (chooser.getFileFilter().equals(JPG)) {
				fileName = generateExtension(fileName, ".jpg");
				ImageIO.write(image, "jpg", new File(fileName));
			}
		} catch (IOException e1) {
			JOptionPane.showMessageDialog(null, "Could not export file.");
		}
	}
}
