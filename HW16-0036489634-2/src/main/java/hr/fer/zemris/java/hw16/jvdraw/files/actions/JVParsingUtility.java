package hr.fer.zemris.java.hw16.jvdraw.files.actions;

import java.awt.Color;
import java.awt.Point;
import java.rmi.NoSuchObjectException;

import hr.fer.zemris.java.hw16.jvdraw.geom.Circle;
import hr.fer.zemris.java.hw16.jvdraw.geom.FilledCircle;
import hr.fer.zemris.java.hw16.jvdraw.geom.GeometricalObject;
import hr.fer.zemris.java.hw16.jvdraw.geom.Line;

/**
 * This utility class offers functionality to create aspecified
 * {@link GeometricalObject} based on the specified description and parametrs of
 * the wanted object.
 * 
 * @author Matej Balun
 *
 */
public class JVParsingUtility {

	/**
	 * The line string constant.
	 */
	private static final String LINE = "LINE";

	/**
	 * the circle string constant
	 */
	private static final String CIRCLE = "CIRCLE";

	/**
	 * The filled circle string constant
	 */
	private static final String FCIRCLE = "FCIRCLE";

	/**
	 * Private constructor for disabling the class instantiation.
	 */
	private JVParsingUtility() {
	}

	/**
	 * Returns the new instance of specified {@link GeometricalObject} based on
	 * the defined parameters of the object.
	 * 
	 * @param line
	 *            string describing the parameters of the concrete
	 *            {@link GeometricalObject} object
	 * @param id
	 *            id of the object
	 * @return new instance of {@link GeometricalObject}
	 * @throws NoSuchObjectException
	 *             if the specified description does not match any of the
	 *             provided {@link GeometricalObject}s
	 * @throws NumberFormatException
	 *             if there was an erro in parameter parsing
	 */
	public static GeometricalObject getGeomObject(String line, int id)
			throws NoSuchObjectException, NumberFormatException {
		try {
			if (line.toUpperCase().startsWith(LINE)) {
				return getLine(line, id);

			} else if (line.toUpperCase().startsWith(CIRCLE)) {
				return getCircle(line, id);

			} else if (line.toUpperCase().startsWith(FCIRCLE)) {
				return getFilledCircle(line, id);
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new NumberFormatException("Illegal number of argments in line: \"" + line + "\".");
		}

		throw new NoSuchObjectException("Illegal gemotric object name type in file.");
	}

	/**
	 * Returns the new instance of {@link FilledCircle}.
	 * 
	 * @param line
	 *            string describing the parameters of the concrete
	 *            {@link GeometricalObject} object
	 * @param id
	 *            id of the object
	 * @return new instance of {@link FilledCircle}
	 * 
	 * @throws NumberFormatException
	 *             if there was an error in parameter parsing
	 * @throws ArrayIndexOutOfBoundsException
	 *             if the number of specified parameters in description is
	 *             incorrect.
	 */
	private static FilledCircle getFilledCircle(String line, int id)
			throws NumberFormatException, ArrayIndexOutOfBoundsException {

		line = line.substring(line.indexOf(" ") + 1);
		String[] args = line.split(" ");

		return new FilledCircle("filled circle " + id, Double.parseDouble(args[2]),
				new Point(Integer.parseInt(args[0]), Integer.parseInt(args[1])),
				new Color(Integer.parseInt(args[3]), Integer.parseInt(args[4]), Integer.parseInt(args[5])),
				new Color(Integer.parseInt(args[6]), Integer.parseInt(args[7]), Integer.parseInt(args[8])));
	}

	/**
	 * Returns the new instance of {@link Circle}
	 * 
	 * @param line
	 *            string describing the parameters of the concrete
	 *            {@link GeometricalObject} object
	 * @param id
	 *            id of the object
	 * @return new instance of {@link Circle}
	 * 
	 * @throws NumberFormatException
	 *             if there was an error in parameter parsing
	 * @throws ArrayIndexOutOfBoundsException
	 *             if the number of specified parameters in description is
	 *             incorrect.
	 */
	private static Circle getCircle(String line, int id) throws NumberFormatException, ArrayIndexOutOfBoundsException {
		line = line.substring(line.indexOf(" ") + 1);
		String[] args = line.split(" ");

		return new Circle("circle " + id, Double.parseDouble(args[2]),
				new Point(Integer.parseInt(args[0]), Integer.parseInt(args[1])),
				new Color(Integer.parseInt(args[3]), Integer.parseInt(args[4]), Integer.parseInt(args[5])));
	}

	/**
	 * Returns the new instance of {@link Line}.
	 * 
	 * @param line
	 *            string describing the parameters of the concrete
	 *            {@link GeometricalObject} object
	 * @param id
	 *            id of the object
	 * @return new instance of {@link FilledCircle}
	 * 
	 * @throws NumberFormatException
	 *             if there was an error in parameter parsing
	 * @throws ArrayIndexOutOfBoundsException
	 *             if the number of specified parameters in description is
	 *             incorrect.
	 */
	private static Line getLine(String line, int id) throws NumberFormatException, ArrayIndexOutOfBoundsException {
		line = line.substring(line.indexOf(" ") + 1);
		String[] args = line.split(" ");

		return new Line("line " + id, new Point(Integer.parseInt(args[0]), Integer.parseInt(args[1])),
				new Point(Integer.parseInt(args[2]), Integer.parseInt(args[3])),
				new Color(Integer.parseInt(args[4]), Integer.parseInt(args[5]), Integer.parseInt(args[6])));
	}

}
