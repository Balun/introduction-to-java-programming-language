package hr.fer.zemris.java.hw16.jvdraw.geom;

import hr.fer.zemris.java.hw16.jvdraw.JVDraw;

/**
 * This class represent a simple bounding box for the {@link GeometricalObject}s
 * in the {@link JVDraw} application. It calculates the smallest possible
 * bounding box for the specified {@link GeometricalObject} for eventual object
 * export in the picture file format.
 * 
 * @author Matej Balun
 *
 */
public class Bounds {

	/**
	 * Minimal x-axis value
	 */
	private int minX;

	/**
	 * Maximum x-axis value
	 */
	private int maxX;

	/**
	 * minimum y-axis value
	 */
	private int minY;

	/**
	 * maximal y-axis value
	 */
	private int maxY;

	/**
	 * Initialises the {@link Bounds} with its specified parameters
	 * 
	 * @param minX
	 *            minimum x-axis value
	 * @param maxX
	 *            maximum x-axis value
	 * @param minY
	 *            minimum y-axis value
	 * @param maxY
	 *            maximum y-axis value
	 */
	public Bounds(int minX, int maxX, int minY, int maxY) {
		super();
		this.minX = minX;
		this.maxX = maxX;
		this.minY = minY;
		this.maxY = maxY;
	}

	/**
	 * Returns the minimum x-axis value.
	 * 
	 * @return minimum x-axis value
	 */
	public int getMinX() {
		return minX;
	}

	/**
	 * Determines the minimum x-axis value
	 * 
	 * @param minX
	 *            specified x-axis value
	 */
	public void setMinX(int minX) {
		this.minX = Math.min(this.minX, minX);
	}

	/**
	 * Returns the maximum x-axis value.
	 * 
	 * @return maximum x-axis value
	 */
	public int getMaxX() {
		return maxX;
	}

	/**
	 * Determines the maximum x-axis value
	 * 
	 * @param maxX
	 *            specified x-axis value
	 */
	public void setMaxX(int maxX) {
		this.maxX = Math.max(this.maxX, maxX);
	}

	/**
	 * Returns the minimum y-axis value
	 * 
	 * @return minimum y-axis value
	 */
	public int getMinY() {
		return minY;
	}

	/**
	 * Sets the minimum y-axis value
	 * 
	 * @param minY
	 *            minimum y-axis value
	 */
	public void setMinY(int minY) {
		this.minY = Math.min(this.minY, minY);
	}

	/**
	 * Returns the maximum y-axis value
	 * 
	 * @return maximum y-axis value
	 */
	public int getMaxY() {
		return maxY;
	}

	/**
	 * Determines the maximum y-axis value.
	 * 
	 * @param maxY
	 *            specified y-axis value
	 */
	public void setMaxY(int maxY) {
		this.maxY = Math.max(this.maxY, maxY);
	}

	@Override
	public String toString() {
		return "x=" + minX + ", X=" + maxX + ", y=" + minY + ", Y=" + maxY;
	}
}
