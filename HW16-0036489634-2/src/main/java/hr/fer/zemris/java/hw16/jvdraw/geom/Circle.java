package hr.fer.zemris.java.hw16.jvdraw.geom;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import hr.fer.zemris.java.hw16.jvdraw.JColorArea;

/**
 * This class represents a simple circle object. It extends the
 * {@link GeometricalObject} class, implementing the method for specific object
 * drawing and graphical updating.
 * 
 * @author Matej Balun
 *
 */
public class Circle extends GeometricalObject {

	/**
	 * Radius of the circle
	 */
	protected double radius;

	/**
	 * Centre of the circle.
	 */
	protected Point center;

	/**
	 * Initialises the circle with its specified parameters.
	 * 
	 * @param name
	 *            name of the circle
	 * @param radius
	 *            radius of the circle
	 * @param center
	 *            centre of the circle
	 */
	public Circle(String name, double radius, Point center) {
		super(name);
		this.radius = radius;
		this.center = center;
	}

	/**
	 * Initialises the circle with its specified parameters and selected colour.
	 * 
	 * @param name
	 *            name of the circle
	 * @param radius
	 *            radius of the circle
	 * @param center
	 *            centre of the circle
	 * @param color
	 *            specified colour of the circle
	 */
	public Circle(String name, double radius, Point center, Color color) {
		this(name, radius, center);
		setColor(color);
	}

	@Override
	public void getBounds(Bounds bounds) {
		bounds.setMinX(center.x - (int) radius);
		bounds.setMaxX(center.x + (int) radius);
		bounds.setMinY(center.y - (int) radius);
		bounds.setMaxY(center.y + (int) radius);
	}

	@Override
	public Bounds getBounds() {
		return new Bounds(center.x - (int) radius, center.x + (int) radius, center.y - (int) radius,
				center.y + (int) radius);
	}

	/**
	 * Sets the radius of the circle
	 * 
	 * @param radius
	 *            specified radius
	 */
	public void setRadius(double radius) {
		this.radius = radius;
	}

	/**
	 * Sets the centre of the circle.
	 * 
	 * @param center
	 *            specified centre of the circle.
	 */
	public void setCenter(Point center) {
		this.center = center;
	}

	@Override
	public void draw(Graphics g, Bounds bounds) {
		g.setColor(getColor());
		g.drawOval(center.x - (int) radius - bounds.getMinX(), center.y - (int) radius - bounds.getMinY(),
				(int) (2 * radius), (int) (2 * radius));
	}

	@Override
	public String getDesc() {
		return "CIRCLE " + center.x + " " + center.y + " " + String.format("%.1f", radius) + " " + getColor().getRed()
				+ " " + getColor().getGreen() + " " + getColor().getBlue();
	}

	@Override
	public void update() {
		JPanel panel = new JPanel();
		JTextField centerField = new JTextField(center.x + ", " + center.y);
		JTextField radiusField = new JTextField(Double.toString(radius));

		JColorArea colorArea = new JColorArea(getColor());

		panel.add(new JLabel("Center:"));
		panel.add(centerField);
		panel.add(new JLabel("Radius:"));
		panel.add(radiusField);
		panel.add(new JLabel("Color"));
		panel.add(colorArea);

		if (JOptionPane.showConfirmDialog(null, panel) == JOptionPane.OK_OPTION) {
			int x = 0;
			int y = 0;
			double radiusNew = 0;
			try {
				x = Integer.parseInt(centerField.getText().split(",")[0].trim());
				y = Integer.parseInt(centerField.getText().split(",")[1].trim());
				radiusNew = Double.parseDouble(radiusField.getText().trim());

			} catch (NumberFormatException e) {
				return;
			}

			setColor(colorArea.getCurrentColor());
			setCenter(new Point(x, y));
			setRadius(radiusNew);
		}
	}
}
