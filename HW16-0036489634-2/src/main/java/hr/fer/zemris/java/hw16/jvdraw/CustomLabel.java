package hr.fer.zemris.java.hw16.jvdraw;

import java.awt.Color;

import javax.swing.JLabel;

import hr.fer.zemris.java.hw16.jvdraw.listeners.ChangeColorListener;

/**
 * This class represents the custom label for the {@link JVDraw} application. It
 * displays the currently selected foreground and background colours with their
 * RGB values. The class extends the {@link JLabel} class and implements the
 * {@link ChangeColorListener} interface.
 * 
 * @author Matej Balun
 *
 */
public class CustomLabel extends JLabel implements ChangeColorListener {

	/**
	 * The serial version UID for this class.
	 */
	private static final long serialVersionUID = -1544634457116691337L;

	/**
	 * Background colour to display
	 */
	private Color backgroundColor;

	/**
	 * Foreground colour to display
	 */
	private Color foregroundColor;

	/**
	 * The foreground {@link IColorProvider} for the label.
	 */
	private IColorProvider foregroundProvider;

	/**
	 * The background {@link IColorProvider} for the label.
	 */
	private IColorProvider backgroundProvider;

	/**
	 * This constructor initialises the {@link CustomLabel} with its specified
	 * parameters
	 * 
	 * @param foregroundProvider
	 *            {@link IColorProvider} for the foreground.
	 * @param backgroundProvider
	 *            {@link IColorProvider} for the background.
	 */
	public CustomLabel(IColorProvider foregroundProvider, IColorProvider backgroundProvider) {
		super();
		this.foregroundProvider = foregroundProvider;
		this.backgroundProvider = backgroundProvider;
		this.foregroundColor = foregroundProvider.getCurrentColor();
		this.backgroundColor = backgroundProvider.getCurrentColor();
		setText();
	}

	/**
	 * Passes the foreground and background color information to the super
	 * method.
	 */
	private void setText() {
		super.setText(String.format("Foreground color: (%d, %d, %d, args), background color: (%d, %d, %d)",
				foregroundColor.getRed(), foregroundColor.getGreen(), foregroundColor.getBlue(),
				backgroundColor.getRed(), backgroundColor.getGreen(), backgroundColor.getBlue()));
	}

	@Override
	public void newColorSelected(IColorProvider provider, Color oldColor, Color newColor) {
		if (provider == foregroundProvider && foregroundColor.equals(oldColor)) {
			foregroundColor = newColor;
		}

		if (provider == backgroundProvider && backgroundColor == oldColor) {
			backgroundColor = newColor;
		}

		setText();
	}
}
