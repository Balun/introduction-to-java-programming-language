package hr.fer.zemris.java.hw16.jvdraw.geom;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;


/**
 * This class represents the filled circle geometric object. It extends the
 * {@link Circle} class, overriding the methods specific for this geomoetric
 * object.
 * 
 * @author Matej Balun
 *
 */
public class FilledCircle extends Circle {

	/**
	 * Background of the filled circle
	 */
	private Color background;

	/**
	 * Initialises the circle with its specified parameters and selected
	 * foreground and background colour.
	 * 
	 * @param name
	 *            name of the circle
	 * @param radius
	 *            radius of the circle
	 * @param center
	 *            centre of the circle
	 * @param color
	 *            specified colour of the circle
	 * @param background
	 *            specified background of the circle
	 */
	public FilledCircle(String name, double radius, Point center, Color color, Color background) {
		super(name, radius, center, color);
		this.background = background;
	}

	/**
	 * Sets the background colour of the circle
	 * 
	 * @param background
	 *            the specified background color
	 */
	public void setBackground(Color background) {
		this.background = background;
	}

	@Override
	public void draw(Graphics g, Bounds bounds) {
		g.setColor(background);
		g.fillOval(center.x - (int) radius - bounds.getMinX(), center.y - (int) radius - bounds.getMinY(),
				(int) (2 * radius), (int) (2 * radius));
		super.draw(g, bounds);
	}

	@Override
	public String getDesc() {
		return "FCIRCLE " + center.x + " " + center.y + " " + String.format("%.1f", radius) + " " + getColor().getRed()
				+ " " + getColor().getGreen() + " " + getColor().getBlue() + " " + background.getRed() + " "
				+ background.getGreen() + " " + background.getBlue();
	}

	@Override
	public void update() {
		super.update();
		setBackground(background);
	}
}
