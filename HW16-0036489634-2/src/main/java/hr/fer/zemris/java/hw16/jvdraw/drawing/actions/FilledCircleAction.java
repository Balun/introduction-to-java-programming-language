package hr.fer.zemris.java.hw16.jvdraw.drawing.actions;

import java.awt.Color;
import java.awt.Point;

import hr.fer.zemris.java.hw16.jvdraw.geom.FilledCircle;
import hr.fer.zemris.java.hw16.jvdraw.geom.GeometricalObject;

/**
 * This class represents a command for the creation of
 * {@link FilledCircleAction} geometric object. It extends the
 * {@link CircleAction} class, offering the creation of the custom filled
 * circle.
 * 
 * @author Matej Balun
 *
 */
public class FilledCircleAction extends CircleAction {

	@Override
	public GeometricalObject createGeometricObject(Point firstPoint, Point secondPoint, Color foreground,
			Color background, int id) {
		return new FilledCircle("filled circle " + id, super.calculateRadius(firstPoint, secondPoint), firstPoint,
				foreground, background);
	}
}
