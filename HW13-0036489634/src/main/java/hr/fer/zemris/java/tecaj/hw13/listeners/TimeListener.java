package hr.fer.zemris.java.tecaj.hw13.listeners;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * This class represents the context listener for measuring run time of the
 * application. It implements the {@link ServletContextListener} interface.
 * 
 * @author Matej Balun
 *
 */
@WebListener
public class TimeListener implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		arg0.getServletContext().removeAttribute("time");
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		arg0.getServletContext().setAttribute("time", System.currentTimeMillis());
	}

}
