package hr.fer.zemris.java.tecaj.hw13.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import org.jfree.util.Rotation;

/**
 * This class represents a simple servlet that displays the content of the OS
 * survey on the web page with a pie chart.
 * 
 * @author Matej Balun
 *
 */
public class DrawReportServlet extends HttpServlet {

	/**
	 * The serial version UID for this class.
	 */
	private static final long serialVersionUID = -1590301520025278167L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		PieDataset dataset = createDataset();
		JFreeChart chart = createChart(dataset, "OS usage");
		byte[] bi = ChartUtilities.encodeAsPNG(chart.createBufferedImage(500, 500));
		resp.getOutputStream().write(bi);
	}

	/**
	 * This method creates the data set for the OS chart.
	 * 
	 * @return {@link PieDataset} representation of the vote data
	 */
	private PieDataset createDataset() {
		DefaultPieDataset result = new DefaultPieDataset();
		result.setValue("Linux", 29);
		result.setValue("Mac", 20);
		result.setValue("Windows", 51);
		return result;

	}

	/**
	 * This method creates a simple pie chart based on the {@link PieDataset}
	 * object.
	 * 
	 * @param dataset
	 *            the specified {@link PieDataset} for the chart
	 * @param title
	 *            title of the chart
	 * @return new instance of {@link JFreeChart}
	 */
	private JFreeChart createChart(PieDataset dataset, String title) {

		JFreeChart chart = ChartFactory.createPieChart3D(title, // chart title
				dataset, // data
				true, // include legend
				true, false);

		PiePlot3D plot = (PiePlot3D) chart.getPlot();
		plot.setStartAngle(290);
		plot.setDirection(Rotation.CLOCKWISE);
		plot.setForegroundAlpha(0.5f);
		return chart;

	}

}
