package hr.fer.zemris.java.tecaj.hw13.servlets.voting;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import org.jfree.util.Rotation;

/**
 * This class represents a simple servlet for displaying graphical results of
 * the music voting in a pie-chart format. It extends the {@link HttpServlet}
 * class.
 * 
 * @author Matej Balun
 *
 */
public class GlasanjeGrafikaServlet extends HttpServlet {

	/**
	 * The serial version UID for this class.
	 */
	private static final long serialVersionUID = 7769545304191066178L;

	@SuppressWarnings("unchecked")
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Map<String, Artist> map = (Map<String, Artist>) req.getSession().getAttribute("definition");
		Set<ArtistPoints> points = (Set<ArtistPoints>) req.getSession().getAttribute("pointSet");

		PieDataset dataset = createDataset(map, points);
		JFreeChart chart = createChart(dataset, "Rezultati glasanja");
		byte[] bi = ChartUtilities.encodeAsPNG(chart.createBufferedImage(500, 500));
		resp.setContentType("image/png");
		resp.getOutputStream().write(bi);
	}

	/**
	 * THis method creates the data set for the music chart.
	 * 
	 * @param map
	 *            the specified {@link Artist}s
	 * @param points
	 *            the specified {@link ArtistPoints}
	 * @return {@link PieDataset} representation of the vote data
	 */
	private PieDataset createDataset(Map<String, Artist> map, Set<ArtistPoints> points) {
		DefaultPieDataset result = new DefaultPieDataset();

		for (ArtistPoints point : points) {
			result.setValue(map.get(point.getID()).getName(), Integer.parseInt(point.getPoints()));
		}
		return result;

	}

	/**
	 * This method creates a simple pie char based on the {@link PieDataset}
	 * object.
	 * 
	 * @param dataset
	 *            the specified {@link PieDataset} for the chart
	 * @param title
	 *            title of the chart
	 * @return new instance of {@link JFreeChart}
	 */
	private JFreeChart createChart(PieDataset dataset, String title) {

		JFreeChart chart = ChartFactory.createPieChart3D(title, // chart title
				dataset, // data
				true, // include legend
				true, false);

		PiePlot3D plot = (PiePlot3D) chart.getPlot();
		plot.setStartAngle(290);
		plot.setDirection(Rotation.CLOCKWISE);
		plot.setForegroundAlpha(0.5f);
		return chart;
	}

}
