package hr.fer.zemris.java.tecaj.hw13.servlets.voting;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This class represents a simple servlet for the index of the music artists
 * voting application. It provides the selection of several artists with links
 * to the result page.
 * 
 * @author Matej Balun
 *
 */
public class GlasanjeServlet extends HttpServlet {

	/**
	 * The serial version UID of this class.
	 */
	private static final long serialVersionUID = -5914289551562628974L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String fileName = req.getServletContext().getRealPath("/WEB-INF/glasanje-definicija.txt");

		Map<String, Artist> map = new TreeMap<>();

		for (String line : Files.readAllLines(Paths.get(fileName))) {
			map.put(line.split("\t")[0], new Artist(line.split("\t")[0], line.split("\t")[1], line.split("\t")[2]));
		}

		req.getSession().setAttribute("definition", map);

		req.getRequestDispatcher("/WEB-INF/pages/glasanjeIndex.jsp").forward(req, resp);
	}

}
