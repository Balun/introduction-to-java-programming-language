package hr.fer.zemris.java.tecaj.hw13.servlets.voting;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 * This class represents a simple servlet for downloading the results of the
 * voting application in an Microsoft excel document form.
 * 
 * @author Matej Balun
 *
 */
public class GlasanjeXLSServlet extends HttpServlet {

	/**
	 * The serial version UID for this class.
	 */
	private static final long serialVersionUID = -3234834091565020346L;

	@SuppressWarnings("unchecked")
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Map<String, Artist> map = (Map<String, Artist>) req.getSession().getAttribute("definition");
		Set<ArtistPoints> points = (Set<ArtistPoints>) req.getSession().getAttribute("pointSet");

		HSSFWorkbook book = new HSSFWorkbook();
		HSSFSheet sheet = book.createSheet("Rezultati glasanja");

		int k = 0;
		HSSFRow rowhead = sheet.createRow(k++);
		rowhead.createCell(0).setCellValue("Ime izvođdača");
		rowhead.createCell(1).setCellValue("bodovi");

		for (ArtistPoints point : points) {
			HSSFRow row = sheet.createRow(k++);
			row.createCell(0).setCellValue(map.get(point.getID()).getName());
			row.createCell(1).setCellValue(point.getPoints());
		}

		resp.setContentType("application/vnd.ms-excel");
		book.write(resp.getOutputStream());
		book.close();

	}

}
