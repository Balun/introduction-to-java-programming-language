package hr.fer.zemris.java.tecaj.hw13.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This class represents a servlet for calculating and displaying the values of
 * trigonometric sine and cosine functions on the web page. It takes two
 * parameters, bounds of the interval for trigonometric calculation.
 * 
 * @author Matej Balun
 *
 */
public class TrigServlet extends HttpServlet {

	/**
	 * The serial version UID for this class.
	 */
	private static final long serialVersionUID = -3547155597329828127L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		int a = 0;
		int b = 360;

		if (req.getParameterMap().containsKey("a")) {
			a = Integer.parseInt(req.getParameter("a"));
		}

		if (req.getParameterMap().containsKey("b")) {
			b = Integer.parseInt(req.getParameter("b"));
		}

		if (a > b) {
			int tmp = a;
			a = b;
			b = tmp;
		} else if (b > (a + 720)) {
			b = a + 720;
		}

		req.setAttribute("a", a);
		req.setAttribute("b", b);
		req.getRequestDispatcher("/WEB-INF/pages/trigonometric.jsp").forward(req, resp);
	}
}
