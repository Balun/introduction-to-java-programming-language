package hr.fer.zemris.java.tecaj.hw13.servlets.voting;

import java.text.Collator;
import java.util.Locale;

/**
 * This class represents a structure for voting points of {@link Artist}s in
 * voting application. It implements the {@link Comparable} interface.
 * 
 * @author Matej Balun
 *
 */
public class ArtistPoints implements Comparable<ArtistPoints> {

	/**
	 * ID of the {@link Artist}.
	 */
	private String ID;

	/**
	 * Points for the {@link Artist}.
	 */
	private String points;

	/**
	 * This constructor initialises the {@link ArtistPoints} with its specified
	 * parameters.
	 * 
	 * @param ID
	 *            ID of the {@link Artist}.
	 * @param points
	 *            scored points of the {@link Artist}.
	 */
	public ArtistPoints(String ID, String points) {
		this.ID = ID;
		this.points = points;
	}

	/**
	 * Returns the ID of the {@link Artist}.
	 * 
	 * @return ID of the {@link Artist}
	 */
	public String getID() {
		return ID;
	}

	/**
	 * Returns the scored points of the {@link Artist}.
	 * 
	 * @return points of the {@link Artist}
	 */
	public String getPoints() {
		return points;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ID == null) ? 0 : ID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ArtistPoints other = (ArtistPoints) obj;
		if (ID == null) {
			if (other.ID != null)
				return false;
		} else if (!ID.equals(other.ID))
			return false;
		return true;
	}

	@Override
	public int compareTo(ArtistPoints o) {
		int r = -this.points.compareTo(o.points);

		if (r == 0) {
			r = Collator.getInstance(new Locale("en")).compare(this.ID, o.ID);
		}

		return r;
	}

}
