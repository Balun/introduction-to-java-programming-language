package hr.fer.zemris.java.tecaj.hw13.servlets;

import java.io.IOException;

import javax.servlet.ServletContextListener;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This class represents a simple servlet for obtaining run time of the
 * application via {@link ServletContextListener} instance.
 * 
 * @author Matej Balun
 *
 */
public class TimeServlet extends HttpServlet {

	/**
	 * The serial version UID for this class.
	 */
	private static final long serialVersionUID = -7878082781391092874L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getRequestDispatcher("/WEB-INF/pages/appinfo.jsp").forward(req, resp);
	}

}
