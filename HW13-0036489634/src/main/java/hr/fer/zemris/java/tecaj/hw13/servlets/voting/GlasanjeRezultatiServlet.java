package hr.fer.zemris.java.tecaj.hw13.servlets.voting;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This class represents a simple servlet for the displaying the results of the
 * music voting in table, graphical and excel form. It also provides the links
 * to the winners' songs.
 * 
 * @author Matej Balun
 *
 */
public class GlasanjeRezultatiServlet extends HttpServlet {

	/**
	 * The serial version UID for this class.
	 */
	private static final long serialVersionUID = 8856357268158616660L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String fileName = req.getServletContext().getRealPath("/WEB-INF/glasanje-rezultati.txt");

		if (!Files.exists(Paths.get(fileName))) {
			Files.createFile(Paths.get(fileName));
		}

		if (req.getSession().getAttribute("definition") == null) {
			String definition = req.getServletContext().getRealPath("/WEB-INF/glasanje-definicija.txt");
			Map<String, Artist> map = new TreeMap<>();

			for (String line : Files.readAllLines(Paths.get(definition))) {
				map.put(line.split("\t")[0], new Artist(line.split("\t")[0], line.split("\t")[1], line.split("\t")[2]));
			}
			req.getSession().setAttribute("definition", map);
		}

		req.getSession().setAttribute("points", fileName);

		req.getRequestDispatcher("/WEB-INF/pages/glasanjeRez.jsp").forward(req, resp);
	}

}
