package hr.fer.zemris.java.tecaj.hw13.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * THis class represents a servlet to provide change colour option for the
 * application with links.
 * 
 * @author Matej Balun
 *
 */
public class SetColorServlet extends HttpServlet {

	/**
	 * The serial version UID for this class.
	 */
	private static final long serialVersionUID = 613708837552308389L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String color = req.getParameter("color");
		req.getSession().setAttribute("pickedBgCol", color);
		req.getRequestDispatcher("/WEB-INF/pages/colors.jsp").forward(req, resp);
	}
}
