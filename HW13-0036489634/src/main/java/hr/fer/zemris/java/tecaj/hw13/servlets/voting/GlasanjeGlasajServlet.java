package hr.fer.zemris.java.tecaj.hw13.servlets.voting;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This {@link Servlet} represents a servlet for voting in the music artist
 * voting application. It extends the {@link HttpServlet}.
 * 
 * @author Matej Balun
 *
 */
public class GlasanjeGlasajServlet extends HttpServlet {

	/**
	 * The serial version UID for this class.
	 */
	private static final long serialVersionUID = -4060522650209995958L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String ID = req.getParameter("id");
		String fileName = req.getServletContext().getRealPath("/WEB-INF/glasanje-rezultati.txt");

		if (!Files.exists(Paths.get(fileName))) {
			Files.createFile(Paths.get(fileName));
		}

		addOrIncrementVote(ID, fileName);

		resp.sendRedirect(req.getContextPath() + "/glasanje-rezultati");
	}

	/**
	 * This method increments the number of points for the {@link Artist} with
	 * the specified ID if it exists, and creates a new score if the ID doesn't
	 * exist.
	 * 
	 * @param ID
	 *            ID of the artist
	 * @param fileName
	 *            file path to the results document
	 * @throws IOException
	 *             if there was an error in reading/writing
	 */
	private void addOrIncrementVote(String ID, String fileName) throws IOException {
		StringBuilder builder = new StringBuilder();
		boolean contains = false;

		for (String line : Files.readAllLines(Paths.get(fileName))) {
			String id = line.split("\t")[0];
			String points = line.split("\t")[1];

			if (id.equals(ID)) {
				points = Integer.toString(Integer.parseInt(points) + 1);
				line = id + "\t" + points;
				contains = true;
			}
			builder.append(line + "\r\n");
		}

		if (!contains) {
			builder.append(ID + "\t" + 1 + "\r\n");
		}

		Files.write(Paths.get(fileName), builder.toString().getBytes());
	}

}
