package hr.fer.zemris.java.tecaj.hw13.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 * This class represents a servlet for calculating and displaying specified
 * powers of numbers of the web page. The Servlet is called with the arguments;
 * low index, high index and number of roots to calculate in the indexed
 * interval.
 * 
 * @author Matej Balun
 *
 */
public class PowerServlet extends HttpServlet {

	/**
	 * The serial version UID for this class.
	 */
	private static final long serialVersionUID = -1832782453548175386L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			int a = obtainParameter("a", req);
			int b = obtainParameter("b", req);
			int n = obtainParameter("n", req);

			if (a > b) {
				req.setAttribute("error", "'a' cannot be grater than 'b'");
				req.getRequestDispatcher("/WEB-INF/pages/powerError.jsp").forward(req, resp);
				return;
			}

			HSSFWorkbook book = new HSSFWorkbook();
			for (int i = 0; i < n; i++) {
				HSSFSheet sheet = book.createSheet("power " + (i + 1));

				int k = 0;
				HSSFRow rowhead = sheet.createRow(k++);
				rowhead.createCell(0).setCellValue("N");
				rowhead.createCell(1).setCellValue("N^" + (i + 1));

				for (int j = a; j <= b; j++) {
					HSSFRow row = sheet.createRow(k++);
					row.createCell(0).setCellValue(j);
					row.createCell(1).setCellValue(Math.pow(j, i + 1));
				}
			}

			resp.setContentType("application/vnd.ms-excel");
			book.write(resp.getOutputStream());
			book.close();

		} catch (IllegalArgumentException e) {
			req.setAttribute("error", e.getMessage());
			req.getRequestDispatcher("/WEB-INF/pages/powerError.jsp").forward(req, resp);
		}

	}

	/**
	 * THis method obtains and checks the parameter provided int the URL for the
	 * power calculation
	 * 
	 * @param key
	 *            name of the parameter
	 * @param req
	 *            the specified {@link HttpServletRequest}
	 * @return value of the parameter
	 * @throws IllegalArgumentException
	 *             if the parameter's value is not specified, or the value is
	 *             wrong.
	 */
	private int obtainParameter(String key, HttpServletRequest req) throws IllegalArgumentException {
		if (req.getParameterMap().containsKey(key)) {
			int param = Integer.parseInt(req.getParameter(key));

			if ((key.equalsIgnoreCase("a") || key.equalsIgnoreCase("a")) && (param < -100 || param > 100)) {
				throw new IllegalArgumentException("The paramter '" + key + "' is out of bounds");
			} else if (key.equalsIgnoreCase("n") && (param < 1 || param > 5)) {
				throw new IllegalArgumentException("The paramter '" + key + "' is out of bounds");
			}

			return param;
		} else {
			throw new IllegalArgumentException("The paramter '" + key + "' is not specified");
		}
	}

}
