package hr.fer.zemris.java.tecaj.hw13.servlets.voting;

/**
 * This class represents the data structure of a single musical band or artist
 * for the voting application. It also implements the {@link Comparable}
 * interface.
 * 
 * @author Matej Balun
 *
 */
public class Artist implements Comparable<Artist> {

	/**
	 * ID of the artist.
	 */
	private String ID;

	/**
	 * Name of the artist.
	 */
	private String name;

	/**
	 * Link for the example song of the artist.
	 */
	private String songLink;

	/**
	 * This constructor initialises the {@link Artist} with its specified
	 * parameters.
	 * 
	 * @param ID
	 *            ID of the artist
	 * @param name
	 *            name of the artist
	 * @param songLink
	 *            link for the example song of the artist
	 */
	public Artist(String ID, String name, String songLink) {
		this.ID = ID;
		this.name = name;
		this.songLink = songLink;
	}

	/**
	 * Returns the {@link Artist}'s ID.
	 * 
	 * @return the {@link Artist}'s ID
	 */
	public String getID() {
		return ID;
	}

	/**
	 * Returns the {@link Artist}'s name.
	 * 
	 * @return the {@link Artist}'s name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Returns the link for the example song of the {@link Artist}.
	 * 
	 * @return example song link of the {@link Artist}
	 */
	public String getSongLink() {
		return songLink;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ID == null) ? 0 : ID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Artist other = (Artist) obj;
		if (ID == null) {
			if (other.ID != null)
				return false;
		} else if (!ID.equals(other.ID))
			return false;
		return true;
	}

	@Override
	public int compareTo(Artist o) {
		return this.ID.compareTo(o.ID);
	}

}
