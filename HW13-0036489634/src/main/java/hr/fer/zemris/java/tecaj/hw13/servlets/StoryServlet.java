package hr.fer.zemris.java.tecaj.hw13.servlets;

import java.io.IOException;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This servlet displays the short funny story and every time it's called, it
 * changes the text's font colour.
 * 
 * @author Matej Balun
 *
 */
public class StoryServlet extends HttpServlet {

	/**
	 * The serial version UID for this class.
	 */
	private static final long serialVersionUID = -2986481269698274643L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Random random = new Random();
		req.setAttribute("r", random.nextInt(256));
		req.setAttribute("g", random.nextInt(256));
		req.setAttribute("b", random.nextInt(256));

		req.getRequestDispatcher("/WEB-INF/pages/stories/funny.jsp").forward(req, resp);
	}
}
