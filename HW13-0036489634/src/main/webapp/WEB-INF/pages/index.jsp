<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<body style="background-color: <%=session.getAttribute("pickedBgCol")%>">
	<a href="setcolor">Background color chooser</a>
	<br>
	<a href="trigonometric?a=0&b=90">trigonometric table</a>
	<br>
	<a href="stories/funny">Short funny story</a>
	<br>
	<a href="reportImage">Survey report</a>
	<br>
	<a href="powers?a=1&b=100&n=3">Basic power provider</a>
	<br>
	<a href=appinfo>App runtime info</a>
	<br>
	<a href=glasanje>Band Voting</a>
</body>
</html>