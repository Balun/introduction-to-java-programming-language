<%@page import="java.util.function.ToIntFunction"%>
<%@page import="java.util.stream.Collectors"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Set"%>
<%@page
	import="hr.fer.zemris.java.tecaj.hw13.servlets.voting.ArtistPoints"%>
<%@page import="java.util.TreeSet"%>
<%@page import="java.util.Map"%>
<%@page import="hr.fer.zemris.java.tecaj.hw13.servlets.voting.Artist"%>
<%@page import="java.util.Enumeration"%>
<%@page import="java.nio.file.Files"%>
<%@page import="java.nio.file.Paths"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<style type="text/css">
table.rez td {
	text-align: center;
}
</style>
</head>
<body style="background-color: <%=session.getAttribute("pickedBgCol")%>">
	<h1>Rezultati glasanja</h1>
	<p>Ovo su rezultati glasanja.</p>
	<table border="1" cellspacing="0" class="rez">
		<thead>
			<tr>
				<th>Bend</th>
				<th>Broj glasova</th>
			</tr>
		</thead>
		<tbody>
			<%
				Map<String, Artist> map = (Map<String, Artist>) session.getAttribute("definition");
				Set<ArtistPoints> points = new TreeSet<>();
				session.setAttribute("pointSet", points);

				for (String line : Files.readAllLines(Paths.get(session.getAttribute("points").toString()))) {
					points.add(new ArtistPoints(line.split("\t")[0], line.split("\t")[1]));
				}

				for (ArtistPoints point : points) {
			%><tr>
				<td><%=map.get(point.getID()).getName()%></td>
				<td><%=point.getPoints()%></td>
			</tr>
			<%
				}
			%>
		</tbody>
	</table>

	<h2>Grafički prikaz rezultata</h2>
	<img alt="Pie-chart" src=glasanje-grafika width="400" height="400" />

	<h2>Rezultati u XLS formatu</h2>
	<p>
		Rezultati u XLS formatu dostupni su <a href=glasanje-xls>ovdje</a>
	</p>

	<h2>Razno</h2>
	<p>Primjeri pjesama pobjedničkih bendova:</p>
	<ul>
		<%
			int max = 0;
			for (ArtistPoints point : points) {
				if (Integer.parseInt(point.getPoints()) > max) {
					max = Integer.parseInt(point.getPoints());
				}
			}

			for (ArtistPoints point : points) {
				if (Integer.parseInt(point.getPoints()) == max) {
		%><li><a href=<%=map.get(point.getID()).getSongLink()%>
			target="_blank"><%=map.get(point.getID()).getName()%></a></li>
		<%
				}
			}
		%>
	</ul>

</body>
</html>