<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.concurrent.TimeUnit"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<style>
body {
	color: rgb(<%=request.getAttribute("r")%>,<%=request.getAttribute("g")%>,<%=request.getAttribute("b")%>);
	background-color: <%=session.getAttribute("pickedBgCol")%>
}
</style>

<body>
	<h1>App run-time</h1>
	<p>
		<%
			long startingTime = (long) getServletContext().getAttribute("time");
			long millis = System.currentTimeMillis() - startingTime;
			out.println("App run time: " + String.format("%02d days %02d hours %02d minutes %02d seconds",
					TimeUnit.MILLISECONDS.toDays(millis),
					TimeUnit.MILLISECONDS.toHours(millis) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(millis)),
					TimeUnit.MILLISECONDS.toMinutes(millis)
							- TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
					TimeUnit.MILLISECONDS.toSeconds(millis)
							- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis))));
		%>
	</p>
</body>
</html>