<%@page import="java.util.Map"%>
<%@page import="hr.fer.zemris.java.tecaj.hw13.servlets.voting.*"%>
<%@page import="java.util.Enumeration"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<body style="background-color: <%=session.getAttribute("pickedBgCol")%>">
	<h1>Glasanje za omiljeni bend:</h1>
	<p>Od sljedećih bendova, koji Vam je bend najdraži? Kliknite na
		link kako biste glasali!</p>
	<ol>
		<%
			Map<String, Artist> map = (Map<String, Artist>) session.getAttribute("definition");
			for (Map.Entry<String, Artist> entry : map.entrySet()) {
		%><li><a href="glasanje-glasaj?id=<%=entry.getKey()%>"><%=entry.getValue().getName()%></a></li>
		<%
			}
		%>
	</ol>
</body>
</html>