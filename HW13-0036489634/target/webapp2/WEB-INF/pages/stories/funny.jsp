<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>

<style>
body {
	color: rgb(<%=request.getAttribute("r")%>,<%=request.getAttribute("g")%>,<%=request.getAttribute("b")%>);
	background-color: <%=session.getAttribute("pickedBgCol")%>
}
</style>

<body>
	<p>
		Izmislili Amerikanci neki prepametan kompjuter, zna odgovore na sva
		pitanja. Da bi proverili da li je to istina dodju Rus, Kinez i
		Bosanac.<br> Prvi ulazi Rus. Nakon 2 minuta izlazi on sa hrpom
		papira. Pitaju Kinez i Bosanac<br> - ,,Sta si ga pit'o" a ovaj
		odgovara<br> - ,,Pit'o ga 60 pitanja i na sve zna odgovor".<br>
		Sledeci je Kinez. On se vrati nakon 3 minuta, pros'o isto k'o Rus. Red
		je na Bosanca.<br> Udje on, nema ga 3-4 sata. Nakon tih 3-4 sata
		izlazi on,a kompjuter se oce raspadne i soba je puna papira. Kinez i
		Rus gledaju u cudu i pitaju ga<br> -,,Sta si ga ti pit'o"? A
		Bosanac kaze<br> -,, Ja us'o i rek'o djes jarane, sta ima, i otad
		izbacuje papire.<br>
	</p>
</body>
</html>