<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<style>
td, tr {
  text-align: center;
    border: 1px solid black;
}

body {
	color: rgb(<%=request.getAttribute("r")%>,<%=request.getAttribute("g")%>,<%=request.getAttribute("b")%>);
	background-color: <%=session.getAttribute("pickedBgCol")%>
}
</style>
	<body>
	<p>Sine and cosine values</p>
	<table border="5">
		 <% int a = (Integer) request.getAttribute("a");
		    int b = (Integer) request.getAttribute("b"); 
		    for(int i = a; i <= b; i++){ %>
		   		<tr>
		   			<td>sin(<%= i%>)</td><td><%= Math.sin(i) %></td><td>cos(<%= i%>)</td><td><%= Math.cos(i)%></td>
		   		</tr>
		   	<% }%>
	</table>
	</body>
</html>