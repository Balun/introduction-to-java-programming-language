<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
	<body style = "background-color: <%= session.getAttribute("pickedBgCol")%>">
		<p><a href = "setcolor?color=white">WHITE</a></p>
		<p><a href = "setcolor?color=red">RED</a></p>
		<p><a href = "setcolor?color=green">GREEN</a></p>
		<p><a href = "setcolor?color=cyan">CYAN</a></p>
	</body>
</html>