package hr.fer.zemris.java.fractals;

/**
 * This class represents the polynom with complex coefficients and rots. The
 * class offers methods for all basic polynom operations. The class is
 * immutable, methods return new instances of {@link ComplexPolynomial} after
 * the values are chaged
 * 
 * @author Matej Balun
 *
 */
public class ComplexPolynomial {

	/**
	 * Array of the polynom factors.
	 */
	private Complex[] factors;

	/**
	 * Thi instrucotr initializes the {@link ComplexPolynomial} with the
	 * specified factors of the polynom.
	 * 
	 * @param factors
	 *            the specified factors of the polynom
	 */
	public ComplexPolynomial(Complex... factors) {

		this.factors = factors;
	}

	/**
	 * Returns the order of this {@link ComplexPolynomial}.
	 * 
	 * @return order of this {@link ComplexPolynomial}
	 */
	public short order() {
		return (short) (this.factors.length - 1);
	}

	/**
	 * Iteratively multiplies this {@link ComplexPolynomial} with the specfied
	 * {@link ComplexPolynomial}.
	 * 
	 * @param polynom
	 *            the specified {@link ComplexPolynomial} for multiplication
	 * @return new instance of multiplied polynom
	 */
	public ComplexPolynomial multiply(ComplexPolynomial polynom) {
		Complex[] multiplyed = new Complex[factors.length + polynom.factors.length - 1];

		for (int i = 0; i < multiplyed.length; i++) {
			multiplyed[i] = Complex.ZERO;
		}

		for (int i = 0; i < factors.length; i++) {
			for (int j = 0; j < polynom.factors.length; j++) {
				Complex muliplication = factors[i].multiply(polynom.factors[j]);
				multiplyed[i + j] = multiplyed[i + j].add(muliplication);
			}
		}

		return new ComplexPolynomial(multiplyed);
	}

	/**
	 * Returns the first derivative of this {@link ComplexPolynomial}.
	 * 
	 * @return the first derivative of this {@link ComplexPolynomial}
	 */
	public ComplexPolynomial derive() {
		Complex[] derived = new Complex[this.factors.length - 1];

		for (int i = 0; i < this.order(); i++) {
			derived[i] = factors[i].multiply(new Complex(this.order() - i, 0));
		}

		return new ComplexPolynomial(derived);
	}

	/**
	 * Calculates the valu of this {@link ComplexPolynomial} with the specified
	 * point z
	 * 
	 * @param z
	 *            the secified pointe for value calculation
	 * @return value of this {@link ComplexPolynomial} at point z
	 */
	public Complex apply(Complex z) {
		Complex sum = Complex.ZERO;
		Complex exp = Complex.ONE;

		for (int i = this.factors.length - 1; i >= 0; i--) {
			sum = sum.add(exp.multiply(this.factors[i]));
			exp = exp.multiply(z);
		}

		return sum;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();

		for (int i = factors.length - 1; i >= 0; i--) {

			if (factors[i].getReal() != 0) {
				if (factors[i].getImaginary() != 0) {

					if (factors[i].getImaginary() != 1) {
						builder.append("(" + factors[i].getReal() + "+" + factors[i].getImaginary() + "i)");
					} else {
						builder.append("(" + factors[i].getReal() + "+i)");
					}

				} else {
					builder.append(factors[i].getReal());
				}

			} else if (factors[i].getImaginary() != 0) {
				builder.append(factors[i].getImaginary() + "i");

			} else {
				continue;
			}

			switch (i) {

			case 0:
				break;

			case 1:
				builder.append("z");

			default:
				builder.append("z^" + i);
			}
		}

		return builder.toString();
	}

}
