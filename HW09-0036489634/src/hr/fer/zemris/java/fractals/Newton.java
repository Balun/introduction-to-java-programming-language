package hr.fer.zemris.java.fractals;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import hr.fer.zemris.java.fractals.viewer.FractalViewer;
import hr.fer.zemris.java.fractals.viewer.IFractalProducer;
import hr.fer.zemris.java.fractals.viewer.IFractalResultObserver;

/**
 * This program demnstrates the calculation and the visual projection of the
 * Newton-Raphson fractal images. The program takes variable number of arguments
 * (greater or equal to two) from the command-line, representing the
 * {@link Complex} numbers as roots of the complex polynom (input is terminated
 * with string "done"). The program works in multy-thread environment, offering
 * the optimal efficency of the processor's cores in calculaton.
 * 
 * @author Matej Balun
 *
 */
public class Newton {

	/**
	 * The {@link String} constant for "done" string.
	 */
	private static final String DONE = "done";

	/**
	 * The rooted-viewec complex polynom
	 */
	private static ComplexRootedPolynomial rooted;

	/**
	 * The derived complex polynom.
	 */
	private static ComplexPolynomial derived;

	/**
	 * The main method that executes the Newton-Rapshon calculation of fractals.
	 */
	public static void main(String[] args) {

		System.out.println("Welcome to Newton-Raphson iteration-based fractal viewer.\n"
				+ "Please enter at least two roots, one root per line. Enter 'done' when done.");

		Scanner sc = new Scanner(System.in);
		List<Complex> roots = new ArrayList<>();
		int i = 1;
		String line;

		do {
			System.out.print("Root " + i + "> ");
			line = sc.nextLine();

			if (line.equalsIgnoreCase(DONE)) {
				if (roots.size() < 2) {
					System.out.println("You have to enter at least two roots");
					i++;
					continue;

				} else {
					break;
				}
			}

			i++;
			roots.add(ComplexUtil.fromString(line));
		} while (!line.equalsIgnoreCase(DONE));

		sc.close();

		System.out.println("Image of fractal will appear shortly. Thank you.");

		rooted = new ComplexRootedPolynomial(roots.toArray(new Complex[roots.size()]));
		derived = rooted.toComplexPolynom().derive();

		FractalViewer.show(new FractalProducerImpl());

	}

	/**
	 * This class implements the {@link Callable} interface. It serves as a sing
	 * thread in dispathcing the calculation to multiple threads. Specifically,
	 * this class calculates the n-th number in Newton-Raphson iteration.
	 * 
	 * @author Matej Balun
	 *
	 */
	private static class FractalComputation implements Callable<Void> {

		/**
		 * Number of iterations for fractal calculation.
		 */
		private static final int NUM_OF_ITER = 16 * 16 * 16;

		/**
		 * The threshold for iterative aproximation.
		 */
		private static final double THRESHOLD = 0.002;

		/**
		 * The minimum real value.
		 */
		private double reMin;

		/**
		 * The maxiumum real value.
		 */
		private double reMax;

		/**
		 * The minimum imaginary value.
		 */
		private double imMin;

		/**
		 * The maximum imaginary value.
		 */
		private double imMax;

		/**
		 * Width of the raster.
		 */
		private int width;

		/**
		 * Height of the raster.
		 */
		private int height;

		/**
		 * The minimum y-coordinate.
		 */
		private int yMin;

		/**
		 * The maximum y-coordinate.
		 */
		private int yMax;

		/**
		 * The data array.
		 */
		private short[] data;

		/**
		 * This constructor initializes the {@link FractalComputation} with its
		 * computation parameters.
		 * 
		 * @param reMin
		 *            the minimum real value
		 * @param reMax
		 *            the maximum real value
		 * @param imMin
		 *            the minimum imaginary value
		 * @param imMax
		 *            the maximum imaginary value
		 * @param width
		 *            width of the raster
		 * @param height
		 *            height of the raster
		 * @param yMin
		 *            the minimum y-coordinate
		 * @param yMax
		 *            the maximum y-coordinate
		 * @param data
		 *            the specified data
		 */
		public FractalComputation(double reMin, double reMax, double imMin, double imMax, int width, int height,
				int yMin, int yMax, short[] data) {

			super();
			this.reMin = reMin;
			this.reMax = reMax;
			this.imMin = imMin;
			this.imMax = imMax;
			this.width = width;
			this.height = height;
			this.yMin = yMin;
			this.yMax = yMax;
			this.data = data;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Void call() {

			int offset = yMin * width;
			for (int j = yMin; j <= yMax; j++) {
				for (int i = 0; i < width; i++) {

					double real = i / (width - 1.) * (reMax - reMin) + reMin;
					double imaginary = (height - 1 - j) / (height - 1.) * (imMax - imMin) + imMin;

					Complex current = new Complex(real, imaginary);
					Complex next = new Complex();

					int k = 0;
					double mod;

					do {

						next = current.sub(rooted.apply(current).divide(derived.apply(current)));
						mod = next.sub(current).module();
						current = next;

					} while (k < NUM_OF_ITER && mod > THRESHOLD);

					int index = rooted.indexOflosestRootFor(next, THRESHOLD);

					if (index < 0) {
						data[offset++] = 0;
					} else {
						data[offset++] = (short) (index + 1);
					}
				}
			}

			return null;
		}
	}

	/**
	 * This class implements the {@link IFractalProducer} interface. It produces
	 * the "jobs", dispatching calculation of Newton-Rapshon iterative fractals
	 * via thread pool {@link ExecutorService}. The calculation is dispatched in
	 * optimal way, ensuring the processor's cores are optimaly used.
	 * 
	 * @author Matej Balun
	 *
	 */
	private static class FractalProducerImpl implements IFractalProducer {

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void produce(double reMin, double reMax, double imMin, double imMax, int width, int height,
				long requestNo, IFractalResultObserver observer) {

			System.out.println("Starting calculation...");

			int order = rooted.order() + 1;
			short[] data = new short[width * height];
			final int noOfStrips = height / 8 * Runtime.getRuntime().availableProcessors();
			int heightOfStrips = height / noOfStrips;

			ExecutorService pool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
			List<Future<Void>> rezultati = new ArrayList<>();

			for (int i = 0; i < noOfStrips; i++) {
				int yMin = i * heightOfStrips;
				int yMax = (i + 1) * heightOfStrips - 1;

				if (i == noOfStrips - 1) {
					yMax = height - 1;
				}

				FractalComputation job = new FractalComputation(reMin, reMax, imMin, imMax, width, height, yMin, yMax,
						data);
				rezultati.add(pool.submit(job));
			}

			for (Future<Void> job : rezultati) {
				try {
					job.get();
				} catch (InterruptedException | ExecutionException e) {
				}
			}

			pool.shutdown();
			System.out.println("Computation finished. I'm notifiying the observer!");
			observer.acceptResult(data, (short) order, requestNo);
		}
	}

}
