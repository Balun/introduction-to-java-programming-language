package hr.fer.zemris.java.fractals;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This utility cass offers the method for parsing the string representing the
 * complex number into the new instance of {@link Complex}. THe class usses
 * regular expressions.
 * 
 * @author Matej Balun
 *
 */
public class ComplexUtil {

	/**
	 * The regular expression for the real part.
	 */
	private static String REAL_REGEX = "(\\+|-){0,1}\\s*([0-9]+)|([0-9]*\\.[0-9]+)";

	/**
	 * The reguar expresion for the imaginary part.
	 */
	private static String IM_REGEX = "(\\+|-){0,1}\\s*i(([0-9]+)|([0-9]*\\.[0-9]+)){0,1}";

	/**
	 * The regular expression for the imaginary part, without the specified
	 * number.
	 */
	private static String ONLY_I_REGEX = "(\\+|-){0,1}\\s*i";

	/**
	 * The regular expression for unlimited ammount of whitespaces.
	 */
	private static String WHITESPACE = "\\s+";

	/**
	 * The internally kept pattern for parsing the complex number inputs.
	 */
	private static Pattern pattern = Pattern
			.compile("(" + REAL_REGEX + ")|(" + IM_REGEX + ")|(" + ONLY_I_REGEX + ")|(" + WHITESPACE + ")");

	/**
	 * The default private constructor, it permits the class instanciation.
	 */
	private ComplexUtil() {
		super();
	}

	/**
	 * Parses the soecified compplex string into the {@link Complex}. The method
	 * uses {@link Pattern} and {@link Matcher} intrnlly.
	 * 
	 * @param complex
	 *            {@link String} representaton of complx number
	 * @return bew instance of {@link Complex} number
	 */
	public static Complex fromString(String complex) {
		Matcher matcher = pattern.matcher(complex);

		String real = null;
		String im = null;
		String imNoNum = null;

		while (matcher.find()) {
			String tmp = matcher.group().replace(" ", "").trim();

			if (tmp.isEmpty()) {
				continue;
			} else if (tmp.contains("i")) {
				try {
					Double.parseDouble(tmp.replace("i", ""));
					im = tmp;
				} catch (NumberFormatException e) {
					imNoNum = tmp;
				}

			} else {
				real = tmp;
			}
		}

		return createComplex(real, im, imNoNum);
	}

	/**
	 * Helper methd, initializes the new {@link Complex} based on the various
	 * cases.
	 * 
	 * @param real
	 *            real part of the number
	 * @param im
	 *            imaginary part of the number
	 * @param imNoNum
	 *            imaginary part of the number without nuber value (i, -i)
	 * @return new instance of {@link Complex} number
	 */
	private static Complex createComplex(String real, String im, String imNoNum) {

		if (real != null) {

			if (im != null) {
				return new Complex(Double.parseDouble(real), Double.parseDouble(im.replace("i", "")));

			} else if (imNoNum != null) {
				return new Complex(Double.parseDouble(real), Double.parseDouble(imNoNum.replace("i", "").concat("1")));

			} else {
				return new Complex(Double.parseDouble(real), 0);
			}

		} else {

			if (im != null) {
				return new Complex(0, Double.parseDouble(im.replace("i", "")));

			} else if (imNoNum != null) {
				return new Complex(0, Double.parseDouble(imNoNum.replace("i", "").concat("1")));

			} else {
				throw new IllegalArgumentException("Invalid complex input String");
			}
		}
	}

}
