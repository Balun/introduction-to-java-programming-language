package hr.fer.zemris.java.fractals;

/**
 * This class represents the polynom with {@link Complex} coefficients and roots
 * int the root form. The class offers methods for standard roted polynom
 * operations. The class is immutable, returning new instances of specified
 * objects after the change
 * 
 * @author Matej Balun
 *
 */
public class ComplexRootedPolynomial {

	/**
	 * Array of roots of the {@link ComplexRootedPolynomial}.
	 */
	private Complex roots[];

	/**
	 * Initializes the {@link ComplexRootedPolynomial} with the specified array
	 * of roots.
	 * 
	 * @param roots
	 *            the specified array of roots for the
	 *            {@link ComplexRootedPolynomial}
	 */
	public ComplexRootedPolynomial(Complex... roots) {
		this.roots = roots;
	}

	/**
	 * Calculates the value of this {@link ComplexRootedPolynomial} at the
	 * specified point.
	 * 
	 * @param z
	 *            the specified pont for value calculation
	 * @return value of this {@link ComplexRootedPolynomial} at the point z
	 */
	public Complex apply(Complex z) {
		Complex prod = Complex.ONE;

		for (int i = 0; i < roots.length; i++) {
			Complex current = z.sub(roots[i]);
			prod = prod.multiply(current);
		}

		return prod;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();

		for (int i = 0; i < roots.length; i++) {
			builder.append("(z - (" + roots[i].toString() + "))");
		}

		return builder.toString();
	}

	/**
	 * Finds index of closest root for given complex number z that is within
	 * treshould. If there is no such root, returns -1.
	 * 
	 * @param z
	 *            the specified {@link Complex}
	 * @param treshold
	 *            the specified treshold for search
	 * @return index if closes root in the polynom, or -1
	 */
	public int indexOflosestRootFor(Complex z, double treshold) {
		int closest = -1;
		double min = Double.MAX_VALUE;

		for (int i = 0; i < roots.length; i++) {
			double tmp = Math.abs(z.sub(roots[i]).module());

			if (tmp < treshold) {
				if (tmp < min) {
					closest = i;
					min = tmp;
				}
			}
		}

		return closest;
	}

	/**
	 * Transforms this {@link ComplexRootedPolynomial} to the standard
	 * {@link ComplexPolynomial}
	 * 
	 * @return new instance of {@link ComplexPolynomial}
	 */
	public ComplexPolynomial toComplexPolynom() {
		ComplexPolynomial result = new ComplexPolynomial(Complex.ONE);

		for (Complex root : roots) {
			result = result.multiply(new ComplexPolynomial(Complex.ONE, root.negate()));
		}

		return result;
	}

	/**
	 * Returns the order of this polynom.
	 * 
	 * @return order of this polynom
	 */
	public int order() {
		return this.roots.length;
	}

}
