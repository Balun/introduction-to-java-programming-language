package hr.fer.zemris.java.fractals;

import java.util.LinkedList;
import java.util.List;

/**
 * This class represents the complex number, with all it's characteristics and
 * mathematical operations. The real and imaginary values are internally stored
 * as double values, and the class is immutable; Every operation returns the new
 * instance of {@link Complex} number.
 * 
 * @author Matej Balun
 *
 */

public class Complex {

	/**
	 * The complex value of number zero.
	 */
	public static final Complex ZERO = new Complex(0, 0);

	/**
	 * The complex value of number one.
	 */
	public static final Complex ONE = new Complex(1, 0);

	/**
	 * The complex valu of number minus one.
	 */
	public static final Complex ONE_NEG = new Complex(-1, 0);

	/**
	 * The complex value of number i.
	 */
	public static final Complex IM = new Complex(0, 1);

	/**
	 * The complex value of number -i.
	 */
	public static final Complex IM_NEG = new Complex(0, -1);

	/**
	 * Real part of the {@link Complex}.
	 */
	private double real;

	/**
	 * Imaginary part of the {@link Complex}.
	 */
	private double imaginary;

	/**
	 * The default constructor, initializes the zero-valued {@link Complex} by
	 * passing arguments to the next constructor.
	 */
	public Complex() {
		this(0, 0);
	}

	/**
	 * This constructor initializes the {@link Complex} withe the secified real
	 * and imaginary values.
	 * 
	 * @param real
	 *            the real value of the {@link Complex}
	 * @param imaginary
	 *            the imaginary value of the {@link Complex}
	 */
	public Complex(double real, double imaginary) {
		this.real = real;
		this.imaginary = imaginary;
	}

	/**
	 * Return the real part of the {@link Complex}.
	 * 
	 * @return real part of he {@link Complex}
	 */
	public double getReal() {
		return real;
	}

	/**
	 * Returns the imaginary part of the {@link Complex}.
	 * 
	 * @return imaginary part of the {@link Complex}.
	 */
	public double getImaginary() {
		return imaginary;
	}

	/**
	 * Returns the module of the {@link Complex}.
	 * 
	 * @return module of the {@link Complex}
	 */
	public double module() {
		return Math.hypot(real, imaginary);
	}

	/**
	 * Multiplies thi {@link Complex} with the specified complex.
	 * 
	 * @param c
	 *            the specified {@link Complex} fo multiplication
	 * @return new instance of multiplied {@link Complex}
	 */
	public Complex multiply(Complex c) {
		double realPart = real * c.real - imaginary * c.imaginary;
		double imPart = imaginary * c.real + real * c.imaginary;

		return new Complex(realPart, imPart);
	}

	/**
	 * Divides this {@link Complex} withe the specified complex.
	 * 
	 * @param c
	 *            the specified complex for divison.
	 * @return new instance of the divisioned complex
	 */
	public Complex divide(Complex c) {
		double squared = c.real * c.real + c.imaginary * c.imaginary;
		Complex conj = new Complex(c.real, -c.imaginary);

		return this.multiply(conj).multiply(new Complex(1 / squared, 0));
	}

	/**
	 * Adds the specified {@link Complex} to this {@link Complex}.
	 * 
	 * @param c
	 *            the specified {@link Complex} for addition
	 * @return new instance of {@link Complex} after addition
	 */
	public Complex add(Complex c) {
		double realPart = real + c.real;
		double imPart = imaginary + c.imaginary;

		return new Complex(realPart, imPart);
	}

	/**
	 * Subtracts this {@link Complex} withe the specified {@link Complex}.
	 * 
	 * @param c
	 *            the specified {@link Complex} for subtracition.
	 * @return new instance of subcracted {@link Complex}
	 */
	public Complex sub(Complex c) {
		double realPart = real - c.real;
		double imPart = imaginary - c.imaginary;

		return new Complex(realPart, imPart);
	}

	/**
	 * Negates the real and imahinary values of this {@link Complex}.
	 * 
	 * @return new instance of negated {@link Complex}
	 */
	public Complex negate() {
		return new Complex(-real, -imaginary);
	}

	/**
	 * Returns the angle of this {@link Complex} in radians.
	 * 
	 * @return angle of this {@link Complex} in radians.
	 */
	public double getAngle() {
		return Math.atan2(imaginary, real);
	}

	/**
	 * Calculates the n-th power of this complex.
	 * 
	 * @param n
	 *            power for calculation
	 * @return new instance of powered {@link Complex}
	 */
	public Complex power(int n) {
		if (n < 0) {
			throw new IllegalArgumentException("The value n for power operation must not be less than zero");
		}

		if (n == 0) {
			return ONE;
		}

		double radius = Math.pow(module(), n);
		double angle = getAngle();

		double realPart = radius * Math.cos(n * angle);
		double imPart = radius * Math.sin(n * angle);

		return new Complex(realPart, imPart);
	}

	/**
	 * Calculates the first n roots of this {@link Complex}.
	 * 
	 * @param n
	 *            number of the roots for calculation
	 * @return {@link List} of {@link Complex} that are roots of this
	 *         {@link Complex}
	 */
	public List<Complex> root(int n) {
		if (n <= 0) {
			throw new IllegalArgumentException("The value n for the root must be greater than zero");
		}

		double radius = Math.pow(module(), 1. / n);
		double angle = getAngle();
		List<Complex> roots = new LinkedList<>();

		for (int i = 0; i < n; i++) {
			double realPart = Math.pow(radius, 1 / n) * Math.cos((angle + 2 * i * Math.PI) / n);
			double imPart = Math.pow(radius, 1 / n) * Math.sin((angle + 2 * i * Math.PI) / n);

			roots.add(new Complex(realPart, imPart));
		}

		return roots;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		if (this.imaginary == 0) {
			return Double.toString(real);

		} else if (this.real == 0) {
			if (this.imaginary < 0) {
				return "-i" + (-this.imaginary);
			} else {
				return "i" + this.imaginary;
			}

		} else if (this.imaginary < 0) {
			return this.real + " - " + "i" + (-this.imaginary);
		}

		return this.real + " + " + "i" + this.imaginary;
	}

}
