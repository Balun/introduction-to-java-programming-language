package hr.fer.zemris.java.raytracer;

import hr.fer.zemris.java.raytracer.model.GraphicalObject;
import hr.fer.zemris.java.raytracer.model.IRayTracerProducer;
import hr.fer.zemris.java.raytracer.model.IRayTracerResultObserver;
import hr.fer.zemris.java.raytracer.model.LightSource;
import hr.fer.zemris.java.raytracer.model.Point3D;
import hr.fer.zemris.java.raytracer.model.Ray;
import hr.fer.zemris.java.raytracer.model.RayIntersection;
import hr.fer.zemris.java.raytracer.model.Scene;
import hr.fer.zemris.java.raytracer.model.Sphere;
import hr.fer.zemris.java.raytracer.viewer.RayTracerViewer;

/**
 * This program thre-dimensional projection of {@link Sphere}s simulating the
 * projection with the geometric-based ray structure. The program produces the
 * image on the specified raster using the RGF (red-green-blue) rendering for
 * color based image.
 * 
 * @author Matej Balun
 *
 */
public class RayCaster {

	/**
	 * The default ambient of the RGB spectre.
	 */
	private static final int DEFAULT_AMBIENT = 15;

	/**
	 * The relative small offset for double value comparison.
	 */
	private static final double OFFSET = 0.05;

	/**
	 * The scallar constant for scalar production.
	 */
	private static final int SCALAR = -1;

	/**
	 * The main method that starts the program.
	 */
	public static void main(String[] args) {
		RayTracerViewer.show(getIRayTracerProducer(), new Point3D(10, 0, 0), new Point3D(0, 0, 0),
				new Point3D(0, 0, 10), 20, 20);
	}

	/**
	 * This method returns the new nstance of class implementing the
	 * {@link IRayTracerProducer} interface.
	 * 
	 * @return new implemented instance of {@link IRayTracerProducer}.
	 */
	private static IRayTracerProducer getIRayTracerProducer() {

		return new IRayTracerProducer() {

			/**
			 * {@inheritDoc}
			 */
			@Override
			public void produce(Point3D eye, Point3D view, Point3D viewUp, double horizontal, double vertical,
					int width, int height, long requestNo, IRayTracerResultObserver observer) {

				System.out.println("Startig calculations...");

				short[] red = new short[width * height];
				short[] green = new short[width * height];
				short[] blue = new short[width * height];

				Point3D zAxis = view.sub(eye).modifyNormalize();
				Point3D yAxis = viewUp.normalize().sub(zAxis.scalarMultiply(viewUp.normalize().scalarProduct(zAxis)));
				Point3D xAxis = zAxis.vectorProduct(yAxis).normalize();

				Point3D corner = view.sub(xAxis.scalarMultiply(horizontal / 2.))
						.add(yAxis.scalarMultiply(vertical / 2.));

				Scene scene = RayTracerViewer.createPredefinedScene();

				short[] rgb = new short[3];
				int offset = 0;
				for (int y = 0; y < height; y++) {
					for (int x = 0; x < width; x++) {

						Point3D screenPoint = corner.add(xAxis.scalarMultiply(x / (width - 1.0) * horizontal))
								.sub(yAxis.scalarMultiply(y / (height - 1.0) * vertical));

						Ray ray = Ray.fromPoints(eye, screenPoint);

						tracer(scene, ray, rgb);

						red[offset] = rgb[0] > 255 ? 255 : rgb[0];
						green[offset] = rgb[1] > 255 ? 255 : rgb[1];
						blue[offset] = rgb[2] > 255 ? 255 : rgb[2];

						offset++;
					}
				}

				System.out.println("Computations finished...");
				observer.acceptResult(red, green, blue, requestNo);
				System.out.println("Notification finished...");
			}
		};
	}

	/**
	 * This method traces the ray-based image using the ray-casting model.
	 * 
	 * @param scene
	 *            the {@link Scene} of the image
	 * @param ray
	 *            the geometric {@link Ray} from the viewer's perspective
	 * @param RGB
	 *            the array of red-green-blue colors
	 */
	public static void tracer(Scene scene, Ray ray, short[] RGB) {

		double[] newRGB = new double[3];

		setDefaultAmbient(newRGB);

		RayIntersection intersection = getIntersec(scene, ray);

		if (intersection != null) {
			color(scene, ray, newRGB, intersection);
		}

		setCalculatedAmbient(RGB, newRGB);
	}

	/**
	 * This method sets the calculated ambient of the RGB scheme to the
	 * specified scheme.
	 * 
	 * @param RGB
	 *            The specified RGB scheme
	 * @param newRGB
	 *            the calculated RGB scheme
	 */
	private static void setCalculatedAmbient(short[] RGB, double[] newRGB) {
		for (int i = 0; i < RGB.length; i++) {
			RGB[i] = (short) newRGB[i];
		}
	}

	/**
	 * This method finds the color for the specified intersection in the
	 * ray-mode based image. The possible diffuse and reflective components of
	 * the image are added if needed.
	 * 
	 * @param scene
	 *            the {@link Scene} of the image
	 * @param ray
	 *            the {@link Ray} for the intersection
	 * @param newRGB
	 *            the RGB color data for the calculation
	 * @param intersection
	 *            intersection between the specified ray and the possible object
	 *            in the 3D image
	 */
	private static void color(Scene scene, Ray ray, double[] newRGB, RayIntersection intersection) {

		for (LightSource elem : scene.getLights()) {

			Ray tmpRay = Ray.fromPoints(elem.getPoint(), intersection.getPoint());
			RayIntersection tmpIntersec = getIntersec(scene, tmpRay);

			if (tmpIntersec != null) {
				double sourceDist = elem.getPoint().sub(tmpIntersec.getPoint()).norm();
				double viewDist = elem.getPoint().sub(intersection.getPoint()).norm();

				if (sourceDist + OFFSET >= viewDist) {
					updateDiff(elem, newRGB, tmpIntersec);
					updateRef(elem, ray, newRGB, tmpIntersec);
				}
			}
		}
	}

	/**
	 * This method updates the Reflective component of the RGB color scheme.
	 * 
	 * @param light
	 *            {@link LightSource} for the color scheme
	 * @param ray
	 *            the {@link Ray} object for the intersection
	 * @param RGB
	 *            the RGB color scheme for calculation
	 * @param intersec
	 *            the specified intersection
	 */
	private static void updateRef(LightSource light, Ray ray, double[] RGB, RayIntersection intersec) {

		Point3D point1 = intersec.getNormal();
		Point3D point2 = light.getPoint().sub(intersec.getPoint());
		Point3D point3 = point1.scalarMultiply(point2.scalarProduct(point1));
		Point3D point4 = point3.add(point3.negate().add(point2).scalarMultiply(SCALAR));
		Point3D point5 = ray.start.sub(intersec.getPoint());
		double scalar = point4.normalize().scalarProduct(point5.normalize());

		if (scalar >= 0) {
			scalar = Math.pow(scalar, intersec.getKrn());

			setReflectiveAmbient(light, intersec, scalar, RGB);
		}

	}

	/**
	 * This method updates all components of the RGB color scheme based on the
	 * light source and intersection in reflective manner.
	 * 
	 * @param source
	 *            the light source for the color scheme
	 * @param intersec
	 *            the specified intersection
	 * @param scalar
	 *            the specified scalar
	 * @param RGB
	 *            the RGB color scheme to update
	 */
	private static void setReflectiveAmbient(LightSource source, RayIntersection intersec, double scalar,
			double[] RGB) {

		for (int i = 0; i < RGB.length; i++) {
			switch (i) {
			case 0:
				RGB[i] = RGB[i] + source.getR() * intersec.getKrr() * scalar;
				break;

			case 1:
				RGB[i] = RGB[i] + source.getG() * intersec.getKrg() * scalar;
				break;

			case 2:
				RGB[i] = RGB[i] + source.getB() * intersec.getKrb() * scalar;
				break;

			default:
				break;
			}
		}
	}

	/**
	 * This method updates the diffuse component of the RGB color scheme.
	 * 
	 * @param light
	 *            {@link LightSource} for the color scheme
	 * @param ray
	 *            the {@link Ray} object for the intersection
	 * @param RGB
	 *            the RGB color scheme for calculation
	 * @param intersec
	 *            the specified intersection
	 */
	private static void updateDiff(LightSource source, double[] RGB, RayIntersection intersec) {
		Point3D point1 = intersec.getNormal();
		Point3D point2 = source.getPoint().sub(intersec.getPoint()).normalize();

		double scalar = point2.scalarProduct(point1);

		setDiffusseAmbient(source, intersec, scalar, RGB);

	}

	/**
	 * This method updates all components of the RGB color scheme based on the
	 * light source and intersection in diffuse manner.
	 * 
	 * @param source
	 *            the light source for the color scheme
	 * @param intersec
	 *            the specified intersection
	 * @param scalar
	 *            the specified scalar
	 * @param RGB
	 *            the RGB color scheme to update
	 */
	private static void setDiffusseAmbient(LightSource source, RayIntersection intersec, double scalar, double[] RGB) {
		for (int i = 0; i < RGB.length; i++) {
			switch (i) {
			case 0:
				RGB[i] = RGB[i] + source.getR() * intersec.getKdr() * Math.max(scalar, 0);
				break;

			case 1:
				RGB[i] = RGB[i] + source.getG() * intersec.getKdg() * Math.max(scalar, 0);
				break;

			case 2:
				RGB[i] = RGB[i] + source.getB() * intersec.getKdb() * Math.max(scalar, 0);
				break;

			default:
				break;
			}
		}

	}

	/**
	 * This method finds the closest intersection between the specified
	 * {@link Ray} an the object in the {@link Scene}.
	 * 
	 * @param scene
	 *            the {@link Scene} of the image.
	 * @param ray
	 *            the specified {@link Ray} for the intersection
	 * @return the closest intersection with an object in the scene,
	 *         null-reference if there is no such specified object
	 */
	private static RayIntersection getIntersec(Scene scene, Ray ray) {

		RayIntersection result = null;

		for (GraphicalObject elem : scene.getObjects()) {

			RayIntersection intersec = elem.findClosestRayIntersection(ray);

			if (intersec != null) {
				if (result == null || intersec.getDistance() < result.getDistance()) {
					result = intersec;
				}
			}
		}

		return result;
	}

	/**
	 * This method sets the default ambient for the RGB color scheme.
	 * 
	 * @param newRGB
	 *            the specified RGB color scheme
	 */
	private static void setDefaultAmbient(double[] newRGB) {

		for (int i = 0; i < newRGB.length; i++) {
			newRGB[i] = DEFAULT_AMBIENT;
		}
	}

}
