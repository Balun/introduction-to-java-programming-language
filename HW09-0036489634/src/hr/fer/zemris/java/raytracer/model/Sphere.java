package hr.fer.zemris.java.raytracer.model;

/**
 * This class represents the sphere geometric object in a three-dimensional
 * space. The class extends the {@link GraphicalObject} class, implementing the
 * intersection method. The {@link Sphere} can be shown in any color specified
 * by the RGB spectre.
 * 
 * @author Matej Balun
 *
 */
public class Sphere extends GraphicalObject {

	/**
	 * The scalar for scalar production.
	 */
	private static final double SCALAR = 2.0;

	/**
	 * Center of the sphere.
	 */
	private Point3D center;

	/**
	 * Radius of the sphere.
	 */
	private double radius;

	/**
	 * The diffuse component coefficient for red color.
	 */
	private double diffuseRed;

	/**
	 * The diffuse component coefficient for green color.
	 */
	private double diffuseGreen;

	/**
	 * The diffuse component coefficient for blue color.
	 */
	private double diffuseBlue;

	/**
	 * The reflective component coefficient for red color.
	 */
	private double reflectiveRed;

	/**
	 * The reflective component coefficient for green color.
	 */
	private double reflectiveGreen;

	/**
	 * The reflective component coefficient for blue color.
	 */
	private double reflectiveBlue;

	/**
	 * Power exponent for illumination.
	 */
	private double powerExp;

	/**
	 * This constructor initializes the {@link Sphere} with its parameters.
	 * 
	 * @param center
	 *            center of the sphere
	 * @param radius
	 *            radius of the sphere
	 * @param diffuseRed
	 *            diffuse red component coefficient
	 * @param diffuseGreen
	 *            diffuse green component coefficient
	 * @param diffuseBlue
	 *            diffuse blue component coefficient
	 * @param reflectiveRed
	 *            reflective red component coefficient
	 * @param reflectiveGreen
	 *            reflective green component coefficient
	 * @param reflectiveBlue
	 *            reflective blue component coefficient
	 * @param powerExp
	 *            power exponent for illumination
	 */
	public Sphere(Point3D center, double radius, double diffuseRed, double diffuseGreen, double diffuseBlue,
			double reflectiveRed, double reflectiveGreen, double reflectiveBlue, double powerExp) {

		super();
		this.center = center;
		this.radius = radius;
		this.diffuseRed = diffuseRed;
		this.diffuseGreen = diffuseGreen;
		this.diffuseBlue = diffuseBlue;
		this.reflectiveRed = reflectiveRed;
		this.reflectiveGreen = reflectiveGreen;
		this.reflectiveBlue = reflectiveBlue;
		this.powerExp = powerExp;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RayIntersection findClosestRayIntersection(Ray ray) {

		Point3D vector1 = ray.start;
		Point3D vector2 = this.center;
		Point3D vector3 = vector1.sub(vector2);
		Point3D vector4 = ray.direction;

		double coeffA = vector4.scalarProduct(vector4);
		double coeffB = vector3.scalarMultiply(SCALAR).scalarProduct(vector4);
		double coeffC = vector3.scalarProduct(vector3) - radius * radius;

		double disc = coeffB * coeffB - 4 * coeffA * coeffC;

		if (disc < 0) {
			return null;
		}

		double solve1 = (-coeffB + Math.sqrt(disc)) / (2 * coeffA);
		double solve2 = (-coeffB - Math.sqrt(disc)) / (2 * coeffA);

		if (solve1 <= 0 && solve2 <= 0) {
			return null;
		}

		Point3D firstIntersec = vector1.add(vector4.scalarMultiply(solve1));
		Point3D secondIntersec = vector1.add(vector4.scalarMultiply(solve2));

		double firstDist = firstIntersec.sub(vector1).norm();
		double secondDist = secondIntersec.sub(vector1).norm();

		Point3D closer = null;
		double closerDistance = 0;

		if (firstDist > secondDist) {
			closer = secondIntersec;
			closerDistance = secondDist;

		} else {
			closer = firstIntersec;
			closerDistance = firstDist;
		}

		boolean outer = false;

		if (closer.sub(center).norm() > radius) {
			outer = true;
		}

		return new RayIntersectionImpl(closer, closerDistance, outer);

	}

	/**
	 * Returns the center of the {@link Sphere}.
	 * 
	 * @return the center of the {@link Sphere}.
	 */
	public Point3D getCenter() {
		return center;
	}

	/**
	 * Returns the radius of the {@link Sphere}.
	 * 
	 * @return radius of the {@link Sphere}
	 */
	public double getRadius() {
		return radius;
	}

	/**
	 * Return the diffuse red coefficient of the {@link Sphere}.
	 * 
	 * @return the diffuse red coefficient
	 */
	public double getDiffuseRed() {
		return diffuseRed;
	}

	/**
	 * Returns the diffuse green coefficient of the {@link Sphere}.
	 * 
	 * @return the diffuse green coefficient
	 */
	public double getDiffuseGreen() {
		return diffuseGreen;
	}

	/**
	 * Returns the diffuse blue coefficient of the {@link Sphere}.
	 * 
	 * @return the diffuse blue coefficient
	 */
	public double getDiffuseBlue() {
		return diffuseBlue;
	}

	/**
	 * Returns the reflective red coefficient of the {@link Sphere}.
	 * 
	 * @return the reflective red coefficient
	 */
	public double getReflectiveRed() {
		return reflectiveRed;
	}

	/**
	 * Returns reflective green coefficient of the {@link Sphere}.
	 * 
	 * @return the reflective green coefficient
	 */
	public double getReflectiveGreen() {
		return reflectiveGreen;
	}

	/**
	 * Returns the reflective blue coefficient of the {@link Sphere}.
	 * 
	 * @return the reflective blue coefficient
	 */
	public double getReflectiveBlue() {
		return reflectiveBlue;
	}

	/**
	 * Returns the power exponent for the illumination of the {@link Sphere}
	 * 
	 * @return the power exponent for the illumination
	 */
	public double getPowerExp() {
		return powerExp;
	}

	/**
	 * This nested class represents the intersection beetwen the two rays of
	 * light. It extends the {@link RayIntersection} class, imolementing her
	 * abstract methods.
	 * 
	 * @author Matej Balun
	 *
	 */
	private class RayIntersectionImpl extends RayIntersection {

		protected RayIntersectionImpl(Point3D point, double distance, boolean outer) {
			super(point, distance, outer);
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Point3D getNormal() {
			return this.getPoint().sub(center).normalize();
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public double getKdr() {
			return diffuseRed;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public double getKdg() {
			return diffuseGreen;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public double getKdb() {
			return diffuseBlue;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public double getKrr() {
			return reflectiveRed;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public double getKrg() {
			return reflectiveGreen;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public double getKrb() {
			return reflectiveBlue;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public double getKrn() {
			return powerExp;
		}

	}

}
