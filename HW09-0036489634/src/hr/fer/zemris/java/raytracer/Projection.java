package hr.fer.zemris.java.raytracer;

import java.util.concurrent.RecursiveAction;

import hr.fer.zemris.java.raytracer.model.Point3D;
import hr.fer.zemris.java.raytracer.model.Ray;
import hr.fer.zemris.java.raytracer.model.Scene;

import static hr.fer.zemris.java.raytracer.RayCaster.*;

/**
 * This class calculates the image data for rendering the three-dimensional
 * image with ray-model, using the multi-thread environment via
 * {@link RecursiveAction}.
 * 
 * @author Matej Balun
 *
 */
public class Projection extends RecursiveAction {

	/**
	 * The calculated serial version UID for this {@link Projection} class.
	 */
	private static final long serialVersionUID = 7219190891578364619L;

	/**
	 * the specified {@link Scene} of the image.
	 */
	private Scene scene;

	/**
	 * The minimum y-axis value.
	 */
	private int yMin;

	/**
	 * The maximum y-axis value.
	 */
	private int yMax;

	/**
	 * Width of the strip.
	 */
	private int width;

	/**
	 * Height of the strip.
	 */
	private int height;

	/**
	 * width oof the space.
	 */
	private double horizontal;

	/**
	 * Height of the space.
	 */
	private double vertical;

	/**
	 * Upper left corner of the immage.
	 */
	private Point3D corner;

	/**
	 * The x-axis vector.
	 */
	private Point3D xAxis;

	/**
	 * The y-axis vector.
	 */
	private Point3D yAxis;

	/**
	 * The z-axis vectar (viewer's perspective)
	 */
	private Point3D zAxis;

	/**
	 * Red color scheme.
	 */
	private short[] red;

	/**
	 * Blue color scheme.
	 */
	private short[] blue;

	/**
	 * Green color scheme.
	 */
	private short[] green;

	/**
	 * This constructor initializes the {@link Projection} object with it's
	 * specified parameters.
	 * 
	 * @param scene
	 *            the specified {@link Scene} of the image
	 * @param yMin
	 *            The minimum y-axis value
	 * @param yMax
	 *            The maximum y-axis value
	 * @param width
	 *            Width of the strip
	 * @param height
	 *            Height of the strip
	 * @param horizontal
	 *            width oof the space
	 * @param vertical
	 *            Height of the space
	 * @param corner
	 *            Upper left corner of the immage
	 * @param xAxis
	 *            The x-axis vector
	 * @param yAxis
	 *            The y-axis vector
	 * @param zAxis
	 *            The z-axis vectar (viewer's perspective)
	 * @param red
	 *            Red color scheme
	 * @param blue
	 *            Blue color scheme
	 * @param green
	 *            Green color scheme
	 */
	public Projection(Scene scene, int yMin, int yMax, int width, int height, double horizontal, double vertical,
			Point3D corner, Point3D xAxis, Point3D yAxis, Point3D zAxis, short[] red, short[] blue, short[] green) {

		super();
		this.scene = scene;
		this.yMin = yMin;
		this.yMax = yMax;
		this.width = width;
		this.height = height;
		this.horizontal = horizontal;
		this.vertical = vertical;
		this.corner = corner;
		this.xAxis = xAxis;
		this.yAxis = yAxis;
		this.zAxis = zAxis;
		this.red = red;
		this.blue = blue;
		this.green = green;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void compute() {

		int dist = yMax - yMin + 1;

		if (dist <= (8 / Runtime.getRuntime().availableProcessors())) {
			nonRecursive();
			return;
		}

		RecursiveAction task1 = new Projection(scene, yMin, yMin + (yMax - yMin) / 2, width, height, horizontal,
				vertical, corner, xAxis, yAxis, zAxis, red, blue, green);

		RecursiveAction task2 = new Projection(scene, yMin + (yMax - yMin) / 2, yMax, width, height, horizontal,
				vertical, corner, xAxis, yAxis, zAxis, red, blue, green);

		invokeAll(task1, task2);
	}

	/**
	 * This method computes the projection and the color scheme non recursively,
	 * after the certain number of recursive iterations was achieved.
	 */
	private void nonRecursive() {
		short[] rgb = new short[3];
		int offset = yMin * width;
		for (int y = yMin; y < yMax; y++) {
			for (int x = 0; x < width; x++) {
				Point3D screenPoint = corner.add(xAxis.scalarMultiply(x / (width - 1.0) * horizontal))
						.sub(yAxis.scalarMultiply(y / (height - 1.0) * vertical));

				Ray ray = Ray.fromPoints(zAxis, screenPoint);

				tracer(scene, ray, rgb);

				red[offset] = rgb[0] > 255 ? 255 : rgb[0];
				green[offset] = rgb[1] > 255 ? 255 : rgb[1];
				blue[offset] = rgb[2] > 255 ? 255 : rgb[2];

				offset++;
			}
		}

	}

}
