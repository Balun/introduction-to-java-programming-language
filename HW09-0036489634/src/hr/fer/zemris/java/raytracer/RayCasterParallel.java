package hr.fer.zemris.java.raytracer;

import java.util.concurrent.ForkJoinPool;
import hr.fer.zemris.java.raytracer.model.IRayTracerProducer;
import hr.fer.zemris.java.raytracer.model.IRayTracerResultObserver;
import hr.fer.zemris.java.raytracer.model.Point3D;
import hr.fer.zemris.java.raytracer.model.Scene;
import hr.fer.zemris.java.raytracer.model.Sphere;
import hr.fer.zemris.java.raytracer.viewer.RayTracerViewer;

/**
 * This program thre-dimensional projection of {@link Sphere}s simulating the
 * projection with the geometric-based ray structure. The program produces the
 * image on the specified raster using the RGF (red-green-blue) rendering for
 * color based image. The program works in the multi-thread environment,
 * offering the best ussage of avaiable processors and cores, using the
 * {@link ForkJoinPool}.
 * 
 * @author Matej Balun
 *
 */
public class RayCasterParallel {

	/**
	 * The main method that starts the multi-thread projection.
	 */
	public static void main(String[] args) {
		RayTracerViewer.show(getIRayTracerProducer(), new Point3D(10, 0, 0), new Point3D(0, 0, 0),
				new Point3D(0, 0, 10), 20, 20);
	}

	/**
	 * This method returns the new nstance of class implementing the
	 * {@link IRayTracerProducer} interface. The class is specified to use the
	 * {@link ForkJoinPool} in the multi-thread environment.
	 * 
	 * @return new implemented instance of {@link IRayTracerProducer}, using the
	 *         multi-thread environment.
	 */
	private static IRayTracerProducer getIRayTracerProducer() {

		return new IRayTracerProducer() {

			/**
			 * {@inheritDoc}
			 */
			@Override
			public void produce(Point3D eye, Point3D view, Point3D viewUp, double horizontal, double vertical,
					int width, int height, long requestNo, IRayTracerResultObserver observer) {

				System.out.println("Startig calculations...");

				short[] red = new short[width * height];
				short[] green = new short[width * height];
				short[] blue = new short[width * height];

				Point3D zAxis = view.sub(eye).modifyNormalize();
				Point3D yAxis = viewUp.normalize().sub(zAxis.scalarMultiply(viewUp.normalize().scalarProduct(zAxis)));
				Point3D xAxis = zAxis.vectorProduct(yAxis).normalize();

				Point3D corner = view.sub(xAxis.scalarMultiply(horizontal / 2.))
						.add(yAxis.scalarMultiply(vertical / 2.));

				Scene scene = RayTracerViewer.createPredefinedScene();

				ForkJoinPool forkJoinPool = new ForkJoinPool();
				forkJoinPool.invoke(new Projection(scene, 0, height, width, height, horizontal, vertical, corner, xAxis,
						yAxis, eye, red, blue, green));
				forkJoinPool.shutdown();

				System.out.println("Computations finished...");
				observer.acceptResult(red, green, blue, requestNo);
				System.out.println("Notification finished...");
			}
		};
	}

}
