package hr.fer.zemris.java.tecaj.hw6.observer2;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents a custom storage of an integer value. The storage
 * participates as a subject in an observer pattern, containing the list of
 * registered observers for the storage. On every change of the value, the list
 * of observers is traversed and and all the registered observers are informed
 * about the change, executing their specified actions.
 * 
 * @author Matej Balun
 *
 */
public class IntegerStorage {

	/**
	 * Integer value of the storage.
	 */
	private int value;

	/**
	 * The list of registered observers.
	 */
	private List<IntegerStorageObserver> observers;

	/**
	 * The list for removal from last iteration over the observers.
	 */
	private List<IntegerStorageObserver> forRemoval;

	/**
	 * Is the storage currently in iteration.
	 */
	private boolean inIteration;

	/**
	 * Initialises the storage with the specified value.
	 * 
	 * @param initialValue
	 *            the specified storage value
	 */
	public IntegerStorage(int initialValue) {
		this.value = initialValue;
		this.forRemoval = new ArrayList<>();
		this.inIteration = false;
	}

	/**
	 * Returns the iteration status of the storage.
	 * 
	 * @return true if the storage is currently iterating over a {@link List} of
	 *         observers, otherwise false
	 */
	public boolean getIterationStatus() {
		return this.inIteration;
	}

	/**
	 * Returns the list of observers to be removed after the current iteration ends.
	 * @return the list of observers to be removed.
	 */
	public List<IntegerStorageObserver> getForRemovalList(){
		return this.forRemoval;
	}

	/**
	 * Adds the observer to the list of registered observers.
	 * 
	 * @param observer
	 *            the specified observer for registration
	 */
	public void addObserver(IntegerStorageObserver observer) {

		if (this.observers == null) {
			this.observers = new ArrayList<>();
			this.observers.add(observer);

		} else if (!this.observers.contains(observer)) {
			this.observers.add(observer);
		}
	}

	/**
	 * Removes the specified observer from the list of registered observers.
	 * 
	 * @param observer
	 *            the specified observer for removal.
	 */
	public void removeObserver(IntegerStorageObserver observer) {

		if (this.observers.contains(observer)) {
			this.observers.remove(observer);
		}
	}

	/**
	 * Unregisters all the observers in the list.
	 */
	public void clearObservers() {
		this.observers.clear();
	}

	/**
	 * Returns the value of the storage
	 * 
	 * @return value of the storage
	 */
	public int getValue() {
		return value;
	}

	/**
	 * Sets the value of the storage if the value is different from the current
	 * value. If the change is successful, all the registered observers are
	 * informed about the change of value, executing their designated
	 * operations.
	 * 
	 * @param value
	 *            the new value for the storage
	 */
	public void setValue(int value) {

		if (this.value != value) {
			int oldValue = this.value;
			this.value = value;

			if (observers != null) {
				IntegerStorageChange change = new IntegerStorageChange(this, oldValue, this.value);
				this.inIteration = true;

				for (IntegerStorageObserver observer : observers) {
					observer.valueChanged(change);

				}

				this.inIteration = false;
				this.observers.removeAll(forRemoval);
				this.forRemoval.clear();
			}
		}
	}
}