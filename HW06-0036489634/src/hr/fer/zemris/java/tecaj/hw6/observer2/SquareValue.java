package hr.fer.zemris.java.tecaj.hw6.observer2;

/**
 * This class implements the observer interface {@link IntegerStorageObserver}.
 * It tracks the given object for the changes in in its fields and prints the
 * square value of the new value on the standard output when the change occurs.
 * 
 * @author Matej Balun
 *
 */
public class SquareValue implements IntegerStorageObserver {

	/**
	 * {@inheritDoc} This method prints the square value of the new value on the
	 * standard output.
	 */
	@Override
	public void valueChanged(IntegerStorageChange storageChange) {

		System.out.println("Provided new value: " + storageChange.getAfter() + ", square is "
				+ storageChange.getStorage().getValue() * storageChange.getStorage().getValue()
				+ ", previous square value was " + storageChange.getBefore() * storageChange.getBefore());
	}
}
