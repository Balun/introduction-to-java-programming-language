package hr.fer.zemris.java.tecaj.hw6.deo5;

import java.util.Comparator;

/**
 * This class represents a record of a course taken by the specified student.
 * The class contains the student's attributes on the course as read-only
 * values, as well as some {@link Comparator}s for various types of comparing.
 * 
 * @author Matej Balun
 *
 */
public class StudentRecord {

	/**
	 * Jmbag of the student.
	 */
	private String jmbag;

	/**
	 * Surname of the student.
	 */
	private String surName;

	/**
	 * Name of the student.
	 */
	private String name;

	/**
	 * Number of points achieved on the mid-term exam.
	 */
	private double midTermPoints;

	/**
	 * 
	 * Number of points achieved on the final exam.
	 */
	private double finalsPoints;

	/**
	 * Number of points achieved on the laboratory exercises.
	 */
	private double labPoints;

	/**
	 * Final grade of the student on the course.
	 */
	private int finalGrade;

	/**
	 * The comparator for comparing based on jmbags.
	 */
	public static final Comparator<StudentRecord> BY_JMBAG = (s1, s2) -> s1.jmbag.compareTo(s2.jmbag);

	/**
	 * The comparator for comparing based on total points number.
	 */
	public static final Comparator<StudentRecord> BY_ALL_POINTS = new Comparator<StudentRecord>() {

		/**
		 * {@inheritDoc}
		 */
		@Override
		public int compare(StudentRecord arg0, StudentRecord arg1) {
			Double s1 = arg0.finalsPoints + arg0.labPoints + arg0.midTermPoints;
			Double s2 = arg1.finalsPoints + arg1.labPoints + arg1.midTermPoints;
			return s1.compareTo(s2);
		}
	};

	/**
	 * This constructor initialises the {@link StudentRecord} with the student's
	 * attributes on the course.
	 * 
	 * @param jmbag
	 *            - jmbag of the student
	 * @param surName
	 *            - surname of the student
	 * @param name
	 *            - name of the student
	 * @param midTermPoints
	 *            - points achieved on the mid-term exam
	 * @param finalsPoints
	 *            - points achieved on the final exam
	 * @param labPoints
	 *            - points achieved on the laboratory exercises
	 * @param finalGrade
	 *            - final grade of the student on the course
	 */
	public StudentRecord(String jmbag, String surName, String name, double midTermPoints, double finalsPoints,
			double labPoints, int finalGrade) {
		super();

		this.jmbag = jmbag;
		this.surName = surName;
		this.name = name;
		this.midTermPoints = midTermPoints;
		this.finalsPoints = finalsPoints;
		this.labPoints = labPoints;
		this.finalGrade = finalGrade;
	}

	/**
	 * Return the student's jmbag
	 * 
	 * @return the student's jmbag
	 */
	public String getJmbag() {
		return jmbag;
	}

	/**
	 * Return the student's surname
	 * 
	 * @return the student's surname
	 */
	public String getSurName() {
		return surName;
	}

	/**
	 * Return the student's name
	 * 
	 * @return the student's name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Return the student's points on the mid-term exam
	 * 
	 * @return the student's points on the mid-term exam
	 */
	public double getMidTermPoints() {
		return midTermPoints;
	}

	/**
	 * Return the student's points on the final exam.
	 * 
	 * @return the student's points on the final exam
	 */
	public double getFinalsPoints() {
		return finalsPoints;
	}

	/**
	 * Return the student's points on the laboratory exercises.
	 * 
	 * @return the student's points on the laboratory exercises
	 */
	public double getLabPoints() {
		return labPoints;
	}

	/**
	 * Return the student's final grade on the course.
	 * 
	 * @return the student's final grade on the course
	 */
	public int getFinalGrade() {
		return finalGrade;
	}
}
