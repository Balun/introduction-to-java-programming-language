package hr.fer.zemris.java.tecaj.hw6.observer2;

/**
 * This class represents the event for {@link IntegerStorage} subject in
 * Observer pattern. The class contains value before and after the change, as
 * well as a reference to the changed {@link IntegerStorage} object.
 * 
 * @author Matej Balun
 *
 */
public class IntegerStorageChange {

	/**
	 * A reference to the changed object.
	 */
	private IntegerStorage storage;

	/**
	 * Value before the change.
	 */
	private int before;

	/**
	 * Value after the change.
	 */
	private int after;

	/**
	 * Initialises the {@link IntegerStorageChange} with a reference to the
	 * changed {@link IntegerStorage} object and with values before and after
	 * the change.
	 * 
	 * @param storage
	 *            the changed {@link IntegerStorage} object
	 * @param before
	 *            value before the change
	 * @param after
	 *            value after the change
	 */
	public IntegerStorageChange(IntegerStorage storage, int before, int after) {
		super();
		this.storage = storage;
		this.before = before;
		this.after = after;
	}

	/**
	 * Returns the reference to the changed {@link IntegerStorage} object.
	 * 
	 * @return the changed {@link IntegerStorage} object
	 */
	public IntegerStorage getStorage() {
		return storage;
	}

	/**
	 * Returns the value of the {@link IntegerStorage} object before the change.
	 * 
	 * @return Value before the change
	 */
	public int getBefore() {
		return before;
	}

	/**
	 * Returns the value of the {@link IntegerStorage} object after the change.
	 * 
	 * @return Value after the change
	 */
	public int getAfter() {
		return after;
	}

}
