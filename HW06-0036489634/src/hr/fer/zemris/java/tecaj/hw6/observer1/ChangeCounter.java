package hr.fer.zemris.java.tecaj.hw6.observer1;

/**
 * This class implements the observer interface {@link IntegerStorageObserver}.
 * It tracks the given object for the changes in in its fields and counts the
 * changes, writing them to standard output.
 * 
 * @author Matej Balun
 *
 */
public class ChangeCounter implements IntegerStorageObserver {

	/**
	 * The number of changes made on the specified object for tracking.
	 */
	private int changes = 0;

	/**
	 * {@inheritDoc} This method counts the number of changes made and prints
	 * them to the standard output.
	 */
	@Override
	public void valueChanged(IntegerStorage istorage) {
		this.changes++;
		System.out.println("Number of value changes since tracking: " + this.changes);
	}

}
