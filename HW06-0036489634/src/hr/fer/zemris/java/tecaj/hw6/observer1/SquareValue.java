package hr.fer.zemris.java.tecaj.hw6.observer1;

/**
 * This class implements the observer interface {@link IntegerStorageObserver}.
 * It tracks the given object for the changes in in its fields and prints the
 * square value of the new value on the standard output when the change occurs.
 * 
 * @author Matej Balun
 *
 */
public class SquareValue implements IntegerStorageObserver {

	/**
	 * {@inheritDoc} This method prints the square value of the new value on the
	 * standard output.
	 */
	@Override
	public void valueChanged(IntegerStorage istorage) {
		System.out.println("Provided new value: " + istorage.getValue() + ", square is "
				+ istorage.getValue() * istorage.getValue());
	}

}
