package hr.fer.zemris.java.tecaj.hw6.observer1;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents a custom storage of an integer value. The storage
 * participates as a subject in an observer pattern, containing the list of
 * registered observers for the storage. On every change of the value, the list
 * of observers is traversed and and all the registered observers are informed
 * about the change, executing their specified actions.
 * 
 * @author Matej Balun
 *
 */
public class IntegerStorage {

	/**
	 * Integer value of the storage.
	 */
	private int value;

	/**
	 * Indicates if the storage is currently in iteration over a collection of
	 * observers.
	 */
	private boolean inIeration;

	/**
	 * The list of registered observers.
	 */
	private List<IntegerStorageObserver> observers;

	/**
	 * The list containing the observers to be removed when the current
	 * iteration ends.
	 */
	private List<IntegerStorageObserver> forRemoval;

	/**
	 * Initialises the storage with the specified value.
	 * 
	 * @param initialValue
	 *            the specified storage value
	 */
	public IntegerStorage(int initialValue) {
		this.value = initialValue;
		this.forRemoval = new ArrayList<>();
		this.inIeration = false;
	}

	/**
	 * Returns the iteration status of this storage.
	 * 
	 * @return the iteration status of this storage
	 */
	public boolean getIterationStatus() {
		return this.inIeration;
	}

	/**
	 * Returns the list of observers to be removed when the current iteration
	 * ends.
	 * 
	 * @return the list of to-be-removed observers
	 */
	public List<IntegerStorageObserver> getForRemovalList() {
		return this.forRemoval;
	}

	/**
	 * Adds the observer to the list of registered observers.
	 * 
	 * @param observer
	 *            the specified observer for registration
	 */
	public void addObserver(IntegerStorageObserver observer) {

		if (this.observers == null) {
			this.observers = new ArrayList<>();
			this.observers.add(observer);

		} else if (!this.observers.contains(observer)) {
			this.observers.add(observer);
		}
	}

	/**
	 * Removes the specified observer from the list of registered observers.
	 * 
	 * @param observer
	 *            the specified observer for removal.
	 */
	public void removeObserver(IntegerStorageObserver observer) {

		if (this.observers.contains(observer)) {
			this.observers.remove(observer);
		}
	}

	/**
	 * Unregisters all the observers in the list.
	 */
	public void clearObservers() {
		this.observers.clear();
	}

	/**
	 * Returns the value of the storage
	 * 
	 * @return value of the storage
	 */
	public int getValue() {
		return value;
	}

	/**
	 * Sets the value of the storage if the value is different from the current
	 * value. If the change is successful, all the registered observers are
	 * informed about the change of value, executing their designated
	 * operations.
	 * 
	 * @param value
	 *            the new value for the storage
	 */
	public void setValue(int value) {

		if (this.value != value) {
			this.value = value;
			if (observers != null) {

				this.inIeration = true;

				for (IntegerStorageObserver observer : observers) {
					observer.valueChanged(this);

				}

				this.inIeration = false;
				this.observers.removeAll(forRemoval);
				this.forRemoval.clear();
			}
		}
	}
}