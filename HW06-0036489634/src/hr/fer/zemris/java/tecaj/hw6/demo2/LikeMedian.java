package hr.fer.zemris.java.tecaj.hw6.demo2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.TreeSet;

/**
 * This generic class represents a container for the various type of elements.
 * The class returns the median element of the collection. All generic types
 * must have the natural order defined and implement the {@link Comparable}
 * interface.
 * 
 * @author Matej Balun
 *
 * @param <T>
 *            the specified parameter of the container value
 */
public class LikeMedian<T> implements Iterable<T> {

	/**
	 * The internal collection of elements, favours the natural order, without
	 * duplicates.
	 */
	private List<T> elements;

	/**
	 * The internal {@link List} specified for elements with defined natural
	 * order for median calculation.
	 */
	private List<T> forCalculation;

	/**
	 * This constructor initialises the {@link LikeMedian} class with the new
	 * instance of {@link TreeSet} in specified reversed comparator.
	 */
	public LikeMedian() {
		this.elements = new ArrayList<>();
		this.forCalculation = new ArrayList<>();
	}

	/**
	 * Adds the value to the internal collection.
	 * 
	 * @param value
	 *            the specified value for insertion
	 */
	public void add(T value) {
		this.elements.add(value);
		this.forCalculation.add(value);
	}

	/**
	 * Returns the median element of the collection. If the collection is empty,
	 * the method returns a new instance of {@link Optional} with empty value,
	 * otherwise return the median element wrapped in a new instance of
	 * {@link Optional} with value of the specified median.
	 * 
	 * @return the {@link Optional} value of the median
	 */
	public Optional<T> get() {
		if (this.elements.isEmpty()) {
			return Optional.empty();
		}
		
		this.forCalculation.sort(Collections.reverseOrder());
		
		int median = this.forCalculation.size() / 2;
		int current = 0;

		for (T element : this.forCalculation) {
			if (current == median) {
				return Optional.of(element);
			} else {
				current++;
			}
		}

		return Optional.empty();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Iterator<T> iterator() {
		return this.elements.iterator();
	}

}
