package hr.fer.zemris.java.tecaj.hw6.demo3;

/**
 * This program demonstrates working with simple collection of prime numbers.
 * 
 * @author Matej Balun
 *
 */
public class PrimesDemo1 {

	/**
	 * The main method that executes the program.
	 */
	public static void main(String[] args) {

		PrimesCollection primesCollection = new PrimesCollection(7);

		for (Integer prime : primesCollection) {
			System.out.println("Got prime: " + prime);
		}
	}

}
