package hr.fer.zemris.java.tecaj.hw6.observer1;

/**
 * This interface represents the observer in an Observer design pattern. The
 * subject of the observer is class {@link IntegerStorage}.
 * 
 * @author Matej Balun
 *
 */
public interface IntegerStorageObserver {

	/**
	 * Performs the specified action when the observer is informed of a value
	 * change.
	 * 
	 * @param istorage
	 *            the subject of the observer
	 */
	public void valueChanged(IntegerStorage istorage);
	
}