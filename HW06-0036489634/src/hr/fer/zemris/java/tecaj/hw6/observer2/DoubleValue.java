package hr.fer.zemris.java.tecaj.hw6.observer2;

/**
 * This class implements the observer interface {@link IntegerStorageObserver}.
 * It tracks the given object for the changes in in its fields and prints the
 * double value of the value field when the change occurs. The class prints the
 * double values for a specified number of times.
 * 
 * @author Matej Balun
 *
 */
public class DoubleValue implements IntegerStorageObserver {

	/**
	 * Number of times to print the double value.
	 */
	private int times;

	/**
	 * Initialises the class, specifying the number of times for tracking.
	 * 
	 * @param times
	 *            the specified number of times for tracking
	 * @throws IllegalArgumentException
	 *             if the specified number of time for action is less or equal
	 *             to zero
	 */
	public DoubleValue(int times) {
		if (times <= 0) {
			throw new IllegalArgumentException("The number of times for action performed must be greater than zero");
		}

		this.times = times;
	}

	/**
	 * {@inheritDoc} This method prints the double value of the changed value to
	 * the standard output for the specified number of times.
	 */
	@Override
	public void valueChanged(IntegerStorageChange storageChange) {

		System.out.println("Double value: " + 2 * storageChange.getAfter() + ",  previous value was "
				+ storageChange.getBefore() + " and the value after the change is " + storageChange.getAfter());
		this.times--;

		if (this.times == 0 && storageChange.getStorage().getIterationStatus()) {
			storageChange.getStorage().getForRemovalList().add(this);
		} else if (this.times == 0) {
			storageChange.getStorage().removeObserver(this);
		}

	}
}
