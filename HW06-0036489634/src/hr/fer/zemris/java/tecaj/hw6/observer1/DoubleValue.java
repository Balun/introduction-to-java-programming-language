package hr.fer.zemris.java.tecaj.hw6.observer1;

/**
 * This class implements the observer interface {@link IntegerStorageObserver}.
 * It tracks the given object for the changes in in its fields and prints the
 * double value of the value field when the change occurs. The class prints the
 * double values for a specified number of times.
 * 
 * @author Matej Balun
 *
 */
public class DoubleValue implements IntegerStorageObserver {

	/**
	 * Number of times to print the double value.
	 */
	private int times;

	/**
	 * Initialises the class, specifying the number of times for tracking.
	 * 
	 * @param times
	 *            the specified number of times for tracking
	 * @throws IllegalArgumentException
	 *             if the number of times for action is less or equal to zero.
	 */
	public DoubleValue(int times) {
		if (times <= 0) {
			throw new IllegalArgumentException("The number of actions must be greater than zero.");
		}

		this.times = times;
	}

	/**
	 * {@inheritDoc} This method prints the double value of the changed value to
	 * the standard output for the specified number of times.
	 */
	@Override
	public void valueChanged(IntegerStorage istorage) {
		System.out.println("Double value: " + 2 * istorage.getValue());
		this.times--;

		if (this.times == 0 && istorage.getIterationStatus()) {
			istorage.getForRemovalList().add(this);
			
		} else if (this.times == 0) {
			istorage.removeObserver(this);
		}
	}

}
