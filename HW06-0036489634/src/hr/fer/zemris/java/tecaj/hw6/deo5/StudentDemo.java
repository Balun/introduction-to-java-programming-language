package hr.fer.zemris.java.tecaj.hw6.deo5;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * This program demonstrates the work with the Java Collection Stream API. The
 * program opens a text file containing the student records for the course as
 * {@link String}s and converts them into {@link StudentRecord} objects. The
 * program applies several stream filtering, mapping and counting operations
 * without ever changing the initial collection of records. All the requested
 * informations from the streams are written on the standard output-
 *
 * @author Matej Balun
 *
 */
public class StudentDemo {

	/**
	 * The main method that executes the program.
	 *
	 * @throws IOException
	 *             if there is an error in reading the file.
	 */
	public static void main(String[] args) throws IOException {

		List<String> lines = Files.readAllLines(Paths.get("Studenti.txt"));
		List<StudentRecord> records = convert(lines);

		// task 1
		countStudents(records);

		// task 2
		countExcellent(records);

		// task 3
		getExcellent(records);

		// task 4
		getExcellentSorted(records);

		// task 5
		getFailedSorted(records);

		// task 6
		mapByGrades(records);

		// task 7
		mapByGradesAndCount(records);

		// task 8
		mapBySucces(records);

	}

	/**
	 * This method converts the {@link List} of {@link String}s containing lines
	 * of the file into the {@link List} of {@link StudentRecord}s.
	 *
	 * @param lines
	 *            - the initial {@link List} of {@link String}s.
	 * @return the {@link List} of {@link StudentRecord}s
	 */
	private static List<StudentRecord> convert(List<String> lines) {

		List<StudentRecord> list = new ArrayList<>();

		for (String line : lines) {
			String[] attrs = line.split("\t");
			if (attrs.length != 7) {
				break;
			}

			list.add(new StudentRecord(attrs[0], attrs[1], attrs[2], Double.parseDouble(attrs[3]),
					Double.parseDouble(attrs[4]), Double.parseDouble(attrs[5]), Integer.parseInt(attrs[6])));
		}

		return list;
	}

	/**
	 * Prints the number of students that have more than 25 points on mid-terms,
	 * finals and labs altogether. The method uses the {@link Stream} API for
	 * the calculation.
	 *
	 * @param records
	 *            {@link List} of {@link StudentRecord}s
	 */
	private static void countStudents(List<StudentRecord> records) {

		long count = records.stream().filter(s -> (s.getFinalsPoints() + s.getLabPoints() + s.getMidTermPoints()) > 25)
				.count();

		System.out.println(
				"The number of students that have more than 25 points on mid-terms, finals and labs: " + count);
	}

	/**
	 * Prints the number of student with grade excellent(5) on the course. The
	 * method uses the {@link Stream} API for the calculation.
	 *
	 * @param records
	 *            {@link List} of {@link StudentRecord}s.
	 */
	private static void countExcellent(List<StudentRecord> records) {

		long numberOfExcellent = records.stream().filter(s -> s.getFinalGrade() == 5).count();

		System.out.println("\nThe number of students that scored excellent(5) on the course: " + numberOfExcellent);
	}

	/**
	 * Prints the {@link List} of students that have scored excellent(5) on the
	 * course. The method uses {@link Stream} API for collecting.
	 *
	 * @param records
	 *            {@link List} of {@link StudentRecord}s.
	 */
	private static void getExcellent(List<StudentRecord> records) {

		List<StudentRecord> excellent = records.stream().filter(s -> s.getFinalGrade() == 5)
				.collect(Collectors.toList());

		System.out.println("\nStudents that scored excellent(5) on the course: ");

		excellent.forEach(s -> System.out.printf("%10s %10s\n", s.getSurName(), s.getName()));
	}

	/**
	 * Prints the {@link List} of students that have scored excellent(5) on the
	 * course sorted by the total number of points. The method uses
	 * {@link Stream} API for collecting.
	 *
	 * @param records
	 *            {@link List} of {@link StudentRecord}s.
	 */
	private static void getExcellentSorted(List<StudentRecord> records) {

		List<StudentRecord> excellentSorted = records.stream().filter(s -> s.getFinalGrade() == 5)
				.sorted(StudentRecord.BY_ALL_POINTS).collect(Collectors.toList());

		System.out.println("\nStudents that scored excellent(5) on the course, sorted by points: ");

		excellentSorted.forEach(s -> System.out.printf("%10s %10s %10f\n", s.getSurName(), s.getName(),
				s.getFinalsPoints() + s.getLabPoints() + s.getMidTermPoints()));
	}

	/**
	 * Prints the {@link List} of students that have failed the course sorted by
	 * their jmbags. The method uses {@link Stream} API for collecting.
	 *
	 * @param records
	 *            {@link List} of {@link StudentRecord}s.
	 */
	private static void getFailedSorted(List<StudentRecord> records) {

		List<String> failedJMBAGs = records.stream().filter(s -> s.getFinalGrade() == 1).sorted(StudentRecord.BY_JMBAG)
				.map(StudentRecord::getJmbag).collect(Collectors.toList());

		System.out.println("\nJmbags of students that have failed the copurse, sorted by jmbag: ");

		failedJMBAGs.forEach(System.out::println);
	}

	/**
	 * The method maps the the students based on their final grade on the course
	 * and prints the mapping. The method uses {@link Stream} API for
	 * collecting.
	 *
	 * @param records
	 *            {@link List} of {@link StudentRecord}s.
	 */
	private static void mapByGrades(List<StudentRecord> records) {

		Map<Integer, List<StudentRecord>> mapByGrades = records.stream()
				.collect(Collectors.groupingBy(StudentRecord::getFinalGrade, Collectors.toList()));

		System.out.println("\nStudents mapped by final grades:");

		for (Integer key : mapByGrades.keySet()) {
			System.out.print(key + " -> ");

			for (StudentRecord record : mapByGrades.get(key)) {
				System.out.print(record.getJmbag() + ", ");
			}

			System.out.println();
		}

	}

	/**
	 * The method maps the count of specified grades achieved by the students of
	 * the course and prints the mapping. The method uses {@link Stream} API for
	 * collecting.
	 *
	 * @param records
	 *            {@link List} of {@link StudentRecord}s.
	 */
	private static void mapByGradesAndCount(List<StudentRecord> records) {

		Map<Integer, Integer> mapGradesCount = records.stream()
				.collect(Collectors.toMap(StudentRecord::getFinalGrade, s -> 1, (s, a) -> s + a));

		System.out.println("\nFinal grades mapped by count of grades: ");

		mapGradesCount.keySet().forEach(g -> System.out.println(g + " -> " + mapGradesCount.get(g)));
	}

	/**
	 * The method maps the students based on their success on the course and
	 * prints the mapping. of the course and prints the mapping. The method uses
	 * {@link Stream} API for collecting.
	 *
	 * @param records
	 *            {@link List} of {@link StudentRecord}s.
	 */
	private static void mapBySucces(List<StudentRecord> records) {

		Map<Boolean, List<StudentRecord>> mapSucces = records.stream()
				.collect(Collectors.partitioningBy(s -> s.getFinalGrade() > 1));

		System.out.println("\nStudents mapped by passage: ");

		for (Boolean key : mapSucces.keySet()) {
			System.out.print(key + " -> ");

			for (StudentRecord record : mapSucces.get(key)) {
				System.out.print(record.getJmbag() + ", ");
			}

			System.out.println();
		}
	}

}
