package hr.fer.zemris.java.tecaj.hw6.demo3;

/**
 * This program demonstrates working with a simple collection of prime numbers
 * in a nested for-loop context.
 * 
 * @author Matej Balun
 *
 */
public class PrimesDemo2 {

	/**
	 * The main method that executes the program.
	 */
	public static void main(String[] args) {

		PrimesCollection primesCollection = new PrimesCollection(2);

		for (Integer prime : primesCollection) {
			for (Integer prime2 : primesCollection) {
				System.out.println("Got prime pair: " + prime + ", " + prime2);
			}
		}

	}

}
