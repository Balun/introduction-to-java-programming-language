package hr.fer.zemris.java.tecaj.hw6.demo3;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * This class represents a simple collection of prime numbers. The collection
 * contains no actual elements, but calculates the prime numbers when the
 * iterator has been instantiated and called.
 * 
 * @author Matej Balun
 *
 */
public class PrimesCollection implements Iterable<Integer> {

	/**
	 * The number of primes to calculate, size of the collection.
	 */
	private int numOfPrimes;

	/**
	 * This constructor initialises the collection with the number of primes.
	 * 
	 * @param numOfPrimes
	 *            the specified size of the collection
	 * @throws IllegalArgumentException
	 *             if the input is less than zero
	 */
	public PrimesCollection(int numOfPrimes) {
		if (numOfPrimes < 0) {
			throw new IllegalArgumentException("The number of primes must not be less than zero");
		}

		this.numOfPrimes = numOfPrimes;
	}

	/**
	 * Returns the size of the collection.
	 * 
	 * @return size of the collection
	 */
	public int getNumberOfPrimes() {
		return this.numOfPrimes;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Iterator<Integer> iterator() {
		return new primesIterator();
	}

	/**
	 * This custom iterator class does not iterate over a fixed collection; It
	 * calculates and returns the prime numbers as long as counter has reached
	 * the specified number of primes.
	 * 
	 * @author Matej Balun
	 *
	 */
	private class primesIterator implements Iterator<Integer> {

		/**
		 * Current index of the iterator.
		 */
		private int currentIndex = 0;

		/**
		 * {@inheritDoc}
		 */
		@Override
		public boolean hasNext() {
			if (currentIndex < numOfPrimes) {
				return true;
			}

			return false;
		}

		/**
		 * {@inheritDoc} Delegates the calculation of current prime element to
		 * other methods.
		 */
		@Override
		public Integer next() {
			if (!hasNext()) {
				throw new NoSuchElementException();
			}

			return getPrime(++currentIndex);
		}

		/**
		 * Returns the n-th prime number.
		 * 
		 * @param n
		 *            the specified prime index
		 * @return n-th prime number
		 */
		private Integer getPrime(int n) {
			int counter = 0;

			for (int i = 1;; i++) {
				if (isPrime(i)) {
					counter++;
				}

				if (counter == n) {
					return i;
				}
			}
		}

		/**
		 * Checks if the specified number is a prime number.
		 * 
		 * @param n
		 *            the specified number
		 * @return true if the number is prime, otherwise false
		 */
		private boolean isPrime(int n) {
			if (n < 2) {
				return false;
			}

			double sqrt = Math.sqrt(n);

			for (long i = 2; i <= sqrt; i++) {
				if (n % i == 0)
					return false;
			}
			return true;

		}

	}

}
