package hr.fer.zemris.java.tecaj.hw6.demo2;

import java.util.Optional;

/**
 * This program demonstrates the work with the {@link LikeMedian} class for
 * retrieving the median element in a collection of elements.
 * 
 * @author Matej Balun
 *
 */
public class MedianDemo2 {

	/**
	 * The main method that executes the program.
	 */
	public static void main(String[] args) {

		LikeMedian<String> likeMedian = new LikeMedian<String>();
		likeMedian.add("Joe");
		likeMedian.add("Jane");
		likeMedian.add("Adam");
		likeMedian.add("Zed");
		Optional<String> result = likeMedian.get();
		System.out.println(result.get()); // Writes: Jane
	}

}
