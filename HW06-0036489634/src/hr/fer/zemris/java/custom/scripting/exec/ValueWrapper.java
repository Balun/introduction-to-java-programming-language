package hr.fer.zemris.java.custom.scripting.exec;

import java.util.function.BiFunction;

/**
 * This class represents a wrapper for the several types of values. The legal
 * value types are {@link Integer}, {@link Double}, {@link String} and a
 * null-reference. The class offers methods for mathematical operations over the
 * specified types. {@link String} values are converted in {@link Integer}s or
 * {@link Double}s, depending on the second operand. The null values are
 * interpreted as {@link Integer}s with value zero.
 * 
 * @author Matej Balun
 *
 */
public class ValueWrapper {

	/**
	 * The current value of the wrapper.
	 */
	private Number value;

	/**
	 * The {@link BiFunction} implementation of the integer increment operation.
	 */
	private static final BiFunction<Integer, Integer, Integer> INCREMENT_INT = (x, y) -> x + y;

	/**
	 * The {@link BiFunction} implementation of the integer decrement operation.
	 */
	private static final BiFunction<Integer, Integer, Integer> DECREMENT_INT = (x, y) -> x - y;

	/**
	 * The {@link BiFunction} implementation of the integer multiplication
	 * operation.
	 */
	private static final BiFunction<Integer, Integer, Integer> MULTIPLY_INT = (x, y) -> x * y;

	/**
	 * The {@link BiFunction} implementation of the integer division operation.
	 */
	private static final BiFunction<Integer, Integer, Integer> DIVIDE_INT = (x, y) -> x / y;

	/**
	 * The {@link BiFunction} implementation of the decimal increment operation.
	 */
	private static final BiFunction<Double, Double, Double> INCREMENT_DOUBLE = (x, y) -> x + y;

	/**
	 * The {@link BiFunction} implementation of the decimal decrement operation.
	 */
	private static final BiFunction<Double, Double, Double> DECREMENT_DOUBLE = (x, y) -> x - y;

	/**
	 * The {@link BiFunction} implementation of the decimal multiplication
	 * operation.
	 */
	private static final BiFunction<Double, Double, Double> MULTIPLY_DOUBLE = (x, y) -> x * y;

	/**
	 * The {@link BiFunction} implementation of the decimal division operation.
	 */
	private static final BiFunction<Double, Double, Double> DIVIDE_DOUBLE = (x, y) -> x / y;

	/**
	 * This constructor initialises the {@link ValueWrapper} with the specified
	 * value.
	 * 
	 * @param value
	 *            the specified value of the {@link ValueWrapper}
	 * @throws IllegalArgumentException
	 *             if the value is not one of the legal types (see
	 *             {@link ValueWrapper} class documentation)
	 */
	public ValueWrapper(Object value) {
		value = checkAndTransform(value);
		this.value = (Number) value;
	}

	/**
	 * Returns the current value in the {@link ValueWrapper}.
	 * 
	 * @return the current value
	 */
	public Object getValue() {
		return value;
	}

	/**
	 * Sets the specified value for the wrapper.
	 * 
	 * @param value
	 *            the specified value
	 * @throws IllegalArgumentException
	 *             if the value is not one of the legal types (see
	 *             {@link ValueWrapper} class documentation)
	 */
	public void setValue(Object value) {

		checkValue(value);
		this.value = (Number) value;
	}

	/**
	 * Increments the value currently stored in the {@link ValueWrapper} with
	 * the specified argument value.
	 * 
	 * @param incValue
	 *            the specified value for the operation
	 * @throws IllegalArgumentException
	 *             if the value is not one of the legal types (see
	 *             {@link ValueWrapper} class documentation)
	 */
	public void increment(Object incValue) {

		incValue = checkAndTransform(incValue);

		if ((value instanceof Double) || (incValue instanceof Double)) {
			value = applyDoubleOperation(value, incValue, INCREMENT_DOUBLE);

		} else {
			value = applyIntegerOperation(value, incValue, INCREMENT_INT);
		}
	}

	/**
	 * Decrements the value currently stored in the {@link ValueWrapper} with
	 * the specified argument value.
	 * 
	 * @param incValue
	 *            the specified value for the operation
	 * @throws IllegalArgumentException
	 *             if the value is not one of the legal types (see
	 *             {@link ValueWrapper} class documentation)
	 */
	public void decrement(Object decValue) {

		decValue = checkAndTransform(decValue);

		if ((value instanceof Double) || (decValue instanceof Double)) {
			value = applyDoubleOperation(value, decValue, DECREMENT_DOUBLE);

		} else {
			value = applyIntegerOperation(value, decValue, DECREMENT_INT);
		}
	}

	/**
	 * Multiplies the value currently stored in the {@link ValueWrapper} with
	 * the specified argument value.
	 * 
	 * @param incValue
	 *            the specified value for the operation
	 * @throws IllegalArgumentException
	 *             if the value is not one of the legal types (see
	 *             {@link ValueWrapper} class documentation)
	 */
	public void multiply(Object mulValue) {

		mulValue = checkAndTransform(mulValue);

		if ((value instanceof Double) || (mulValue instanceof Double)) {
			value = applyDoubleOperation(value, mulValue, MULTIPLY_DOUBLE);

		} else {
			value = applyIntegerOperation(value, mulValue, MULTIPLY_INT);
		}
	}

	/**
	 * Divides the value currently stored in the {@link ValueWrapper} with the
	 * specified argument value.
	 * 
	 * @param incValue
	 *            the specified value for the operation
	 * @throws IllegalArgumentException
	 *             if the value is not one of the legal types (see
	 *             {@link ValueWrapper} class documentation)
	 * @throws ArithmeticException
	 *             if the division by zero has occurred.
	 */
	public void divide(Object divValue) {

		divValue = checkAndTransform(divValue);

		if (divValue.toString().equals("0") || divValue.toString().matches("0.(0)+")) {
			throw new ArithmeticException("Cannot divide by zero.");
		}

		if ((value instanceof Double) || (divValue instanceof Double)) {
			value = applyDoubleOperation(value, divValue, DIVIDE_DOUBLE);

		} else {
			value = applyIntegerOperation(value, divValue, DIVIDE_INT);
		}
	}

	/**
	 * Compares the value currently stored in the {@link ValueWrapper} with the
	 * specified argument value.
	 * 
	 * @param incValue
	 *            the specified value for the operation
	 * @return the value greater than 0 if the current value is greater than the
	 *         specified value, less then 0 if the current value is less, and 0
	 *         if the values are equal
	 * @throws IllegalArgumentException
	 *             if the value is not one of the legal types (see
	 *             {@link ValueWrapper} class documentation)
	 */
	public int numCompare(Object withValue) {

		withValue = checkAndTransform(withValue);

		Number tmp = (Number) withValue;
		Double arg1 = this.value.doubleValue();
		Double arg2 = tmp.doubleValue();

		return arg1.compareTo(arg2);

	}

	/**
	 * Checks The given object value if its an instance of the legal types (see
	 * {@link ValueWrapper} documentation). Other methods call this method for
	 * checking and they catch an throw further the possible exception.
	 * 
	 * @param value
	 *            the specified value
	 * @throws IllegalArgumentException
	 *             if the value is not one of the legal types (see
	 *             {@link ValueWrapper} class documentation)
	 */
	private void checkValue(Object value) {

		if (!(value instanceof String) && !(value instanceof Integer) && !(value instanceof Double)
				&& (value != null)) {
			throw new RuntimeException(
					"The value for the wrapper must be an instance of String, Integer or Double, as well as a null-reference");
		}
	}

	/**
	 * Checks the value if its a legal type, and transforms the value (if
	 * needed) into the math compatible value for defined {@link ValueWrapper}
	 * operations (See {@link ValueWrapper} documentation).
	 * 
	 * @param value
	 *            the specified value
	 * @return the possibly transformed value
	 */
	private Object checkAndTransform(Object value) {
		checkValue(value);

		if (value == null) {
			value = new Integer(0);
		} else if (value instanceof String) {

			try {
				if (value.toString().contains(".") || value.toString().contains("E")) {
					value = new Double(Double.parseDouble(value.toString()));
				} else {
					value = new Integer(Integer.parseInt(value.toString()));
				}
			} catch (NumberFormatException e) {
				throw new RuntimeException("Error in parsing string", e.fillInStackTrace());
			}
		}

		return value;
	}

	/**
	 * Applies the specified operation over a two arguments, resulting in a
	 * result of type that is an instance of {@link Integer}.
	 * 
	 * @param arg1
	 *            the first argument
	 * @param arg2
	 *            the second argument
	 * @param function
	 *            the specified operation for the arguments
	 * @return the {@link Integer} result of the operation
	 */
	private Integer applyIntegerOperation(Number arg1, Object arg2, BiFunction<Integer, Integer, Integer> function) {

		return function.apply((Integer) arg1, (Integer) arg2);
	}

	/**
	 * Applies the specified operation over a two arguments, resulting in a
	 * result of type that is an instance of {@link Double}.
	 * 
	 * @param arg1
	 *            the first argument
	 * @param arg2
	 *            the second argument
	 * @param function
	 *            the specified operation for the elements
	 * @return the {@link Double} result of the operation
	 */
	private Double applyDoubleOperation(Number arg1, Object arg2, BiFunction<Double, Double, Double> function) {
		Double newArg1 = null;
		Double newArg2 = null;

		if (arg1 instanceof Integer) {
			newArg1 = Double.valueOf(Double.parseDouble(arg1.toString()));
			newArg2 = (Double) arg2;
		} else {
			newArg2 = Double.valueOf(Double.parseDouble(arg2.toString()));
			newArg1 = (Double) arg1;
		}

		return function.apply(newArg1, newArg2);

	}

}
