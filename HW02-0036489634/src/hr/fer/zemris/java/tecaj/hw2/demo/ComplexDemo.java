package hr.fer.zemris.java.tecaj.hw2.demo;

import hr.fer.zemris.java.tecaj.hw2.ComplexNumber;

/**
 * This program demonstrates the usage of {@link ComplexNumber} custom class. It
 * instances a couple of complex numbers on different ways and executes a couple
 * of operations.
 * 
 * @author Matej Balun
 *
 */
public class ComplexDemo {

	/**
	 * The main method that executes the program.
	 */
	public static void main(String[] args) {

		ComplexNumber c1 = new ComplexNumber(2, 3);
		ComplexNumber c2 = ComplexNumber.parse("2.5-3i");
		ComplexNumber c3 = c1.add(ComplexNumber.fromMagnitudeAndAngle(2, 1.57)).div(c2).power(3).root(2)[1];
		System.out.println(c3);
	}

}
