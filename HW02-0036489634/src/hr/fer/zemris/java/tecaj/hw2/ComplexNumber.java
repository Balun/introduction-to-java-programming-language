package hr.fer.zemris.java.tecaj.hw2;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This custom class represents a complex number. The class offers all the
 * regular methods for operations with complex numbers, as well as static
 * factory methods for instancing new complex numbers. The class also offers
 * methods for basic math operations.
 * 
 * @author Matej Balun
 *
 */
public class ComplexNumber {

	private double real;
	private double imaginary;

	/**
	 * This constructor initializes the new complex number. It takes two double
	 * values: the real and imaginary part of the number.
	 * 
	 * @param real
	 *            - real part of the number
	 * @param imaginary
	 *            - imaginary part of the number.
	 */
	public ComplexNumber(double real, double imaginary) {
		this.real = real;
		this.imaginary = imaginary;
	}

	/**
	 * A static factory method. Instances the new complex number with only real
	 * value.
	 * 
	 * @param real
	 *            - real part of the number.
	 * @return new instance of {@link ComplexNumber}.
	 */
	public static ComplexNumber fromReal(double real) {
		return new ComplexNumber(real, 0);
	}

	/**
	 * A static factory method. Instances the new complex number with only
	 * imaginary value.
	 * 
	 * @param imaginary
	 * @return new instance of {@link ComplexNumber}.
	 */
	public static ComplexNumber fromImaginary(double imaginary) {
		return new ComplexNumber(0, imaginary);
	}

	/**
	 * A static factory method. Instances the new {@link ComplexNumber} from
	 * it's magnitude and angle.
	 * 
	 * @param magnitude
	 *            - magnitude of the specified complex number.
	 * @param angle
	 *            - angle of the specified complex number.
	 * @return - new instance of {@link ComplexNumber}.
	 */

	public static ComplexNumber fromMagnitudeAndAngle(double magnitude, double angle) {
		double real;
		double imaginary;

		real = magnitude * Math.cos(angle);
		imaginary = magnitude * Math.sin(angle);

		return new ComplexNumber(real, imaginary);
	}

	/**
	 * A static factory method. It instances the new {@link ComplexNumber} based
	 * on a string representation of the number sent by user. It divides into
	 * two different methods for different cases of the complex numbers by using
	 * the regular expressions.
	 * 
	 * @param complex
	 *            - string representation of the complex number.
	 * @return new instance of {@link ComplexNumber}.
	 * @throws IllegalArgumentException
	 *             if the string is empty.
	 */
	public static ComplexNumber parse(String complex) {

		if (complex.isEmpty()) {
			throw new IllegalArgumentException("The pecified string for parsing is Empty.");
		}

		complex = complex.replaceAll(" ", ""); // remove the spaces

		if (complex.matches("[-]?\\d*\\.?\\d*i")) { // check if the given string
													// represents only
													// complex-part number via
													// regular expression
			return parseOnlyImaginary(complex);
		} else {
			return parseRealAndImaginary(complex);
		}

	}

	/**
	 * This method returns the new instance of the {@link ComplexNumber} based
	 * on the string representation of the number. This method in particular
	 * works only with the complex numbers that have only imaginary part.
	 * 
	 * @param complex
	 *            - a string representing an only imaginary complex number.
	 * @return new instance of {@link ComplexNumber}
	 */
	private static ComplexNumber parseOnlyImaginary(String complex) {

		if (complex.isEmpty() || (complex.contains("-") && complex.length() == 1)) {
			complex = complex.concat("1");
		}

		try {
			double imaginary = Double.parseDouble(complex);
			return new ComplexNumber(0, imaginary);
		} catch (NumberFormatException e) {
			System.err.println("cannot parse to complex value");
			return null;
		}
	}

	/**
	 * This private method returns the new instance of {@link ComplexNumber}.
	 * This method in particular works only with the complete (real and
	 * imaginary) complex number string, or with only real complex numbers. The
	 * method uses the regular expressions for parsing a string.
	 * 
	 * @param complex
	 *            - string representation of the complex number.
	 * @return new instance of {@link ComplexNumber}.
	 */
	private static ComplexNumber parseRealAndImaginary(String complex) {

		Pattern complexNumberPattern = Pattern.compile("([+-]?\\d+\\.?\\d*)?([+-]?\\d*\\.?\\d*i)?");
		Matcher matches = complexNumberPattern.matcher(complex);

		if (matches.find()) {
			boolean reFound = false;
			double re = 0;
			if ((matches.group(1) != null) && (matches.group(1).length() > 0)) {
				reFound = true;

				try {
					re = Double.parseDouble(matches.group(1));
				} catch (Exception parseException) {
					System.err.println("Cannot parse '" + re + "' as double.");
				}
			}

			boolean imFound = false;
			double im = 0;
			if ((matches.group(2) != null) && (matches.group(2).length() > 0)) {
				imFound = true;

				String imString = matches.group(2).replace("i", "");

				// Check if only "i" or "-i" is provided as input - add value of
				// 1
				if (imString.length() <= 1) {
					imString = imString + "1";
				}

				try {
					im = Double.parseDouble(imString);
				} catch (Exception parseException) {
					System.err.println("Cannot parse '" + im + "' as double.");
				}
			}

			if (!reFound && !imFound) // Input is invalid
				return null;
			else
				return new ComplexNumber(re, im);
		} else {
			return null;
		}
	}

	/**
	 * Returns the real part of the number.
	 * 
	 * @return real part of the number.
	 */
	public double getReal() {
		return real;
	}

	/**
	 * Returns the imaginary part of the number.
	 * 
	 * @return imaginary part of the number.
	 */
	public double getImaginary() {
		return imaginary;
	}

	/**
	 * Returns the magnitude of the number.
	 * 
	 * @return magnitude of the number.
	 */
	public double getMagnitude() {
		return Math.hypot(real, imaginary);
	}

	/**
	 * Returns the angle of the number.
	 * 
	 * @return angle of the number.
	 */
	public double getAngle() {
		return Math.atan2(imaginary, real);
	}

	/**
	 * Adds the specified complex number to this complex number. Returns the
	 * result of the addition.
	 * 
	 * @param complex
	 *            - specified complex number for addition.
	 * @return new instance of the resulting {@link ComplexNumber}.
	 */
	public ComplexNumber add(ComplexNumber complex) {
		double real = this.real + complex.real;
		double imaginary = this.imaginary + complex.imaginary;

		return new ComplexNumber(real, imaginary);
	}

	/**
	 * Subtracts the the specified complex number from this complex number.
	 * Returns the new instance of the resulting {@link ComplexNumber}.
	 * 
	 * @param complex
	 *            - specified complex number for subtraction.
	 * @return new instance of the resulting {@link ComplexNumber}.
	 */
	public ComplexNumber sub(ComplexNumber complex) {
		double real = this.real - complex.real;
		double imaginary = this.imaginary - complex.imaginary;

		return new ComplexNumber(real, imaginary);
	}

	/**
	 * Multiples this complex number with the specified {@link ComplexNumber}.
	 * Return the new instance of the resulting {@link ComplexNumber}.
	 * 
	 * @param complex
	 *            - specified {@link ComplexNumber} for multiplying.
	 * @return new instance of the resulting {@link ComplexNumber}.
	 */
	public ComplexNumber mul(ComplexNumber complex) {
		double real = this.real * complex.real - this.imaginary * complex.imaginary;
		double imaginary = this.real * complex.imaginary + this.imaginary * complex.real;

		return new ComplexNumber(real, imaginary);
	}

	/**
	 * Divides this {@link ComplexNumber} with the specified
	 * {@link ComplexNumber}. Return the new instance of the resulting
	 * {@link ComplexNumber}.
	 * 
	 * @param complex
	 *            - the specified {@link ComplexNumber} for division.
	 * @return new instance of the resulting {@link ComplexNumber}.
	 */
	public ComplexNumber div(ComplexNumber complex) {

		double denominator = (Math.pow(complex.real, 2)) + Math.pow(complex.imaginary, 2);
		double real = (this.real * complex.real + this.imaginary * complex.imaginary) / denominator;
		double imaginary = (this.imaginary * complex.real - this.real * complex.imaginary) / denominator;

		return new ComplexNumber(real, imaginary);
	}

	/**
	 * Calculates the n-th power of this {@link ComplexNumber}. return the
	 * resulting {@link ComplexNumber}.
	 * 
	 * @param n
	 *            - the specified power.
	 * @return new instance of the resulting {@link ComplexNumber}.
	 */
	public ComplexNumber power(int n) {
		if (n < 0) {
			throw new IllegalArgumentException("The value n for power operation must not be less than zero");
		}

		double radius = getMagnitude();
		double angle = getAngle();

		double real = radius * Math.cos(n * angle);
		double imaginary = radius * Math.sin(n * angle);

		return new ComplexNumber(real, imaginary);
	}

	/**
	 * Calculates the n roots of this {@link ComplexNumber}. Return the array of
	 * resulting {@link ComplexNumber}s.
	 * 
	 * @param n
	 *            - the specified root.
	 * @return array of the resulting {@link ComplexNumber}s.
	 */
	public ComplexNumber[] root(int n) {
		if (n <= 0) {
			throw new IllegalArgumentException("The value n for the root must be greater than zero");
		}

		double radius = getMagnitude();
		double angle = getAngle();
		ComplexNumber[] roots = new ComplexNumber[n];

		for (int i = 0; i < n; i++) {
			double real = Math.pow(radius, 1 / n) * Math.cos((angle + 2 * i * Math.PI) / n);
			double imaginary = Math.pow(radius, 1 / n) * Math.sin((angle + 2 * i * Math.PI) / n);

			roots[i] = new ComplexNumber(real, imaginary);
		}

		return roots;
	}

	/**
	 * Returns a string representation of this {@link ComplexNumber} in format
	 * {real + imaginary*i}.
	 */
	@Override
	public String toString() {
		if (this.imaginary == 0) {
			return this.real + "";

		} else if (this.real == 0) {
			return this.imaginary + "i";

		} else if (this.imaginary < 0) {
			return this.real + " - " + (-this.imaginary) + "i";
		}

		return this.real + " + " + this.imaginary + "i";
	}

}
