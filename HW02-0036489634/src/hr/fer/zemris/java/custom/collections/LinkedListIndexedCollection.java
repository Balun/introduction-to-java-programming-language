package hr.fer.zemris.java.custom.collections;

/**
 * This class implements the doubly Linked list indexed collection. It extends
 * the class Collection, with implementations of its empty methods. Operations
 * of addition are done in O(1) time complexity, while operations of retrieval
 * and searching are done in O(n/2 + 1) time complexity
 * 
 * @author Matej Balun
 *
 */
public class LinkedListIndexedCollection extends Collection {

	/**
	 * A private static inner class that implements a node of the doubly linked
	 * list. It consists node's data, a reference to the next node and a
	 * reference to the previous node.
	 * 
	 * @author Matej Balun
	 *
	 */
	private static class ListNode {
		ListNode prev;
		ListNode next;
		Object value;
	}

	private int size;
	private ListNode first;
	private ListNode last;

	/**
	 * This default constructor initializes the doubly linked list, by setting
	 * the references to first and last element on null, as well as size on
	 * zero.
	 */
	public LinkedListIndexedCollection() {
		super();
		this.first = null;
		this.last = null;
		this.size = 0;
	}

	/**
	 * This constructor initializes the doubly linked list and adds the objects
	 * from another custom collection into this list.
	 * 
	 * @param other
	 *            - specified collection for addition.
	 */
	public LinkedListIndexedCollection(Collection other) {
		this();

		for (Object element : other.toArray()) {
			this.add(element);
		}
	}

	/**
	 * Adds the specified object on the end of this linked list. The addition is
	 * accomplished in O(1) complexity.
	 */
	@Override
	public void add(Object value) {
		if (value == null) {
			throw new IllegalArgumentException("The value must not be null-reference.");
		}

		ListNode newNode = new ListNode();
		newNode.value = value;
		newNode.prev = null;
		newNode.next = null;

		if (this.first == null) {
			this.first = newNode;
			this.last = newNode;
			size++;
		} else {
			this.last.next = newNode;
			newNode.prev = this.last;
			this.last = newNode;
			size++;
		}
	}

	/**
	 * Returns the object on the specified index in the list. The operation is
	 * accomplished in max O(n/2 + 1) time complexity.
	 * 
	 * @param index
	 *            - index of the specified element.
	 * @return the specified element.
	 * 
	 * @throws IndexOutOfBoundsException
	 *             if the index is out of range.
	 */
	public Object get(int index) {
		if (index < 0 || index > (size - 1)) {
			throw new IndexOutOfBoundsException(
					"The index must be a value greater or equal to 0 and less or equal to size - 1.");
		}

		if (index < size / 2) {
			int current = 0;
			for (ListNode tmp = first; tmp != null; tmp = tmp.next, current++) {
				if (current == index) {
					return tmp.value;
				}
			}
		} else {
			int current = size - 1;
			for (ListNode tmp = last; tmp != null; tmp = tmp.prev, current--) {
				if (current == index) {
					return tmp.value;
				}
			}
		}

		return null;
	}

	@Override
	public void clear() {
		this.first = null;
		this.last = null;
		this.size = 0;
	}

	/**
	 * Inserts the specified object into the specified position in the doubly
	 * linked list. The average complexity of this method is O(n/2 + 1).
	 * 
	 * @param value
	 *            - the specified object for insertion.
	 * @param position
	 *            - the specified position for insertion.
	 * @throws IndexOutOfBoundsException
	 *             if the index is out of bounds.
	 */
	public void insert(Object value, int position) {
		if (position < 0 || position > size) {
			throw new IllegalArgumentException(
					"The position must be a value greater or equal to 0 and less or equal to size - 1.");
		}

		ListNode node = new ListNode();
		node.value = value;
		node.next = null;
		node.prev = null;
		ListNode current;

		if (position < size / 2) {
			int index = 0;
			for (current = first; current != null; current = current.next, index++) {
				if (index == position) {
					break;
				}
			}
			node.next = current;
			node.prev = current.prev;
			current.prev = node;
		} else if (position >= size / 2) {
			int index = size - 1;
			for (current = last; current != null; current = current.prev, index--) {
				if (index == position) {
					break;
				}
			}
			node.next = current;
			node.prev = current.prev;
			current.prev = node;
		} else {
			node.prev = last;
			last = node;
		}

		size++;
	}

	/**
	 * Returns the index of the specified element in the collection. If the
	 * specified object is not in the collection it returns -1.
	 * 
	 * @param value
	 *            - The specified object in the collection.
	 * @return index of the specified object, -1 if the object is not in the
	 *         list.
	 */
	public int indexOf(Object value) {
		int index = 0;
		for (ListNode current = first; current != null; current = current.next, index++) {
			if (current.value.equals(value)) {
				return index;
			}
		}

		return -1;
	}

	/**
	 * Removes the item from this linked list. The removal is done in O(n/2 + 1)
	 * average complexity.
	 * 
	 * @param index
	 *            - index of the specified element.
	 * @throws IndexOutOfBoundsException
	 *             if the index is out of range.
	 */
	public void remove(int index) {
		if (index < 0 || index > (size - 1)) {
			throw new IndexOutOfBoundsException(
					"The index must be a value greater or equal to 0 and less or equal to size - 1.");
		}

		ListNode current;
		if (index < size / 2) {
			int i = 0;
			for (current = first; current != null; current = current.next, i++) {
				if (index == i) {
					break;
				}
			}
		} else {
			int i = size - 1;
			for (current = last; current != null; current = current.prev, i--) {
				if (index == i) {
					break;
				}
			}
		}

		if (current.prev == null) {
			this.first = current.next;
			current.next.prev = null;
			
		} else if (current.next == null) {
			this.last = current.prev;
			this.last.next = null;
			
		} else {
			current.prev.next = current.next;
			current.next.prev = current.prev;
		}

		size--;
	}

	@Override
	public boolean remove(Object value) {
		if (this.contains(value)) {
			this.remove(this.indexOf(value));
			return true;
		}

		return false;
	}

	@Override
	public Object[] toArray() {
		Object[] array = new Object[size];
		int i = 0;
		for (ListNode current = first; current != null; current = current.next, i++) {
			array[i] = current.value;
		}
		return array;
	}

	@Override
	public void forEach(Processor processor) {
		for (Object element : this.toArray()) {
			processor.process(element);
		}
	}

	@Override
	public boolean contains(Object element) {
		for (ListNode current = first; current != null; current = current.next) {
			if (current.value.equals(element)) {
				return true;
			}
		}

		return false;
	}

}
