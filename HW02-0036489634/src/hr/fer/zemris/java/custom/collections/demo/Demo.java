package hr.fer.zemris.java.custom.collections.demo;

import hr.fer.zemris.java.custom.collections.ObjectStack;

/**
 * A demonstration program for the operations with object stack. The program
 * takes one command-line argument which represents a simple math operation, parses
 * the operation and executes it. The operation is a string in post fix
 * representation.
 * 
 * @param args
 *            - A single command line argument that represents the post fix math
 *            expression.
 * 
 * @author Matej Balun
 *
 */
public class Demo {

	/**
	 * The main method that executes the program.
	 */
	public static void main(String[] args) {
		if (args.length != 1) {
			System.err.println("Invalid number of arguments.");
			System.exit(1);
		}

		String line = args[0].trim();
		line = line.trim();
		int result = parseAndCalculate(line);
		System.out.println("The expression evaluates to " + result + ".");
	}

	/**
	 * This method takes the user-specified string line that represents math
	 * expression. It parses the expression and via class ObjectStack decodes
	 * the operation and executes it. If the expression is invalid, the method
	 * closes the demonstration program.
	 * 
	 * @param line
	 *            - the math expression in post fix representation.
	 * @return the result of the expression.
	 */
	private static int parseAndCalculate(String line) {
		String[] elements = line.split(" ");
		ObjectStack stack = new ObjectStack();
		int result;

		for (String element : elements) {
			element = element.trim();

			try {
				int number = Integer.parseInt(element);
				stack.push(number);
			} catch (NumberFormatException e) {

				int number1 = (int) stack.pop();
				int number2 = (int) stack.pop();

				switch (element) {

				case "*":
					result = number1 * number2;
					stack.push(result);
					break;

				case "/":
					result = number2 / number1;
					stack.push(result);
					break;

				case "%":
					result = number2 % number1;
					stack.push(result);
					break;

				case "+":
					result = number2 + number1;
					stack.push(result);
					break;

				case "-":
					result = number2 - number1;
					stack.push(result);
					break;

				default:
					System.err.println("The characters must be numbers or operators");
					System.exit(1);
				}

			}

		}

		if (stack.size() != 1) {
			System.err.println("The given expression is invalid.");
			System.exit(1);
		}

		result = (int) stack.pop();
		return result;

	}

}
