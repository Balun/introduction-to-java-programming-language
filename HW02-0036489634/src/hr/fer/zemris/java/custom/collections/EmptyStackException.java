package hr.fer.zemris.java.custom.collections;

/**
 * This class describes the custom EmptyStackException used in the custom stack
 * demonstration class. The Exception is used when the stack is empty. The class
 * extends RuntimeException from java.lang.
 * 
 * @author Matej Balun
 *
 */
public class EmptyStackException extends RuntimeException {

	/**
	 * The generated serial version UID for this class.
	 */
	private static final long serialVersionUID = 5019575938076748881L;

	/**
	 * The default constructor. It calls the super class constructor.
	 */
	public EmptyStackException() {
		super();
	}

	/**
	 * This constructor takes the String message and passes it to the super
	 * class constructor (RuntimeException).
	 * 
	 * @param message
	 *            - the specified message of the exception.
	 */
	public EmptyStackException(String message) {
		super(message);
	}

}
