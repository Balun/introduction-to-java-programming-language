package hr.fer.zemris.java.custom.collections;

/**
 * This class implements the array-like indexed collection. The collection
 * implements the unimplemented methods from the custom class Collection. The
 * operations of addition are done in O(1) time complexity, while the operations
 * of retrieval are also done in O(1) average complexity. The collection
 * consists of internal array that reallocates in need.
 * 
 * @author Matej Balun
 *
 */
public class ArrayIndexedCollection extends Collection {

	private int size;
	private int capacity;
	private Object[] elements;

	/**
	 * The default constructor, initializes the internal array, and sets the
	 * initial capacity to 16.
	 */
	public ArrayIndexedCollection() {
		super();
		this.size = 0;
		this.capacity = 16;
		this.elements = new Object[16];
	}

	/**
	 * This constructor passes the initialization to the default constructor and
	 * reallocates the internal array with the specified capacity.
	 * 
	 * @param initialCapacity
	 */
	public ArrayIndexedCollection(int initialCapacity) {
		this();
		if (initialCapacity < 1) {
			throw new IllegalArgumentException("The initial capacity must not be less than 1.");
		}
		this.capacity = initialCapacity;
		this.elements = new Object[initialCapacity];
	}

	/**
	 * This constructor initializes the collection and adds all the elements
	 * from another collection into this collection. The collection will
	 * reallocate if needed.
	 * 
	 * @param other
	 */
	public ArrayIndexedCollection(Collection other) {
		this();
		for (Object element : other.toArray()) {
			this.add(element);
		}
	}

	/**
	 * This constructor passes the initialization to the simpler constructor
	 * along with the specified initial capacity, and adds all of the elements
	 * from the specified collection to this collection.
	 * 
	 * @param other
	 * @param initialCapacity
	 */
	public ArrayIndexedCollection(Collection other, int initialCapacity) {
		this(initialCapacity);
		for (Object element : other.toArray()) {
			this.add(element);
		}
	}

	/**
	 * Adds the elements into the collection. The Addition is accomplished in
	 * O(1) average time complexity.
	 * 
	 * @throws IllegalArgumentException
	 *             if the null-reference is sent.
	 */
	@Override
	public void add(Object element) {
		if (element.equals(null)) {
			throw new IllegalArgumentException("Null reference cannot be added as an element.");
		}
		if (size == capacity) {
			Object[] tmp = this.elements;
			this.elements = new Object[2 * capacity];
			for (int i = 0; i < size; i++) {
				this.elements[i] = tmp[i];
			}
			capacity *= 2;
		}

		this.elements[size] = element;
		size++;
	}

	/**
	 * Returns the value of the element on the specified position. The average
	 * complexity is O(1).
	 * 
	 * @param index
	 *            - index of the specified object.
	 * @return the object on the specified index.
	 * @throws IndexOutOfBoundsException
	 *             if the index is out of range.
	 */
	public Object get(int index) {
		if (index < 0 || index > (size - 1)) {
			throw new IndexOutOfBoundsException(
					"The index must be a value greater or equal to 0 and less or equal to size - 1.");
		}

		return this.elements[index];
	}

	@Override
	public void clear() {
		for (int i = 0; i < size; i++) {
			elements[i] = null;
		}
		this.size = 0;
	}

	/**
	 * Inserts the value into the specified position in the collection. The
	 * insertion along is done in O(1) time complexity, but shifting the
	 * elements to right and possible reallocation of the array makes it O(n).
	 * 
	 * @param value
	 *            - the object for insertion.
	 * @param position
	 *            - the specified position for insertion.
	 * @throws IllegalArgumentException
	 *             if the position is out of bounds.
	 */
	public void insert(Object value, int position) {
		if (position < 0 || position > size) {
			throw new IllegalArgumentException(
					"The position for insertion must be greater or equal to 0 and less or equal to size.");
		}

		if (size == capacity) {
			Object[] tmp = this.elements;
			this.elements = new Object[2 * capacity];
			for (int i = 0; i < size; i++) {
				this.elements[i] = tmp[i];
			}
			capacity *= 2;
		}

		Object tmp = elements[position];
		for (int i = position; i < size; i++) {
			elements[i + 1] = tmp;
			tmp = elements[i + 1];
			elements[i + 1] = elements[i];
		}

		elements[position] = value;
		size++;
	}

	/**
	 * Returns the index of the specified object in the collection.
	 * 
	 * @param value
	 *            - the specified object
	 * @return - index of the element, -1 if there is no element specified in
	 *         the collection
	 */
	public int indexOf(Object value) {
		for (int i = 0; i < size; i++) {
			if (elements[i].equals(value)) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * Removes the element on the specified position from the collection. The
	 * removal is accomplished in O(n) average time complexity.
	 * 
	 * @param index
	 *            - specified index of the element for removal.
	 * @throws IndexOutOfBoundsException
	 *             if the specified index is out of range.
	 */
	public void remove(int index) {
		if (index < 0 || index > (size - 1)) {
			throw new IndexOutOfBoundsException("The index must greater or equal to 0 and less or equal to size - 1.");
		}

		for (int i = index; i < size - 1; i++) {
			elements[i] = elements[i + 1];
		}
		elements[size - 1] = null;
		size--;
	}

	@Override
	public boolean remove(Object value) {
		if (this.contains(value)) {
			this.remove(this.indexOf(value));
			return true;
		}

		return false;
	}

	@Override
	public boolean contains(Object element) {
		for (int i = 0; i < size; i++) {
			if (elements[i].equals(element)) {
				return true;
			}
		}

		return false;
	}

	@Override
	public int size() {
		return this.size;
	}

	@Override
	public Object[] toArray() {
		Object[] array = new Object[size];
		for (int i = 0; i < size; i++) {
			array[i] = this.elements[i];
		}
		return array;
	}

	@Override
	public void forEach(Processor processor) {
		for (Object element : this.toArray()) {
			processor.process(element);
		}
	}

}
