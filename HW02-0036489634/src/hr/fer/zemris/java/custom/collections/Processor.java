package hr.fer.zemris.java.custom.collections;

/**
 * This class describes the Processor object for executing operations on a
 * custom collection. it consist from only one method, process, which executes
 * the specified operation on a collection.
 * 
 * @author Matej Balun
 *
 */
public class Processor {

	/**
	 * Executes the specified operation on a collection.
	 * 
	 * @param value
	 *            - the value applied for specified operation.
	 */
	public void process(Object value) {

	}

}
