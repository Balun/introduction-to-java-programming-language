package hr.fer.zemris.java.simplecomp;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

import hr.fer.zemris.java.simplecomp.impl.ComputerImpl;
import hr.fer.zemris.java.simplecomp.impl.ExecutionUnitImpl;
import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.ExecutionUnit;
import hr.fer.zemris.java.simplecomp.models.InstructionCreator;
import hr.fer.zemris.java.simplecomp.parser.InstructionCreatorImpl;
import hr.fer.zemris.java.simplecomp.parser.ProgramParser;

/**
 * Ovaj program je demonstracija jednostavne aplikacije koja simulira rad
 * procesora i procesorskih naredbi pomocu klasičnih asemlblerskih naredbi.
 * Program može primiti jedan argument preko naredbenog retka koji predstavlja
 * putanju do datoteke s asemblesrkim programom. Ako taj argument nije naveden,
 * program će prilikom izvođenja automatski upitati korisnika za stazu. Program
 * potom prevodi navedenu datoteku, te ako je prevođenje uspješno, inicijalizira
 * procesor, memoriju i registre te pokreće izvođenje specificiranih naredbi.
 * 
 * @author Matej Balun
 *
 */
public class Simulator {

	/**
	 * Glavna metoda koja pokreće program.
	 */
	public static void main(String[] args) {

		String filePath = null;

		switch (args.length) {

		case 0:
			Scanner sc = new Scanner(System.in);
			System.out.println("Unesite putanju do datoteke s asemblerskim kodom");
			filePath = sc.nextLine();
			break;

		case 1:
			filePath = args[0];
			break;

		default:
			System.err.println("Neispravan broj argumenata!");
			System.exit(-1);
			break;
		}

		if (!Files.exists(Paths.get(filePath))) {
			System.err.println("Unesena je nepostojeća datoteka");
			System.exit(-1);
		}

		try {

			Computer comp = new ComputerImpl(16, 256);

			InstructionCreator creator = new InstructionCreatorImpl("hr.fer.zemris.java.simplecomp.impl.instructions");

			ProgramParser.parse(filePath, comp, creator);

			ExecutionUnit exec = new ExecutionUnitImpl();

			exec.go(comp);

		} catch (Exception e) {
			System.err.println("Tijekom izvođenja programa iz datoteke " + filePath + " došlo je do iznimke:");
			e.printStackTrace();
		}
	}

}
