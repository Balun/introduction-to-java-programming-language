package hr.fer.zemris.java.simplecomp;

/**
 * Ovaj utility razred pruži metode za očitavanje raznih vrijednosti iz
 * 32-bitnog binarnog zapisa koji redstavlja opisnik jednog registra. 24. bit
 * opisnika služi kao zatavica koja pruži informaciju radi li se o indirektnom
 * adresiranju ili ne. Ako se radi o indirektnom adresiranju, bitovi 23-8 služe
 * kako bi se pohranila vrijednost odmaka, ako nije podignuta zastavica za
 * indirektno adresiranje, ovi bitovi se zanemaruju. Bitovi 7 - 0 služe za
 * pohranu vrijednosti indexa doticnog registra. Razred pruži metode za
 * očitavanje navedenih vrijednosti, uglavnom pomoću Javinih bitovnih operatora.
 * 
 * @author Matej Balun
 *
 */
public class RegisterUtil {

	/**
	 * Ova metoda služi za očitanje indeksa registra iz opisnika registra pomocu
	 * zadnjih 8 bitova opisnika. Metoda radi pomocu Javinih bitovnih operatora.
	 * 
	 * @param registerDescriptor
	 *            opisnik specificiranog registra
	 * @return index navedenog registra
	 */
	public static int getRegisterIndex(int registerDescriptor) {
		int index = registerDescriptor & 0x000000FF;
		return index;
	}

	/**
	 * Ova metoda služi za očiavanje vrijednosti 24. bita opisnika registra koji
	 * predstavlja zastavicu za indirektno adresiranje. Ako je taj bit 1, onda
	 * je ukljucen način indirektnog adresiranja, inače nula. Metoda koristi
	 * Javine bitovne operatore za očitanje.
	 * 
	 * @param registerDescriptor
	 *            opisnik navedenog registra
	 * @return true ako je podignuta zastavica za indirektno adresiranje, inače
	 *         false
	 */
	public static boolean isIndirect(int registerDescriptor) {
		int indirect = registerDescriptor >> 24;
		if (indirect == 1) {
			return true;
		}

		return false;
	}

	/**
	 * Ova metoda vraća odmak za indirektno adresiranje ako je zastavica
	 * indirektnog adresiranja (24. bit) podignuta. Ako nije, vrijednost odmaka
	 * se zanemaruje i metoda vraća nulu. Metoda koristi Javine bitovne
	 * operatore i 16-bitni primitivni tip (short) za izračun odmaka.
	 * 
	 * @param registerDescriptor
	 * @return
	 */
	public static int getRegisterOffset(int registerDescriptor) {
		if (!isIndirect(registerDescriptor)) {
			return 0;
		}

		int offset = registerDescriptor >> 8;
		offset = offset & 0xFFFF;
		return (short) offset;
	}

}
