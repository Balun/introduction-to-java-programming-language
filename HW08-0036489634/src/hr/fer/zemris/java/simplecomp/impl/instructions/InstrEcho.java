package hr.fer.zemris.java.simplecomp.impl.instructions;

import java.util.List;

import hr.fer.zemris.java.simplecomp.RegisterUtil;
import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.Instruction;
import hr.fer.zemris.java.simplecomp.models.InstructionArgument;

/**
 * Ovaj razred predstavlja standardu ECHO instrukciju procesora. Instrukcija
 * prima jedan argument koji mora biti validni opsinik registra. Ako je
 * specificirani registar u modu indirektnog adresiranja, instrukcija ispisuje
 * sadržaj koji se nalazi na memorijskoj lokaciji specificiranoj indirektnim
 * adresiranjem. U protivnom, instrukcija ispisuje sadržaj registra sa
 * specificiranim indexom.
 * 
 * @author Matej Balun
 *
 */
public class InstrEcho implements Instruction {

	/**
	 * Argument instrukcije koji predstavlja validni opisnik registra.
	 */
	private InstructionArgument argument;

	/**
	 * Ovaj konstruktor inicijalizira {@link InstrEcho} objekt pomoću
	 * {@link InstructionArgument} objektna sadržanog u listi argumenata.
	 * Argument mora predstavljati validni opisnik registra, inače konstruktor
	 * baca iznimku.
	 * 
	 * @param arguments
	 *            lista {@link InstructionArgument} argumenata za instrukciju.
	 * @throws IllegalArgumentException
	 *             ako argument nije valjani opisnik registra
	 */
	public InstrEcho(List<InstructionArgument> arguments) {
		if (arguments.size() != 1) {
			throw new IllegalArgumentException(
					"Neispravan broj argumenata za instrukciju echo, broj argumenata mora biti jedan!");
		} else if (!arguments.get(0).isRegister()) {
			throw new IllegalArgumentException(
					"Argument naredbe echo mora biti index registar ili indirektno adresirana memorijska lokacija!");
		}

		this.argument = arguments.get(0);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean execute(Computer computer) {

		int regIndex = RegisterUtil.getRegisterIndex((Integer) argument.getValue());

		if (RegisterUtil.isIndirect((Integer) argument.getValue())) {
			int offset = RegisterUtil.getRegisterOffset((Integer) argument.getValue());
			System.out.print(computer.getMemory()
					.getLocation(offset + (Integer) computer.getRegisters().getRegisterValue(regIndex)));

		} else {
			System.out.print(computer.getRegisters().getRegisterValue(regIndex).toString());
		}

		return false;
	}

}
