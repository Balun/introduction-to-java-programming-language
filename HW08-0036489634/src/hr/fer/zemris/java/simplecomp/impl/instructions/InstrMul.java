package hr.fer.zemris.java.simplecomp.impl.instructions;

import java.util.List;
import hr.fer.zemris.java.simplecomp.RegisterUtil;
import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.Instruction;
import hr.fer.zemris.java.simplecomp.models.InstructionArgument;

/**
 * Ovaj razred predstavlja standardnu MUL instrukciju procesora. Instrukcija
 * pomoću liste argumenata prima tri argumenta koji moraju biti valjani opisnici
 * registara. Instrukcija vrši operaciju množenja nad drugim i trećim argumentom
 * pohranjući rezultat u registar opisan prvim argumentom.
 */
public class InstrMul implements Instruction {

	/**
	 * Index prvog registra za instrukciju.
	 */
	private int indexRegistra1;

	/**
	 * Index drugog registra za instrukciju.
	 */
	private int indexRegistra2;

	/**
	 * Index trećeg registra za instrukciju.
	 */
	private int indexRegistra3;

	/**
	 * Ovaj javni konstruktor inicijalizira {@link InstrMul} naredbu, primajući
	 * listu {@link InstructionArgument} objekata očitavajući indexe registara
	 * za operaciju. Ako argumenti nisu validni opisnici registara, konstruktor
	 * baca iznimku.
	 * 
	 * 
	 * @param argments
	 *            lista {@link InstructionArgument} objekata za argumente
	 *            instrukcije {@link InstrMul}
	 * @throws IllegalArgumentException
	 *             ako argumenti nisu validni opisnici registara.
	 */
	public InstrMul(List<InstructionArgument> arguments) {

		if (arguments.size() != 3) {
			throw new IllegalArgumentException("Mul naredba ocekuje 3 argumenta!");
		}

		for (int i = 0; i < 3; i++) {
			if (!arguments.get(i).isRegister() || RegisterUtil.isIndirect((Integer) arguments.get(i).getValue())) {
				throw new IllegalArgumentException("Krivi tip arguemnta " + i + "!");
			}
		}

		this.indexRegistra1 = RegisterUtil.getRegisterIndex((Integer) arguments.get(0).getValue());
		this.indexRegistra2 = RegisterUtil.getRegisterIndex((Integer) arguments.get(1).getValue());
		this.indexRegistra3 = RegisterUtil.getRegisterIndex((Integer) arguments.get(2).getValue());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean execute(Computer computer) {

		Object value1 = computer.getRegisters().getRegisterValue(indexRegistra2);
		Object value2 = computer.getRegisters().getRegisterValue(indexRegistra3);

		computer.getRegisters().setRegisterValue(indexRegistra1, Integer.valueOf((Integer) value1 * (Integer) value2));

		return false;
	}
}