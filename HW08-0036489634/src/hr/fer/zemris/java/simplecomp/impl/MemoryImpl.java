package hr.fer.zemris.java.simplecomp.impl;

import hr.fer.zemris.java.simplecomp.models.Memory;

/**
 * Ovaj razred implementira sučelje {@link Memory} i predstavlja fizičku radnu
 * memoriju računalnog sustava. Za razliku od memorije u stvarnim računalnim
 * sustava koja je oktetno-orijentirana, ova memorija je objektno-orijentirana,
 * što znači da je svaka memorijska lokacija spremnik za proizvoljno velik Java
 * {@link Object}.
 * 
 * @author Matej Balun
 *
 */
public class MemoryImpl implements Memory {

	/**
	 * Polje objekata koje predstavlja radnu memoriju računala.
	 */
	private Object[] memory;

	/**
	 * Ovaj konstruktor inicijalizira memoriju računala, alocirajući željenu
	 * veličinu polja objekata unesenu od strane korisnika.
	 * 
	 * @param size
	 */
	public MemoryImpl(int size) {
		this.memory = new Object[size];
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @throws IndexOutOfBoundsException
	 *             ako je index manji od nule, a veći od veličina memorije - 1
	 */
	@Override
	public void setLocation(int location, Object value) {
		if (location < 0 || location >= memory.length) {
			throw new IndexOutOfBoundsException(
					"Index memorije mora biti broj veći ili jednak nuli, " + "a manji od " + memory.length + ".");
		}

		this.memory[location] = value;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @throws IndexOutOfBoundsException
	 *             ako je index manji od nule, a veći od veličina memorije - 1
	 */
	@Override
	public Object getLocation(int location) {
		if (location < 0 || location >= memory.length) {
			throw new IndexOutOfBoundsException(
					"Index memorije mora biti broj veći ili jednak nuli, " + "a manji od " + memory.length + ".");
		}

		return this.memory[location];
	}

}
