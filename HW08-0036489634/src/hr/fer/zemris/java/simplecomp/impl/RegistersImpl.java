package hr.fer.zemris.java.simplecomp.impl;

import hr.fer.zemris.java.simplecomp.models.Registers;

/**
 * Ovaj razred implementira sučelje {@link Registers} i predstavlja simulirani
 * skup registaraza računalni sustav. Skup registara se sastoji od minimalno 15
 * registara opće namjene, registra kazaljke stoga koji je uvijek registar s
 * rednim brojem specificiranim kao konstantom sučelja {@link Registers}, te
 * zastavice koja je reprezentirana kao boolean vrijednost i fizičko odvojena od
 * ostatka registara (index registra kazaljke stoga se određuje prilikom pisanja
 * asemblerskog programa. Sadržaj registra je pohranjen kao Java {@link Object}.
 * Za operacije s registrima u većini slučajeva koristimo 32-bitne (int)
 * opisnike registra.
 * 
 * @author Matej Balun
 *
 */
public class RegistersImpl implements Registers {

	/**
	 * Skup registara opće namjene i kazaljke stoga.
	 */
	private Object[] registers;

	/**
	 * programsko brojilo.
	 */
	private int programCouter;

	/**
	 * Registar zastavice za razne provjere nad vrijednostima.
	 */
	private boolean flag;

	/**
	 * Inicijalizira {@link RegistersImpl} razred pomoću željene količine
	 * registara opće namjene (uvijek veća ili jednaka 16)
	 * 
	 * @param regsLen
	 *            željena količina registara opće namjene
	 */
	public RegistersImpl(int regsLen) {
		this.programCouter = 0;
		this.registers = new Object[regsLen];
		this.flag = false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object getRegisterValue(int index) {
		if (index < 0 || index >= registers.length) {
			throw new IndexOutOfBoundsException(
					"Index registra mora biti broj veći ili jednak nuli, a manji od " + registers.length + ".");
		}

		return this.registers[index];

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setRegisterValue(int index, Object value) {
		if (index < 0 || index >= registers.length) {
			throw new IndexOutOfBoundsException(
					"Index registra mora biti broj veći ili jednak nuli, a manji od " + registers.length + ".");
		}

		this.registers[index] = value;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getProgramCounter() {
		return this.programCouter;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setProgramCounter(int value) {
		this.programCouter = value;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void incrementProgramCounter() {
		this.programCouter++;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean getFlag() {
		return this.flag;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setFlag(boolean value) {
		this.flag = value;

	}

}
