package hr.fer.zemris.java.simplecomp.impl.instructions;

import java.util.List;

import hr.fer.zemris.java.simplecomp.RegisterUtil;
import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.Instruction;
import hr.fer.zemris.java.simplecomp.models.InstructionArgument;

/**
 * Ovaj razred predstavlja standardnu instrukciju procesora INCREMENT.
 * Intrukcija prima jedan argument preko liste argumenata. Taj argument mora
 * biti valjani opisnik registra, u kojem indirektno adresiranje nije
 * dozvoljeno. Instrukcija uzima sadržaj navedenog registra pokušava ga
 * protumačiti kao broj, te uvećava vrijednost za jedan. Naredba nece
 * obavijstiti korisnika ako je se sadržaj registr ne može protumačiti kao broj.
 * 
 * @author Matej Balun
 *
 */
public class InstrIncrement implements Instruction {

	/**
	 * Index registra čiji sadržaj želimo uvećati.
	 */
	private int indexReg;

	/**
	 * Ovaj konstruktor inicijalizira {@link InstrIncrement} objekt pomoću liste
	 * {@link InstructionArgument} objekata. Konsktruktor prima jedan objekt
	 * koji mora biti validni opisnik registra (bez indirektnog adresiranja)
	 * čiji sadraj želimo uvećati. Ako broj argumenata ili tip nisu validni,
	 * konstruktor baca inznmku.
	 * 
	 * @param arguments
	 *            lista {@link InstructionArgument} objekata za
	 *            {@link InstrIncrement} instrukciju.
	 * @throws IllegalArgumentException
	 *             ako je unesen pogrešan broj argumenata, ili ako je unesen
	 *             krivi tip argmenta
	 */
	public InstrIncrement(List<InstructionArgument> arguments) {
		if (arguments.size() != 1) {
			throw new IllegalArgumentException("Instrukcija decrement mora primiti samo jedan argument!");

		} else if (!arguments.get(0).isRegister() || RegisterUtil.isIndirect((Integer) arguments.get(0).getValue())) {
			throw new IllegalArgumentException(
					"Arguments instrukcije decrement mora biti registar, bez indirektnog adresiranja!");
		}

		this.indexReg = RegisterUtil.getRegisterIndex((Integer) arguments.get(0).getValue());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean execute(Computer computer) {

		Object value = computer.getRegisters().getRegisterValue(indexReg);

		computer.getRegisters().setRegisterValue(indexReg, (Integer) value + 1);

		return false;
	}

}
