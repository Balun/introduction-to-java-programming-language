package hr.fer.zemris.java.simplecomp.impl.instructions;

import java.util.List;

import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.Instruction;
import hr.fer.zemris.java.simplecomp.models.InstructionArgument;
import hr.fer.zemris.java.simplecomp.models.Registers;

/**
 * Ovaj razred predstavlja stndardnu instrukciju procesora CALL. Instrukcija
 * poziva potprogram koji se nalazi na adresi unesenoj kao argument fukcije,
 * spremajući trenutačni sadržaj proramskog brojila na stog te stavlja
 * specificiranu memorijsku adresu potprograma u programsko brojilo.
 * 
 * @author Matej Balun
 *
 */
public class InstrCall implements Instruction {

	/**
	 * Adresa memorijske lokacije na kojoj počinje željeni potprogram.
	 */
	private int location;

	/**
	 * Ovaj konstruktor inicijlizira {@link InstrCall} objekt pomoću
	 * specificiranog {@link InstructionArgument} objekta iz liste argumenata
	 * koji predstavlja adresu potprograma. argument mora biti validan broj koji
	 * predstgavlja memorijsku lokaciju, inače konstruktor baca iznimku.
	 * 
	 * @param arguments
	 *            lista {@link InstructionArgument} objekata u kojoj se nalati
	 *            argument instrukcije
	 * @throws IllegalArgumentException
	 *             ako uneseni argument nije validan broj koji predstavlja
	 *             adresu memorijske lokacije
	 */
	public InstrCall(List<InstructionArgument> arguments) {
		if (arguments.size() != 1) {
			throw new IllegalArgumentException("Instrukcija call može primiti samo jedan argument");
		} else if (!arguments.get(0).isNumber()) {
			throw new IllegalArgumentException(
					"Argument instrukcije call mora biti adresa ili labela koja predstavlja validnu adresu potprograma");
		}

		location = (Integer) arguments.get(0).getValue();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean execute(Computer computer) {
		int currentAddress = computer.getRegisters().getProgramCounter();

		int stackPointer = (Integer) computer.getRegisters().getRegisterValue(Registers.STACK_REGISTER_INDEX);

		computer.getMemory().setLocation(stackPointer, currentAddress);

		computer.getRegisters().setRegisterValue(Registers.STACK_REGISTER_INDEX, stackPointer - 1);

		computer.getRegisters().setProgramCounter(location);

		return false;
	}

}
