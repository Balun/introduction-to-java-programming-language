package hr.fer.zemris.java.simplecomp.impl;

import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.Memory;
import hr.fer.zemris.java.simplecomp.models.Registers;

/**
 * Ovaj razred implementira sučelje {@link Computer} koje predstavlja koncept
 * jednostavnog računalnog procesora. Razred interno sadrži reference na skup
 * registara i memoriju, kako bi mogao obavljati specificirane procesorske
 * instrukcije. Procesor sam inicijalizira memoriju i registre pomocu unesenih
 * argumenata.
 * 
 * @author Matej Balun
 *
 */
public class ComputerImpl implements Computer {

	/**
	 * Referenca na skup registara.
	 */
	private Registers registers;

	/**
	 * Reference na dodjeljenu memoriju.
	 */
	private Memory memory;

	/**
	 * Ovaj konstruktor inicijalizira računalo {@link ComputerImpl} pomoću
	 * željene veličine memorije i željenog broja registara koji mora biti
	 * veći ili jednak 16. 
	 * 
	 * @param regSize
	 *            željeni broj registara
	 * @param memSize
	 *            željena veličina memorije
	 * @throws IllegalArgumentException
	 *             ako je željeni broj regstara manji od 16 (default), ili ako
	 *             je željena veličina memorije nula.
	 */
	public ComputerImpl(int regSize, int memSize) {
		if (regSize < 16) {
			throw new IllegalArgumentException(
					"Potrebno je kreirati barem šesnaest registara, petnaest opće namjene i jedan "
							+ "za kazaljku stoga");
		} else if (memSize <= 0) {
			throw new IllegalArgumentException("Memorija mora imati barem jednu memorijsku lokaciju");
		}

		this.registers = new RegistersImpl(regSize);
		this.memory = new MemoryImpl(memSize);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Registers getRegisters() {
		return this.registers;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Memory getMemory() {
		return this.memory;
	}

}
