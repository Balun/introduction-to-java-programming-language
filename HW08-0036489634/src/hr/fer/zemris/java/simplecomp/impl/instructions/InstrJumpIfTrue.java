package hr.fer.zemris.java.simplecomp.impl.instructions;

import java.util.List;

import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.Instruction;
import hr.fer.zemris.java.simplecomp.models.InstructionArgument;

/**
 * Ovaj razred predstavlja standardnu uvjetnu JUMP instrukciju procesora.
 * Instrukcija provjerava dali je postavljena vrijednosta registra sa zastavicom
 * na <code>true</code>. Ako je, naredba preko liste argumenata prima jedan
 * argument koji predstavlja validnu memorijsku adresu potprograma na koji
 * izvođenje treba skočiti, te ju stavlja u programsko brojilo. Uprotivnom
 * instrukcija ne radi ništa i nastavlja se daljnje izvođenje naredni.
 * 
 * @author Matej Balun
 *
 */
public class InstrJumpIfTrue implements Instruction {

	/**
	 * Adresa memorijske lokacije na kojoj počinje željeni dio programa.
	 */
	private int index;

	/**
	 * Ovaj konstruktor nicijalizira {@link InstrJumpIfTrue} objekt pomocu liste
	 * {@link InstructionArgument} objekata. Konstruktor prima jedan argument iz
	 * liste koji predstavlja validnu adresu memorijske lokacije. Ako je broj
	 * argumenata pogrešan ili je tip argumenta kriv, konstruktor baca iznimku.
	 * 
	 * @param arguments
	 *            lista {@link InstructionArgument} objekata za
	 *            {@link InstrJumpIfTrue} instrukciju
	 * @throws IllegalArgumentException
	 *             ako je unesen pogrešan broj argumenata, ili ako je tip
	 *             argumenta kriv
	 */
	public InstrJumpIfTrue(List<InstructionArgument> arguments) {
		if (arguments.size() != 1) {
			throw new IllegalArgumentException("Instrukcija jumpIfTrue mora imati samo jedan argument!");
		} else if (!arguments.get(0).isNumber()) {
			throw new IllegalArgumentException(
					"Argument instrukcija jumpIfTrue mora biti broj koji je index memorijske lokacije!");
		}

		this.index = (Integer) arguments.get(0).getValue();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean execute(Computer computer) {

		if (computer.getRegisters().getFlag()) {
			computer.getRegisters().setProgramCounter(index);
		}

		return false;
	}

}
