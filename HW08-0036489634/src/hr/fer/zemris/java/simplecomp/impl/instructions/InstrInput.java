package hr.fer.zemris.java.simplecomp.impl.instructions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.Instruction;
import hr.fer.zemris.java.simplecomp.models.InstructionArgument;

/**
 * Ovaj razred predstavlja standardnu instrukciju procesora INPUT. Instrukcija
 * prima jedan argument preko lite argumenata koji predstavlja adresu memorijske
 * lokacije, čita sadržaj sa standardnog ulaza i tumači ga kao {@link Integer}
 * (ako se nemože protumačiti kao integer zastavica se zaustavlja na false,
 * inače true), te zapisuje pročitani broj na specificiranu memorijsku lokaciju.
 * 
 * @author Matej Balun
 *
 */
public class InstrInput implements Instruction {

	/**
	 * Memorijska lokaciju na koju se želi pohraniti pročitan broj.
	 */
	private int location;

	/**
	 * Ovaj konstruktor insicijalizira {@link InstrInput} objekt pomoću liste
	 * {@link InstructionArgument} objekata. Konstruktor prima jedan argument
	 * koji predstavlja validnu adresu memorijske lokacije. Ako je unesen krivi
	 * broj argumenta ili ako je argument krivog tipa, konstruktor baca iznimku.
	 * 
	 * @param arguments
	 *            lista {@link InstructionArgument} objekata za
	 *            {@link InstrInput} instrukciju.
	 * @throws IllegalArgumentException
	 *             ako je unesen krivi broj argumenata, ili ako je unesen
	 *             argument krivog tipa
	 */
	public InstrInput(List<InstructionArgument> arguments) {
		if (arguments.size() != 1) {
			throw new IllegalArgumentException("Instrukcija input mora primiti tocno jedan argument!");
		} else if (!arguments.get(0).isNumber()) {
			throw new IllegalArgumentException(
					"Argument intrukcije input mora biti broj ili labela koja predstavlja validnu memorijsku aresu!");
		}

		this.location = (Integer) arguments.get(0).getValue();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @throws NumberFormatException
	 *             ako se pročitani sadržaj ne može protumačiti kao broj
	 * @throws IOException
	 *             ako je došo do pogreške u čitanju sa standardnog ulaza
	 */
	@Override
	public boolean execute(Computer computer) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		try {
			int value = Integer.parseInt(reader.readLine());
			computer.getMemory().setLocation(location, value);
			computer.getRegisters().setFlag(true);

		} catch (NumberFormatException | IOException e) {
			computer.getRegisters().setFlag(false);
		}

		return false;
	}

}
