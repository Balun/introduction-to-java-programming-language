package hr.fer.zemris.java.simplecomp.impl.instructions;

import java.util.List;

import hr.fer.zemris.java.simplecomp.RegisterUtil;
import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.Instruction;
import hr.fer.zemris.java.simplecomp.models.InstructionArgument;

/**
 * Ovaj razred predstavlja standardnu instrukciju procesora DECREMENT.
 * Instrukcija prima jedan argument preko liste argumenata koji predstavlja
 * opisnik registra čiju vrijednost treba umanjiti za 1. Instrukcija
 * predpostavlja da se u navedenom regisru nalazi vrijednost u formatu legalnom
 * za matematičke operacije (broj).
 * 
 * @author Matej Balun
 *
 */
public class InstrDecrement implements Instruction {

	/**
	 * Index registra čiji sadržaj treba umanjiti.
	 */
	private int indexReg;

	/**
	 * Ovaj konsruktor inicijalizira {@link InstrDecrement} objekt pomoću
	 * {@link InstructionArgument} objekta iz liste argumenata koji predstavlja
	 * validni opisnik registra koji korisnik želi umanjiti.
	 * 
	 * @param arguments
	 *            lista {@link InstructionArgument} argumenata za instrukciju
	 */
	public InstrDecrement(List<InstructionArgument> arguments) {
		if (arguments.size() != 1) {
			throw new IllegalArgumentException("Instrukcija decrement mora primiti samo jedan argument!");

		} else if (!arguments.get(0).isRegister() || RegisterUtil.isIndirect((Integer) arguments.get(0).getValue())) {
			throw new IllegalArgumentException(
					"Arguments instrukcije decrement mora biti registar, bez indirektnog adresiranja!");
		}

		this.indexReg = RegisterUtil.getRegisterIndex((Integer) arguments.get(0).getValue());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean execute(Computer computer) {

		Object value = computer.getRegisters().getRegisterValue(indexReg);

		computer.getRegisters().setRegisterValue(indexReg, (Integer) value - 1);

		return false;
	}

}
