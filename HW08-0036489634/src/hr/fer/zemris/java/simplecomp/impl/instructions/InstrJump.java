package hr.fer.zemris.java.simplecomp.impl.instructions;

import java.util.List;

import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.Instruction;
import hr.fer.zemris.java.simplecomp.models.InstructionArgument;

/**
 * Ovaj razred predstavlja standardnu instrukciju procesora JUMP. Instrukcija
 * prima jedan argument preko liste argumenata koji predstavlja validnu
 * memorijsku adresu dijela programa na koji izvođenje treba prebaciti.
 * Instrukcija stavlja navedenu lokaciju u programsko brojilo.
 * 
 * @author Matej Balun
 *
 */
public class InstrJump implements Instruction {

	/**
	 * Adresa memorijske lokacije na kojoj se nalazi željeni dio programa.
	 */
	private int index;

	/**
	 * Ovaj konstruktor inicijalizira {@link InstrJump} objekt koristeći listu
	 * {@link InstructionArgument} objekata. Konstruktor čita jedan argument iz
	 * liste koji predstavlja validnu adresu memorijske lokacije. Ako je unesen
	 * krivi broj argumenata ili argument nije validnog tipa, konstruktor baca
	 * iznimku.
	 * 
	 * @param arguments
	 *            lista {@link InstructionArgument} objekata za
	 *            {@link InstrJump} instrukciju.
	 */
	public InstrJump(List<InstructionArgument> arguments) {
		if (arguments.size() != 1) {
			throw new IllegalArgumentException("Instrukcija jump mora imati samo jedan argument!");
		} else if (!arguments.get(0).isNumber()) {
			throw new IllegalArgumentException(
					"Argument instrukcija jump mora biti broj koji je index memorijske lokacije!");
		}

		this.index = (Integer) arguments.get(0).getValue();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean execute(Computer computer) {
		computer.getRegisters().setProgramCounter(index);

		return false;
	}

}
