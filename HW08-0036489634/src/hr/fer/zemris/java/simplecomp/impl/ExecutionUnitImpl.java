package hr.fer.zemris.java.simplecomp.impl;

import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.ExecutionUnit;
import hr.fer.zemris.java.simplecomp.models.Instruction;

/**
 * Ovaj razred predstavlja implementira sučelje {@link ExecutionUnit} i
 * predstavlja jednostavno ostvarenje izvršne jedinice računalnog sustava.
 * Razred ima samo jednu metodu, koja pomoću reference na {@link Computer}
 * objekt pokreće izvađanje specificiranih procesorskih instrukcija iz memorije
 * računala i simulira rad procesora.
 * 
 * @author Matej Balun
 *
 */
public class ExecutionUnitImpl implements ExecutionUnit {

	/**
	 * {@inheritDoc}
	 * 
	 * @throws RuntimeException
	 *             ako se desila greška u izvođenju instrukcija.
	 */
	@Override
	public boolean go(Computer computer) {
		if (computer == null) {
			throw new IllegalArgumentException("Predani Computer objekt ne smije biti null-referenca.");
		}

		computer.getRegisters().setProgramCounter(0);

		try {

			while (true) {
				Instruction instruction = (Instruction) computer.getMemory()
						.getLocation(computer.getRegisters().getProgramCounter());

				computer.getRegisters().setProgramCounter(computer.getRegisters().getProgramCounter() + 1);

				if (instruction.execute(computer)) {
					break;
				}
			}

		} catch (Exception e) {
			throw new RuntimeException(e.getMessage(), e.getCause());
		}

		return true;

	}

}
