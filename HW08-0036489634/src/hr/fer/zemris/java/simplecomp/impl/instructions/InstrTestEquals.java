package hr.fer.zemris.java.simplecomp.impl.instructions;

import java.util.List;

import hr.fer.zemris.java.simplecomp.RegisterUtil;
import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.Instruction;
import hr.fer.zemris.java.simplecomp.models.InstructionArgument;

/**
 * Ovaj razred predstavlja standardnu instrukciju procesora TESTEQUALS. Ova
 * instrukcija prima dva argumenta preko liste argumenata koji predstavljaju
 * validne opisnike registara. Instrukcija uspoređuje vrijednosti koje se nalaze
 * u registrima s indexima navedenim u opisnicima pomoću metode equals(). Ako
 * metoda vraća <code>true</code>, registar zastavice se postavlja na
 * <code>true</code>, inače false.
 * 
 * @author Matej Balun
 *
 */
public class InstrTestEquals implements Instruction {

	/**
	 * Index prvog registra.
	 */
	private int regIndex1;

	/**
	 * Index drugog registra.
	 */
	private int regIndex2;

	/**
	 * Ovaj konstruktor inicijalizira {@link InstrTestEquals} objekt pomocu
	 * liste {@link InstructionArgument} objekata. Konstrutor prima dva
	 * argumenta iz liste koji predstavljaju validne opisnike registara.
	 * Konstruktor uzima postavlja indexe iz opisnika. AKo je unesen pogrešan
	 * broj argumenata ili ako su argumenti krivog tipa, konstruktor baca
	 * iznimku.
	 * 
	 * @param arguments
	 * @throws IllegalArgumentException
	 *             ako je unesen pogrešan broj argumenata ili su argumenti
	 *             krivog tipa
	 */
	public InstrTestEquals(List<InstructionArgument> arguments) {
		if (arguments.size() != 2) {
			throw new IllegalArgumentException("Instrukcija testEquals mora primiti dva argumenta!");

		} else if (!arguments.get(0).isRegister() || !arguments.get(1).isRegister()) {
			throw new IllegalArgumentException("Argumenti instrukcije testEquals moraju biti registri");

		} else if (RegisterUtil.isIndirect((Integer) arguments.get(0).getValue())
				|| RegisterUtil.isIndirect((Integer) arguments.get(1).getValue())) {
			throw new IllegalArgumentException(
					"Argumenti instrukcije testEquals moraju biti registri, bez indirektnog adresiranja");
		}

		this.regIndex1 = RegisterUtil.getRegisterIndex((Integer) arguments.get(0).getValue());
		this.regIndex2 = RegisterUtil.getRegisterIndex((Integer) arguments.get(1).getValue());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean execute(Computer computer) {

		Object value1 = computer.getRegisters().getRegisterValue(regIndex1);
		Object value2 = computer.getRegisters().getRegisterValue(regIndex2);

		computer.getRegisters().setFlag(value1.equals(value2));

		return false;
	}

}
