package hr.fer.zemris.java.simplecomp.impl.instructions;

import java.util.List;

import hr.fer.zemris.java.simplecomp.RegisterUtil;
import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.Instruction;
import hr.fer.zemris.java.simplecomp.models.InstructionArgument;
import hr.fer.zemris.java.simplecomp.models.Registers;

/**
 * Ovaj razred predstavlja standardnu instrukciju procesora POP. Instrukcija
 * prima jedan argument preko liste argumenata koji predstavlja validni opisnik
 * registra, bez opcije indirektnog adresiranja. Instrukcija dohvaća s
 * memorijske lokacije koja predstavlja trenutni vrh stoga vrijednost uvećanu za
 * jedan te ju pohranjuje u registar sa specificiranim indeksom iz opisnika.
 * Također uvećava adresu stoga za jedan, efektnivno brišući spomenutu
 * vrijednost sa stoga.
 * 
 * @author Matej Balun
 *
 */
public class InstrPop implements Instruction {

	/**
	 * Index registra za pohranu vijednosti.
	 */
	private int regIndex;

	/**
	 * Ovaj konstruktor incijalizira {@link InstrPop} objekt pomocu liste
	 * {@link InstructionArgument} objekata. Konstruktor prima jedan argument
	 * koji predstavlja validni opisnik registra te određuje index registra za
	 * pohranu vrijednosti. Uslučaju da je unesen pogrešan broj argumenata ili
	 * je tip argumenta kriv, konstruktor baca iznimku.
	 * 
	 * @param arguments
	 *            lista {@link InstructionArgument} objekata za {@link InstrPop}
	 *            instrukciju.
	 * @throws arguments
	 *             ako je unesen pogrešan broj argumanata ili je argument krivog
	 *             tipa.
	 */
	public InstrPop(List<InstructionArgument> arguments) {
		if (arguments.size() != 1) {
			throw new IllegalArgumentException("Instrukcija pop mora primiti tocno jedan argument!");
		} else if (!arguments.get(0).isRegister() || RegisterUtil.isIndirect((Integer) arguments.get(0).getValue())) {
			throw new IllegalArgumentException(
					"Argument instrukcije pop mra biti registar, bez indirektnog adresiranja!");
		}

		regIndex = RegisterUtil.getRegisterIndex((Integer) arguments.get(0).getValue());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean execute(Computer computer) {
		int stackPointer = (Integer) computer.getRegisters().getRegisterValue(Registers.STACK_REGISTER_INDEX);
		Object value = computer.getMemory().getLocation(stackPointer + 1);

		computer.getRegisters().setRegisterValue(regIndex, value);
		computer.getRegisters().setRegisterValue(Registers.STACK_REGISTER_INDEX, stackPointer + 1);

		return false;
	}

}
