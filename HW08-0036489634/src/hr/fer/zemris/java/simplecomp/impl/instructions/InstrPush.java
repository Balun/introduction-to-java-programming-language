package hr.fer.zemris.java.simplecomp.impl.instructions;

import java.util.List;

import hr.fer.zemris.java.simplecomp.RegisterUtil;
import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.Instruction;
import hr.fer.zemris.java.simplecomp.models.InstructionArgument;
import hr.fer.zemris.java.simplecomp.models.Registers;

/**
 * Ovaj razred predstavlja standardnu instrukciju procesora PUSH. Instrukcija
 * prima jedan argument preko liste argumenata koji predstavlja validni opisnik
 * registra. Instrukcija pohranjuje vrijednost koja se u navedenom registru iz
 * opisnika na memorijsku lokaciju koja redstavlja trenutni vrh stoga, te
 * umanjuje adresu vrha stoga za jedan.
 * 
 * @author Matej Balun
 *
 */
public class InstrPush implements Instruction {

	/**
	 * Index registra čiju vrijednost želimo staviti na stog.
	 */
	private int regIndex;

	/**
	 * Ovaj konstruktor inicijalizira {@link InstrPush} objekt pomocu liste
	 * {@link InstructionArgument} objekata. Konstruktor prima jedan argument
	 * koji predstavlja validni opisnik registra te čita index registra u kojem
	 * se nalazi vrijednost za pohranu na stog. U slučaju da je unesen pogrešan
	 * broj argumenata ili je argument krivog tipa, konstruktor baca iznimku.
	 * 
	 * @param arguments
	 *            lista {@link InstructionArgument} objekata za {@link InstrPop}
	 *            instrukciju
	 * @throws arguments
	 *             ako je unesen pogrešan broj argumanata ili je argument krivog
	 *             tipa.
	 */
	public InstrPush(List<InstructionArgument> arguments) {
		if (arguments.size() != 1) {
			throw new IllegalArgumentException("Instrukcija push mora primiti tocno jedan argument!");
		} else if (!arguments.get(0).isRegister() || RegisterUtil.isIndirect((Integer) arguments.get(0).getValue())) {
			throw new IllegalArgumentException(
					"Argument instrukcije push mra biti registar, bez indirektnog adresiranja!");
		}

		regIndex = RegisterUtil.getRegisterIndex((Integer) arguments.get(0).getValue());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean execute(Computer computer) {
		int stackPointer = (Integer) computer.getRegisters().getRegisterValue(Registers.STACK_REGISTER_INDEX);
		Object value = computer.getRegisters().getRegisterValue(regIndex);

		computer.getMemory().setLocation(stackPointer, value);

		computer.getRegisters().setRegisterValue(Registers.STACK_REGISTER_INDEX, stackPointer - 1);
		return false;
	}

}
