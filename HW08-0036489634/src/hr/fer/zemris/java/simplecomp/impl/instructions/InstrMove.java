package hr.fer.zemris.java.simplecomp.impl.instructions;

import java.util.List;

import hr.fer.zemris.java.simplecomp.RegisterUtil;
import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.Instruction;
import hr.fer.zemris.java.simplecomp.models.InstructionArgument;

/**
 * Ovaj razred predstavlja standardnu instrukciju procesora MOVE. Instrukcija
 * prima dva argumenta preko liste argumenata. Prvi argument može biti registar
 * ili indirektna adresa. Drugi registar može biti registar, indirektna adresa
 * ili broj. Instrukcija prifružuje vrijednost navedenu u drugom argumentu
 * registru ili memorijskoj lokaciji navedenoj u prvom argumentu.
 * 
 * @author Matej Balun
 *
 */
public class InstrMove implements Instruction {

	/**
	 * Prvi argument instrukcije.
	 */
	private InstructionArgument arg1;

	/**
	 * Drugi argument instrukcije.
	 */
	private InstructionArgument arg2;

	/**
	 * Ovaj konstruktor inicijalizira {@link InstrMove} objekt pomoću liste
	 * {@link InstructionArgument} objekata. Kontruktor prima dva argumenta,
	 * prvi predstavlja regista ili indirektnu adresu, a drugi može biti
	 * registar, indrektna adresa ili broj. Ako je unesen pogrešan broj
	 * argumenata ili su argumenti krivog tipa, konstruktor baca inznimku
	 * 
	 * @param arguments
	 *            lista {@link InstructionArgument} objekata za
	 *            {@link InstrMove} instrukciju.
	 * @throws IllegalArgumentException
	 *             ako su objekti krivog tipa ili ako je unesen pogrešan broj
	 *             argumenata.
	 */
	public InstrMove(List<InstructionArgument> arguments) {
		if (arguments.size() != 2) {
			throw new IllegalArgumentException("Instrukcija move mora primiti dva argumenta");

		} else if (!arguments.get(0).isRegister()) {
			throw new IllegalArgumentException(
					"Prvi argument move naredbe mora biti registar, sa ili bez indirektnog adresiranja!");

		} else if (!arguments.get(1).isRegister() && !arguments.get(1).isNumber()) {
			throw new IllegalArgumentException(
					"Drugi argument move naredbe mora bit registar, sa ili bez indirektnog adresiraja, ili može biti broj!");
		}

		this.arg1 = arguments.get(0);
		this.arg2 = arguments.get(1);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean execute(Computer computer) {
		if (arg2.isRegister()) {
			applyRegisterMove(computer);

		} else if (arg2.isNumber()) {
			applyNumberMove(computer);
		}

		return false;
	}

	/**
	 * Ova pomoćna metoda obavlja funkcionalnost MOVE instrukcije u slučaju da
	 * je drugi argument broj. Instrukcija sprema dotični broj u registar opisan
	 * s prvim argumentom, ili na memorijsku lokaciju ako je uključeno
	 * indirektno adresiranje.
	 * 
	 * @param computer
	 *            inicijalizirano računalo za obavljanje procesorskih
	 *            instrukcija
	 */
	private void applyNumberMove(Computer computer) {

		if (RegisterUtil.isIndirect((Integer) arg1.getValue())) {

			int location = RegisterUtil.getRegisterOffset((Integer) arg1.getValue()) + (Integer) computer.getRegisters()
					.getRegisterValue(RegisterUtil.getRegisterIndex((Integer) arg1.getValue()));

			computer.getMemory().setLocation(location, (Integer) arg2.getValue());

		} else {

			int reg1 = RegisterUtil.getRegisterIndex((Integer) arg1.getValue());
			computer.getRegisters().setRegisterValue(reg1, (Integer) arg2.getValue());
		}

	}

	/**
	 * Ova pomoćna metoda obalja rad MOVE instrukcije u slučaju kad je drugi
	 * argument validni opisnik registra bez indirektnog adresiranja. Metoda
	 * pohranjuje sadržaj koji se nalazi u tom registru na memorijsku lokaciju
	 * specificiranu prvim registrom kao opisnikom kad je uključeno indirektno
	 * adrsiranje, ili u registar specificiran dotičnim opisnikom.
	 * 
	 * @param computer
	 *            inicijalizirano računalo za obalvljanje procesorskih
	 *            instrukcija.
	 */
	private void applyRegisterMove(Computer computer) {

		if (RegisterUtil.isIndirect((Integer) arg1.getValue())) {
			applyOffsetMove(computer);

		} else {
			applyNoOffsetMove(computer);
		}

	}

	/**
	 * Pomoćna metoda koja obalja funkcionalnost MOVE instrukcije u slučaju kada
	 * je prvi argument zadan kao indirektna adresa. Instrukcija pohranjuje
	 * sadržaj koji se nalazi u registru opisanim drugm argumentom ili koji se
	 * nalazi na memorijskoj lokaciji ako je uključeno indirektno adresiranje na
	 * memorijsku lokaciju specificiranu indirektno prvim argumentom.
	 * 
	 * @param computer
	 *            inicijalizirano računalo za obavljanje procesorskih
	 *            instrukcijama.
	 */
	private void applyOffsetMove(Computer computer) {

		int location1 = RegisterUtil.getRegisterOffset((Integer) arg1.getValue()) + (Integer) computer.getRegisters()
				.getRegisterValue(RegisterUtil.getRegisterIndex((Integer) arg1.getValue()));

		if (RegisterUtil.isIndirect((Integer) arg2.getValue())) {
			int location2 = RegisterUtil.getRegisterOffset((Integer) arg2.getValue()) + (Integer) computer
					.getRegisters().getRegisterValue(RegisterUtil.getRegisterIndex((Integer) arg2.getValue()));

			computer.getMemory().setLocation(location1, computer.getMemory().getLocation(location2));

		} else {
			computer.getMemory().setLocation(location1,
					computer.getRegisters().getRegisterValue(RegisterUtil.getRegisterIndex((Integer) arg2.getValue())));
		}
	}

	/**
	 * Ova pomoćna metoda obalja funkcionalnost MOVE naredbe u slučaju kada je
	 * prvi argument validni opisnik registra bez indirektnog adresiranja.
	 * Metoda pohranjuje sadržaj koji se nalazi na memorijskoj adresi
	 * specificiranoj drugim registrom ako je uključeno indirektno adresiranje
	 * ili ako nije, sadržaj koji se nalazi u registru specificiranim drugim
	 * argumentom u registar opisan prvim argumentom.
	 * 
	 * @param computer
	 *            inicijalizirano računalo za obavljanje procesorskih
	 *            instrukcija.
	 */
	private void applyNoOffsetMove(Computer computer) {

		int reg1 = RegisterUtil.getRegisterIndex((Integer) arg1.getValue());

		if (RegisterUtil.isIndirect((Integer) arg2.getValue())) {

			int location2 = RegisterUtil.getRegisterOffset((Integer) arg2.getValue()) + (Integer) computer
					.getRegisters().getRegisterValue(RegisterUtil.getRegisterIndex((Integer) arg2.getValue()));

			computer.getRegisters().setRegisterValue(reg1, computer.getMemory().getLocation(location2));

		} else {
			computer.getRegisters().setRegisterValue(reg1,
					computer.getRegisters().getRegisterValue(RegisterUtil.getRegisterIndex((Integer) arg2.getValue())));
		}
	}

}
