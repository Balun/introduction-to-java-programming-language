package hr.fer.zemris.java.simplecomp.impl.instructions;

import java.util.List;

import hr.fer.zemris.java.simplecomp.RegisterUtil;
import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.Instruction;
import hr.fer.zemris.java.simplecomp.models.InstructionArgument;

/**
 * Ovaj razred predstavlja standardnu ADD instrukciju procesora.
 * Instrukcija pomoću liste argumenata prima tri argumenta
 * koji moraju biti valjani opisnici registara. Instrukcija
 * vrši operaciju zbrajanja nad drugim i trećim argumentom
 * pohranjući rezultat u registar opisan prvim argumentom.
 */
public class InstrAdd implements Instruction {

	/**
	 * Index registra prvog argumenta.
	 */
	private int indexReg1;

	/**
	 * Index registra drugog argumenta.
	 */
	private int indexReg2;

	/**
	 * Index registra trećeg argumenta.
	 */
	private int indexReg3;

	/**
	 * Ovaj javni konstruktor inicijalizira {@link InstrAdd} naredbu, primajući
	 * listu {@link InstructionArgument} objekata očitavajući indexe registara
	 * za operaciju. Ako argumenti nisu validni opisnici registara, konstruktor
	 * baca iznimku.
	 * 
	 * 
	 * @param argments
	 *            lista {@link InstructionArgument} objekata za argumente
	 *            instrukcije
	 * @throws IllegalArgumentException
	 *             ako argumenti nisu validni opisnici registara.
	 */
	public InstrAdd(List<InstructionArgument> argments) {
		if (argments.size() != 3) {
			throw new IllegalArgumentException("Intrukcija add mora primiti tri argumenta!");
		}

		for (int i = 0; i < 3; i++) {
			if (!argments.get(i).isRegister() || RegisterUtil.isIndirect((Integer) argments.get(i).getValue())) {
				throw new IllegalArgumentException(
						"Argumenti instrukcije add mraju biti registri bez opcije indirektnog adresiranja");
			}
		}

		this.indexReg1 = RegisterUtil.getRegisterIndex((Integer) argments.get(0).getValue());
		this.indexReg2 = RegisterUtil.getRegisterIndex((Integer) argments.get(1).getValue());
		this.indexReg3 = RegisterUtil.getRegisterIndex((Integer) argments.get(2).getValue());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean execute(Computer computer) {

		Object value1 = computer.getRegisters().getRegisterValue(indexReg2);
		Object value2 = computer.getRegisters().getRegisterValue(indexReg3);

		computer.getRegisters().setRegisterValue(indexReg1, (Integer) value1 + (Integer) value2);

		return false;
	}

}
