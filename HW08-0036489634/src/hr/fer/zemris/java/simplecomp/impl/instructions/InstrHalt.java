package hr.fer.zemris.java.simplecomp.impl.instructions;

import java.util.List;

import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.Instruction;
import hr.fer.zemris.java.simplecomp.models.InstructionArgument;

/**
 * Ovaj razred predstavlja standardnu HALT insrukciju procesora. Instrukcija ne
 * prima nikakve argumente, te njenim pozivom prestaje rad procesora.
 * 
 * @author Matej Balun
 *
 */
public class InstrHalt implements Instruction {

	/**
	 * Ovaj konstruktor inicijalizira {@link InstrHalt} objekt, bez ikakvih
	 * argumenata iz liste argumenata. AKo lista sadrži arumente, konstruktor
	 * baca inznimku.
	 * 
	 * @param arguments
	 *            lista {@link InstructionArgument} objekata (prazna)
	 * @throws IllegalArgumentException
	 *             ako su navedeni argumenti u listi
	 */
	public InstrHalt(List<InstructionArgument> arguments) {
		if (arguments.size() != 0) {
			throw new IllegalArgumentException("Instrukcija halt ne smije imati argumenata!");
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean execute(Computer computer) {
		return true;
	}

}
