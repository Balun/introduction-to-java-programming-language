package hr.fer.zemris.java.simplecomp.impl.instructions;

import java.util.List;

import hr.fer.zemris.java.simplecomp.RegisterUtil;
import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.Instruction;
import hr.fer.zemris.java.simplecomp.models.InstructionArgument;

/**
 * Ovaj razred predtstavlja standrardnu instrukciju procesora LOAD. Instruckija
 * prima dva argumenta preko liste argumenata. Prvi argument predstavlja validni
 * opisnik registra, dok drugi argument predstavlja adresu memorijske lokacije.
 * Instrukcija uzima sadržaj sa dotične memorijske lokacije te ga pohranjuje u
 * navedeni registar. Instrukcija ne podržava indirektno adresiranje.
 * 
 * @author Matej Balun
 *
 */
public class InstrLoad implements Instruction {

	/**
	 * Index specificiranog registra za pohranu vrijednosti.
	 */
	private int indexReg;

	/**
	 * Adresa memorijske lokacije na kojoj se nalazi dotična vrijednost.
	 */
	private int memLocation;

	/**
	 * Ovaj kosntruktor inicijalizira {@link InstrLoad} objekt pomoću liste
	 * {@link InstructionArgument} objekata. Konstruktor očekuje dva argumenta,
	 * jedan kao validni opisnikregistra, drugi kao validnu adresu memorijske
	 * lokacije. Ako je unesen pogrešan broj argumenata ili ako su argumenti
	 * krivog tipa, kosntruktor baca iznimku.
	 * 
	 * @param arguments
	 *            lista {@link InstructionArgument} objekata za
	 *            {@link InstrLoad} instrukciju.
	 * @throws IllegalArgumentException
	 *             ako je unesen pogrešan broj argumenata ili su argumenti
	 *             krivog tipa
	 */
	public InstrLoad(List<InstructionArgument> arguments) {
		if (arguments.size() != 2) {
			throw new IllegalArgumentException("Load naredba ocekuje dva argumenta!");
		} else if (!arguments.get(0).isRegister() || RegisterUtil.isIndirect((Integer) arguments.get(0).getValue())) {
			throw new IllegalArgumentException(
					"Prvi argument load naredbe mora biti valjani index registra, bez indirektnog adresiranja!");
		} else if (!arguments.get(1).isNumber()) {
			throw new IllegalArgumentException("Drugi argument load naredbe mora biti memorijska lokacija!");
		}

		this.indexReg = RegisterUtil.getRegisterIndex((Integer) arguments.get(0).getValue());
		this.memLocation = (Integer) arguments.get(1).getValue();

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean execute(Computer computer) {

		Object value = computer.getMemory().getLocation(memLocation);
		computer.getRegisters().setRegisterValue(indexReg, value);
		return false;
	}

}
