package hr.fer.zemris.java.simplecomp.impl.instructions;

import java.util.List;

import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.Instruction;
import hr.fer.zemris.java.simplecomp.models.InstructionArgument;
import hr.fer.zemris.java.simplecomp.models.Registers;

/**
 * Ovaj razred predstavlja standardnu instrukciju procesora RET. Instrukcija
 * efektivno vraća izvođenje programa iz potprograma na adresu u pri kojo je
 * potprogram pozvan. Instrukcija skida jednu vrijednost sa trenutnog vrha toga
 * te ju stavlja u programsko brojilo kako bi se nastavilo izvođenje programa
 * prekinuto prilikom poziva potprorama.
 * 
 * @author Matej Balun
 *
 */
public class InstrRet implements Instruction {

	/**
	 * Ovaj konstruktor inicijalizira {@link InstrRet} objekt pomoću liste
	 * {@link InstructionArgument} objekata. Konstruktor ne uzima nikakve
	 * argumente iz liste. U slučaju pogrešnog broja argumenata (tj. ako ima
	 * argumenata) konstruktor baca iznimku.
	 * 
	 * @param arguments
	 *            lista {@link InstructionArgument} objekata za {@link InstrRet}
	 *            instrukciju.
	 * @throws IllegalArgumentException
	 *             ako je unesen pogrešan broj argumenata ili su argumenti
	 *             krivog tipa
	 */
	public InstrRet(List<InstructionArgument> arguments) {
		if (arguments.size() != 0) {
			throw new IllegalArgumentException("Instrukcija ret ne smije imati argumente!");
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean execute(Computer computer) {
		int stackPointer = (Integer) computer.getRegisters().getRegisterValue(Registers.STACK_REGISTER_INDEX);
		int location = (Integer) computer.getMemory().getLocation(stackPointer + 1);

		computer.getRegisters().setProgramCounter(location);
		computer.getRegisters().setRegisterValue(Registers.STACK_REGISTER_INDEX, stackPointer + 1);

		return false;
	}

}
