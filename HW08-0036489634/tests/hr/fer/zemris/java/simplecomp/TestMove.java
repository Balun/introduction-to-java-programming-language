package hr.fer.zemris.java.simplecomp;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Test;
import org.mockito.Mock;

import hr.fer.zemris.java.simplecomp.impl.instructions.InstrMove;
import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.InstructionArgument;
import hr.fer.zemris.java.simplecomp.models.Memory;
import hr.fer.zemris.java.simplecomp.models.Registers;

public class TestMove {
	@Mock
	InstructionArgument regArgument = mock(InstructionArgument.class);
	InstructionArgument numArgument = mock(InstructionArgument.class);
	InstructionArgument regArgument2 = mock(InstructionArgument.class);
	Computer c = mock(Computer.class);
	Registers r = mock(Registers.class);
	Memory m = mock(Memory.class);
	@SuppressWarnings("unchecked")
	List<InstructionArgument> arguments = mock(List.class);

	@Test
	public void testMoveNumber() {

		// move r5, 10
		when(regArgument.isRegister()).thenReturn(true);
		when(regArgument.getValue()).thenReturn(5);
		when(numArgument.isNumber()).thenReturn(true);
		when(numArgument.getValue()).thenReturn(10);

		when(arguments.get(0)).thenReturn(regArgument);
		when(arguments.get(1)).thenReturn(numArgument);
		when(arguments.size()).thenReturn(2);

		when(c.getRegisters()).thenReturn(r);

		moveInRegister(c);
		// dohvati registar i pohrani u njega 10
		verify(c, times(1)).getRegisters();
		verify(r, times(1)).setRegisterValue(5, 10);

	}

	@Test
	public void testMoveNumberWithOffset() {

		// move [r5 + 10] 10
		// na lokaciju 5 + 10 = 15
		// u registru r5 pohranjeno 5
		when(regArgument.isRegister()).thenReturn(true);
		when(regArgument.getValue()).thenReturn(0x1000A05);
		when(numArgument.isNumber()).thenReturn(true);
		when(numArgument.getValue()).thenReturn(10);

		when(arguments.get(0)).thenReturn(regArgument);
		when(arguments.get(1)).thenReturn(numArgument);
		when(arguments.size()).thenReturn(2);

		when(c.getRegisters()).thenReturn(r);
		when(c.getMemory()).thenReturn(m);
		when(r.getRegisterValue(5)).thenReturn(5);
		moveInRegister(c);
		// dohvati registar i pohrani u njega 10
		verify(c, times(1)).getRegisters();
		verify(c, times(1)).getMemory();
		verify(m, times(1)).setLocation(15, 10);

	}

	@Test
	public void testMoveWithBothIndirect() {

		// move [r10 + 10] [r11 + 20 ]
		// na lokaciju 5 + 10 = 15
		// pohranjujes se ono sa mem lokacije 30 (50 je pohranjeno)
		// u registru r10 pohranjeno 5
		// u registru r11 pohranjeno 10
		when(regArgument.isRegister()).thenReturn(true);
		when(regArgument.getValue()).thenReturn(0x1000A0A);
		when(regArgument2.isRegister()).thenReturn(true);
		when(regArgument2.getValue()).thenReturn(0x100140B);

		when(arguments.get(0)).thenReturn(regArgument);
		when(arguments.get(1)).thenReturn(regArgument2);
		when(arguments.size()).thenReturn(2);

		when(c.getRegisters()).thenReturn(r);
		when(c.getMemory()).thenReturn(m);
		when(m.getLocation(30)).thenReturn(50);
		when(r.getRegisterValue(10)).thenReturn(5);
		when(r.getRegisterValue(11)).thenReturn(10);
		moveInRegister(c);
		// dohvati oba registra
		verify(c, times(2)).getRegisters();
		// dohvati sa mem lokacije i zapisi na lokaciju
		verify(c, times(2)).getMemory();
		verify(m, times(1)).setLocation(15, 50);

	}

	public void moveInRegister(Computer c) {
		InstrMove move = new InstrMove(arguments);
		move.execute(c);
	}

}
