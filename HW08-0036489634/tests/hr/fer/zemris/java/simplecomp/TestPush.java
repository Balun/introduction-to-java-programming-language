package hr.fer.zemris.java.simplecomp;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Test;
import org.mockito.Mock;

import hr.fer.zemris.java.simplecomp.impl.RegistersImpl;
import hr.fer.zemris.java.simplecomp.impl.instructions.InstrPush;
import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.InstructionArgument;
import hr.fer.zemris.java.simplecomp.models.Memory;
import hr.fer.zemris.java.simplecomp.models.Registers;

public class TestPush {
	@Mock
	private List<Integer> list;
	InstructionArgument regArgument = mock(InstructionArgument.class);
	InstructionArgument memArgument = mock(InstructionArgument.class);
	Computer c = mock(Computer.class);
	Registers r = mock(Registers.class);
	Memory m = mock(Memory.class);
	@SuppressWarnings("unchecked")
	List<InstructionArgument> arguments = mock(List.class);

	@Test
	public void testPush() {

		// push r5
		// na ono gdje je stog pokazivac
		// uzet cemo lokaciju 100
		// u r5 pohranjeno npr 10
		when(regArgument.isRegister()).thenReturn(true);

		when(arguments.get(0)).thenReturn(regArgument);
		when(arguments.size()).thenReturn(1);

		when(c.getRegisters()).thenReturn(r);
		when(c.getMemory()).thenReturn(m);

		when(r.getRegisterValue(RegistersImpl.STACK_REGISTER_INDEX)).thenReturn(100);
		when(r.getRegisterValue(5)).thenReturn(10);
		when(arguments.get(0).getValue()).thenReturn(5);

		pushRegister(c);

		// dohvati stackpointer,sadrzaj pohranjen u r5 i smanji stackpointer
		verify(c, times(3)).getRegisters();
		verify(c, times(1)).getMemory();

		// pohrani na memorijsku lokaciju
		verify(m, times(1)).setLocation(100, 10);
		// smanji stack pointer
		verify(r, times(1)).setRegisterValue(RegistersImpl.STACK_REGISTER_INDEX, 99);

	}

	public void pushRegister(Computer c) {
		InstrPush push = new InstrPush(arguments);
		push.execute(c);
	}

}
