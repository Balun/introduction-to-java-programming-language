package hr.fer.zemris.java.simplecomp;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Test;
import org.mockito.Mock;

import hr.fer.zemris.java.simplecomp.impl.RegistersImpl;
import hr.fer.zemris.java.simplecomp.impl.instructions.InstrPop;
import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.InstructionArgument;
import hr.fer.zemris.java.simplecomp.models.Memory;
import hr.fer.zemris.java.simplecomp.models.Registers;

public class TestPop {
	@Mock
	private List<Integer> list;
	InstructionArgument regArgument = mock(InstructionArgument.class);
	InstructionArgument memArgument = mock(InstructionArgument.class);
	Computer c = mock(Computer.class);
	Registers r = mock(Registers.class);
	Memory m = mock(Memory.class);
	@SuppressWarnings("unchecked")
	List<InstructionArgument> arguments = mock(List.class);

	@Test
	public void testLoad() {

		// pop r5
		// ono gdje pokazuje stog pokazivac pohrani u r5
		// uzet cemo lokaciju 100 i na njoj pohranjeno 10

		when(regArgument.isRegister()).thenReturn(true);

		when(regArgument.getValue()).thenReturn(5);
		when(arguments.get(0)).thenReturn(regArgument);
		when(arguments.size()).thenReturn(1);

		when(c.getRegisters()).thenReturn(r);
		when(c.getMemory()).thenReturn(m);

		when(r.getRegisterValue(RegistersImpl.STACK_REGISTER_INDEX)).thenReturn(99);
		when(m.getLocation(100)).thenReturn(10);

		popRegister(c);

		// dohvati stackpointer,sadrzaj pohrani u r5 i smanji stackpointer
		verify(c, times(3)).getRegisters();
		verify(c, times(1)).getMemory();

		// povecaj stack pointer i dohvati pohranjeno na memorijskoj lokaciji
		verify(m, times(1)).getLocation(100);

		// povecaj stack pointer
		verify(r, times(1)).setRegisterValue(RegistersImpl.STACK_REGISTER_INDEX, 100);

		// pohrani u r5 ono sa lokacije 100
		verify(r, times(1)).setRegisterValue(5, 10);

	}

	public void popRegister(Computer c) {
		InstrPop pop = new InstrPop(arguments);
		pop.execute(c);
	}
}
