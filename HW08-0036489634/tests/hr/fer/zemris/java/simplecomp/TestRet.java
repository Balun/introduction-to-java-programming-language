package hr.fer.zemris.java.simplecomp;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Test;
import org.mockito.Mock;

import hr.fer.zemris.java.simplecomp.impl.RegistersImpl;
import hr.fer.zemris.java.simplecomp.impl.instructions.InstrRet;
import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.InstructionArgument;
import hr.fer.zemris.java.simplecomp.models.Memory;
import hr.fer.zemris.java.simplecomp.models.Registers;

public class TestRet {
	@Mock
	private List<Integer> list;
	InstructionArgument regArgument = mock(InstructionArgument.class);
	InstructionArgument memArgument = mock(InstructionArgument.class);
	Computer c = mock(Computer.class);
	Registers r = mock(Registers.class);
	Memory m = mock(Memory.class);
	@SuppressWarnings("unchecked")
	List<InstructionArgument> arguments = mock(List.class);

	@Test
	public void testRet() {

		// pozivamo ret u potprogramu
		// vracamo u pc adresu sljedece naredbe npr 3

		when(regArgument.isRegister()).thenReturn(false);

		when(arguments.size()).thenReturn(0);

		when(c.getRegisters()).thenReturn(r);
		when(c.getMemory()).thenReturn(m);

		when(r.getRegisterValue(RegistersImpl.STACK_REGISTER_INDEX)).thenReturn(99);
		when(m.getLocation(100)).thenReturn(3);
		when(r.getRegisterValue(5)).thenReturn(10);

		retMethod(c);

		// dohvati stog pointer, postavi pc, povecaj stog pointer
		verify(c, times(3)).getRegisters();
		verify(c, times(1)).getMemory();

		// pohrani pc na memorijsku lokaciju na poku pokazuje stog pokazivac
		verify(r, times(1)).setProgramCounter(3);
		;
		// smanji stack pointer
		verify(r, times(1)).setRegisterValue(RegistersImpl.STACK_REGISTER_INDEX, 100);

	}

	public void retMethod(Computer c) {
		InstrRet ret = new InstrRet(arguments);
		ret.execute(c);
	}
}
