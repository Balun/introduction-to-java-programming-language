package hr.fer.zemris.java.simplecomp;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import hr.fer.zemris.java.simplecomp.impl.instructions.InstrLoad;
import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.InstructionArgument;
import hr.fer.zemris.java.simplecomp.models.Memory;
import hr.fer.zemris.java.simplecomp.models.Registers;

@RunWith(MockitoJUnitRunner.class)

public class TestLoad {

	@Mock
	InstructionArgument regArgument = mock(InstructionArgument.class);
	InstructionArgument memArgument = mock(InstructionArgument.class);
	Computer c = mock(Computer.class);
	Registers r = mock(Registers.class);
	Memory m = mock(Memory.class);
	@SuppressWarnings("unchecked")
	List<InstructionArgument> arguments = mock(List.class);

	@Test
	public void testLoad() {

		// load r5, @location
		// @location DEFINT 20
		when(regArgument.isRegister()).thenReturn(true);
		when(memArgument.isNumber()).thenReturn(true);
		when(memArgument.getValue()).thenReturn(5);

		when(arguments.get(0)).thenReturn(regArgument);
		when(arguments.get(1)).thenReturn(memArgument);
		when(arguments.size()).thenReturn(2);

		when(c.getRegisters()).thenReturn(r);
		when(c.getMemory()).thenReturn(m);

		when(m.getLocation(5)).thenReturn(20);
		when(regArgument.getValue()).thenReturn(5);

		loadInRegister(c);
		verify(c, times(1)).getRegisters();
		verify(c, times(1)).getMemory();
		verify(m, times(1)).getLocation(5);
		verify(r, times(1)).setRegisterValue(5, 20);

	}

	public void loadInRegister(Computer c) {
		InstrLoad load = new InstrLoad(arguments);
		load.execute(c);
	}

}
