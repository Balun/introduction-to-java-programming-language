package hr.fer.zemris.java.simplecomp;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Test;
import org.mockito.Mock;

import hr.fer.zemris.java.simplecomp.impl.RegistersImpl;
import hr.fer.zemris.java.simplecomp.impl.instructions.InstrCall;
import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.InstructionArgument;
import hr.fer.zemris.java.simplecomp.models.Memory;
import hr.fer.zemris.java.simplecomp.models.Registers;

public class TestCall {
	@Mock
	InstructionArgument regArgument = mock(InstructionArgument.class);
	InstructionArgument memArgument = mock(InstructionArgument.class);
	Computer c = mock(Computer.class);
	Registers r = mock(Registers.class);
	Memory m = mock(Memory.class);
	@SuppressWarnings("unchecked")
	List<InstructionArgument> arguments = mock(List.class);

	@Test
	public void testCall() {

		// call @potprogram
		// npr. prva instrukcija potprograma je na adresi 50
		// pc je trenutno na 1

		when(regArgument.isRegister()).thenReturn(false);

		when(arguments.get(0)).thenReturn(memArgument);
		when(arguments.size()).thenReturn(1);
		when(memArgument.getValue()).thenReturn(50);
		when(memArgument.isNumber()).thenReturn(true);
		when(c.getRegisters()).thenReturn(r);
		when(c.getMemory()).thenReturn(m);

		when(r.getRegisterValue(RegistersImpl.STACK_REGISTER_INDEX)).thenReturn(100);
		when(r.getProgramCounter()).thenReturn(1);
		when(r.getRegisterValue(5)).thenReturn(10);
		when(arguments.get(0).getValue()).thenReturn(5);

		callMethod(c);

		// ,dohvati pc,dohvati stog pokazivac, smanji stog pokazivac, pohrani
		// adresu u pc
		verify(c, times(4)).getRegisters();
		verify(c, times(1)).getMemory();

		// pohrani pc na memorijsku lokaciju na poku pokazuje stog pokazivac
		verify(m, times(1)).setLocation(100, 1);
		// smanji stack pointer
		verify(r, times(1)).setRegisterValue(RegistersImpl.STACK_REGISTER_INDEX, 99);

	}

	public void callMethod(Computer c) {
		InstrCall call = new InstrCall(arguments);
		call.execute(c);
	}
}
