package hr.fer.zemris.java.gui.calc;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.util.Stack;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import javax.swing.*;
import hr.fer.zemris.java.gui.layouts.CalcLayout;
import hr.fer.zemris.java.gui.layouts.RCPosition;

/**
 * This class represents the panel for all {@link Calculator} buttons and
 * functions to be displayed. It extends the {@link JPanel} class, putting the
 * elements in the predefined {@link CalcLayout} manager layout.
 * 
 * @author Matej Balun
 *
 */
public class CalcPanel extends JPanel {

	/**
	 * The serial version UID for this {@link CalcPanel} class.
	 */
	private static final long serialVersionUID = -1691243801587227962L;

	/**
	 * The colour constant for the buttons of the {@link Calculator}.
	 */
	private static final String COLOR = "#20aaff";

	/**
	 * The {@link Font} constant for the buttons of the {@link Calculator}.
	 */
	private static final Font FONT = new Font("DIALOG", Font.BOLD, 15);

	/**
	 * The flag for indication of complex operation.
	 */
	private boolean secondOperator;

	/**
	 * The flag for indication of operator-type operation.
	 */
	private boolean hasOperator;

	/**
	 * The flag for indication of calculator reset.
	 */
	private boolean reset;

	/**
	 * The flag for indication of inverse functions.
	 */
	private boolean inverse;

	/**
	 * The display of the calculator.
	 */
	private JLabel screen;

	/**
	 * The current value for calculation.
	 */
	private double value;

	/**
	 * The current operator for calculation.
	 */
	private String operator;

	/**
	 * The {@link Stack} for push/pop operations.
	 */
	private Stack<String> stack;

	/**
	 * The implementation of strategy-based interface for initialisation of
	 * listeners for operator buttons.
	 */
	private final Consumer<String> SET_OPERATOR = x -> {
		if (secondOperator) {
			calculate();
			secondOperator = !secondOperator;
		}

		value = Double.parseDouble(screen.getText());
		operator = x;
		hasOperator = true;
	};

	/**
	 * The implementation of strategy-based interface for initialisation of
	 * listeners for number buttons.
	 */
	private final Consumer<JButton> SET_NUMBER = x -> {

		if (screen.getText().startsWith("0") && !screen.getText().contains(".")) {
			screen.setText("");
			screen.setText(screen.getText() + x.getText());

		} else if (reset) {
			screen.setText(x.getText());
			reset = !reset;

		} else if (!hasOperator) {
			screen.setText(screen.getText() + x.getText());

		} else if (hasOperator) {
			hasOperator = !hasOperator;
			screen.setText(x.getText());
			secondOperator = true;
		}
	};

	/**
	 * The implementation of strategy-based interface for initialisation of
	 * listeners for function buttons.
	 */
	private final BiConsumer<Function<Double, Double>, Function<Double, Double>> FUNCTION = (x, y) -> {
		double result = 0;

		if (inverse) {
			result = y.apply(Double.parseDouble(screen.getText()));
		} else {
			result = x.apply(Double.parseDouble(screen.getText()));
		}

		screen.setText(Double.toString(result));
		reset = true;
	};

	/**
	 * This constructor initialises the {@link CalcPanel} via default values of
	 * its attributes.
	 */
	public CalcPanel() {

		this.setLayout(new CalcLayout(5));
		this.value = 0;
		this.operator = "";
		this.stack = new Stack<>();

		reset = false;
		secondOperator = false;
		inverse = false;
		hasOperator = false;

		initGUI();
	}

	/**
	 * This methods adds the {@link Component}s to the {@link CalcPanel} with
	 * their {@link ActionListener}s and default {@link Color} and {@link Font}.
	 */
	private void initGUI() {

		screen = new JLabel("0");
		screen.setOpaque(true);
		screen.setHorizontalAlignment(JTextField.RIGHT);
		screen.setFont(new Font("DIALOG", Font.BOLD, 40));

		this.add(screen, new RCPosition(1, 1));

		addOperators();
		addFunctions();
		addNumbers();
		addRightFunctions();

		for (Component comp : this.getComponents()) {
			comp.setFont(FONT);
			comp.setBackground(Color.decode(COLOR));
		}

		this.getComponent(0).setBackground(Color.YELLOW);
		this.getComponent(0).setFont(new Font("DIALOG", Font.BOLD, 40));
	}

	/**
	 * The method for initialisation of clr, res, push, pop and inv buttons of
	 * {@link Calculator}.
	 */
	private void addRightFunctions() {

		JButton clr = new JButton("clr");
		clr.addActionListener(l -> screen.setText("0"));
		this.add(clr, new RCPosition(1, 7));

		JButton res = new JButton("res");
		res.addActionListener(l -> {
			reset = false;
			operator = "";
			hasOperator = false;
			value = 0;
			screen.setText("0");
			secondOperator = false;
			stack.clear();
		});

		this.add(res, new RCPosition(2, 7));

		JButton push = new JButton("push");
		push.addActionListener(l -> stack.push(screen.getText()));
		this.add(push, new RCPosition(3, 7));

		JButton pop = new JButton("pop");
		pop.addActionListener(l -> {
			if (stack.isEmpty()) {
				screen.setText("emptyStack");
			} else {
				screen.setText(stack.pop());
			}
		});
		this.add(pop, new RCPosition(4, 7));

		JCheckBox inv = new JCheckBox("inv");
		inv.setHorizontalAlignment(JTextField.CENTER);
		inv.addActionListener(l -> inverse = !inverse);
		this.add(inv, new RCPosition(5, 7));
	}

	/**
	 * This method adds the operator buttons to the {@link CalcPanel} of the
	 * {@link Calculator}.
	 */
	private void addOperators() {

		JButton equals = new JButton("=");
		equals.addActionListener(e -> {
			if (!operator.isEmpty()) {
				calculate();
				operator = "";
				reset = true;
				hasOperator = secondOperator = false;
			}
		});
		this.add(equals, new RCPosition(1, 6));

		CalcUtility.addOperators(this, SET_OPERATOR, screen);
	}

	/**
	 * This method calculates the current operation of the {@link Calculator}
	 * based on the current value and operator stored, as well as the inverse
	 * mode.
	 */
	private void calculate() {
		CalcUtility.calculate(value, screen, operator, inverse);
	}

	/**
	 * This method adds the number {@link JButton}s as well as their
	 * {@link ActionListener}s to the {@link CalcPanel}.
	 */
	private void addNumbers() {
		CalcUtility.addNumbers(this, SET_NUMBER);
	}

	/**
	 * This method adds the function {@link JButton}s as well as their listeners
	 * to the {@link CalcPanel}.
	 */
	private void addFunctions() {
		CalcUtility.addFunctions(this, FUNCTION);
	}

}
