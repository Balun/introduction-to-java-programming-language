package hr.fer.zemris.java.gui.calc;

import java.awt.Component;
import java.awt.event.ActionListener;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import javax.swing.*;
import hr.fer.zemris.java.gui.layouts.RCPosition;

/**
 * This utility class offers the methods for initialisation of {@link Component}
 * s in the {@link CalcPanel} for the {@link Calculator} application.
 * 
 * @author Matej Balun
 *
 */
public class CalcUtility {

	/**
	 * The private constructor that prevents the instancing of this utility
	 * class
	 */
	private CalcUtility() {
	}

	/**
	 * This method adds the function buttons to the specified {@link CalcPanel}
	 * with their listeners using the {@link BiConsumer} strategy interface
	 * implementation.
	 * 
	 * @param panel
	 *            the specified {@link CalcPanel} extending tje {@link JPanel}
	 * @param function
	 *            the specified {@link BiConsumer} strategy interface
	 *            implementation for listeners
	 */
	public static void addFunctions(JPanel panel,
			BiConsumer<Function<Double, Double>, Function<Double, Double>> function) {

		JButton fract = new JButton("1/x");
		fract.addActionListener(l -> function.accept(x -> 1 / x, x -> 1 / x));
		panel.add(fract, new RCPosition(2, 1));

		JButton sin = new JButton("sin (asin)");
		sin.addActionListener(l -> function.accept(Math::sin, Math::asin));
		panel.add(sin, new RCPosition(2, 2));

		JButton log = new JButton("log (10^n)");
		log.addActionListener(l -> function.accept(Math::log10, x -> Math.pow(10, x)));
		panel.add(log, new RCPosition(3, 1));

		JButton cos = new JButton("cos (acos)");
		cos.addActionListener(l -> function.accept(Math::cos, Math::acos));
		panel.add(cos, new RCPosition(3, 2));

		JButton ln = new JButton("ln (e^x)");
		ln.addActionListener(l -> function.accept(Math::log, x -> Math.pow(Math.E, x)));
		panel.add(ln, new RCPosition(4, 1));

		JButton tan = new JButton("tan (atan)");
		tan.addActionListener(l -> function.accept(Math::tan, Math::atan));
		panel.add(tan, new RCPosition(4, 2));

		JButton ctg = new JButton("ctg (actg)");
		ctg.addActionListener(l -> function.accept(x -> 1 / Math.tan(x), x -> 1 / Math.atan(x)));
		panel.add(ctg, new RCPosition(5, 2));
	}

	/**
	 * This method add the operator {@link JButton} with their
	 * {@link ActionListener}s too the specified {@link JPanel}, using the
	 * {@link Consumer} strategy interface implementation.
	 * 
	 * @param panel
	 *            the specified {@link JPanel}, specially here {@link CalcPanel}
	 * @param operator
	 *            the {@link Consumer} strategy based interface for
	 *            {@link JButton} {@link ActionListener}s
	 * @param screen
	 *            the screen of the specified {@link Calculator} application
	 */
	public static void addOperators(JPanel panel, Consumer<String> operator, JLabel screen) {

		JButton div = new JButton("/");
		div.addActionListener(l -> operator.accept(div.getText()));
		panel.add(div, new RCPosition(2, 6));

		JButton mul = new JButton("*");
		mul.addActionListener(l -> operator.accept(mul.getText()));
		panel.add(mul, new RCPosition(3, 6));

		JButton sub = new JButton("-");
		sub.addActionListener(l -> operator.accept(sub.getText()));
		panel.add(sub, new RCPosition(4, 6));

		JButton plus = new JButton("+");
		plus.addActionListener(l -> operator.accept(plus.getText()));
		panel.add(plus, new RCPosition(5, 6));

		JButton dot = (new JButton("."));
		dot.addActionListener(l -> {
			if (!screen.getText().contains(".")) {
				screen.setText(screen.getText() + ".");
			}
		});
		panel.add(dot, new RCPosition(5, 5));

		JButton pow = new JButton("x^n (nrt)");
		pow.addActionListener(l -> operator.accept("^"));
		panel.add(pow, new RCPosition(5, 1));

		JButton sig = new JButton("+/-");
		sig.addActionListener(l -> {
			try {
				screen.setText(Integer.toString(-Integer.parseInt(screen.getText())));
			} catch (NumberFormatException e) {
				screen.setText(Double.toString(-Double.parseDouble(screen.getText())));
			}
		});
		panel.add(sig, new RCPosition(5, 4));
	}

	/**
	 * This method add the number {@link JButton} to the specified
	 * {@link JPanel}, with their {@link ActionListener}s.
	 * 
	 * @param panel
	 *            the specified {@link CalcPanel}
	 * @param setNumber
	 *            the {@link Consumer} strategy-based interface for
	 *            {@link ActionListener}s
	 */
	public static void addNumbers(JPanel panel, Consumer<JButton> setNumber) {
		int num = 1;

		for (int i = 4; i >= 2; i--) {
			for (int j = 3; j <= 5; j++) {

				JButton number = new JButton(Integer.toString(num));
				number.addActionListener(e -> setNumber.accept(number));
				panel.add(number, new RCPosition(i, j));
				num++;
			}
		}

		JButton zero = new JButton("0");
		zero.addActionListener(e -> setNumber.accept(zero));
		panel.add(zero, new RCPosition(5, 3));
	}

	/**
	 * This method calculates the result of the specified operation in the
	 * {@link Calculator} application. The operation is defined via operator, as
	 * well as with display value and currently stored value.
	 * 
	 * @param value
	 *            the currently stored {@link Calculator} value
	 * @param screen
	 *            display of the {@link Calculator}
	 * @param operator
	 *            the specified operator for calculation
	 * @param inverse
	 *            the mode for the calcuation
	 */
	public static void calculate(double value, JLabel screen, String operator, boolean inverse) {
		double secoundValue = Double.parseDouble(screen.getText());
		double result = 0;

		switch (operator) {

		case "+":
			result = value + secoundValue;
			break;

		case "*":
			result = value * secoundValue;
			break;

		case "/":
			result = value / secoundValue;
			break;

		case "-":
			result = value - secoundValue;
			break;

		case "^":
			if (inverse) {
				result = Math.pow(value, 1 / secoundValue);
			} else {
				result = Math.pow(value, secoundValue);
			}

		default:
			break;
		}

		screen.setText(Double.toString(result));
	}

}
