package hr.fer.zemris.java.gui.calc;

import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import hr.fer.zemris.java.gui.layouts.CalcLayout;

/**
 * This Program represents the simple scientific calculator made by Java Swing
 * GUI API. The Application creates the calculator offering various operators
 * and functions. More functions can be achieved with inverting the existing
 * functions.
 * 
 * @author Matej Balun
 *
 */
public class Calculator extends JFrame {

	/**
	 * The serial version UID for this frame.
	 */
	private static final long serialVersionUID = -5148492242213114522L;

	/**
	 * This constructor initialises the {@link Calculator}, setting default size
	 * and position on the screen, as well as the default close operation and
	 * layout manager.
	 */
	public Calculator() {
		setLocation(200, 200);
		setTitle("Swing calculator");
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		this.setLayout(new BorderLayout());
		initGUI();
	}

	/**
	 * This method finishes the initialisation of the {@link Calculator},
	 * creating the panel with the {@link CalcLayout} for the standard
	 * calculator layout.
	 */
	private void initGUI() {
		JPanel panel = new CalcPanel();
		panel.setSize(this.getSize());
		getContentPane().add(panel, BorderLayout.CENTER);
		pack();
	}

	/**
	 * The main method that executes the program.
	 * 
	 * @param args
	 *            not used
	 */
	public static void main(String[] args) {

		SwingUtilities.invokeLater(() -> {
			JFrame frame = new Calculator();
			frame.setVisible(true);
		});
	}

}
