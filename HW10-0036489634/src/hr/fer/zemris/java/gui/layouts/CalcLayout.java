package hr.fer.zemris.java.gui.layouts;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager2;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Function;

import hr.fer.zemris.java.gui.calc.Calculator;

/**
 * This class represents the custom layout manager for the simple
 * {@link Calculator} application. It implements the {@link LayoutManager2}
 * interface, offering the standard scientific calculator layout of the
 * {@link Component}s.
 * 
 * @author Matej Balun
 *
 */
public class CalcLayout implements LayoutManager2 {

	/**
	 * The default space between the {@link Component}s.
	 */
	private int space;

	/**
	 * The predefined number of columns.
	 */
	public static final int COLS = 7;

	/**
	 * The predefined number of rows.
	 */
	public static final int ROWS = 5;

	/**
	 * Maximum number of {@link Component}s on the {@link CalcLayout}.
	 */
	public static final int MAX_NUMBER = 31;

	/**
	 * The constant for central alignment of the {@link Component}s.
	 */
	private static final float CENTER_ALIGNMENT = 0.5f;

	/**
	 * The special first position of the {@link CalcLayout}.
	 */
	private static final RCPosition FIRST = new RCPosition(1, 1);

	/**
	 * The map of the layout positions and {@link Component}s.
	 */
	private Map<RCPosition, Component> layoutMap;

	/**
	 * This constructor initialises the {@link CalcLayout} with the specified
	 * space for the layout.
	 * 
	 * @param space
	 *            the specified space of the layout
	 */
	public CalcLayout(int space) {
		super();
		this.space = space;
		this.layoutMap = new HashMap<>();
	}

	/**
	 * This default constructor passes the zero-sized space for the
	 * {@link Component}s to the next constructor.
	 */
	public CalcLayout() {
		this(0);
	}

	@Deprecated
	@Override
	public void addLayoutComponent(String name, Component comp) {
		// ignore
	}

	@Override
	public void removeLayoutComponent(Component comp) {
		RCPosition key = null;

		for (Entry<RCPosition, Component> entry : this.layoutMap.entrySet()) {
			if (entry.getValue().equals(comp)) {
				key = entry.getKey();
			}
		}

		this.layoutMap.remove(key);
	}

	@Override
	public Dimension preferredLayoutSize(Container parent) {
		return getSize(parent, c -> (int) c.getPreferredSize().getWidth(), c -> (int) c.getPreferredSize().getHeight());
	}

	@Override
	public Dimension minimumLayoutSize(Container parent) {
		return getSize(parent, c -> (int) c.getMinimumSize().getWidth(), c -> (int) c.getMinimumSize().getHeight());
	}

	@Override
	public void layoutContainer(Container parent) {
		Dimension maxpreferred = getMaxPreferredDim();

		double x = (double) parent.getWidth() / preferredLayoutSize(parent).getWidth();
		double y = (double) parent.getHeight() / preferredLayoutSize(parent).getHeight();

		maxpreferred.setSize(x * maxpreferred.getWidth(), y * maxpreferred.getHeight());

		for (Entry<RCPosition, Component> entry : this.layoutMap.entrySet()) {

			if (entry.getKey().equals(FIRST)) {
				entry.getValue().setBounds(0, 0, 5 * maxpreferred.width + 4 * space, maxpreferred.height);

			} else {
				entry.getValue().setBounds((entry.getKey().getColumn() - 1) * (maxpreferred.width + space),
						(entry.getKey().getRow() - 1) * (maxpreferred.height + space), maxpreferred.width,
						maxpreferred.height);
			}
		}
	}

	/**
	 * this method determines the maximum preferred dimension of all
	 * {@link Component}s in the {@link CalcLayout}.
	 * 
	 * @return the maximum preferred dimension
	 */
	private Dimension getMaxPreferredDim() {
		int maxWidth = 0;
		int maxHeight = 0;

		for (Entry<RCPosition, Component> entry : this.layoutMap.entrySet()) {

			if (entry.getValue().getPreferredSize() != null) {
				maxHeight = Math.max(maxHeight, entry.getValue().getPreferredSize().height);

				if (entry.getKey().equals(new RCPosition(1, 1))) {
					int tmpWidth = entry.getValue().getPreferredSize().width;
					tmpWidth /= 5;
					tmpWidth -= 4 * space;

					maxWidth = Math.max(maxWidth, tmpWidth);

				} else {
					maxWidth = Math.max(maxWidth, entry.getValue().getPreferredSize().width);
				}
			}
		}

		return new Dimension(maxWidth, maxHeight);
	}

	@Override
	public void addLayoutComponent(Component comp, Object constraints) {
		RCPosition position;

		if (constraints instanceof String) {
			String tmp = (String) constraints;
			try {
				position = new RCPosition(Integer.parseInt(tmp.split(",")[0]), Integer.parseInt(tmp.split(",")[1]));

			} catch (NumberFormatException e) {
				throw new IllegalArgumentException("Illegal format, unable to parse string into intagers.");

			} catch (ArrayIndexOutOfBoundsException e) {
				throw new IllegalArgumentException("Illegally separated string.");
			}

		} else if (constraints instanceof RCPosition) {
			position = (RCPosition) constraints;

		} else {
			throw new IllegalArgumentException("Illegal constraint object specified.");
		}

		if (this.layoutMap.size() == MAX_NUMBER) {
			throw new IllegalArgumentException("The calculator layout is currently full.");

		} else if (!check(position)) {
			throw new IllegalArgumentException("The specified position is out of bounds");

		} else if (this.layoutMap.containsKey(position)) {
			throw new IllegalArgumentException("The specified position is already taken.");
		}

		this.layoutMap.put(position, comp);
	}

	/**
	 * this method checks if the specified {@link RCPosition} is in the correct
	 * bounds of the {@link CalcLayout}.
	 * 
	 * @param position
	 *            the specified {@link RCPosition}
	 * @return true if the position is in the bounds, otherwise false
	 */
	private boolean check(RCPosition position) {
		int row = position.getRow();
		int col = position.getColumn();

		if (row < 1 || row > 5) {
			return false;

		} else if (col < 1 || col > 7) {
			return false;

		} else if (row == 1 && col > 1 && col < 6) {
			return false;
		}

		return true;
	}

	@Override
	public Dimension maximumLayoutSize(Container parent) {
		Insets insets = parent.getInsets();

		int width = Integer.MAX_VALUE;
		int height = Integer.MAX_VALUE;

		for (Entry<RCPosition, Component> entry : this.layoutMap.entrySet()) {

			int tmpWidth = Integer.MAX_VALUE;
			int tmpHeight = Integer.MAX_VALUE;

			try {
				tmpWidth = entry.getValue().getMaximumSize().width;
			} catch (NullPointerException e) {
				// The specified value is a null-reference, ignore
			}

			try {
				tmpHeight = entry.getValue().getMaximumSize().height;
			} catch (NullPointerException e) {
				// The specified value is a null-reference, ignore
			}

			if (entry.getKey().getRow() == 1 && entry.getKey().getColumn() == 1) {
				tmpWidth /= 5;
				tmpWidth -= 4 * space;
			}

			if (height > tmpHeight) {
				height = tmpHeight;
			}

			if (width > tmpWidth) {
				width = tmpWidth;
			}
		}

		return new Dimension(insets.left + insets.right + COLS * width + (COLS - 1) * space,
				insets.top + insets.bottom + COLS * height + (COLS - 1) * space);
	}

	/**
	 * This method returns the size specified with the strategy-based
	 * {@link Function} interfaces for preferred size calculation.
	 * 
	 * @param parent
	 *            the parenc {@link Container}
	 * @param functionWidth
	 *            {@link Function} interface implementation for height
	 *            manipulation
	 * @param functionHeight
	 *            {@link Function} interface implementation for width
	 *            manipulation
	 * @return the calculated preferred {@link Dimension}
	 */
	private Dimension getSize(Container parent, Function<Component, Integer> functionWidth,
			Function<Component, Integer> functionHeight) {

		Insets insets = parent.getInsets();

		int width = 0;
		int height = 0;

		for (Entry<RCPosition, Component> entry : this.layoutMap.entrySet()) {

			int tmpWidth = 0;
			int tmpHeight = 0;

			try {
				tmpWidth = functionWidth.apply(entry.getValue());
			} catch (NullPointerException e) {
				// The specified value is a null-reference, ignore
			}

			try {
				tmpHeight = functionHeight.apply(entry.getValue());
			} catch (NullPointerException e) {
				// The specified value is a null-reference, ignore
			}

			if (entry.getKey().getRow() == 1 && entry.getKey().getColumn() == 1) {
				tmpWidth /= 5;
				tmpWidth -= 4 * space;
			}

			if (height < tmpHeight) {
				height = tmpHeight;
			}

			if (width < tmpWidth) {
				width = tmpWidth;
			}
		}

		return new Dimension(insets.left + insets.right + COLS * width + (COLS - 1) * space,
				insets.top + insets.bottom + COLS * height + (COLS - 1) * space);

	}

	@Override
	public float getLayoutAlignmentX(Container target) {
		return CENTER_ALIGNMENT;
	}

	@Override
	public float getLayoutAlignmentY(Container target) {
		return CENTER_ALIGNMENT;
	}

	@Override
	public void invalidateLayout(Container target) {
		// ignore
	}

}
