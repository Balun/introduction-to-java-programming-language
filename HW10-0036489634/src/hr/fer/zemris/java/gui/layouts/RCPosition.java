package hr.fer.zemris.java.gui.layouts;

import java.awt.Component;

import hr.fer.zemris.java.gui.calc.Calculator;

/**
 * This class represents the position of the {@link Component} on the
 * {@link CalcLayout} for the simple {@link Calculator} application.
 * 
 * @author Matej Balun
 *
 */
public class RCPosition {

	/**
	 * The row index.
	 */
	private int row;

	/**
	 * The column index
	 */
	private int column;

	/**
	 * This constructor initialises the {@link RCPosition} with the specified
	 * row and column index.
	 * 
	 * @param row
	 *            the specified row index
	 * @param column
	 *            the specified column index
	 */
	public RCPosition(int row, int column) {
		super();
		this.row = row;
		this.column = column;
	}

	/**
	 * Return the row index.
	 * 
	 * @return the row index
	 */
	public int getRow() {
		return row;
	}

	/**
	 * Returns the column index.
	 * 
	 * @return the column index
	 */
	public int getColumn() {
		return column;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + column;
		result = prime * result + row;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RCPosition other = (RCPosition) obj;
		if (column != other.column)
			return false;
		if (row != other.row)
			return false;
		return true;
	}

}
