package hr.fer.zemris.java.gui.charts;

import java.util.List;

/**
 * This class represents the simple {@link BarChart} data for the
 * {@link BarChartDemo} application. The class offers couple of read-only values
 * for the chart creation.
 * 
 * @author Matej Balun
 *
 */
public class BarChart {

	/**
	 * List of the chart data values.
	 */
	private List<XYValue> values;

	/**
	 * The x-axis description.
	 */
	private String descX;

	/**
	 * The y-axis description
	 */
	private String descY;

	/**
	 * The minimum y-axis value.
	 */
	private int minY;

	/**
	 * The maximum y-axis value.
	 */
	private int maxY;

	/**
	 * The space between two values.
	 */
	private int space;

	/**
	 * This constructor initialises the {@link BarChart} with its specified
	 * values.
	 * 
	 * @param values
	 *            the data for the chart
	 * @param descX
	 *            the x-axis description
	 * @param descY
	 *            the y-axis description
	 * @param minY
	 *            the minimum y-axis value
	 * @param maxY
	 *            the maximum y-axis value
	 * @param space
	 *            the space between two values
	 */
	public BarChart(List<XYValue> values, String descX, String descY, int minY, int maxY, int space) {
		super();
		this.values = values;
		this.descX = descX;
		this.descY = descY;
		this.minY = minY;
		this.maxY = maxY;
		this.space = space;
	}

	/**
	 * Returns the data of the chart.
	 * 
	 * @return data of the chart
	 */
	public List<XYValue> getValues() {
		return values;
	}

	/**
	 * Returns the x-axis description.
	 * 
	 * @return the x-axis description.
	 */
	public String getDescX() {
		return descX;
	}

	/**
	 * Returns the y-axis description.
	 * 
	 * @return the y-axis description
	 */
	public String getDescY() {
		return descY;
	}

	/**
	 * Returns the minimum y-axis value.
	 * 
	 * @return the minimum y-axis value.
	 */
	public int getMinY() {
		return minY;
	}

	/**
	 * Returns the maximum y-axis value.
	 * 
	 * @return the maximum y-axis value
	 */
	public int getMaxY() {
		return maxY;
	}

	/**
	 * Returns the space between the two values.
	 * 
	 * @return the space between the two values.
	 */
	public int getSpace() {
		return space;
	}

}
