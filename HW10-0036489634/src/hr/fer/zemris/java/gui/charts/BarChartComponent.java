package hr.fer.zemris.java.gui.charts;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;

import javax.swing.JComponent;

/**
 * 
 * This class represents the main component in the {@link BarChartDemo}
 * application. It extends the {@link JComponent}, inheriting all its methods
 * and overrides the paint() method to specify the custom way of projecting the
 * chart on the display.
 * 
 * @author Matej Balun
 *
 */
public class BarChartComponent extends JComponent {

	/**
	 * The serial version UID of this {@link BarChartComponent} class.
	 */
	private static final long serialVersionUID = 14540194252179561L;

	/**
	 * The {@link BarChart} with data to display.
	 */
	private BarChart chart;

	/**
	 * Width of the component.
	 */
	private int width;

	/**
	 * Height of the component
	 */
	private int height;

	/**
	 * 
	 * The default space inside the component.
	 */
	private static final int SPACE = 7;

	/**
	 * This nested static class contains most of the necessary informations
	 * about the component, chart and the picture for right projection of the
	 * {@link BarChartComponent} on the display
	 * 
	 * @author Matej Balun
	 *
	 */
	private static class DimInfo {

		/**
		 * Number of horizontal lines.
		 */
		int horizontalLines;

		/**
		 * The bottom space.
		 */
		int spaceDown;

		/**
		 * The left space.
		 */
		int spaceLeft;

		/**
		 * Length of the space.
		 */
		int spaceLen;

		/**
		 * End position of horizontal line.
		 */
		int horizontalLinePos;

		/**
		 * Length of the vertical line
		 */
		int verticalLineLen;

		/**
		 * Position of y-axis description
		 */
		int descYPos;

		/**
		 * Number of vertical lines.
		 */
		int verticalLines;

		/**
		 * Width of the chart's columns.
		 */
		int columnWidth;
	}

	/**
	 * This constructor initialises the {@link BarChartComponent} with the
	 * specified {@link BarChart} containing the data to display.
	 * 
	 * @param chart
	 *            the specified {@link BarChart} data
	 */
	public BarChartComponent(BarChart chart) {
		super();
		this.chart = chart;
	}

	@Override
	protected void paintComponent(Graphics graphics) {

		Rectangle rectangle = new Rectangle(getInsets().left, getInsets().top,
				getSize().width - getInsets().left - getInsets().right,
				getSize().height - getInsets().top - getInsets().bottom);

		width = getSize().width - getInsets().left - getInsets().right;
		height = getSize().height - getInsets().top - getInsets().bottom;

		graphics.setColor(getBackground());
		graphics.fillRect(rectangle.x, rectangle.y, rectangle.width, rectangle.height);

		graphics.setColor(Color.DARK_GRAY);
		FontMetrics metrics = graphics.getFontMetrics();

		graphics.drawString(chart.getDescX(), width / 2 - metrics.stringWidth(chart.getDescX()) / 2, height);

		DimInfo info = new DimInfo();

		info.horizontalLines = (chart.getMaxY() - chart.getMinY()) / chart.getSpace() + 1;

		info.spaceDown = 2 * (SPACE + metrics.getAscent());

		info.spaceLeft = SPACE * 3 + metrics.getAscent() + metrics.stringWidth(Integer.toString(chart.getMaxY()));

		info.spaceLen = (height - info.spaceDown - SPACE) / info.horizontalLines;

		info.horizontalLinePos = width - info.spaceLeft - SPACE;

		info.verticalLineLen = getInsets().bottom - SPACE * 3 + height - metrics.getAscent() * 2;

		info.descYPos = -info.verticalLineLen / 2 - (SPACE * 2 + info.spaceLen)
				- metrics.stringWidth(chart.getDescY()) / 2;

		info.verticalLines = chart.getValues().size();

		info.columnWidth = (info.horizontalLinePos - info.spaceLeft) / info.verticalLines;

		draw(metrics, graphics, info);
	}

	/**
	 * This method draws the most of the chart on the window.
	 * 
	 * @param metrics
	 *            the specified {@link FontMetrics}
	 * @param graphics
	 *            the specified {@link Graphics}
	 * @param info
	 *            the {@link DimInfo} class with the specified dimension
	 *            informations
	 */
	private void draw(FontMetrics metrics, Graphics graphics, DimInfo info) {

		Graphics2D twoDimGraph = (Graphics2D) graphics;
		AffineTransform affine = twoDimGraph.getTransform();
		AffineTransform newAffine = new AffineTransform();

		newAffine.rotate(-Math.PI / 2);
		twoDimGraph.setTransform(newAffine);

		twoDimGraph.drawString(chart.getDescY(), info.descYPos, SPACE * 3);
		twoDimGraph.setTransform(affine);

		graphics.setFont(graphics.getFont().deriveFont(Font.BOLD));

		int vertical = drawHorizontal(graphics, info, metrics);
		drawVertical(graphics, info, vertical);

		graphics.setColor(Color.ORANGE);

		for (int i = 0, j = info.spaceLeft; i < info.verticalLines; i++, j += info.columnWidth) {
			graphics.fillRect(j,
					info.verticalLineLen
							- (chart.getValues().get(i).getY() - chart.getMinY()) * info.spaceLen / chart.getSpace(),
					info.columnWidth - SPACE,
					(chart.getValues().get(i).getY() - chart.getMinY()) * info.spaceLen / chart.getSpace());

		}
	}

	/**
	 * This method draws the vertical lines of the {@link BarChartComponent}.
	 * 
	 * @param graphics
	 *            the specified {@link Graphics}
	 * @param info
	 *            the specified {@link DimInfo} with dimension informations
	 * @param vertical
	 *            the start of the vertical line
	 */
	private void drawVertical(Graphics graphics, DimInfo info, int vertical) {

		for (int i = 0, j = info.spaceLeft + info.columnWidth; i < info.verticalLines; i++, j += info.columnWidth) {

			graphics.setColor(Color.DARK_GRAY);
			String stringX = Integer.toString(chart.getValues().get(i).getX());
			graphics.drawString(stringX, j - info.columnWidth / 2, info.verticalLineLen + SPACE * 3);
			graphics.setColor(Color.LIGHT_GRAY);
			graphics.drawLine(j, vertical, j, info.verticalLineLen);
		}
	}

	/**
	 * This method draws the horizontal lines of the {@link BarChartComponent}.
	 * 
	 * @param graphics
	 *            the specified {@link Graphics}
	 * @param info
	 *            the specified {@link DimInfo} with dimension information
	 * @param metrics
	 *            the specified {@link FontMetrics}
	 * @return the start of the vertical line
	 */
	private int drawHorizontal(Graphics graphics, DimInfo info, FontMetrics metrics) {
		int inset = getInsets().bottom - SPACE * 3 + height - metrics.getAscent() * 2;
		int vertical = 0;

		for (int i = 0, j = inset, k = chart.getMinY(); i < info.horizontalLines; i++, j -= info.spaceLen, k += chart
				.getSpace()) {

			graphics.setColor(Color.DARK_GRAY);
			String stringY = Integer.toString(k);
			graphics.drawString(stringY, info.spaceLeft - SPACE - metrics.stringWidth(stringY), j + SPACE);

			graphics.setColor(Color.LIGHT_GRAY);
			graphics.drawLine(info.spaceLeft, j, info.horizontalLinePos, j);
			vertical = j;
		}

		graphics.drawLine(info.spaceLeft, vertical, info.spaceLeft, info.verticalLineLen);
		return vertical;
	}

}
