package hr.fer.zemris.java.gui.charts;

import java.awt.BorderLayout;
import java.awt.Color;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

/**
 * This program represents the bar-styled chart for the various data types. The
 * application is a simple excel-like chart, offering resizing, colour and
 * precise projection of values on the chart. The program takes one arguments
 * from the command line representing the valid path to the file with chart
 * data.
 * 
 * There is a example file named "charts.txt" in the project directory for
 * application testing.
 * 
 * @author Matej Balun
 *
 */
public class BarChartDemo extends JFrame {

	/**
	 * The serial version UID for this {@link BarChartDemo} class.
	 */
	private static final long serialVersionUID = 2672091066982218872L;

	/**
	 * The {@link BarChart} with the data for the projection.
	 */
	private BarChart chart;

	/**
	 * The path to the file with the data.
	 */
	private String path;

	/**
	 * This constructor initialises the {@link BarChartDemo} application with
	 * the specified {@link BarChart} and path to the file with chart data.
	 * 
	 * @param chart
	 *            the specified {@link BarChart} to display
	 * @param path
	 *            the path to the file with data for the {@link BarChart}
	 */
	public BarChartDemo(BarChart chart, String path) {

		this.chart = chart;
		this.path = path;

		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("Bar chart - HW10");
		setLocation(200, 200);
		setSize(800, 400);
		initGUI();
	}

	/**
	 * This method initialises the GUI for the {@link BarChartDemo} application
	 * with the layout manager, border and specified {@link BarChartComponent}.
	 */
	private void initGUI() {
		getContentPane().setLayout(new BorderLayout());

		BarChartComponent chartComponent = new BarChartComponent(chart);

		chartComponent.setBorder(BorderFactory.createLineBorder(Color.WHITE, 5));

		chartComponent.setBackground(Color.WHITE);
		getContentPane().add(chartComponent, BorderLayout.CENTER);

		JLabel label = new JLabel(path, SwingConstants.CENTER);
		label.setOpaque(true);
		label.setBackground(Color.GREEN);
		getContentPane().add(label, BorderLayout.NORTH);
	}

	/**
	 * The main method that executes the application and reads from the data
	 * file.
	 * 
	 * @param args
	 *            the path to the data file
	 */
	public static void main(String[] args) {
		if (args.length != 1) {
			System.err.println("Illegal number of arguments provided.");
			System.exit(-1);
		}

		List<String> lines = null;
		Path path = Paths.get(args[0].trim()).toAbsolutePath();

		try {
			lines = Files.readAllLines(path);
		} catch (IOException e) {
			System.err.println("Error in reading file.");
			System.exit(-1);
		}

		BarChart chart = createChart(lines);
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				JFrame frame = new BarChartDemo(chart, path.toString());
				frame.setVisible(true);
			}
		});
	}

	/**
	 * This method creates the {@link BarChart} for the {@link BarChartDemo}
	 * application with the parameters found in the specified file.
	 * 
	 * @param lines
	 *            the content of the data file
	 * @return the new {@link BarChart} object
	 */
	private static BarChart createChart(List<String> lines) {
		List<XYValue> values = new ArrayList<>();
		String descX = null;
		String descY = null;
		int minY = 0;
		int maxY = 0;
		int space = 0;

		int i = 0;

		for (String line : lines) {
			switch (i) {

			case 0:
				descX = line.trim();
				i++;
				break;

			case 1:
				descY = line.trim();
				i++;
				break;

			case 2:
				String[] pairs = line.trim().split(" ");
				for (String pair : pairs) {
					values.add(new XYValue(Integer.parseInt(pair.split(",")[0]), Integer.parseInt(pair.split(",")[1])));
				}
				i++;
				break;

			case 3:
				minY = Integer.parseInt(line.trim());
				i++;
				break;

			case 4:
				maxY = Integer.parseInt(line.trim());
				i++;
				break;

			case 5:
				space = Integer.parseInt(line.trim());
				i++;
				break;

			default:
				break;
			}

		}

		return new BarChart(values, descX, descY, minY, maxY, space);
	}

}
