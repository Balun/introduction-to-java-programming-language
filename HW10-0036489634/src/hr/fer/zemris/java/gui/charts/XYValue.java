package hr.fer.zemris.java.gui.charts;

/**
 * This class contains the X-axis and Y-axis values of the specified element in
 * the {@link BarChartDemo} application.
 * 
 * @author Matej Balun
 *
 */
public class XYValue {

	/**
	 * The x-axis value.
	 */
	private int x;

	/**
	 * the y-axis value.
	 */
	private int y;

	/**
	 * This constructor initialises the {@link XYValue} with the specified
	 * x-axis and y-axis values.
	 * 
	 * @param x
	 *            the specified x-axis value
	 * @param y
	 *            the specified y-axis value
	 */
	public XYValue(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}

	/**
	 * Returns the x-axis value
	 * 
	 * @return the x-axis value
	 */
	public int getX() {
		return x;
	}

	/**
	 * Returns the y-axis value
	 * 
	 * @return the y-axis value
	 */
	public int getY() {
		return y;
	}

}
