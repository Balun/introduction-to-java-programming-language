package hr.fer.zemris.java.gui.prim;

import java.util.ArrayList;
import java.util.List;

import javax.swing.ListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

/**
 * 
 * This class represents the implementation of the {@link ListModel} interface
 * for the prime number calculation application.
 * 
 * @author Matej Balun
 *
 */
public class PrimListModel implements ListModel<Integer> {

	/**
	 * The list of calculated prime numbers.
	 */
	private List<Integer> list;

	/**
	 * The list of registered listeners for this model.
	 */
	private List<ListDataListener> listeners;

	/**
	 * This constructor initialises the {@link PrimListModel}, adding the number
	 * one as the first element of the list.
	 */
	public PrimListModel() {
		this.list = new ArrayList<>();
		this.list.add(1);
		this.listeners = new ArrayList<>();
	}

	@Override
	public int getSize() {
		return this.list.size();
	}

	@Override
	public Integer getElementAt(int index) {
		return this.list.get(index);
	}

	@Override
	public void addListDataListener(ListDataListener l) {
		listeners = new ArrayList<>(listeners);
		listeners.add(l);
	}

	@Override
	public void removeListDataListener(ListDataListener l) {
		listeners = new ArrayList<>(listeners);
		listeners.remove(l);
	}

	/**
	 * This method calculates the next prime number, puts it in the list and
	 * inform all registered listeners of the new number calculation.
	 */
	public void next() {
		int position = list.size();
		list.add(getPrime());
		ListDataEvent event = new ListDataEvent(this, ListDataEvent.INTERVAL_ADDED, position, position);

		for (ListDataListener listener : listeners) {
			listener.intervalAdded(event);
		}
	}

	/**
	 * Returns the next prim number.
	 * 
	 * @return the next prime number.
	 */
	private int getPrime() {
		if (list.size() != 1) {
			int k = list.get(list.size() - 1);
			for (int i = k + 1;; i++) {
				if (isPrime(i)) {
					return i;
				}
			}
		} else {
			return 2;
		}
	}

	/**
	 * Checks if the specified number is prime.
	 * 
	 * @param n
	 *            the specified number
	 * @return true if the number is primary, otherwise false
	 */
	private static boolean isPrime(int n) {
		if (n % 2 == 0)
			return false;

		double k = Math.sqrt(n);
		for (int i = 3; i <= k; i += 2) {
			if (n % i == 0)
				return false;
		}

		return true;
	}

}
