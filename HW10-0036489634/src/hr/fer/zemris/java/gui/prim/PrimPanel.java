package hr.fer.zemris.java.gui.prim;

import java.awt.GridLayout;

import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListModel;

/**
 * This class represents the panel for the prime calculation application using
 * the Java's Swing API for the manipulation with two {@link JList} sharing the
 * same data {@link ListModel} for the primes calculation.
 * 
 * @author Matej Balun
 *
 */
public class PrimPanel extends JPanel {

	/**
	 * The serial version UID for this {@link PrimPanel} class.
	 */
	private static final long serialVersionUID = 2572278678602940647L;

	/**
	 * The first list for prime numbers.
	 */
	private JList<Integer> firstList;

	/**
	 * The second list for prime numbers.
	 */
	private JList<Integer> secondList;

	/**
	 * The shared {@link ListModel} for prime manipulation.
	 */
	private ListModel<Integer> model;

	/**
	 * This Constructor initialises the {@link PrimPanel} with the specified
	 * {@link ListModel} for shared prime number calculation of the lists.
	 * 
	 * @param model
	 *            the specified {@link ListModel}
	 */
	public PrimPanel(ListModel<Integer> model) {
		super();
		this.model = model;
		this.setLayout(new GridLayout(1, 2));

		initGUI();
	}

	/**
	 * This method initialises the {@link PrimPanel} with its {@link JList}s
	 * sharing the same {@link ListModel}
	 */
	private void initGUI() {
		firstList = new JList<>();
		secondList = new JList<>();

		this.add(new JScrollPane(firstList));
		this.add(new JScrollPane(secondList));

		firstList.setModel(model);
		secondList.setModel(firstList.getModel());

	}
}
