package hr.fer.zemris.java.gui.prim;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListModel;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

/**
 * This program represents the simple prime number calculation application. The
 * application displays two {@link JList}s sharing the sam {@link ListModel} for
 * prime number calculation.
 * 
 * @author Matej Balun
 *
 */
public class PrimDemo extends JFrame {

	/**
	 * The serial version UID for this {@link PrimDemo} class.
	 */
	private static final long serialVersionUID = 2552614965736703542L;

	/**
	 * This constructor initialises the {@link PrimDemo} class with the default
	 * size and position of the window and the layout manager for the
	 * {@link Component}s.
	 */
	public PrimDemo() {
		setLocation(200, 200);
		setSize(200, 300);
		setTitle("Primary numbers");
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setLayout(new BorderLayout());

		initGUI();
	}

	/**
	 * This method initialises the GUI with the new {@link PrimListModel} and a
	 * button with its {@link ActionListener} for the prime number calculation.
	 */
	private void initGUI() {

		PrimListModel listModel = new PrimListModel();

		JButton next = new JButton("next");

		next.addActionListener(l -> {

			listModel.next();

		});
		getContentPane().add(next, BorderLayout.SOUTH);

		JPanel panel = new PrimPanel(listModel);
		getContentPane().add(panel, BorderLayout.CENTER);

	}

	/**
	 * The main method that starts the program and displays the application.
	 * 
	 * @param args
	 *            not used
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> {
			JFrame frame = new PrimDemo();
			frame.setVisible(true);
		});
	}

}
