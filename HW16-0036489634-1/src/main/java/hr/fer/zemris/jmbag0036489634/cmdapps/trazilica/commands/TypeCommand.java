package hr.fer.zemris.jmbag0036489634.cmdapps.trazilica.commands;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import hr.fer.zemris.jmbag0036489634.cmdapps.trazilica.Environment;
import hr.fer.zemris.jmbag0036489634.cmdapps.trazilica.ShellStatus;

/**
 * This command represents the command for typing the content of the file
 * specified by index in the last search. The command extends the
 * {@link AbstractCommand}, implementing the specific execute action.
 * 
 * @author Matej Balun
 *
 */
public class TypeCommand extends AbstractCommand {

	/**
	 * 
	 */
	private static final String NAME = "type";

	/***
	 * Description of the command
	 */
	private static final String DESC = "Prints the content of the indexed file to the screen";

	/**
	 * MArgin for tying the content of the file
	 */
	private static final String MARGIN = "----------------------------------------------";

	/**
	 * Initialises the {@link TypeCommand}. passing its parameters to the super
	 * constructor {@link AbstractCommand}.
	 */
	public TypeCommand() {
		super(NAME, DESC);
	}

	@Override
	public ShellStatus execute(Environment environment, String args) {

		try {
			int index = Integer.parseInt(args.trim());
			int i = 0;

			if (environment.getResults().isEmpty()) {
				environment.writeln("No results found in last query");
				return ShellStatus.CONTINUE;
			}

			for (String filePath : environment.getResults().values()) {
				if (index == i++) {
					environment.writeln(MARGIN);
					Files.readAllLines(Paths.get(filePath), StandardCharsets.UTF_8)
							.forEach(l -> environment.writeln(l));
					environment.writeln("\n" + MARGIN);
					return ShellStatus.CONTINUE;
				}
			}

			environment.writeln("The match with specified index does not exists.");

		} catch (NumberFormatException e) {
			environment.writeln("illegal index provided");
		} catch (Exception e2) {
			environment.writeln(e2.getMessage());
		}

		return ShellStatus.CONTINUE;
	}

}
