package hr.fer.zemris.jmbag0036489634.cmdapps.trazilica;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import hr.fer.zemris.jmbag0036489634.cmdapps.trazilica.commands.ExitCommand;
import hr.fer.zemris.jmbag0036489634.cmdapps.trazilica.commands.QueryCommand;
import hr.fer.zemris.jmbag0036489634.cmdapps.trazilica.commands.ResultsCommand;
import hr.fer.zemris.jmbag0036489634.cmdapps.trazilica.commands.TypeCommand;

/**
 * This program demonstrates the simple shell implementation {@link Konzola}.
 * The {@link Konzola} offers the user set various {@link ShellCommand}s for
 * manipulating the data and searching the files on the file system¸with the
 * specified key words. Valid commands {@link Konzola} are:
 * 
 * The program ends when the users enters "exit" command.
 * 
 * @author Matej Balun
 *
 */
public class Konzola {

	/**
	 * The static map with command names as keys and command implementations as
	 * values.
	 */
	private static Map<String, ShellCommand> commands;

	/**
	 * The static initialisation block for the shell commands.
	 */
	static {
		commands = new HashMap<>();
		ShellCommand[] shellCommands = { new ExitCommand(), new QueryCommand(), new ResultsCommand(),
				new TypeCommand() };

		for (ShellCommand command : shellCommands) {
			commands.put(command.getCommandName(), command);
		}
	}

	/**
	 * The start message.
	 */
	private static final String MESSAGE = "Welcome to custom search shell";

	/**
	 * The symbol string constant.
	 */
	private static final String SYMBOL = ">";

	/**
	 * The end message.
	 */
	private static final String GOODBYE = "Thank you for using this search app. Goodbye!";

	/**
	 * The environment of the {@link Konzola}.
	 */
	private static Environment environment = new EnvironmentImpl(commands.values());

	/**
	 * The main method that executes the program.
	 * 
	 * @param args
	 *            a single argument containing the path to the directory
	 *            containing the files and the stop words fo the application.
	 */
	public static void main(String[] args) {
		environment.writeln(MESSAGE);

		if (args.length != 1) {
			environment.writeln("Illegal number of arguments provided.");
			System.exit(-1);
		}

		try {
			environment.initialiseEnvironment(args[0].trim());
		} catch (IOException e1) {
			environment.writeln(e1.getMessage());
		}

		environment.writeln("The vocabulary has " + environment.getVocabularySize() + " words\n");

		while (true) {
			environment.write("Enter command" + SYMBOL + " ");
			String line = environment.readLine().trim();

			String[] input = line.split(" ", 2);

			String cmd = input[0].trim();
			String arg;

			try {
				arg = input[1].trim();
			} catch (ArrayIndexOutOfBoundsException e) {
				arg = "";
			}

			ShellCommand command = commands.get(cmd.toLowerCase());
			if (command == null) {
				environment.writeln("Unknown command!");
				continue;
			}

			if (command.execute(environment, arg) == ShellStatus.TERMINATE) {
				break;
			}
		}

		environment.writeln(GOODBYE);
	}

}
