package hr.fer.zemris.jmbag0036489634.cmdapps.trazilica;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * This interface describes the environment of the {@link Konzola} shell
 * application. It specifies methods for communication between the user and the
 * shell commands. All operations regarding the content of the shell are
 * executed via this methods.
 * 
 * @author Matej Balun
 *
 */
public interface Environment {

	/**
	 * Reads the user's input from the {@link Konzola}.
	 * 
	 * @return The users input from the {@link Konzola}.
	 */
	public String readLine();

	/**
	 * Writes the specified text on the {@link Konzola}, without the line
	 * separator.
	 * 
	 * @param text
	 *            to be written on the {@link Konzola}
	 */
	public void write(String text);

	/**
	 * Writes the specified text on the {@link Konzola}, with the line
	 * separator.
	 * 
	 * @param text
	 *            to be written on the {@link Konzola}
	 */
	public void writeln(String text);

	/**
	 * Returns the {@link Iterable} collection of all {@link ShellCommand}s.
	 * 
	 * @return the collection of {@link ShellCommand}s.
	 */
	public Iterable<ShellCommand> commands();

	/**
	 * Returns the path of the directory containing the files for the query
	 * search
	 * 
	 * @return the directory path
	 */
	public String getDirectoryPath();

	/**
	 * Returns the size of the search vocabulary
	 * 
	 * @return size of the vocabulary
	 */
	public long getVocabularySize();

	/**
	 * Sets the flag for query completion
	 * 
	 * @param flag
	 *            flag for the query
	 */
	public void setQueryFlag(boolean flag);

	/**
	 * Returns the status of the query execution.
	 * 
	 * @return <code>true</code> if the query has executed, otherwise false.
	 */
	public boolean getQueryFlag();

	/**
	 * Returns the {@link Set} of the words in the search vocabulary
	 * 
	 * @return {@link Set} of the word in the vocabulary
	 */
	public Set<String> getVocabulary();

	/**
	 * Returns the map containing the file paths and words frequency
	 * 
	 * @return the file frequency {@link Map}
	 */
	public Map<String, Map<String, Integer>> getFileMap();

	/**
	 * Returns the {@link Map} of the file file vectors for the search
	 * application
	 * 
	 * @return {@link Map} of the file vectors
	 */
	public Map<String, List<Double>> getVectors();

	/**
	 * Sets the results of the last query search.
	 * 
	 * @param results
	 *            {@link Map} of the query results
	 */
	public void setResults(Map<Double, String> results);

	/**
	 * Returns the results of the last query search
	 * 
	 * @return {@link Map} of the last query results
	 * @throws NullPointerException
	 *             if the query hasn't been executed
	 */
	public Map<Double, String> getResults() throws NullPointerException;

	/**
	 * Initialises the environment of the application with the specified path to
	 * the directory containing the files for the search match algorithms
	 * 
	 * @param dir
	 *            path to the files directory
	 * @throws IOException
	 *             if there was an error in file management
	 */
	public void initialiseEnvironment(String dir) throws IOException;
}
