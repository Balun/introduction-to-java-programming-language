package hr.fer.zemris.jmbag0036489634.cmdapps.trazilica;

import hr.fer.zemris.jmbag0036489634.cmdapps.trazilica.commands.ExitCommand;

/**
 * This enumeration describes the state of the {@link Konzola} after the
 * execution of the last command. The shell can continue its work, or end if the
 * {@link ExitCommand} has executed.
 * 
 * @author Matej Balun
 *
 */
public enum ShellStatus {

	/**
	 * The state that continues the work of the {@link Konzola}.
	 */
	CONTINUE,

	/**
	 * The state that terminates the work of the {@link Konzola}.
	 */
	TERMINATE;

}
