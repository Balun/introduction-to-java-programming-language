package hr.fer.zemris.jmbag0036489634.cmdapps.trazilica.commands;

import hr.fer.zemris.jmbag0036489634.cmdapps.trazilica.Environment;
import hr.fer.zemris.jmbag0036489634.cmdapps.trazilica.ShellStatus;

/**
 * This command represents the standard exit command for the simple search
 * application. It extends the {@link AbstractCommand} class, implementing the
 * specified execute action.
 * 
 * @author Matej Balun
 *
 */
public class ExitCommand extends AbstractCommand {

	/**
	 * Name of the command
	 */
	private static final String NAME = "exit";

	/**
	 * Description of the command
	 */
	private static final String DESC = "Exits te search shell application.";

	/**
	 * Initialises the {@link ExitCommand} and passes the parameters to the
	 * super constructor {@link AbstractCommand}.
	 */
	public ExitCommand() {
		super(NAME, DESC);
	}

	public ShellStatus execute(Environment environment, String args) {
		return ShellStatus.TERMINATE;
	}

}
