package hr.fer.zemris.jmbag0036489634.cmdapps.trazilica;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * This class represents the utility for the calculation of the file vectors for
 * the search application.
 * 
 * @author Matej Balun
 *
 */
public class VectorUtility {

	/**
	 * Private constructor that permits the class instantiation
	 */
	private VectorUtility() {
	}

	/**
	 * Calculates the file vector based on the list of words and provided
	 * vocabulary and other file vectors
	 * 
	 * @param file
	 *            the specified file
	 * @param files
	 *            collection of other files in the application
	 * @param vocabulary
	 *            vocabulary of the application
	 * @return The {@link List} representing the file vector
	 */
	public static List<Double> calculateVector(List<String> file, Map<String, Map<String, Integer>> files,
			Set<String> vocabulary) {

		List<Double> vector = new ArrayList<>();
		for (String vocWord : vocabulary) {

			long tf = 0;
			double idf = 0;
			if (file.contains(vocWord)) {
				tf = file.parallelStream().filter(w -> w.equalsIgnoreCase(vocWord)).count();
				idf = Math.log((double) (files.size() + 1)
						/ (files.values().parallelStream().filter(f -> f.containsKey(vocWord)).count() + 1));
			}
			vector.add(tf * idf);
		}

		return vector;
	}

	/**
	 * Calculates the norm of the provided file vector
	 * 
	 * @param vector
	 *            the specified file vector
	 * @return norm of the file vector
	 */
	public static double calculateNorm(List<Double> vector) {
		double sum = 0;
		for (double x : vector) {
			if (x > 0) {
				sum += x * x;
			}
		}

		return Math.sqrt(sum);
	}
}
