package hr.fer.zemris.jmbag0036489634.cmdapps.trazilica.commands;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import java.util.Map.Entry;

import hr.fer.zemris.jmbag0036489634.cmdapps.trazilica.Environment;
import hr.fer.zemris.jmbag0036489634.cmdapps.trazilica.ShellStatus;
import hr.fer.zemris.jmbag0036489634.cmdapps.trazilica.VectorUtility;

/**
 * This class represents the query command for the simple search application.
 * The command takes array of words ad queries them into search algorithm to
 * find the best matches in the document database. The class extends the
 * {@link AbstractCommand} class, implementing the specific execute operation.
 * 
 * @author Matej Balun
 *
 */
public class QueryCommand extends AbstractCommand {

	/**
	 * Name of the command
	 */
	private static final String NAME = "query";

	/**
	 * Description of the command
	 */
	private static final String DESC = "Used for querying the search results";

	/**
	 * Initialises the {@link QueryCommand}, passing its parameters to the super
	 * constructor {@link AbstractCommand}.
	 */
	public QueryCommand() {
		super(NAME, DESC);
	}

	@Override
	public ShellStatus execute(Environment environment, String args) {
		String[] elems = args.trim().split(" ");
		List<String> words = new ArrayList<>();

		for (String elem : elems) {
			if (environment.getVocabulary().contains(elem.trim().toLowerCase())) {
				words.add(elem.toLowerCase());
			}
		}

		if (words.isEmpty()) {
			environment.writeln("No words provided for query");
			return ShellStatus.CONTINUE;
		}

		environment.write("Query is: [");
		words.forEach(w -> environment.write(w + ", "));
		environment.writeln("]");

		findResults(words, environment);

		environment.setQueryFlag(true);
		return ShellStatus.CONTINUE;
	}

	/**
	 * Finds the results bases on the specified key words and file vectors.
	 * 
	 * @param words
	 *            array of key words for search
	 * @param environment
	 *            the specified {@link Environment}
	 */
	private void findResults(List<String> words, Environment environment) {
		List<Double> vector = VectorUtility.calculateVector(words, environment.getFileMap(),
				environment.getVocabulary());

		Map<Double, String> matches = new TreeMap<>(Collections.reverseOrder());

		for (Entry<String, List<Double>> file : environment.getVectors().entrySet()) {
			Iterator<Double> v1 = vector.iterator();
			Iterator<Double> v2 = file.getValue().iterator();

			double scalar = 0;

			while (v1.hasNext() && v2.hasNext()) {
				double d1 = v1.next();
				double d2 = v2.next();
				if (d1 > 0 && d2 > 0) {
					scalar += (d1 * d2);
				}
			}

			matches.put(scalar / (VectorUtility.calculateNorm(vector) * VectorUtility.calculateNorm(file.getValue())),
					file.getKey());
		}

		int i = 0;
		for (Entry<Double, String> match : matches.entrySet()) {
			if (i < 10 && match.getKey() > 0.0) {
				environment
						.writeln("[" + i++ + "] (" + String.format("%.4f", match.getKey()) + ") " + match.getValue());
			} else {
				break;
			}
		}

		environment.setResults(matches);
	}

}
