package hr.fer.zemris.jmbag0036489634.cmdapps.trazilica;

import java.util.List;


/**
 * This interface specifies the general command of the {@link Konzola} shell
 * application. Classes that implement this interface must give concrete
 * implementations of all specified methods.
 * 
 * @author Matej Balun
 *
 */
public interface ShellCommand {

	/**
	 * Return the name of the {@link ShellCommand}.
	 * 
	 * @return name of the command
	 */
	public String getCommandName();

	/**
	 * Returns the list of lines representing the {@link ShellCommand}s
	 * description.
	 * 
	 * @return the command's description.
	 */
	public List<String> getCommandDescription();

	/**
	 * Executes the {@link ShellCommand}.
	 * 
	 * @param environment
	 *            the specified environment of the {@link Konzola}.
	 * @param args
	 *            optional arguments for the {@link ShellCommand}.
	 * @return the {@link ShellStatus} representing the status of the
	 *         {@link Konzola} after the command's execution
	 */
	public ShellStatus execute(Environment environment, String args);

}
