package hr.fer.zemris.jmbag0036489634.cmdapps.trazilica.commands;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import hr.fer.zemris.jmbag0036489634.cmdapps.trazilica.Konzola;
import hr.fer.zemris.jmbag0036489634.cmdapps.trazilica.ShellCommand;

/**
 * This class describes the abstract {@link Konzola} command. It implements the
 * {@link ShellCommand} interface with the getter methods for commands. Classes
 * that inherit this class must implement the {@link ShellCommand}'s method
 * execute().
 * 
 * @author Matej Balun
 *
 */
public abstract class AbstractCommand implements ShellCommand {

	/**
	 * Name of the command.
	 */
	private final String commandName;

	/**
	 * Lines with description of the command.
	 */
	private final List<String> commandDescription;

	/**
	 * Initialises the {@link AbstractCommand}, with values for name and short
	 * description of the command.
	 * 
	 * @param commandName
	 *            name of the command
	 * @param commandDescription
	 *            dscription of he command
	 */
	public AbstractCommand(String commandName, String... commandDescription) {

		this.commandName = commandName;
		this.commandDescription = new ArrayList<String>(Arrays.asList(commandDescription));
	}

	/**
	 * {@inheritDoc}
	 */
	public String getCommandName() {
		return commandName;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<String> getCommandDescription() {
		return commandDescription;
	}

}
