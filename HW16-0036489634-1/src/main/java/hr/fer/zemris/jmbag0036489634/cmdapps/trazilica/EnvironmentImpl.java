package hr.fer.zemris.jmbag0036489634.cmdapps.trazilica;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * This class implements the {@link Environment} of the {@link Konzola} shell
 * application. It offers the concrete implementations of all operations
 * regarding the {@link Konzola} input/output.
 * 
 * @author Matej Balun
 *
 */
public class EnvironmentImpl implements Environment {

	/**
	 * Pattern for the splitting of the words in application's files
	 */
	private static final Pattern splitPattern = Pattern.compile("\\s+|\\d+|\\W+",
			Pattern.UNICODE_CHARACTER_CLASS | Pattern.CASE_INSENSITIVE);

	/**
	 * Path to the directory containing the top words of the vocabulary.
	 */
	private static final String STOP_WORDS_PATH = "src/main/resources/hrvatski_stoprijeci.txt";

	/**
	 * Directory of the files.
	 */
	private String dir;

	/**
	 * The specified reader to read from prompt.
	 */
	private BufferedReader reader;

	/**
	 * The specified writer for writing on the {@link Konzola}.
	 */
	private BufferedWriter writer;

	/**
	 * {@link Iterable} collection of all {@link Konzola} {@link ShellCommand}s.
	 */
	private Iterable<ShellCommand> commands;

	/**
	 * Map of the file paths and word frequencies
	 */
	private Map<String, Map<String, Integer>> documents;

	/**
	 * The vocabulary of the search application
	 */
	private Set<String> vocabulary;

	/**
	 * Collection of stop words of the vocabulary
	 */
	private Set<String> stopWords;

	/**
	 * Map of the file vectors for the search
	 */
	private Map<String, List<Double>> vectors;

	/**
	 * Returns the map of the query results
	 */
	private Map<Double, String> results;

	/**
	 * Status of the query execution
	 */
	private boolean queryExecuted;

	/**
	 * Initialises the {@link EnvironmentImpl} with the specified initial
	 * symbols and collection of {@link ShellCommand}s.
	 * 
	 * @param commands
	 *            {@link Iterable} collection of {@link ShellCommand}s
	 */
	public EnvironmentImpl(Iterable<ShellCommand> commands) {
		super();

		this.commands = commands;
		this.reader = new BufferedReader(new InputStreamReader(System.in));
		this.writer = new BufferedWriter(new OutputStreamWriter(System.out));
		this.vocabulary = new HashSet<>();
		this.stopWords = new HashSet<>();
		this.vectors = new HashMap<>();
		this.documents = new HashMap<>();
		this.queryExecuted = false;
	}

	public void initialiseEnvironment(String dir) throws IOException {
		if (dir == null) {
			throw new IllegalArgumentException("The directory has not been specified");
		} else {
			this.dir = dir;
		}

		this.stopWords = new HashSet<>(Files.readAllLines(Paths.get(STOP_WORDS_PATH)));

		Files.walkFileTree(Paths.get(dir), new InitVisitor());

		for (Entry<String, Map<String, Integer>> document : documents.entrySet()) {

			List<Double> vector = new ArrayList<>();
			vocabulary.stream().forEach((w) -> {

				int tf = 0;
				double idf = 0;
				if (document.getValue().containsKey(w)) {
					tf = document.getValue().get(w);
					idf = Math.log((double) documents.size()
							/ documents.values().parallelStream().filter(d -> d.containsKey(w)).count());
				}
				vector.add(tf * idf);

			});

			vectors.put(document.getKey(), vector);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String readLine() {
		try {
			return reader.readLine();
		} catch (IOException e) {
			System.err.println("Error in reading");
			return null;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void write(String text) {
		try {
			writer.write(text);
			writer.flush();
		} catch (IOException e) {
			System.err.println("Error in writing");
		}

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void writeln(String text) {
		try {
			writer.write(text);
			writer.newLine();
			writer.flush();
		} catch (IOException e) {
			System.err.println("Error in writing");
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Iterable<ShellCommand> commands() {
		return commands;
	}

	@Override
	public String getDirectoryPath() {
		return this.dir;
	}

	@Override
	public long getVocabularySize() {
		return vocabulary.size();
	}

	@Override
	public void setQueryFlag(boolean flag) {
		queryExecuted = flag;
	}

	@Override
	public boolean getQueryFlag() {
		return queryExecuted;
	}

	@Override
	public Set<String> getVocabulary() {
		return Collections.unmodifiableSet(vocabulary);
	}

	@Override
	public Map<String, Map<String, Integer>> getFileMap() {
		return Collections.unmodifiableMap(documents);
	}

	@Override
	public Map<String, List<Double>> getVectors() {
		return vectors;
	}

	@Override
	public void setResults(Map<Double, String> results) {
		this.results = results;
	}

	@Override
	public Map<Double, String> getResults() throws NullPointerException {
		if (queryExecuted) {
			return results;
		}

		throw new NullPointerException("No query was initialised.");
	}

	/**
	 * Returns the directory of the files database
	 * 
	 * @return the files directory
	 */
	public String getDir() {
		return dir;
	}

	/**
	 * Sets the directory of the file database
	 * 
	 * @param dir
	 *            directory of the files
	 */
	public void setDir(String dir) {
		this.dir = dir;
	}

	/**
	 * This class represents the concrete visitor for the initialisation of
	 * vocabulary and file vectors for the simple search application
	 * 
	 * @author Matej Balun
	 *
	 */
	private class InitVisitor extends SimpleFileVisitor<Path> {

		@Override
		public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
			Map<String, Integer> contentOccurenceMap = new HashMap<>();
			documents.put(file.toAbsolutePath().toString(), contentOccurenceMap);

			for (String line : Files.readAllLines(file)) {

				String[] words = splitPattern.split(line);

				for (String word : words) {
					word = word.toLowerCase();
					if (!stopWords.contains(word) && !word.isEmpty()) {
						vocabulary.add(word);

						if (contentOccurenceMap.containsKey(word)) {
							contentOccurenceMap.put(word, contentOccurenceMap.get(word) + 1);
						} else {
							contentOccurenceMap.put(word, 1);
						}
					}
				}
			}

			return FileVisitResult.CONTINUE;
		}
	}
}
