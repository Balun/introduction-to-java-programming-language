package hr.fer.zemris.jmbag0036489634.cmdapps.trazilica.commands;

import java.util.Map.Entry;

import hr.fer.zemris.jmbag0036489634.cmdapps.trazilica.Environment;
import hr.fer.zemris.jmbag0036489634.cmdapps.trazilica.ShellStatus;

/**
 * This class represents the command for displaying the results of the last
 * search query of the application. It extends the {@link AbstractCommand}
 * class.
 * 
 * @author Matej Balun
 *
 */
public class ResultsCommand extends AbstractCommand {

	/**
	 * Name of the command
	 */
	private static final String NAME = "results";

	/**
	 * TDescription of the command
	 */
	private static final String DESC = "Prints the results of the last query.";

	/**
	 * Initialises the {@link ResultsCommand} and passes its parameters to the
	 * super constructor {@link AbstractCommand}.
	 */
	public ResultsCommand() {
		super(NAME, DESC);
	}

	@Override
	public ShellStatus execute(Environment environment, String args) {
		try {
			int i = 0;
			for (Entry<Double, String> match : environment.getResults().entrySet()) {
				if (i < 10 && match.getKey() > 0) {
					environment.writeln(
							"[" + i++ + "] (" + String.format("%.4f", match.getKey()) + ") " + match.getValue());
				} else {
					break;
				}
			}

		} catch (NullPointerException e) {
			environment.writeln(e.getMessage());
		}

		return ShellStatus.CONTINUE;
	}

}
